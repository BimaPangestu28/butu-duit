<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAffiliasiMemberTabel extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('affiliasi_member', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_member')->nullable();
            $table->string('nama');
            $table->timestamp('tanggal_lahir')->nullable();
            $table->string('tempat_lahir')->nullable();
            $table->string('number_card')->nullable();
            $table->string('code_affiliasi')->nullable();
            $table->integer('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('affiliasi_member');
    }
}
