<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOtherDataToPemasukanBarangTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pemasukan_barangs', function (Blueprint $table) {
            $table->string('no_transaksi')->nullable();
            $table->integer('diskon')->nullable();
            $table->integer('pajak')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pemasukan_barangs', function (Blueprint $table) {
            //
        });
    }
}
