<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePolaPerilakuHargasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pola_perilaku_hargas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_toko');
            $table->string('beli_barang');
            $table->string('total_beli');
            $table->string('option');
            $table->string('potongan')->nullable();
            $table->string('bonus_barang')->nullable();
            $table->string('bonus_barang_jumlah')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pola_perilaku_hargas');
    }
}
