<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKasTokosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kas_tokos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_toko');
            $table->string('no_transaksi');
            $table->string('jenis');
            $table->string('ke_kas');
            $table->text('keterangan');
            $table->integer('total');
            $table->integer('tipe');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kas_tokos');
    }
}
