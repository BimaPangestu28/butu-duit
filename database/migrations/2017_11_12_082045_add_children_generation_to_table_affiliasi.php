<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddChildrenGenerationToTableAffiliasi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('affiliasi', function (Blueprint $table) {
            $table->integer('child_2')->default(0);
            $table->integer('child_3')->default(0);
            $table->integer('child_4')->default(0);
            $table->integer('child_5')->default(0);
            $table->integer('child_6')->default(0);
            $table->integer('child_7')->default(0);
            $table->integer('child_8')->default(0);
            $table->integer('child_9')->default(0);
            $table->integer('child_10')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('affiliasi', function (Blueprint $table) {
            //
        });
    }
}
