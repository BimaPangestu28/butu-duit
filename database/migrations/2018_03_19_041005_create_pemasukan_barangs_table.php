<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePemasukanBarangsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pemasukan_barangs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('barcode');
            $table->string('produk');
            $table->string('supplier');
            $table->integer('jumlah_barang');
            $table->string('tipe_jumlah');
            $table->integer('jumlah_satuan');
            $table->string('tipe');
            $table->integer('total_harga');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pemasukan_barangs');
    }
}
