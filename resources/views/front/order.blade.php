<!DOCTYPE html>
<html>
<head>
  <title>Daftar Order</title>
  <link rel="stylesheet" type="text/css" href="/assets/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="/admin/font-awesome/css/font-awesome.min.css">
  <script type="text/javascript" src="/admin/js/jquery.min.js"></script>
	<script type="text/javascript" src="/assets/bootstrap/js/bootstrap.bundle.min.js"></script>
  <link rel="stylesheet" type="text/css" href="/assets/css/toko.css">
</head>
<body>


    <main role="main">

      <div id="banner" style="min-height:400px;">
        <section class="jumbotron text-center">
          <div class="container">
            <a href="/"><img src="/assets/img/logo.png" width="150"></a>
            <br>
            <br>
            <br>
            <h1 class="jumbotron-heading">Hallo, {{ session()->get('member')->nama }}</h1>
            <p class="lead text-dark col-sm-9" style="display:inline-block;">Lihat daftar order dari belanja kamu.</p>
            <p>
              <a class="btn-danger btn-nav" style="color: white;" href="/">Cari Toko</a>
              <a class="btn-nav btn-warning" style="color: white;" href="/affiliasi/{{ $id }}">Lihat affiliasi</a>
              <a href="/toko-usahakumart/{{ $id }}" class="btn-nav btn-primary">Kembali Ke Toko</a>
            </p>
          </div>
        </section>
      </div>

      <div class="album text-muted">
        <div class="container">

          <table class="table table-responsive-sm table-hover table-outline mb-0">
            <thead class="thead-light">
              <tr>
                <th>Tanggal</th>
                <th>Nama Penerima</th>
                <th>Alamat Penerima</th>
                <th>Nomor Hp</th>
                <th class="text-center">Nomor Transaksi</th>
                <th class="text-center">Total Pembayaran</th>
                <th class="text-center">Option</th>
                <th>Status</th>
              </tr>
            </thead>
            <tbody>
              @foreach ($trans as $view)
                <tr>
                  <td>{{$view->created_at}}</td>
                  <td>{{$view->penerima}}</td>
                  <td>{{$view->alamat}}</td>
                  <td>{{$view->phone}}</td>
                  <td class="text-center">{{$view->id}}</td>
                  <td class="text-center">Rp. {{ number_format($view->total_belanja,0,',','.') }}</td>
                  <td><a href="/toko-usahakumart/transaksi/sukses/{{$view->id}}" target="_blank" class="btn btn-default">Lihat transaksi</a></td>
                  <td>
                    @if($view->status == 1)
                    <span class="badge badge-success">Diterima</span>
                    @elseif ($view->status == 2)
                    <span class="badge badge-warning">Diproses</span>
                    @else
                    <span class="badge badge-danger">Menunggu</span>
                    @endif
                  </td>
                </tr>
              @endforeach
            </tbody>
          </table>

          <br>
          {!! $trans->render() !!}

        </div>

        </div>
      </div>

    </main>

    <footer id="footer" class="text-muted">
      <div class="container text-center">
        <p>
					<a href="#" class="btn-nav btn-warning uniquePadding">Tentang</a>
					<a href="#" class="btn-nav btn-warning uniquePadding">Kontak</a>
					<a href="#" class="btn-nav btn-warning uniquePadding">Frequent Question</a>
				</p>
				<p class="copyright">Usahaku Toserba &copy; Usahaku.co.id, 2018</p>
      </div>
    </footer>

</body>

</html>
