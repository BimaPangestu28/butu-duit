<table>
	<tr>
		<th>Tanggal</th>
		<th>option</th>
	</tr>
	@foreach($order as $o)
	<tr>
		<td>{{ $aff->number_card }}</td>
		<td>
			<form method="POST" action="/cashier/beli">
			{{ csrf_field() }}
		    <input type="hidden" name="pembayaran" value="{{ $o->total_belanja }}">
			<input type="hidden" name="product_id" id="id" value="{{ \App\Http\Controllers\PagesController::getProduct($o->id) }}"><br>
			<input type="hidden" name="total" id="total-semua" value="{{ $o->total_belanja }}">
			<input type="hidden" name="toko_id" value="{{ $o->id_toko }}">
			<input type="hidden" name="member_card" value="{{ $aff->number_card }}">
			<input type="hidden" name="id_transaksi" value="{{ $o->id }}">
			<br>
			<input type="submit" value="BELI" class="form-control btn btn-primary">
			</form>
		</td>
	</tr>
	@endforeach
</table>