<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="shortcut icon" href="/admin/img/favicon.png">
  <link rel="stylesheet" type="text/css" href="/css/login.css">
  <title>UsahaMart Administrator</title>

  <!-- Icons -->
  <link href="/admin/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="/admin/simple-line-icons/css/simple-line-icons.css" rel="stylesheet">

</head>
<body class="app flex-row align-items-center">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-8">
        <div class="card-group">
          <div class="card p-4">
          	<form method="POST" action="/toko-usahakumart/login/{{ $id }}">
          	{{ csrf_field() }}
            <div class="card-body">
              <h1>Login</h1>
              <p class="text-muted">Sign In to your account</p>
              <div class="input-group mb-3">
                <span class="input-group-addon"><i class="icon-user"></i></span>
                <input type="text" class="inp form-control" name="username" placeholder="Username">
              </div>
              <div class="input-group mb-4">
                <span class="input-group-addon"><i class="icon-lock"></i></span>
                <input type="password" name="password" class="inp form-control" placeholder="Password">
              </div>
              <div class="row">
                <div class="col-5">
                  <button type="submit" class="btn btn-primary px-4 form-control">Login</button>
                </div>
              </div>
              @if($errors->count() > 0 || session()->has('alert'))
              <div class="alert alert-danger">
                @if($errors->count())
                  @foreach($errors->all() as $error)
                <span>{{ $error }}</span><br>
                @endforeach
              @elseif(session()->has('alert'))
                <span>{{ session()->get('alert') }}</span><br>
                @endif
              </div>
              @endif
            </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>

</body>

</html>