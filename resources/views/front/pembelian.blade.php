<!DOCTYPE html>
<html>
<head>
	<title>Toko Usahaku Cab.Batanghari</title>
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<meta name="toko-identity" content="{{$toko->id}}">
	<link rel="stylesheet" type="text/css" href="/assets/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="/admin/font-awesome/css/font-awesome.min.css">
	<script type="text/javascript" src="/admin/js/jquery.min.js"></script>
	<script type="text/javascript" src="/assets/bootstrap/js/bootstrap.bundle.min.js"></script>
	<link rel="stylesheet" type="text/css" href="/assets/css/toko.css">
	<link rel="stylesheet" type="text/css" href="/assets/css/order.css">
</head>
<body style="min-height:auto;">

	<div class="container">
	  <header class="header clearfix top-header" id="fixed-header">
	    <nav>
	      <ul class="nav nav-pills float-right">
					@if(session()->has('member'))
					<!-- <li class="nav-item">
						<p class="nav-link">Pembelian Barang</p>
					</li> -->
					<li class="nav-item">
						<a class="nav-link" href="#">{{Session::get('member')->nama}}</a>
					</li>
					<li class="nav-item">
						<a class="nav-link btn-danger btn-nav" style="color: white;" href="/">Cari Toko</a>
					</li>&nbsp;&nbsp;
					<li class="nav-item">
						<a style="cursor: pointer; color: white;" class="nav-link btn-warning btn-nav" style="color: white;" onclick="return window.history.back();">Kembali Ke Toko</a>
					</li>&nbsp;&nbsp;
					<li class="nav-item">
						<a class="nav-link active btn-nav" href="/toko-usahakumart/logout/{{ $toko->id }}">logout</a>
					</li>
            @endif
	      </ul>
	    </nav>
	    <h3 class="text-muted desktopNav"><img src="/assets/img/logo.png" alt="logo"> </h3>
	  </header>

  <main role="main">
      <div class="address-buyer">
				<div class="row">
					<div class="col-md-12">
						<h5>Pengisian Data Pembeli</h5>
						@if($errors->count() > 0 || session()->has('alert'))
							<div class="alert alert-danger">
								@if($errors->count() > 0)
									@foreach($errors->all() as $error)
										<li>{{ $error }}</li>
									@endforeach
								@elseif(session()->has('alert'))
									<li>{{ session('alert') }}</li>
								@endif
							</div>
						@endif
						<form action="/toko-usahakumart/transaksi/{{$toko->id}}" method="post" id="form">
							{{csrf_field()}}
							<div class="form-group">
								<label for="nama" class="text-muted">Nama Penerima</label>
								<input type="text" class="form-control" name="nama" value="{{Session::get('member')->nama}}">
							</div>
							<div class="form-group">
								<label for="nama" class="text-muted">Nomor Penerima</label>
								<input type="text" class="form-control" name="phone" value="{{Session::get('member')->telepon}}">
							</div>
							<div class="form-group">
								<label class="text-muted">Jenis Pembayaran</label>
								<select name="metode_pembayaran" class="form-control">
									<option value="langsung">Pembayaran Langsung</option>
									@if($aff->total_affiliasi >= 25000)
									<option value="affiliasi">Pembayaran Menggunakan Saldo Affiliasi</option>
									@endif
									@if($dep->saldo >= 25000)
									<option value="deposit">Pembayaran Menggunakan Saldo Deposit</option>
									@endif
								</select>
							</div>
							<div class="form-group">
								<label for="alamat" class="text-muted">Alamat Lengkap</label>
								<textarea name="alamat" class="form-control" rows="8" cols="80">{{ old('alamat') }}</textarea>
							</div>
							<div id="acceptPembeli" class="form-group">
								<button type="submit" class="form-control btn-nav btn-primary">Beli Barang</button>
							</div>
						</form>
					</div>
					<div class="col-md-12" style="float: right;">
						<h5>Daftar Belanja</h5>
						<div class="box-list">
							<table class="table table-responsive">
								<thead>
									<tr class="text-muted">
										<!-- <th>Gambar</th> -->
										<th>Nama Barang</th>
										<th>Jumlah</th>
										<th>Potongan</th>
										<th>Diskon</th>
										<th>Harga</th>
										<th>Affiliasi</th>
										<th>Total</th>
									</tr>
								</thead>
								<tbody>
									@foreach ($cart as $list)
								    <!-- <tr id="HanyaList_{{$list->id}}" class="dataMe" data-tr="0">
											<td><img src="/gambar/{{$list->gambar}}" alt="" width="50"> </td> -->
							      	  <td>{{$list->name}}</td>
									  <td class="jumlah">{{$list->jumlah_barang}}</td>
								      <td>Rp. {{ number_format(App\Http\Controllers\PotonganBarangController::cekBarang($list->id_barang, $list->jumlah_barang), 0, ',','.') }}</td>
								      <td>{{ $list->diskon }}%</td>
								      <td>Rp. {{ number_format(($list->harga_jual - ($list->harga_jual * ($list->diskon / 100))),0,',','.') }}</td>
								      <td>Rp. {{ number_format($list->jumlah_barang * ($list->affiliasi * (25 / 100)), 0, ',','.') }}</td>
								      <td class="harga" id="harga_{{ $list->id }}">Rp. {{ number_format((($list->harga_jual - (($list->harga_jual * ($list->diskon / 100)))) * $list->jumlah_barang) - App\Http\Controllers\PotonganBarangController::cekBarang($list->id_barang, $list->jumlah_barang),0,',','.') }}</td>
								    </tr>
									@endforeach
								</tbody>
							</table><br>
							<h6 class="span">Total Belanja <b><span class="text-danger" id="totalHarga"></span></b></h6>
							<br>
							<hr>
							<br>

							<h5>Data Member</h5><br>
							<span class="span">Saldo Affiliasi <b>Rp. {{ number_format($aff->total_affiliasi, 0, ',', '.') }}</b></span>
							<span class="span">Saldo Deposit <b>Rp. {{ number_format($dep->saldo, 0, ',', '.') }}</b></span>
						</div>
					</div>
				</div>
      </div>
  </main>

</div>

    <footer class="text-muted">
      <div class="container">
        <p class="float-right">
          <a href="#">Back to top</a>
        </p>
        <p>Usahaku Toserba &copy; Usahaku.co.id, 2018</p>
        <p><a href="#" class="col-sm-10" style="padding-left: 0;">Tentang</a> <a href="#" class="col-sm-10">Kontak</a> <a href="#" class="col-sm-10">Frequent Question</a></p>
      </div>
    </footer>

</body>

<script type="text/javascript">
	$(window).scroll(function () {
		if ($(this).scrollTop() > 125) {
			$('#fixed-header').addClass("fixed-top");
		}else {
			$('#fixed-header').removeClass("fixed-top");
		}
	});

	function rupiah(bilangan) {
    let   reverse   = bilangan.toString().split('').reverse().join(''),
          ribuan    = reverse.match(/\d{1,3}/g);
          ribuan    = ribuan.join('.').split('').reverse().join('');

    return ribuan;
  }

	function totalHarga() {
		var tambah = 0;
	    $('.harga').each(function () {
	    	var harga = $(this).html();
				tambah += parseFloat(harga.replace(/[^0-9-,]/g, ''));
	    });

		$('#totalHarga').html('Rp. '+rupiah(tambah));
	}
	totalHarga();
</script>



</html>
