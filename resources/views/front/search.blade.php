<div class="album text-muted" style="min-height:auto;">
    <div class="container">

    <h5>Hasil Pencarian : {{$isi}}</h5>
      @if($products->count() <= 0)
      <span>Maaf, Hasil Pencarian tidak ada</span>
      @else

      <div class="row">
      	@foreach ($products as $view)
        <div class="card" id="card_{{$view->id}}">
        @if (file_exists(public_path('/gambar/'.$view->gambar)))
        <img src="/gambar/{{$view->gambar}}" alt="{{$view->name}}">
        @else
              <img src="/assets/img/no_image.png" alt="No Img">
        @endif
        <p class="card-text">
          <h6 class="judul-barang"><a href="#" class="text-dark">{{$view->name}}</a></h6>
          @if($view->diskon != 0)
          <small class="discount" style="display: inline-block;">Rp. {{ number_format($view->harga_jual,0,',','.') }}</small>  
          <small style="display: inline-block;">Diskon {{ $view->diskon }}%</small>
          @endif
          <h5 class="my-price" data-id="{{$view->harga_jual}}">Rp. {{ number_format($view->harga_jual - ($view->harga_jual * ($view->diskon / 100)),0,',','.') }}</h5>
          <small class="text-muted">Affiliasi : Rp. {{ number_format($view->affiliasi * (75 /100),0,',','.') }}</small>
          <small class="text-muted">Affiliasi Pembeli : Rp. {{ number_format($view->affiliasi * (25 /100),0,',','.') }}</small>
        </p>
        @if(session()->has('member'))
        <button type="submit" class="btn btn-primary beliBarang" id-barang='{{$view->id}}'>Beli</button>
        @else
        <a href="/toko-usahakumart/login/{{ $view->id_toko }}" class="btn btn-primary">Beli</a>
        @endif
      </div>
    	@endforeach
      @endif
      </div>
  </div>
</div>
