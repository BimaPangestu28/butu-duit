<!DOCTYPE html>
<html>
<head>
	<title>{{$toko->name}}</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<meta name="toko-identity" content="{{$toko->id}}">
	<link rel="stylesheet" type="text/css" href="/assets/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="/admin/font-awesome/css/font-awesome.min.css">
	<script type="text/javascript" src="/admin/js/jquery.min.js"></script>
	<script type="text/javascript" src="/assets/bootstrap/js/bootstrap.bundle.min.js"></script>
	<script type="text/javascript" src="/assets/js/toko.js"></script>
	<link rel="stylesheet" type="text/css" href="/assets/css/toko.css">
	<link rel="stylesheet" type="text/css" href="/assets/css/responsive.css">
</head>
<body>

<div id="banner">
	<div class="container">
		<header class="header clearfix top-header" id="fixed-header">
			<nav class="desktopNav">
				<ul class="nav nav-pills float-right nav-top">
			@if(session()->has('member'))
			<li class="nav-item">
				<a class="nav-link text-white" id="cart_offline" data-toggle="modal" data-target="#modalCart" href="#">
					<i class="fa fa-shopping-cart big-font"></i>
					<span id="countCart">{{$cart->count()}}</span>
				</a>
			</li>
			<li class="nav-item">
				<a class="nav-link text-white" href="#">{{Session::get('member')->nama}}</a>
			</li>
			<li class="nav-item">
				<a class="nav-link btn-primary btn-nav" style="color: white;" href="/toko-usahakumart/daftar-order/{{ $toko->id }}">Daftar Order</a>
			</li> &nbsp; &nbsp;
			<li class="nav-item">
				<a class="nav-link btn-warning btn-nav" style="color: white;" href="/affiliasi/{{ $toko->id }}">Lihat affiliasi</a>
			</li> &nbsp; &nbsp;
			<li class="nav-item">
				<a class="nav-link btn-reserve-primary btn-nav" href="/toko-usahakumart/logout/{{ $toko->id }}">logout</a>
			</li>
			@else
			<li class="nav-item">
				<a class="nav-link text-white" href="/toko-usahakumart/login/{{ $toko->id }}"><i class="fa fa-shopping-cart big-font"></i></a>
			</li>
			<li class="nav-item">
				<a class="nav-link btn-reserve-primary btn-nav" href="/toko-usahakumart/login/{{ $toko->id }}">login</a>
			</li>
			@endif &nbsp; &nbsp;
			<li class="nav-item">
				<a class="nav-link btn-danger btn-nav" style="color: white;" href="/">Cari Toko</a>
			</li>
				</ul>
			</nav>

			<a href="/"><h3 class="text-muted desktopNav"><img src="/assets/img/logo.png" alt="logo"> </h3></a>

			<!-- mobbile -->
			<div class="pos-f-t mobileNav">
			  <div class="collapse" id="navbarToggleExternalContent">
			    <div class="bg-inverse p-4">
						<ul class="nav nav-pills text-center">
					@if(session()->has('member'))
					<li class="nav-item">
						<a class="nav-link text-white" id="cart_offline" data-toggle="modal" data-target="#modalCart" href="#">
							<i class="fa fa-shopping-cart big-font"></i>
							<span id="countCart">{{$cart->count()}}</span>
						</a>
					</li>
					<li class="nav-item">
						<a class="nav-link text-white btn-nav" href="#">{{Session::get('member')->nama}}</a>
					</li>
					<li style="width:100%;">
						<li class="nav-item">
							<a class="nav-link btn btn-primary btn-nav" style="color: white;" href="/toko-usahakumart/daftar-order/{{ $toko->id }}">Daftar Order</a>
						</li> &nbsp; &nbsp;
						<li class="nav-item">
							<a class="nav-link btn btn-warning btn-nav" style="color: white;" href="/affiliasi/{{ $toko->id }}">Lihat affiliasi</a>
						</li> &nbsp; &nbsp;
						<li class="nav-item">
							<a class="nav-link btn btn-reserve-primary btn-nav" href="/toko-usahakumart/logout/{{ $toko->id }}">logout</a>
						</li> &nbsp; &nbsp;
						@else
						<li class="nav-item">
							<a class="nav-link text-white btn-nav" href="/toko-usahakumart/login/{{ $toko->id }}"><i class="fa fa-shopping-cart big-font"></i></a>
						</li>
						<li class="nav-item">
							<a class="nav-link btn btn-reserve-primary btn-nav" href="/toko-usahakumart/login/{{ $toko->id }}">login</a>
						</li>
							@endif
						<li class="nav-item">
							<a class="nav-link btn-danger btn-nav" style="color: white;" href="/">Cari Toko</a>
						</li>
					</li>
						</ul>
			    </div>
			  </div>
			  <nav class="navbar navbar-inverse bg-inverse">
					<h3 class="text-muted"><img src="/assets/img/logo.png" alt="logo" width="150px"> </h3>
			    <button class="navbar-toggler" style="outline:none; cursor:pointer;" type="button" data-toggle="collapse" data-target="#navbarToggleExternalContent" aria-controls="navbarToggleExternalContent" aria-expanded="false" aria-label="Toggle navigation">
			      <span class="navbar-toggler-icon"><i class="text-white fa fa-align-justify"></i> </span>
			    </button>
			  </nav>
			</div>


		</header>

			<section class="jumbotron text-center">
				<div class="container">
					<img src="/assets/img/ftr.png">
					<h1 class="heading-row text-capitalize" style="margin-bottom: 0">{{$toko->name}}</h1>
					<p style="margin-bottom: 0; color: #4f4f4f; font-size: 14px;">{{ $toko->alamat }}</p>

					<div class="search-section">
						<form action="/toko-usahakumart/{{ $id }}">
							<div class="form-group">
								<input type="text" name="q" id="mySearch" class="form-control search-input">
							</div>
							<div class="form-group">
								<button type="submit" class="btn btn-warning form-control search-btn">Cari Barang</button>
							</div>
						</form>
					</div>
					</p>
				</div>
			</section>
	 </div>
</div>

<div class="container">
	<section class="text-center additional-text">
		<p class="col-sm-5 text-muted" style="display:inline-block;">Belanja di {{$toko->name}} dengan mudah dan nyaman, melayani jasa COD ( Cash On Delivery ) dan dapatkan keuntungan serta affiliasi disini </p>
	</section>
</div>

    <main role="main">
      <div id="searchResult"></div>

      		@if($ptoko->count() != 0)
			<div id="thisPromo" class="carousel slide" data-ride="carousel">
			  <div class="container">
				  <h5 class="margin-30 text-muted">PROMO TOKO</h5>
					<div class="carousel-inner" role="listbox">
					@foreach($ptoko as $p)
			    	<div class="carousel-item @if($p->id == $firstt->id) active @endif" style="height: 300px">
				      <img class="d-block img-fluid" src="/gambar/{{ $p->gambar }}" alt="First slide" style="object-fit: cover;">
				    </div>
				    @endforeach
				  </div>
				  <a class="carousel-control-prev" href="#thisPromo" role="button" data-slide="prev">
				    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
				    <span class="sr-only">Previous</span>
				  </a>
				  <a class="carousel-control-next" href="#thisPromo" role="button" data-slide="next">
				    <span class="carousel-control-next-icon" aria-hidden="true"></span>
				    <span class="sr-only">Next</span>
				  </a>
			  </div>
			</div>
			@endif

			<!-- Promosi -->
			@if($promo->count() != 0)
			<div id="thisPromo" class="carousel slide" data-ride="carousel">
			  <div class="container">
				  <h5 class="margin-30 text-muted">PROMOSI BARANG</h5>
					<div class="carousel-inner" role="listbox">
					@foreach($promo as $p)
			    	<div class="carousel-item @if($p->id == $first->id) active @endif" style="height: 300px">
			    	<a href="?promo={{ $p->id }}">
				      <img class="d-block img-fluid" src="/gambar/{{ $p->gambar }}" alt="First slide" style="object-fit: cover;">
				    </a>
				    </div>
				    @endforeach
				  </div>
				  <a class="carousel-control-prev" href="#thisPromo" role="button" data-slide="prev">
				    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
				    <span class="sr-only">Previous</span>
				  </a>
				  <a class="carousel-control-next" href="#thisPromo" role="button" data-slide="next">
				    <span class="carousel-control-next-icon" aria-hidden="true"></span>
				    <span class="sr-only">Next</span>
				  </a>
			  </div>
			</div>
			@endif

      <div class="album text-muted myAlbum">
        <div class="container">

        @if($promoB != null)
          <h5 class="margin-30">BARANG PROMO</h5>

          <div id="cardDeck">
            @foreach ($barang as $view)
            <div class="card-custom" id="card_{{$view->id}}" style="margin-top:30px;min-width:255px;">
			  @if (file_exists(public_path('/gambar/'.$view->gambar)))
			  <img src="/gambar/{{$view->gambar}}" alt="{{$view->name}}">
			  @else
              <img src="/assets/img/no_image.png" alt="No Img">
			  @endif
              <p class="card-text">
                <h6 class="judul-barang"><a href="#" class="text-dark">{{$view->name}}</a></h6>
                @if($view->diskon != 0)
                <small class="discount"	 style="display: block;">Rp. {{ number_format($view->harga_jual,0,',','.') }}</small> <br>
                <small style="display: block;">Diskon {{ $view->diskon }}%</small>
				@else
				<small class="discount" style="display: block;visibility:hidden;">tidak ada diskon</small> <br>
				<small style="display: block;visibility:hidden;">tidak ada diskon</small>
                @endif
               <h5 class="my-price" data-id="{{$view->harga_jual - ($view->harga_jual * ($view->diskon / 100))}}">Rp. {{ number_format($view->harga_jual - ($view->harga_jual * ($view->diskon / 100)),0,',','.') }}</h5> <br>
                <small class="text-muted">Affiliasi : Rp. {{ number_format($view->affiliasi * (75 /100),0,',','.') }}</small> <br>
                <small class="text-muted">Affiliasi Pembeli : Rp. {{ number_format($view->affiliasi * (25 /100),0,',','.') }}</small>
              </p>
			  @if(session()->has('member'))
              <button type="submit" class="btn btn-primary form-control beliBarang" id-barang='{{$view->id}}'>Beli</button>
			  @else
			  <a href="/toko-usahakumart/login/{{ $toko->id }}" class="btn btn-primary form-control">Beli</a>
			  @endif
            </div>
            @endforeach
          </div>
          {!! $product->links() !!}
          @endif

        <!-- <h5 class="margin-30">BARANG REKOMENDASI</h5> -->

          <div id="cardDeck">
            @foreach ($product as $view)
            <div class="card-custom" id="card_{{$view->id}}">
			  @if (file_exists(public_path('/gambar/'.$view->gambar)))
			  <img src="/gambar/{{$view->gambar}}" alt="{{$view->name}}">
						
							@if($view->diskon != 0)
                <small style="display: block;" class="myDiscount">Diskon {{ $view->diskon }}%</small>
								@else

                @endif
			  @else
              <img src="/assets/img/no_image.png" alt="No Img">
			  @endif
              <p class="card-text">
                <h6 class="judul-barang"><a class="text-dark">{{$view->name}}</a></h6>
								
                <h5 class="my-price" data-id="{{$view->harga_jual - ($view->harga_jual * ($view->diskon / 100))}}">Rp. {{ number_format($view->harga_jual - ($view->harga_jual * ($view->diskon / 100)),0,',','.') }}</h5> <br>
                <small class="text-muted">Affiliasi : Rp. {{ number_format($view->affiliasi * (75 /100),0,',','.') }}</small> <br>
                <small class="text-muted">Affiliasi Pembeli : Rp. <span id="affiliasi_pembeli_{{ $view->id }}">{{ number_format($view->affiliasi * (25 /100),0,',','.') }}</span></small>
              </p>
			  @if(session()->has('member'))
              <button type="submit" class="btn btn-primary form-control beliBarang btn-nav" id-barang='{{$view->id}}'>Beli</button>
			  @else
			  <a href="/toko-usahakumart/login/{{ $toko->id }}" class="btn btn-primary form-control">Beli</a>
			  @endif
            </div>
            @endforeach
          </div>
          {!! $product->links() !!}
        </div>
      </div>

    </main>

    <footer id="footer" class="text-muted">
      <div class="container text-center">
        <p>
					<a href="#" class="btn-warning uniquePadding btn-nav">Tentang</a>
					<a href="#" class="btn-warning uniquePadding btn-nav">Kontak</a>
					<a href="#" class="btn-warning uniquePadding btn-nav">Frequent Question</a>
				</p>
				<p class="copyright">Usahaku Toserba &copy; Usahaku.co.id, 2018</p>
      </div>
    </footer>

		@if(session()->has('member'))
		<!-- Modal -->
		<div class="modal fade" id="modalCart" tabindex="-1" role="dialog" aria-labelledby="modalCart" aria-hidden="true">
		  <div class="modal-dialog" role="document" style="max-width: 1000px">
		    <div class="modal-content">
		      <div class="modal-header">
		        <h5 class="modal-title" id="exampleModalLabel">Keranjang Belanja</h5>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		      </div>
		      <div class="modal-body">
				<table class="table">
				  <thead>
				    <tr>
				      <th>Nama Barang</th>
				      <th width="10%">Jumlah</th>
				      <th>Harga Satuan</th>
					  <th>Total Harga</th>
					  <th>Affiliasi Pembeli</th>
					  <th>Aksi</th>
				    </tr>
				  </thead>
				  <tbody id="thisCart">
					@foreach ($cart as $list)
				    <tr id="HanyaList_{{$list->id}}" class="dataMe" data-tr="0">
				      <td>{{$list->name}}</td>
				      <td><input id="jumlah_{{ $list->id }}" id-number = "{{ $list->id }}" class="form-control add-core" type="number" min="1" max="{{$list->stok}}" value="{{$list->jumlah_barang}}"></input></td>
							<td>Rp. {{ number_format($list->harga_jual ,0,',','.') }}</td>
				      <td id="harga_{{ $list->id }}">Rp. {{ number_format($list->harga_jual * $list->jumlah_barang,0,',','.') }}</td>
				      <td id="affiliasi_{{ $list->id }}">Rp. {{ number_format(($list->affiliasi * (25/100)) * $list->jumlah_barang,0,',','.') }}</td>
					  	<td><button type="button" data-hapus="{{$list->id}}" class="hapus_barang btn btn-danger"> <i class="fa fa-trash"></i> </button></td>
				    </tr>
					@endforeach
				  </tbody>
				</table>
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn-secondary btn-nav" data-dismiss="modal">Close</button>
		        <a href="/toko-usahakumart/pembelian/{{$toko->id}}" class="btn-primary btn-nav">Lanjut Pembayaran</a>
		      </div>
		    </div>
		  </div>
		</div>
		@endif

<a href="#banner" class="btn btn-warning" id="scrollBtn">	<i class="fa fa-angle-up fafafa"></i> </a>

</body>

<script type="text/javascript">
	$(window).scroll(function () {
		if ($(this).scrollTop() > 125) {
			$('#fixed-header').addClass("fixed-top");
			$('#scrollBtn').show();
		}else {
			$('#fixed-header').removeClass("fixed-top");
			$('#scrollBtn').hide();
		}
	});
</script>

</html>
