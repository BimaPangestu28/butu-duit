<!DOCTYPE html>
<html>
<head>
	<title>Toko Usahaku Cab.Batanghari</title>
	<link rel="stylesheet" type="text/css" href="/assets/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="/admin/font-awesome/css/font-awesome.min.css">
	<script type="text/javascript" src="/admin/js/jquery.min.js"></script>
	<script type="text/javascript" src="/assets/bootstrap/js/bootstrap.bundle.min.js"></script>
	<link rel="stylesheet" type="text/css" href="/assets/css/toko.css">
</head>
<body style="min-height:auto;">

<main>
  <section class="jumbotron text-center">
    <div class="container">
      <h1 class="jumbotron-heading text-capitalize">Transaksi No.{{$transaksi->id}}</h1>
			@if ($transaksi->status == 1)
			<p class="lead text-bold">Transaksi Berhasil, Terimakasih Telah Berbelanja</p>
      @elseif($transaksi->status == 0)
      <p class="lead text-bold">Transaksi Sedang Dalam Pengecekan</p>
			@else
      <p class="lead text-muted">Selamat transaksi anda sedang kami proses, kami akan mengirimkan barang yang anda pesan ke alamat tujuan.</p>
			@endif
      <p>
        <h4>{{$transaksi->penerima}}</h4>
        <h6>{{$transaksi->alamat}}</h6>
      </p>
      @if($transaksi->status == 2)
      <form method="POST" action="/cashier/beli">
        {{ csrf_field() }}
        <input type="hidden" name="pembayaran" value="{{ $transaksi->total_belanja - $transaksi->potongan }}">
        <input type="hidden" name="product_id" id="id" value="{{ \App\Http\Controllers\PagesController::getProduct($transaksi->id) }}"><br>
        <input type="hidden" name="total" id="total-semua" value="{{ $transaksi->total_belanja - $transaksi->potongan }}">
        <input type="hidden" name="toko_id" value="{{ $transaksi->id_toko }}">
        <input type="hidden" name="member_card" value="{{ $aff->number_card }}">
        <input type="hidden" name="id_transaksi" value="{{ $transaksi->id }}">
        <input type="hidden" name="type" value="
          @if($transaksi->metode_pembayaran == 'langsung' || $transaksi->metode_pembayaran == 'deposit') 
          tunai
          @else 
          affiliasi
          @endif
        ">
        <input type="hidden" name="total_affiliasi" value="{{ $affiliasi }}">
        <input type="hidden" name="hpp" value="{{ $hpp }}">
        <input type="hidden" name="total_potongan" value="{{ $transaksi->potongan }}">
        <br>
        <input type="submit" value="Konfirmasi Barang Sudah Diterima" class="form-control btn btn-primary">
      </form>
      @endif
    </div>
  </section>

  <div class="container">
    <div class="table-responsive">
      <table class="table table-striped">
        <thead>
          <tr>
            <th>Nama Barang</th>
            <th>Jumlah</th>
            <th>Potongan</th>
            <th>Diskon</th>
            <th>Harga</th>
            <th>Affiliasi</th>
            <th>Total Harga</th>
          </tr>
        </thead>
        <tbody id="thisCart">
        @foreach ($cart as $list)
          <tr id="HanyaList_{{$list->id}}" class="dataMe" data-tr="0">
            <td>{{$list->name}}</td>
            <td>{{$list->jumlah_barang}}</td>
            <td>Rp. {{ number_format(App\Http\Controllers\PotonganBarangController::cekBarang($list->id_barang, $list->jumlah_barang), 0, ',','.') }}</td>
            <td>{{ $list->diskon }}%</td>
            <td>Rp. {{ number_format(($list->harga_jual - ($list->harga_jual * ($list->diskon / 100))),0,',','.') }}</td>
            <td>Rp. {{ $list->jumlah_barang * ($list->affiliasi * (25 / 100)) }}</td>
            <td class="harga">Rp. {{ number_format((($list->harga_jual - (($list->harga_jual * ($list->diskon / 100)))) * $list->jumlah_barang) - App\Http\Controllers\PotonganBarangController::cekBarang($list->id_barang, $list->jumlah_barang),0,',','.') }}</td>
          </tr>
        @endforeach
        </tbody>
      </table>
    </div>
		<section class="jumbotron text-center">
			<h2 class="jumbotron-heading text-capitalize">Total Pembayaran : <span id="totalHarga"></span> </h2>
		</section>
  </div>

</main>

<script type="text/javascript">
  function rupiah(bilangan) {
    let   reverse   = bilangan.toString().split('').reverse().join(''),
          ribuan    = reverse.match(/\d{1,3}/g);
          ribuan    = ribuan.join('.').split('').reverse().join('');

    return ribuan;
  }
  function totalHarga() {
    var tambah = 0;
    $('.harga').each(function () {
      var harga = $(this).html();
      tambah += parseFloat(harga.replace(/[^0-9-,]/g, ''));
    });

    $('#totalHarga').html('Rp '+rupiah(tambah));
    console.log(tambah);
  }
  totalHarga();
</script>

</body>
