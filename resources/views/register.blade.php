@extends('usahakumart/app')

@section('content')
<!-- Breadcrumb -->
<ol class="breadcrumb">
<li class="breadcrumb-item">Home</li>
<li class="breadcrumb-item">Admin</li>
<li class="breadcrumb-item active">Register Member</li>

<!-- Breadcrumb Menu-->

</ol>

<div class="container-fluid">

  <div class="card">
    <div class="card-body">
      <div class="row">
        <div class="col-sm-5">
          <h4 class="card-title mb-0">Register Member</h4>
          <div class="small text-muted">{{date('d M Y')}}</div>
        </div>
        <!--/.col-->
        <div class="col-sm-7 d-none d-md-block">
          <button type="button" class="btn btn-primary float-right"><i class="icon-cloud-download"></i></button>
          <div class="btn-toolbar float-right" role="toolbar" aria-label="Toolbar with button groups">
          </div>
        </div>
        <!--/.col-->
      </div>
      @if($errors->count() > 0 || session()->has('alert'))
      <div class="row">
      	<div class="alert alert-danger col-sm-12" style="margin-bottom: 0; margin-top: 25px;">
      		@if($errors->count() > 0)
      			@foreach($errors->all() as $error)
      			<p>{{ $error }}</p>
      			@endforeach
      		@else
      			<p>{{ session()->get('alert') }}</p>
      		@endif
      	</div>
      </div>
      @endif
    </div>
  </div>
  <!--/.card-->


  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          Usahaku Toserba - Toko Offline
        </div>
        <div class="card-body">
          <form method="POST" action="/cashier/member/register">
      			{{ csrf_field() }}

      			<span>Nomer Kartu</span>
      			<select name="number_card" class="form-control">
      				@foreach($card as $c)
      					<option value="{{ $c->number_card }}">{{ $c->number_card }}</option>
      				@endforeach
      			</select><br>
      			<span>Nama</span>
      			<input type="text" name="nama" placeholder="nama" class="form-control"><br>
      			<span>Tempat Tanggal Lahir</span>
      			<input type="text" name="tempat_lahir" class="form-control"> <br> <input type="date" name="tanggal_lahir" class="form-control"><br>
            <span>Jenis Kelamin</span>
            <select name="jenis_kelamin" class="form-control">
              <option value="Laki - Laki">Laki - Laki</option>
              <option value="Perempuan">Perempuan</option>
            </select><br>
            <span>Nomer Telepon</span>
            <input type="number" name="telepon" class="form-control"><br>
      			<span>Kode Pengundang. Kosongkan Jika Tidak Ada</span>
      			<input type="number" name="code_invitation" class="form-control"><br>
      			<span>Pilih Member Jika Sudah Terdaftar Di Website</span>
      			<select name="id_member" class="form-control">
      				<option default value="">-- Pilih Member --</option>
      				@foreach($member as $m)
      				<option value="{{ $m->id_member }}">{{ $m->id_member }} - {{ $m->nama }}</option>
      				@endforeach
      			</select><br>
            <input type="hidden" name="premium_cash" value="0">
      			<input type="submit" class="btn btn-primary form-control">
      		</form>
        </div>
      </div>
    </div>
    <!--/.col-->
  </div>
  <!--/.row-->
</div>

</div>
@endsection
