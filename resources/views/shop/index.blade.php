<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="shortcut icon" href="/admin/img/favicon.png">
  <title>UsahaMart Administrator</title>

  <!-- Icons -->
  <link href="/admin/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="/admin/simple-line-icons/css/simple-line-icons.css" rel="stylesheet">

  <!-- Main styles for this application -->
  <link href="/admin/css/style.css" rel="stylesheet">
  <!-- Styles required by this views -->

</head>
<body class="app flex-row align-items-center">
  <div class="loader">
    <img src="/loader/produk.gif">
  </div>

  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-8">
        <div class="card-group">
          <div class="card p-4">
          	<form method="POST" action="/">
          	{{ csrf_field() }}
            <div class="card-body">
              <h1 class="text-center">UsahaMart</h1>
              <p class="text-muted text-center">Pilih Lokasi Mu</p>
              @if(session()->get('alert'))
              <div class="alert alert-danger form-control">
                <h5 class="text-center">{{ session()->get('alert') }}</h5>
              </div>
              @endif
              <div class="input-group mb-3">
                <select class="form-control" name="id_provinsi" style="margin-top: 10px;" id="provinsi" ">
                  @foreach($provinsi as $p)
                  <option value="{{ $p->id }}">{{ $p->provinsi }}</option>
                  @endforeach
                </select>
              </div>
              <div class="input-group mb-4">
                <select class="form-control" name="id_kota" style="margin-top: 10px;" id="kota">
                  @foreach($kota as $k)
                  <option value="{{ $k->id }}">{{ $k->kota }}</option>
                  @endforeach
                </select>
              </div>
              <div class="input-group mb-4">
                <select class="form-control" name="id_kecamatan" id="kecamatan">
                  @foreach($kecamatan as $k)
                  <option value="{{ $k->id }}">{{ $k->kecamatan }}</option>
                  @endforeach
                </select>
              </div>
              <div class="row">
                <div class="col-12">
                  <button type="submit" class="btn btn-primary form-control">Pilih Toko</button>
                </div>
              </div>
            </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>

  <script type="text/javascript" src="/admin/js/jquery.min.js"></script>
  <script type="text/javascript" src="/admin/js/toko.js"></script>
</body>

</html>