<!DOCTYPE html>
<html>
<head>
	<title>Usahaku Toserba Online</title>
	<link rel="stylesheet" type="text/css" href="/assets/bootstrap/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="/assets/css/webview.css">
	<script type="text/javascript" src="/admin/js/jquery.min.js"></script>
	<script type="text/javascript" src="/assets/js/script.js"></script>
</head>
<body>

	<div id="content">
		<div class="container">
			<div class="row">
				<div class="col-md-5" id="location">
					<h4>Cari Toko Usahaku Toserba</h4>
					<form id="cari">
						<div class="form-group">
							<label>Provinsi</label>
							<select type="text" name="provinsi" id="provinsi" class="form-control">
								<option value="default">--Pilih Provinsi--</option>
								@foreach($provinsi as $view)
								<option value="{{$view->id}}">{{$view->provinsi}}</option>
								@endforeach
							</select>
						</div>
						<div class="form-group">
							<label>Kota</label>
							<select type="text" name="kota" id="kota" class="form-control">
								<option value="default">--Pilih Kota--</option>
							</select>
						</div>
						<div class="form-group">
							<label>Kecamatan</label>
							<select type="text" name="kecamatan" id="kecamatan" class="form-control">
								<option value="default">--Pilih Kecamatan--</option>
							</select>
						</div>
						<div class="form-group">
							<label>Kategori Toko</label>
							<select type="text" name="kategori" id="kategori" class="form-control">
								<option value="default">--Pilih Kategori Toko--</option>
								@foreach($kategori as $k)
								<option value="{{ $k->id }}">{{ $k->kategori }}</option>
								@endforeach
							</select>
						</div>
						<input type="submit" name="submit" class="btn btn-primary form-control" value="Cari Toko">
					</form>
				</div>

				<div class="col-md-7">
					<div class="col-md-9 float-right">
						<ul class="list-group" id="list-toko">
							<li class="list-group-item">
								<!-- <span>USAHAKUMART CABANG BATANGHARI</span> <br>
								<small>Alamat : Jl.Kebun Raya Batanghari, RT/RW 021/012</small> -->
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>


</body>

</html>
