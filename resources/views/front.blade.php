<!DOCTYPE html>
<html>
<head>
	<title>Usahaku Toserba Online</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" type="text/css" href="/assets/css/style.css">
	<script type="text/javascript" src="/admin/js/jquery.min.js"></script>
	<script type="text/javascript" src="/assets/js/script.js"></script>
</head>
<body>
	<div class="gambar-back"></div>
	<div class="human"></div>

	<div class="loader">
	  <img src="/loader/produk.gif">
	</div>

	<div id="content">
		<div class="container">
			<div class="logo" style="width: 100%">
				<img src="/assets/img/logo.png">
				<a href="/login" class="btn btn-warning btn-2">LOGIN MEMBER</a>
			</div>
			<div class="row">
				<div class="right" id="location">
					<h4>Cari Toko Usahaku Toserba</h4>
					<form id="cari">
						<div class="form-group">
							<label>Provinsi</label>
							<select type="text" name="provinsi" id="provinsi" class="form-control">
								<option value="default">-- Pilih Provinsi --</option>
								@foreach($provinsi as $view)
								<option value="{{$view->id}}">{{$view->provinsi}}</option>
								@endforeach
							</select>
						</div>
						<div class="form-group">
							<label>Kota</label>
							<select type="text" name="kota" id="kota" class="form-control">
								<option value="default">-- Pilih Kota --</option>
							</select>
						</div>
						<div class="form-group">
							<label>Kecamatan</label>
							<select type="text" name="kecamatan" id="kecamatan" class="form-control">
								<option value="default">-- Pilih Kecamatan --</option>
							</select>
						</div>
						<div class="form-group">
							<label>Kategori Toko</label>
							<select type="text" name="kategori" id="kategori" class="form-control">
								<option value="default">-- Pilih Kategori Toko --</option>
								@foreach($kategori as $k)
								<option value="{{ $k->id }}">{{ $k->kategori }}</option>
								@endforeach
							</select>
						</div>
						<input type="submit" name="submit" class="btn btn-primary form-control" value="Cari Toko">
					</form>
				</div>

				<div class="left">
					<input id="query-search" type="text" name="query" class="form-control" placeholder="Cari nama toko">

					<div class="col-md-9 float-right tutup hll">
						<ul class="list-group" id="list-toko">
							<div class="loading"></div>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>


</body>

</html>
