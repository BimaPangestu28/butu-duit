<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <link rel="shortcut icon" href="/admin/img/favicon.png">
	<script type="text/javascript" src="/admin/js/jquery.min.js"></script>
	<script type="text/javascript" src="/assets/bootstrap/js/bootstrap.bundle.min.js"></script>
  <title>UsahaMart Administrator</title>

  <!-- Icons -->
  <link href="/admin/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="/admin/simple-line-icons/css/simple-line-icons.css" rel="stylesheet">

  <!-- Main styles for this application -->
  <link href="/admin/css/style.css" rel="stylesheet">
  <link href="/admin/css/custom.css" rel="stylesheet">
  <!-- Styles required by this views -->

</head>

<body class="app header-fixed sidebar-fixed aside-menu-fixed aside-menu-hidden">
  <header class="app-header navbar">
    <button class="navbar-toggler mobile-sidebar-toggler d-lg-none mr-auto" type="button">
      <span class="navbar-toggler-icon"></span>
    </button>
    <a class="navbar-brand" href="#"></a>
    <button class="navbar-toggler sidebar-toggler d-md-down-none" type="button">
      <span class="navbar-toggler-icon"></span>
    </button>

    <ul class="nav navbar-nav d-md-down-none">
      <li class="nav-item px-3">
        <a class="nav-link" href="#">
          @if(Auth::user()->id_tipe == 1)
            Official Usahaku Toserba
          @else
            Partner Usahaku Toserba
          @endif
        </a>
      </li>
      <li class="nav-item px-3">
        <a href="/cashier/notifikasi-barang/alert" class="btn btn-warning btn-sm" style="color: white;">Notifikasi Stok Barang Habis <span class="badge badge-pill badge-danger" id="notifyBadgeStok" style="font-size: 12px;">Loading ...</span></a>
      </li>
      <li class="nav-item px-3">
        <!-- <a class="nav-link" href="#">Settings</a> -->
      </li>
    </ul>
    <ul class="nav navbar-nav ml-auto">

    </ul>

  </header>

  <div class="app-body">
    <div class="sidebar">
      <nav class="sidebar-nav">
        <ul class="nav">
          <li class="nav-item">
            <a class="nav-link" href="/cashier/dashboard"><i class="icon-speedometer"></i> Dashboard</a>
          </li>

          <li class="nav-title">
            Layanan Utama
          </li>
          @if(session('user-toko')->level == 1 || session('user-toko')->level == 3)
          <li class="nav-item">
            <a class="nav-link nav-link-warning" href="/cashier/penjualan"><i class="icon-bag"></i> Penjualan (Transaksi)</a>
          </li>
          @endif
          @if(session('user-toko')->level == 1 || session('user-toko')->level == 2 || session('user-toko')->level == 3)
          <li class="nav-item nav-dropdown">
            <a class="nav-link nav-dropdown-toggle" href="#"><i class="icon-docs"></i> Laporan</a>
            <ul class="nav-dropdown-items">
              @if(session('user-toko')->level == 1 || session('user-toko')->level == 3)
              @if(session('user-toko')->level == 1)
              <li class="nav-item">
                <a class="nav-link" href="/cashier/laporan/affiliasi"><i class="icon-layers"></i> Affiliasi</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="/cashier/laporan/pembelian-produk"><i class="icon-basket"></i> Pembelian Produk</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="/cashier/laporan/stok-kartu"><i class="icon-credit-card"></i> Kartu Stok</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="/cashier/laporan/kas"><i class="icon-briefcase"></i> Kas</a>
              </li>
              @endif
              <li class="nav-item">
                <a class="nav-link" href="/cashier/laporan/penjualan"><i class="icon-basket-loaded"></i> Penjualan</a>
              </li>
              <!-- <li class="nav-item">
                <a class="nav-link" href="/cashier/laporan/laporan-kasir"><i class="icon-star"></i> Laporan Kasir</a>
              </li> -->
              @if(session('user-toko')->level == 1)
              <li class="nav-item">
                <a class="nav-link" href="/cashier/laporan/back-office"><i class="icon-event"></i> Back Office</a>
              </li>
              @endif
              @endif
            </ul>
          </li>
          @endif
          @if(Auth::user()->id_tipe == 2 && session('user-toko')->level == 1)
          <li class="nav-item">
            <a class="nav-link" href="/cashier/request-withdraw"><i class="icon-star"></i> Request Withdraw</a>
          </li>

          <li class="nav-item">
            <a class="nav-link" href="/cashier/deposit-saldo"><i class="icon-star"></i> Konfirmasi Deposit</a>
          </li>
          @endif
          @if(session('user-toko')->level == 1 || session('user-toko')->level == 2)
          <li class="nav-item nav-dropdown">
            <a class="nav-link nav-dropdown-toggle" href="#"><i class="icon-drawer"></i> Barang</a>
              <ul class="nav-dropdown-items">
                <li class="nav-item">
                  <a class="nav-link" href="/cashier/supplier"><i class="icon-arrow-right"></i> Tambah Supplier</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="/cashier/daftar-rak"><i class="icon-arrow-right"></i> Daftar Rak</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="/cashier/tambah-barang"><i class="icon-arrow-right"></i> Tambah Barang</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="/cashier/tambah-category"><i class="icon-arrow-right"></i> Tambah Category</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="/cashier/gudang-barang"><i class="icon-arrow-right"></i> Gudang Barang</a>
                </li>
                @if(session('user-toko')->level == 1)
                <li class="nav-item">
                  <a class="nav-link" href="/cashier/pola-perilaku-harga"><i class="icon-arrow-right"></i> Pola Perilaku Harga</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="/cashier/promosi-toko"><i class="icon-arrow-right"></i> Promosi Toko</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="/cashier/promosi-barang"><i class="icon-arrow-right"></i> Promosi Barang</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="/cashier/potongan-barang"><i class="icon-arrow-right"></i> Potongan Barang</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="/cashier/notifikasi-barang"><i class="icon-arrow-right"></i> Notifikasi Barang</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="/cashier/laporan/total-barang-terjual"><i class="icon-star"></i> Total Terjual</a>
                </li>
                <li class="nav-item btn-danger">
                  <a class="nav-link" href="/cashier/stok-opname"><i class="icon-arrow-right"></i> Stok Opname</a>
                </li>
                @endif
              </ul>
          </li>
          @endif
          @if(session('user-toko')->level == 1)
          <li class="nav-item nav-dropdown">
            <a class="nav-link nav-dropdown-toggle" href="#"><i class="icon-user"></i> Pengaturan Users</a>
              <ul class="nav-dropdown-items">
                <li class="nav-item nav-dropdown">
                  <a class="nav-link" href="/cashier/member/register"><i class="icon-plus"></i> Tambah Member</a>
                </li>
                <li class="nav-item nav-dropdown">
                  <a class="nav-link" href="/cashier/user-toko"><i class="icon-people"></i> User Toko</a>
                </li>
              </ul>
          </li>
          <li class="nav-item nav-dropdown">
            <a class="nav-link" href="/cashier/stok-kartu"><i class="icon-screen-tablet"></i> Stok Kartu</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/cashier/riwayat-transaksi"><i class="icon-pie-chart"></i> Riwayat Transaksi</a>
          </li>
          @endif
          @if(session('user-toko')->level == 1 || session('user-toko')->level == 3)
          <li class="nav-item">
            <a class="nav-link" href="/cashier/notifikasi"><i class="icon-bell"></i> Notifikasi <span class="badge badge-pill badge-danger" id="notifyBadge">loading...</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link nav-link-success" href="/cashier" target="_blank"><i class="icon-chart"></i> Mesin Kasir</a>
          </li>
          @endif
          <!-- <li class="nav-item">
            <a class="nav-link" href="charts.html"><i class="icon-pie-chart"></i> Artikel UMart</a>
          </li> -->
          <li class="divider"></li>
          <li class="nav-title">
            Pengaturan Lainnya
          </li>
          @if(session('user-toko')->level == 1)
          <li class="nav-item nav-dropdown">
            <li class="nav-item">
              <a class="nav-link" href="/cashier/pengaturan"><i class="icon-settings"></i> Pengaturan</a>
            </li>
          </li>
          @endif
          <li class="nav-item nav-dropdown">
            <li class="nav-item">
              <a class="nav-link" href="/usahakumart-administrator/logout"><i class="icon-logout"></i> Logout</a>
            </li>
          </li>

        </ul>
      </nav>
      <button class="sidebar-minimizer brand-minimizer" type="button"></button>
    </div>

    <!-- Main content -->
    <main class="main">
      @yield('content')
    </main>

  </div>

  <footer class="app-footer">
    <span>Usahaku © 2017 usahaku.co.id.</span>
  </footer>

  <!-- CoreUI main scripts -->

  <script src="/admin/js/app.js"></script>

  <!-- Plugins and scripts required by this views -->

  <!-- Custom scripts required by this view -->
  <script src="/admin/js/views/main.js"></script>
  <script type="text/javascript" src="/admin/js/chasier.js"></script>
  <script type="text/javascript" src="/admin/js/jquery.cookie.js"></script>

  @yield('script')

  <script type="text/javascript">

    function ajaxNotify() {
      $.ajax({
        type: 'GET',
        url: '/cashier/ajax/notify-count',
        dataType: 'json',
        success:function (data) {
          $('#notifyBadge').html(data);
        }
      });
    };

    setInterval(ajaxNotify, 5000);

    function ajaxNotifyStok() {
      $.ajax({
        type: 'GET',
        url: '/cashier/notifikasi-barang/count',
        dataType: 'json',
        success:function (data) {
          $('#notifyBadgeStok').html(data);
        }
      });
    };

    setInterval(ajaxNotifyStok, 2000);
  </script>

</body>
</html>
