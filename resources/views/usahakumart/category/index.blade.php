@extends('usahakumart/app')

@section('content')
<!-- Breadcrumb -->
<ol class="breadcrumb">
<li class="breadcrumb-item">Home</li>
<li class="breadcrumb-item">Admin</li>
<li class="breadcrumb-item active">Request & Stok Kartu</li>

<!-- Breadcrumb Menu-->

</ol>

<div class="container-fluid">

  <div class="card">
    <div class="card-body">
      <div class="row">
        <div class="col-sm-5">
          <h4 class="card-title mb-0">Request & Stok Kartu</h4>
          <div class="small text-muted">{{date('d M Y')}}</div>
        </div>
        <!--/.col-->
        <div class="col-sm-7 d-none d-md-block">
          <button type="button" class="btn btn-primary float-right"><i class="icon-cloud-download"></i></button>
          <div class="btn-toolbar float-right" role="toolbar" aria-label="Toolbar with button groups">
          </div>
        </div>
        <!--/.col-->
      </div>
      @if(session()->has('alert'))
      <div class="row">
      	<div class="alert alert-danger col-sm-12" style="margin-bottom: 0; margin-top: 25px;">
      			<p>{{ session()->get('alert') }}</p>
      	</div>
      </div>
      @endif
    </div>
  </div>
  <!--/.card-->


  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          Usahaku Toserba - Toko Offline
        </div>
        <div class="card-body">
          @if($cat != null)
          <form method="POST" action="/cashier/update-category/{{ $cat->id }}" enctype="multipart/form-data">
            {{ csrf_field() }}

            <div class="form-group">
              <strong>Nama Kategori</strong><br>
              <input type="text" name="category" class="form-control" style="margin-top: 10px;" value="{{ $cat->category }}"">
            </div>

            <input type="submit" class="btn btn-primary form-control" value="Update Category">
          </form>
          @else
          <form method="POST" action="/cashier/tambah-category" enctype="multipart/form-data">
            {{ csrf_field() }}

            <div class="form-group">
              <strong>Nama Kategori</strong><br>
              <input type="text" name="category" class="form-control" style="margin-top: 10px;" value="{{ old('category') }}"">
            </div>

            <input type="submit" class="btn btn-primary form-control" value="Tambah Category">
          </form>
          @endif
          <br>
          <hr>
          <br>
          <!-- <form method="GET" action="/usahakumart/search-barang" >
              <input class="form-control" type="text" placeholder="Type to search" style="width: 93.5%; display: inline-block;" name="q"></input>
              <button type="submit" class="btn btn-primary" style="margin-left: -4px; margin-top: -5px;">Search</button>
          </form>
          <br> -->
          <form method="GET" action="/cashier/search-kartu" >
              <input class="form-control" type="text" placeholder="Type to search" style="width: 93.5%; display: inline-block;" name="q"></input>
              <button type="submit" class="btn btn-primary" style="margin-left: -4px; margin-top: -5px;">Search</button>
          </form>
          <br>
          <table class="table table-responsive-sm table-hover table-outline mb-0">
            <thead class="thead-light">
              <tr>
                <th class="text-center">#</th>
                <th class="text-center">Nama Category</th>
                @if(session('user-toko')->level == 1)
                <th class="text-center">Option</th>
                @endif
              </tr>
            </thead>
            <tbody>
            @foreach($category as $no => $t)
              <tr>
                <td class="text-center">{{ $no += 1 }}</td>
                <td class="text-center">{{ $t->category }}</td>
                @if(session('user-toko')->level == 1)
                <td class="text-center">
                  <a href="/cashier/edit-category/{{ $t->id }}" class="btn btn-primary">Edit</a>
                  <a href="/cashier/delete-category/{{ $t->id }}" class="btn btn-danger" onclick="return confirm('apakah anda yakin?')">Delete</a>
                </td>
                @endif
              </tr>
            @endforeach
            </tbody>
          </table>

          {{$category->render()}}
        </div>
      </div>
    </div>
    <!--/.col-->
  </div>
  <!--/.row-->
</div>

</div>
@endsection
