@extends('usahakumart/app')

@section('content')
<!-- Breadcrumb -->
<ol class="breadcrumb">
<li class="breadcrumb-item">Home</li>
<li class="breadcrumb-item">Admin</li>
<li class="breadcrumb-item active">Laporan Pembelian Produk Supplier {{$supplier}}</li>

<!-- Breadcrumb Menu-->

</ol>

<div class="container-fluid">

  <div class="card">
    <div class="card-body">
      <div class="row">
        <div class="col-sm-10">
          <h4 class="card-title mb-0">Laporan Pembelian Produk Supplier {{$supplier}}</h4>
          <div class="small text-muted">{{date('d M Y')}}</div>
          <button data-toggle="modal" data-target="#myModal" class="btn btn-warning">Download Laporan</button>

          <br>
          <br>

          <h5>Total Pembayaran : Rp. {{ number_format($dataTotal,0,',','.') }}</h5>
          <h5>Total Pajak : Rp. {{ number_format($dataPajak,0,',','.') }}</h5>
          <h5>Total Diskon : Rp. {{ number_format($dataDiskon,0,',','.') }}</h5>

        </div>
        <!--/.col-->
      </div>
      @if(session()->has('alert'))
      <div class="row">
      	<div class="alert alert-danger col-sm-12" style="margin-bottom: 0; margin-top: 25px;">
      			<p>{{ session()->get('alert') }}</p>
      	</div>
      </div>
      @endif
    </div>
  </div>
  <!--/.card-->

 <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          Usahaku Toserba - Toko Offline
        </div>
        <div class="card-body">
          <form method="GET" action="/cashier/laporan/pembelian-produk/search" >
              <input type="hidden" name="supplier" value="{{$supplier}}">
              <input class="form-control" type="text" placeholder="Isi dengan Nama, Supplier atau Kode Barang" name="q2"></input><br>
              <button type="submit" class="btn btn-primary form-control">Search</button>
          </form>
          
          <br>
          @if(app('request')->get('q2') == null && $showMany > 100)
                Banyak data yang ditampilkan : 
                @foreach (range(100, $showMany, 100) as $i)
                  <a class="btn btn-primary btn-sm" href="/cashier/laporan/pembelian-produk/supplier?supplier={{$supplier}}&banyak={{ $i }}">{{ $i }}</a>
                @endforeach
          @endif

          <br>
          <br>
          <table class="table table-responsive-sm table-hover table-outline mb-0">
            <thead class="thead-light">
              <tr>
                <th>#</th>
                <th class="text-center">Kode Barang</th>
                <th class="text-center">Nama Barang</th>
                <th class="text-center">Category</th>
                <th class="text-center">Rak</th>
                <th class="text-center">Photo</th>
                <th class="text-center">Option</th>
              </tr>
            </thead>
            <tbody>
              @foreach ($datas as $no=>$view)
              <tr
                @if(\App\Http\Controllers\LaporanController::cekGudang($view->produk) == true)
                style="background: crimson; color: white;"
                @endif
              >
                <td class="text-center">{{$no+1}}</td>
                <td class="text-center">{{$view['barcode']}}</td>
                <td class="text-center">{{$view['produk']}}</td>
                <td class="text-center">{{ App\Http\Controllers\LaporanController::category($view['id_category']) }}</td>
                <td class="text-center">{{$view['rak']}}</td>
                <td class="text-center">
                  <img src="/gambar/{{$view['gambar']}}" width="200" height="200">
                </td>
                <td class="text-center">
                  <a href="/cashier/laporan/pembelian-produk/edit/{{ $view['id'] }}" class="btn btn-warning" style="display: inline-block;">Edit</a><br><br>
                  <form method="post" action="/cashier/laporan/pembelian-produk/delete/{{$view->id}}" style="display: inline-block;">
                    {{csrf_field()}}
                    <button type="submit" class="btn btn-danger">Hapus</button>
                  </form>
                  @if(App\Http\Controllers\LaporanController::cekGudang($view->id) == true)
                  <br><br>
                  <a href="/cashier/tambah-barang?produk={{ $view['id'] }}" class="btn btn-primary" style="display: inline-block;" target="_blank">Tambah Ke Gudang</a><br><br>
                  @endif
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
          <br>
        </div>
      </div>
    </div>
  </div>

</div>

</div>

<!-- Modal -->
<div class="modal" id="myModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Download Laporan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="/cashier/laporan/pembelian-produk/print">
      <div class="modal-body">
        <p>
        <input type="hidden" name="supplier" value="{{$supplier}}">
        <label for="from">Dari :</label>
        <input type="date" name="from" class="form-control">
        <label for="to">Sampai :</label>
        <input type="date" name="to" class="form-control">
        </p>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Download Laporan</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
      </form>
    </div>
  </div>
</div>

<script type="text/javascript" src="/js/gudang.js"></script>
@endsection
