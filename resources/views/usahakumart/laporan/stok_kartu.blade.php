@extends('usahakumart/app')

@section('content')
<!-- Breadcrumb -->
<ol class="breadcrumb">
<li class="breadcrumb-item">Home</li>
<li class="breadcrumb-item">Admin</li>
<li class="breadcrumb-item active">Laporan Stok Kartu</li>

<!-- Breadcrumb Menu-->

</ol>

<div class="container-fluid">

  <div class="card">
    <div class="card-body">
      <div class="row">
        <div class="col-sm-5">
          <h4 class="card-title mb-0">Laporan Stok Kartu</h4>
          <div class="small text-muted">{{date('d M Y')}}</div><br>

          <a href="/cashier/laporan/stok-kartu/print?type=today" class="btn btn-warning btn-sm">Download Laporan Hari Ini</a>

          <!-- <a href="/cashier/laporan/stok-kartu/print?type=month" class="btn btn-danger btn-sm">Download Laporan Bulan Ini</a> -->
        </div>
        <!--/.col-->
        <div class="col-sm-7 d-none d-md-block">
          <button type="button" class="btn btn-primary float-right"><i class="icon-cloud-download"></i></button>
          <div class="btn-toolbar float-right" role="toolbar" aria-label="Toolbar with button groups">
          </div>
        </div>
        <!--/.col-->
      </div>
      @if(session()->has('alert'))
      <div class="row">
      	<div class="alert alert-danger col-sm-12" style="margin-bottom: 0; margin-top: 25px;">
      			<p>{{ session()->get('alert') }}</p>
      	</div>
      </div>
      @endif
    </div>
  </div>
  <!--/.card-->


  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          Usahaku Toserba - Toko Offline
        </div>
        <div class="card-body">
          <form method="GET" action="/cashier/search-barang" >
              <input class="form-control" type="date" placeholder="Type to search" style="width: 93.5%; display: inline-block;" name="q"></input>
              <button type="submit" class="btn btn-primary" style="margin-left: -4px; margin-top: -5px;">Search</button>
          </form>
          <br>
          <table class="table table-responsive-sm table-hover table-outline mb-0">
            <thead class="thead-light">
              <tr>
                <th class="text-center">Jumlah Kartu Keseluruhan</th>
                <th class="text-center">Jumlah Kartu Terpakai</th>
                <th class="text-center">Jumlah Kartu Belum Terpakai</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td class="text-center">{{ $jumlah_keseluruhan }}</td>
                <td class="text-center">{{ $jumlah_terpakai }}</td>
                <td class="text-center">{{ $jumlah_belum }}</td>
              </tr>
            </tbody>
          </table>
          <br>
        </div>
      </div>
    </div>
    <!--/.col-->
  </div>
  <!--/.row-->
</div>

</div>
<script type="text/javascript" src="/js/gudang.js"></script>
@endsection
