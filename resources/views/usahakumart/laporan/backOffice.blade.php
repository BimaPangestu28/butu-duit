@extends('usahakumart/app')

@section('content')
<!-- Breadcrumb -->
<ol class="breadcrumb">
<li class="breadcrumb-item">Home</li>
<li class="breadcrumb-item">Admin</li>
<li class="breadcrumb-item active">Laporan Back Office ( Laba Rugi )</li>

<!-- Breadcrumb Menu-->

</ol>

<div class="container-fluid">

  <div class="card">
    <div class="card-body">
      <div class="row">
        <div class="col-sm-12">
          <h4 class="card-title mb-0">Laporan Back Office ( Laba Rugi ) Produk</h4>
          <div class="small text-muted">{{date('d M Y')}}</div>
          @if(session('user-toko')->level == 1)
          <br>
          <button data-toggle="modal" data-target="#myModalPeriode" class="btn btn-danger">Download Laporan Periode</button>
          @endif
        </div>
        <!--/.col-->
      </div>
      @if(session()->has('alert'))
      <div class="row">
      	<div class="alert alert-danger col-sm-12" style="margin-bottom: 0; margin-top: 25px;">
      			<p>{{ session()->get('alert') }}</p>
      	</div>
      </div>
      @endif
    </div>
  </div>
  <!--/.card-->

 <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          Usahaku Toserba - Toko Offline
        </div>
        <div class="card-body">
          <form method="GET" action="/cashier/laporan/back-office" >
              <label for="from">Cari Pada Tanggal</label>
              <input type="date" class="form-control" name="start"><br>
              <label for="from">Sampai Pada Tanggal</label>
              <input type="date" class="form-control" name="end"><br>
              <button type="submit" class="btn btn-primary form-control">Search</button>
          </form>
          <br>
          <table class="table table-responsive-sm table-hover table-outline mb-0">
            <thead class="thead-light">
              <tr>
                <th>#</th>
                <th class="text-center">Tanggal</th>
                <th class="text-center">Keterangan</th>
                <th class="text-center">Pendapatan</th>
                <th class="text-center">HPP</th>
                <th class="text-center">Total Potongan</th>
                <th class="text-center">Affiliasi</th>
                <th class="text-center">Keterangan</th>
                <th class="text-center">Laba Rugi</th>
                <th class="text-center">Operator</th>
              </tr>
            </thead>
            <tbody id="body-data">
              @foreach($period as $p)
                <tr>
                  <td>{{ $no += 1 }}</td>
                  <td>{{ $p->format('d/m/Y') }}</td>
                  <td>Pendapatan Penjualan</td>
                  <td style="display: none;">{{ $pendapatan += (App\Http\Controllers\LaporanController::totalPeriode($p->format('Y-m-d'), null, 'omset') + App\Http\Controllers\LaporanController::totalPeriode($p->format('Y-m-d'), null, 'potongan')) }}</td>
                  <td>Rp. {{ number_format(App\Http\Controllers\LaporanController::totalPeriode($p->format('Y-m-d'), null, 'omset') + App\Http\Controllers\LaporanController::totalPeriode($p->format('Y-m-d'), null, 'potongan'),0,',','.') }}</td>
                  <td style="display: none;">{{ $hpp += App\Http\Controllers\LaporanController::totalPeriode($p->format('Y-m-d'), null, 'hpp') }}</td>
                  <td>Rp. {{ number_format(App\Http\Controllers\LaporanController::totalPeriode($p->format('Y-m-d'), null, 'hpp'),0,',','.') }}</td>
                  <td style="display: none;">{{ $potongan += App\Http\Controllers\LaporanController::totalPeriode($p->format('Y-m-d'), null, 'potongan') }}</td>
                  <td>Rp. {{ number_format(App\Http\Controllers\LaporanController::totalPeriode($p->format('Y-m-d'), null, 'potongan'),0,',','.') }}</td>
                  <td style="display: none;">{{ $affiliasi += App\Http\Controllers\LaporanController::totalPeriode($p->format('Y-m-d'), null, 'affiliasi') }}</td>
                  <td>Rp. {{ number_format(App\Http\Controllers\LaporanController::totalPeriode($p->format('Y-m-d'), null, 'affiliasi'),0,',','.') }}</td>
                  <td>Kas Masuk</td>
                  <td>
                    Rp. {{ 
                      number_format($laba += 
                      (App\Http\Controllers\LaporanController::totalPeriode($p->format('Y-m-d'), null, 'omset') - App\Http\Controllers\LaporanController::totalPeriode($p->format('Y-m-d'), null, 'hpp') - App\Http\Controllers\LaporanController::totalPeriode($p->format('Y-m-d'), null, 'affiliasi')),0,',','.')
                    }}
                  </td>
                  <td>
                    @foreach($user as $u)
                      {{ $u->nama }} <br>
                    @endforeach
                  </td>
                </tr>
                @foreach(App\Http\Controllers\LaporanKasController::kasDate($p->format('Y-m-d')) as $k)
                <tr>
                  <td>{{ $no += 1 }}</td>
                  <td></td>
                  <td>{{ $k->jenis }}</td>
                  <td style="display: none">
                    @if($k->tipe == 1)
                    {{ $pendapatan += $k->total }}
                    @elseif($k->tipe == 2)
                    {{ $pendapatan -= $k->total }}
                    @elseif($k->tipe == 3)
                    {{ $pendapatan += $k->total }}
                    @elseif($k->tipe == 4)
                    {{ $pendapatan -= $k->total }}
                    @endif
                  </td>
                  <td>Rp. {{ number_format($k->total,0,',','.') }}</td>
                  <td>Rp. 0</td>
                  <td>Rp. 0</td>
                  <td>Rp. 0</td>
                  <td>
                    @if($k->tipe == 1)
                      Kas Masuk
                    @elseif($k->tipe == 2)
                      Kas Keluar
                    @elseif($k->tipe == 3)
                      Kas Mutasi Masuk
                    @elseif($k->tipe == 4)
                      Kas Mutasi Keluar
                    @endif
                  </td>
                  <td>Rp.
                    @if($k->tipe == 1)
                      {{ number_format($laba += $k->total,0,',','.') }}
                    @elseif($k->tipe == 2)
                      {{ number_format($laba -= $k->total,0,',','.') }}
                    @elseif($k->tipe == 3)
                      {{ number_format($laba += $k->total,0,',','.') }}
                    @elseif($k->tipe == 4)
                      {{ number_format($laba -= $k->total,0,',','.') }}
                    @endif
                  </td>
                  <td>
                    @foreach($user as $u)
                      {{ $u->nama }} <br>
                    @endforeach
                  </td>
                </tr>
                @endforeach
              @endforeach
              <tr>
                <td></td>
                <td>Total</td>
                <td></td>
                <td>Rp. {{ number_format($pendapatan,0,',','.') }}</td>
                <td>Rp. {{ number_format($hpp,0,',','.') }}</td>
                <td>Rp. {{ number_format($potongan,0,',','.') }}</td>
                <td>Rp. {{ number_format($affiliasi,0,',','.') }}</td>
                <td></td>
                <td>Rp. {{ number_format($laba,0,',','.') }}</td>
                <td></td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>

<div class="modal" id="myModalPeriode" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Download Laporan Per Periode</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="/cashier/laporan/back-office/print-periode">
      <div class="modal-body">
        <p>
        <label for="to">Download Laporan Dari Tanggal</label>
        <input type="date" name="start" class="form-control">
        </p>
        <p>
        <label for="to">Download Laporan Sampai Tanggal</label>
        <input type="date" name="end" class="form-control">
        </p>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Download Laporan</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
      </form>
    </div>
  </div>
</div>

<script type="text/javascript" src="/js/laporan_jualan.js"></script>
@endsection
