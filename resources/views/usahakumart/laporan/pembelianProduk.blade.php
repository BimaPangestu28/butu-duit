@extends('usahakumart/app')

@section('content')
<!-- Breadcrumb -->
<ol class="breadcrumb">
<li class="breadcrumb-item">Home</li>
<li class="breadcrumb-item">Admin</li>
<li class="breadcrumb-item active">Laporan Pembelian Produk</li>

<!-- Breadcrumb Menu-->

</ol>

<div class="container-fluid">

  <div class="card">
    <div class="card-body">
      <div class="row">
        <div class="col-sm-12">
          <h4 class="card-title mb-0">Laporan Pembelian Produk</h4>
          <div class="small text-muted">{{date('d M Y')}}</div><br>
          
          <a href="/cashier/laporan/pembelian-produk/tambah" class="btn btn-primary btn-sm">Buat Laporan Pembelian Produk</a>
          <button data-toggle="modal" data-target="#modalPrintSupplier" class="btn btn-warning btn-sm">Download Laporan Supplier</button>
          <button data-toggle="modal" data-target="#modalPrintSupplierDaftar" class="btn btn-success btn-sm">Download Laporan Daftar Supplier</button>

          <!-- <a href="/cashier/laporan/pembelian-produk/print?date=bulanan" class="btn btn-warning btn-sm">Download Laporan Bulan Ini</a> -->

          <hr>

          <form method="GET" action="/cashier/laporan/pembelian-produk">
            <input type="text" name="q" placeholder="Masukkan nama supplier" class="form-control"><br>
            <button type="submit" class="btn btn-primary form-control">Cari Berdasarkan Supplier</button>
          </form>

        </div>
        <!--/.col-->
      </div>
      @if(session()->has('alert'))
      <div class="row">
      	<div class="alert alert-danger col-sm-12" style="margin-bottom: 0; margin-top: 25px;">
      			<p>{{ session()->get('alert') }}</p>
      	</div>
      </div>
      @endif
    </div>
  </div>
  <!--/.card-->

  <div id="accordion">
    <div class="card">
      <div class="card-header" id="headingOne">
        <h5 class="mb-0">
          <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
            Data berdasarkan supplier
          </button>
        </h5>
      </div>
  
      <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
        <div class="card-body">
          <div class="panel-body">
            <table class="table">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Nama Supplier</th>
                  <th>Jumlah Item</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($dataSup as $no=>$view)
                  <tr>
                    <td>{{$no+1}}</td>
                     <td><a href="/cashier/laporan/pembelian-produk/supplier?supplier={{$view['supplier']}}">{{$view['supplier']}}</a></td>
                    <td>{{ App\Http\Controllers\LaporanController::countItem($view->supplier, 'product') }}</td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
    <div class="card">
      <div class="card-header" id="headingTwo">
        <h5 class="mb-0">
          <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
            Data berdasarkan no transaksi
          </button>
        </h5>
      </div>
      <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
        <div class="card-body">
          
          <div class="panel-body">
            <table class="table">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Nama Supplier</th>
                  <th>Jumlah Faktur</th>
                </tr>
              </thead>
              <tbody>
              @foreach ($dataSup as $no=>$view)
                <tr>
                  <td>{{$no+1}}</td>
                  <td><a href="/cashier/laporan/pembelian-produk/supplier-transaksi?supplier={{$view['supplier']}}">{{$view['supplier']}}</a></td>
                  <td>{{ App\Http\Controllers\LaporanController::countItem($view->supplier, 'faktur') }}</td>
                </tr>
              @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
    <div class="card">
      <div class="card-header" id="headingThree">
        <h5 class="mb-0">
          <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
            Data Pembelian Produk Tempo
          </button>
        </h5>
      </div>
      <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
        <div class="card-body">
          <div class="panel-body">
            <table class="table">
              <thead>
                <tr>
                  <th>#</th>
                  <th>No Transaksi</th>
                  <th>Nama Supplier</th>
                  <th>Total Pembayaran</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody>
              @foreach ($dataTemp as $no=>$view)
                <tr>
                  <td>{{$no+1}}</td>
                  <td><a href="/cashier/laporan/pembelian-produk/no-transaksi?transaksi={{$view['no_transaksi']}}&supplier={{\App\Http\Controllers\LaporanController::noSupplier($view->no_transaksi)}}">{{$view['no_transaksi']}}</a></td>
                  <td> {{\App\Http\Controllers\LaporanController::noSupplier($view->no_transaksi)}} </td>
                  <td>Rp. {{ number_format(\App\Http\Controllers\LaporanController::noSupplierTotal($view->no_transaksi),0,',','.') }} </td>
                  <td>
                    <form method="POST" action="/cashier/laporan/pembelian-produk/update/temp/{{ $view->no_transaksi }} ">
                        {{csrf_field()}}
                        <button type="submit" class="btn btn-success">Bayar Tunai</button>
                    </form>
                  </td>
                </tr>
              @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>

</div>

</div>

<!-- Modal Print -->
<div class="modal" id="modalPrintSupplier" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Download Laporan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="/cashier/laporan/pembelian-produk/print/supplier">
      <div class="modal-body">
        <p>
        <label for="from">Dari :</label>
        <input type="date" name="from" class="form-control">
        <label for="to">Sampai :</label>
        <input type="date" name="to" class="form-control">
        </p>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Download Laporan</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
      </form>
    </div>
  </div>
</div>

<!-- Modal Print -->
<div class="modal" id="modalPrintSupplierDaftar" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Download Laporan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="/cashier/laporan/pembelian-produk/print">
      <div class="modal-body">
        <p>
        <input type="hidden" name="supplier_daftar" value="daftar_supplier">
        <label for="from">Dari :</label>
        <input type="date" name="from" class="form-control">
        <label for="to">Sampai :</label>
        <input type="date" name="to" class="form-control">
        </p>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Download Laporan</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
      </form>
    </div>
  </div>
</div>

<script type="text/javascript" src="/js/gudang.js"></script>
@endsection
