@extends('usahakumart/app')

@section('content')
<!-- Breadcrumb -->
<ol class="breadcrumb">
<li class="breadcrumb-item">Home</li>
<li class="breadcrumb-item">Admin</li>
<li class="breadcrumb-item active">Laporan Pembelian Produk Supplier {{$supplier}}</li>

<!-- Breadcrumb Menu-->

</ol>

<div class="container-fluid">

  <div class="card">
    <div class="card-body">
      <div class="row">
        <div class="col-sm-10">
          <h4 class="card-title mb-0">Laporan Pembelian Produk Supplier {{$supplier}}</h4>
          <div class="small text-muted">{{date('d M Y')}}</div><br>
          <h5>Total Pembayaran : Rp. {{ number_format($dataTotal,0,',','.') }}</h5>
          <h5>Total Pajak : Rp. {{ number_format($dataPajak,0,',','.') }}</h5>
          <h5>Total Diskon : Rp. {{ number_format($dataDiskon,0,',','.') }}</h5>
        </div>
        <!--/.col-->
      </div>
      @if(session()->has('alert'))
      <div class="row">
      	<div class="alert alert-danger col-sm-12" style="margin-bottom: 0; margin-top: 25px;">
      			<p>{{ session()->get('alert') }}</p>
      	</div>
      </div>
      @endif
    </div>
  </div>
  <!--/.card-->

 <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          Usahaku Toserba - Toko Offline
        </div>
        <div class="card-body">
          <!-- <form method="GET" action="/cashier/laporan/pembelian-produk/search" >
              <input class="form-control" type="text" placeholder="Isi dengan Nama, Supplier atau Kode Barang" name="q2"></input><br>
              <button type="submit" class="btn btn-primary form-control">Search</button>
          </form> -->
          <br>
          <table class="table table-responsive-sm table-hover table-outline mb-0">
            <thead class="thead-light">
              <tr>
                <th>#</th>
                <th class="text-center">Nomer Transaksi</th>
                <th class="text-center">Jumlah Item</th>
                <th class="text-center">Tanggal</th>
              </tr>
            </thead>
            <tbody>
              @foreach ($dataNo as $no=>$view)
                <tr>
                  <td>{{$no+1}}</td>
                  @if($view['no_transaksi'] != null)
                  <td><a href="/cashier/laporan/pembelian-produk/no-transaksi?transaksi={{$view['no_transaksi']}}&supplier={{ $supplier }}">{{$view['no_transaksi']}}</a></td>
                  @else
                  <td><a href="/cashier/laporan/pembelian-produk/no-transaksi?transaksi=null&supplier={{ $supplier }}">Tanpa no transaksi</a></td>
                  @endif
                  <td class="text-center">{{$view['total']}}</td>
                  <td class="text-center">{{ App\Http\Controllers\LaporanController::dateTrans($view->no_transaksi) }}</td>
                </tr>
              @endforeach
            </tbody>
          </table>
          <br>
        </div>
      </div>
    </div>
  </div>

</div>

</div>
<script type="text/javascript" src="/js/gudang.js"></script>
@endsection
