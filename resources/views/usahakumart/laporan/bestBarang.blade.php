@extends('usahakumart/app')

@section('content')
<!-- Breadcrumb -->
<ol class="breadcrumb">
<li class="breadcrumb-item">Home</li>
<li class="breadcrumb-item">Admin</li>
<li class="breadcrumb-item active">Laporan Penjualan</li>

<!-- Breadcrumb Menu-->

</ol>

<div class="container-fluid">

  <div class="card">
    <div class="card-body">
      <div class="row">
        <div class="col-sm-10">
          <h4 class="card-title mb-0">Total Penjualan Produk</h4>
          <div class="small text-muted">{{date('d M Y')}}</div>
        </div>
        <!--/.col-->
      </div>
      @if(session()->has('alert'))
      <div class="row">
      	<div class="alert alert-danger col-sm-12" style="margin-bottom: 0; margin-top: 25px;">
      			<p>{{ session()->get('alert') }}</p>
      	</div>
      </div>
      @endif
    </div>
  </div>
  <!--/.card-->

 <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          Usahaku Toserba - Toko Offline
        </div>

        @if(app('request')->get('id'))
        <div class="card-body">
          {{$chart->name}}
          <div class="row">
            <div class="col-8">
              <canvas id="chartPopuler"></canvas>
            </div>
            <div class="col-4">
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto dignissimos quidem expedita maiores sequi debitis quos. Quos consequuntur nesciunt, omnis vitae labore cumque, necessitatibus doloremque tempora velit harum modi suscipit.
            </div>
          </div>
        </div>
        @else
        @endif
        <div class="card-body">
        <h5>Cari Barang</h5>
          <form method="GET" action="/cashier/laporan/total-barang-terjual" id="pilih_tahun">
              <input type="text" name="search" class="form-control">
          </form>
          <br>
          <table class="table table-responsive-sm table-hover table-outline mb-0">
                <thead class="thead-light">
                <tr>
                    <th>#</th>
                    <th>Nama Produk</th>
                    <th>Terjual Hari Ini</th>
                    <th>Terjual Bulan Ini</th>
                    <th>Terjual Tahun Ini</th>
                    <th>Jumlah Terjual</th>
                    <th>Stok Barang</th>
                    <th>Jumlah Total Harga Barang</th>
                </tr>
                </thead>
                <tbody>
                @foreach($datas as $no=>$t)
                <tr>
                    <td>{{$no+1}}</td>
                    <td><a href="?id={{ $t->id }}">{{ $t->name }}</a></td>
                    <td class="text-center">{{ \App\Http\Controllers\CashierController::today($t->id) }}</td>
                    <td class="text-center">{{ \App\Http\Controllers\CashierController::month($t->id) }}</td>
                    <td class="text-center">{{ \App\Http\Controllers\CashierController::year($t->id) }}</td>
                    <td class="text-center">{{ $t->terjual }}</td>
                    <td class="text-center">{{ $t->stok }}</td>
                    <td class="text-center">Rp. {{ number_format($t->stok * $t->harga_jual,0,',','.') }}</td>
                </tr>
                @endforeach
                </tbody>
            </table>
          <br>
        </div>
      </div>
    </div>
  </div>

</div>

</div>


<script type="text/javascript" src="/js/gudang.js"></script>
<script src="/chart/dist/Chart.min.js"></script>
<script>
    $('#pilih_tahun select').on('change', function(){
        $(this).closest('form').submit();
    });


@if(app('request')->get('id'))

    var ctx = document.getElementById('chartPopuler').getContext('2d');
    var chart = new Chart(ctx, {
        // The type of chart we want to create
        type: 'bar',

        // The data for our dataset
        data: {
            labels: ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"],
            datasets: [{
                label: "Jumlah Produk Terjual",
                backgroundColor: 'rgb(255, 99, 132)',
                borderColor: 'rgb(255, 99, 132)',
                data: [
                  @foreach($produkArr as $count)
                  {{$count}},
                  @endforeach
                ],
            }]
        },

        // Configuration options go here
        options: {}
    });

@endif

</script>

@endsection
