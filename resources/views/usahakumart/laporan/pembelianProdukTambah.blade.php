@extends('usahakumart/app')

@section('content')
<!-- Breadcrumb -->
<ol class="breadcrumb">
<li class="breadcrumb-item">Home</li>
<li class="breadcrumb-item">Admin</li>
<li class="breadcrumb-item active">Tambah Laporan Pembelian Barang</li>

<!-- Breadcrumb Menu-->

</ol>

<div class="container-fluid">

  <div class="card">
    <div class="card-body">
      <div class="row">
        <div class="col-sm-5">
          <h4 class="card-title mb-0">Tambah Laporan Pembelian Barang</h4>
          <div class="small text-muted">{{date('d M Y')}}</div>
          <br>
          <a href="/cashier/laporan/pembelian-produk" class="btn btn-primary btn-sm">Lihat daftar laporan pembelian</a>
        </div>
        <!--/.col-->
        <div class="col-sm-7 d-none d-md-block">
          <button type="button" class="btn btn-primary float-right"><i class="icon-cloud-download"></i></button>
          <div class="btn-toolbar float-right" role="toolbar" aria-label="Toolbar with button groups">
          </div>
        </div>
        <!--/.col-->
      </div>
       <br>
      <h6>Upload dari excel</h6>
      <form method="POST" enctype="multipart/form-data" action="/cashier/laporan/pembelian-produk/upload">
        {{ csrf_field() }}

        <input type="file" name="file" class="form-control"><br>
        <button class="btn btn-primary btn-sm" type="submit">Upload File Excel</button>
      </form>
      
      @if($errors->count() > 0 || session()->has('alert'))
      <div class="row">
      	<div class="alert alert-danger col-sm-12" style="margin-bottom: 0; margin-top: 25px;">
      		@if($errors->count() > 0)
      			@foreach($errors->all() as $error)
      			<p>{{ $error }}</p>
      			@endforeach
      		@else
      			<p>{{ session()->get('alert') }}</p>
      		@endif
      	</div>
      </div>
      @endif
    </div>
  </div>
  <!--/.card-->


  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          Usahaku Toserba - Toko Offline
        </div>
        <div class="card-body">
          <form method="POST" id="form" action="/cashier/laporan/pembelian-produk/store" enctype="multipart/form-data">
          	{{ csrf_field() }}

            <div id="keAda" style="display:none;">
              <i class="fa fa-circle text-success"></i> <span class="text-muted">Barang ada di database, silahkan masukan data yang diperlukan</span>
              <br>
              <br>
            </div>
          	<div class="form-group">
              <strong>Barcode Barang</strong><br>
              <small class="text-muted">Tekan enter pada keyboard apabila tidak menggunakan alat barcode untuk melihat apakah barang sudah pernah di input</small>
          		<input type="number" name="barcode" class="form-control" style="margin-top: 10px;" value="{{ old('barcode') }}" id="barcode_barang">
          	</div>
            
            <div class="form-group">
          		<strong>No Transaksi Faktur</strong><br>
          		<input type="text" name="no_transaksi" class="form-control" style="margin-top: 10px;" placeholder="*) Isi Sesuai dengan no transaksi di nota" value="{{ old('no_transaksi') }}">
          	</div>  

            <div class="form-group">
              <strong>Tanggal Faktur</strong><br>
              <input type="date" name="created_at" class="form-control" style="margin-top: 10px;" value="{{ old('created_at') }}">
            </div>  

            <div class="form-group">
          		<strong>Nama Barang</strong><br>
          		<input type="text" name="name" class="form-control" style="margin-top: 10px;" placeholder="*) Sesuaikan Dengan Nama Produk" value="{{ old('name') }}" id="produk" list="barangList">
          	</div>

            <div class="form-group">
              <strong>Category Barang</strong><br>
              <select class="form-control" name="id_category" id="category">
                @foreach($cat as $a)
                  <option value="{{ $a->id }}">{{ $a->category }}</option>
                @endforeach
              </select>
            </div>
            
            <div class="form-group">
              <strong>Nama Supplier</strong><br>
              <select name="supplier" class="form-control" id="supplier">
                @foreach($supplier as $s)
                  <option value="{{ $s->name }}">{{ $s->name }}</option>
                @endforeach
              </select>
            </div> 
            
            <div class="form-group">
          		<strong>Jumlah Barang dari Supplier</strong><br>
          		<input type="number" name="jumlah_barang" class="form-control" style="margin-top: 10px;" placeholder="*) Isi Dengan Angka, Jangan Pake Symbol Atau Huruf" value="{{ old('jumlah_barang') }}" id="jumlah_barang">
          	</div>

            <div class="form-group">
              <strong>Satuan</strong><br>
              <select name="tipe_jumlah" class="form-control" id="tipe_jumlah">
                <!-- <option value="lusin">Lusin</option> -->
                <!-- <option value="gross">Gross</option> -->
                <option value="banded">Banded</option>
                <!-- <option value="rim">Rim</option> -->
                <option value="satuan">Pcs</option>
                <option value="dus">dus</option>
              </select>
            </div>

          	<div class="form-group">
          		<strong>Pola Pembayaran</strong><br>
          		<select class="form-control" name="tipe" id="tipe">
                <!-- <option value="Utang">Utang</option>   -->
                <option value="Tunai">Tunai</option>  
                <option value="Kredit">Tempo</option>  
                <option value="Titipan">Titipan</option>  
              </select>
          	</div>

          	<div class="form-group" id="tanggal_pembayaran">
          		<strong>Tanggal Pembayaran</strong><br>
          		<input type="date" name="tanggal_pembayaran" class="form-control" style="margin-top: 10px;" value="{{ old('tanggal_pembayaran') }}">
          	</div>

            <div class="form-group">
              <strong>Total Harga dari Supplier</strong><br>
              <input type="number" name="total_harga" class="form-control" style="margin-top: 10px;" placeholder="*) Isi Dengan Angka, Jangan Pake Symbol Atau Huruf" value="{{ old('total_harga') }}" id="total_harga">
            </div>

            <div class="form-group">
              <strong>Diskon dari Supplier</strong><br>
              <input type="number" name="diskon" class="form-control" style="margin-top: 10px;" placeholder="*) Isi Dengan Angka, Jangan Pake Symbol Atau Huruf" value="{{ old('diskon') }}">
            </div>

            <div class="form-group">
              <strong>Pajak dari Supplier</strong><br>
              <input type="number" name="pajak" class="form-control" style="margin-top: 10px;" placeholder="*) Isi Dengan Angka, Jangan Pake Symbol Atau Huruf" value="{{ old('pajak') }}">
            </div>


            <hr style="height: 2px;background: lightseagreen;margin: 55px 0;">

          	<div class="form-group">
          		<strong>Harga Modal Satuan</strong><br>
          		<input type="number" name="price" class="form-control" style="margin-top: 10px;" placeholder="*) Isi Dengan Angka, Jangan Pake Symbol Atau Huruf" value="{{ old('price') }}" id="modal">
          	</div>

            <div class="form-group">
              <strong>Presentase Keuntungan</strong><br>
              <input type="text" class="form-control" style="margin-top: 10px;" placeholder="*) Isi Dengan Angka, Jangan Pake Symbol Atau Huruf" value="{{ old('price') }}" id="presentase">
            </div>

            <div class="form-group">
              <strong>Harga Jual</strong><br>
              <input type="number" name="harga_jual" class="form-control" style="margin-top: 10px;" placeholder="*) Isi Dengan Angka, Jangan Pake Symbol Atau Huruf" value="{{ old('harga_jual') }}" id="jual">
            </div>

            <div class="form-group">
              <strong>Keuntungan</strong><br>
              <input type="text" readonly  name="keuntungan" class="form-control" style="margin-top: 10px;" id="keuntungan">
            </div>

          	<div class="form-group">
          		<strong>Affiliasi Barang</strong><br>
          		<input type="number" name="affiliasi" class="form-control" style="margin-top: 10px;" placeholder="*) Isi Dengan Angka, Jangan Pake Symbol Atau Huruf" value="{{ old('affiliasi') }}" id="affiliasi">
          	</div>

            <div class="form-group" id="stok">
              <strong>Persediaan Barang</strong><br>
              <input type="number" name="stok" class="form-control" style="margin-top: 10px;" placeholder="*) Isi Dengan Angka, Jangan Pake Symbol Atau Huruf" value="{{ old('stok') }}">
            </div>

            <div class="form-group">
              <strong>Penempatan Nomor Rak</strong><br>
              <select class="form-control" name="rak" id="rak">
                @for($i=1; $i <= 10; $i++)
                <option value="{{ $i }}">{{ $i }}</option>
                @endfor
              </select>
            </div>

            <!-- <div class="form-group">
              <strong>Jumlah Pada Rak</strong><br>
              <input type="number" name="jumlah_pada_rak" class="form-control" style="margin-top: 10px;" placeholder="*) Isi Dengan Angka, Jangan Pake Symbol Atau Huruf" value="{{ old('jumlah_pada_rak') }}">
            </div> -->

            <div class="form-group">
              <strong>Kondisi Barang</strong><br>
              <select class="form-control" name="kondisi">
                <option value="Baru">Baru</option>
                <option value="Bekas">Bekas</option>
              </select>
            </div>

            <div class="form-group" id="gambar">
              <strong>Gambar Barang</strong><br>
              <input type="file" name="gambare" class="form-control" style="margin-top: 10px;">
            </div>

            <div class="form-group">
              <strong>Keterangan Barang</strong><br>
              <textarea name="deskripsi" class="form-control"></textarea>
            </div>       

          	<br><br>

          	<input id="submit_button" type="submit" class="btn btn-primary form-control" value="Tambah Laporan Pembelian Barang">
          </form>
        </div>
      </div>
    </div>
    <!--/.col-->
  </div>
  <!--/.row-->
</div>

</div>

<script type="text/javascript" src="/js/laporan.js"></script>

<script type="text/javascript">
  $(document).ready(function(){
    setInterval(function() {
      let modal     = $("#modal").val(),
          jual      = $("#jual").val();

      if (modal != '' && jual != '') {
        $("#keuntungan").val(jual - modal);
      }
    }, 1000);
  })
</script>

@endsection
