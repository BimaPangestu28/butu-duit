@extends('usahakumart/app')

@section('content')
<!-- Breadcrumb -->
<ol class="breadcrumb">
<li class="breadcrumb-item">Home</li>
<li class="breadcrumb-item">Admin</li>
<li class="breadcrumb-item active">Laporan Kas</li>

<!-- Breadcrumb Menu-->

</ol>

<div class="container-fluid">

  <div class="card">
    <div class="card-body">
      <div class="row">
        <div class="col-sm-10">
          <h4 class="card-title mb-0">Laporan Kas Perusahaan {{Auth::user()->name}}</h4>
          <div class="small text-muted">{{date('d M Y')}}</div><br>
          
          <a href="/cashier/laporan/kas/tambah" class="btn btn-primary btn-sm">Buat Laporan Kas Perusahaan</a>
          <a button data-toggle="modal" data-target="#modalMasuk" class="btn btn-success btn-sm text-white">Download Laporan Pemasukan</a>
          <a button data-toggle="modal" data-target="#modalKeluar" class="btn btn-danger btn-sm text-white">Download Laporan Pengeluaran</a>

        </div>
        <!--/.col-->
      </div>
      <br>
      <h6>Lihat Laporan Kas</h6>
      <div class="input-group">
        <form action="/cashier/laporan/kas">
            <input type="hidden" name="masuk" value="masuk">
            <button class="btn btn-success btn-sm">Lihat Kas Masuk</button>
        </form>
        <form action="/cashier/laporan/kas">
            <input type="hidden" name="keluar" value="keluar">
            <button class="btn btn-danger btn-sm">Lihat Kas Keluar</button>
        </form>
      </div>
    </div>
  </div>
  <!--/.card-->

  <div class="card">
    <div class="card-body">
        <div class="row">
            <div class="container">
                <div>
                Mutasi Kas Masuk : <strong>Rp. {{ number_format($totalMasuk,0,',','.') }} </strong>
                </div>
                <hr>
                <div>
                Mutasi Kas Keluar : <strong>Rp. {{ number_format($totalKeluar,0,',','.') }} </strong>
                </div>
                <hr>
                <div>
                Saldo Kas : <strong>Rp. {{ number_format($totalAkhir,0,',','.') }} </strong>
                </div>
            </div>
        </div>
    </div>
      
  <div class="row">
    @if(app('request')->input('masuk'))
    <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <b>Kas Masuk</b>
              </div>
              <div class="card-body">
                
                <form action="/cashier/laporan/kas/search" class="input-group">
                    <input type="hidden" name="masuk" value="masuk">

                    <input type="date" name="from" class="form-control">
                    <input type="date" name="to" class="form-control">
                    <button class="form-control btn btn-primary">Cari data</button>
                </form>
                <br>
                <table class="table table-responsive-sm table-hover table-outline mb-0">
                  <thead class="thead-light">
                    <tr>
                      <th>#</th>
                      <th class="text-center">No Transaksi</th>
                      <th class="text-center">Jenis Kas</th>
                      <th class="text-center">Ke Kas</th>
                      <th class="text-center">keterangan</th>
                      <th class="text-center">Total</th>
                      <th class="text-center">Tanggal</th>
                      <th class="text-center">Option</th>
                    </tr>
                  </thead>
                  
                  <tbody>
                    @foreach($kasMasuk->slice(0, 20) as $no=>$view)
                      <tr>
                        <td>{{$no+1}}</td>
                        <td>{{$view['no_transaksi']}}</td>
                        <td>{{$view['jenis']}}</td>
                        <td>{{$view['ke_kas']}}</td>
                        <td>{{$view['keterangan']}}</td>
                        <td>{{number_format($view->total,0,',','.')}}</td>
                        <td>{{$view['created_at']}}</td>
                        <td>
                          <a href="/cashier/laporan/kas/edit/{{$view['id']}}" class="btn btn-warning">Edit</a>
                          <form action="/cashier/laporan/kas/delete/{{$view['id']}}" method="post">
                            {{csrf_field()}}
                            <button type="submit" class="btn btn-danger">Delete</button>
                          </form>
                        </td>
                      </tr>
                    @endforeach
                    
                      <tr>
                        <td>#</td>
                        <td>KAS DARI PENJUALAN</td>
                        <td>Kas Masuk</td>
                        <td>Toko</td>
                        <td>Kas dari penjualan toko</td>
                        <td>{{number_format($totalJualan,0,',','.')}}</td>
                        <td>-</td>
                        <td>-</td>
                      </tr>

                  </tbody>
                </table>
                <br>
              </div>
            </div>
          </div>
          <!--/.col-->
        </div>
    @elseif(app('request')->input('keluar'))  
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <b>Kas Keluar</b>
        </div>
        <div class="card-body">
            <form action="/cashier/laporan/kas/search" class="input-group">
                <input type="hidden" name="keluar" value="keluar">

                <input type="date" name="from" class="form-control">
                <input type="date" name="to" class="form-control">
                <button class="form-control btn btn-primary">Cari data</button>
            </form>
          <br>
          <table class="table table-responsive-sm table-hover table-outline mb-0">
            <thead class="thead-light">
              <tr>
                <th>#</th>
                <th class="text-center">No Transaksi</th>
                <th class="text-center">Jenis Kas</th>
                <th class="text-center">Mutasi</th>
                <th class="text-center">keterangan</th>
                <th class="text-center">Total</th>
                <th class="text-center">Tanggal</th>
                <th class="text-center">Option</th>
              </tr>
            </thead>
            
            <tbody>
              @foreach($kasKeluar->slice(0, 20) as $no=>$view)
                <tr>
                  <td>{{$no+1}}</td>
                  <td>{{$view['no_transaksi']}}</td>
                  <td>{{$view['jenis']}}</td>
                  <td>{{$view['ke_kas']}}</td>
                  <td>{{$view['keterangan']}}</td>
                  <td>Rp. {{number_format($view->total,0,',','.')}}</td>
                  <td>{{$view['created_at']}}</td>
                  <td>
                    <a href="/cashier/laporan/kas/edit/{{$view['id']}}" class="btn btn-warning">Edit</a>
                    <form action="/cashier/laporan/kas/delete/{{$view['id']}}" method="post">
                      {{csrf_field()}}
                      <button type="submit" class="btn btn-danger">Delete</button>
                    </form>
                  </td>
                </tr>
              @endforeach

                <tr>
                  <td>#</td>
                  <td>KAS DARI PEMBELIAN BARANG</td>
                  <td>Kas Keluar</td>
                  <td>Toko</td>
                  <td>Kas dari pengeluaran toko</td>
                  <td>{{number_format($totalSupplierHarga,0,',','.')}}</td>
                  <td>-</td>
                  <td>-</td>
                </tr>

            </tbody>
          </table>
          <br>
        </div>
      </div>
    </div>
    <!--/.col-->
  </div>
    @endif
  </div>

</div>


</div>

<!-- Modal Print -->
<div class="modal" id="modalMasuk" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Download Kas Masuk</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="/cashier/laporan/kas/print">
        <div class="modal-body">
          <p>
          <input type="hidden" name="data" value="masuk">
          <label for="from">Dari :</label>
          <input type="date" name="from" class="form-control">
          <label for="to">Sampai :</label>
          <input type="date" name="to" class="form-control">
          </p>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary">Download Laporan</button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
        </form>
      </div>
    </div>
  </div>
<div class="modal" id="modalKeluar" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Download Kas Keluar</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="/cashier/laporan/kas/print">
        <div class="modal-body">
          <p>
          <input type="hidden" name="data" value="keluar">
          <label for="from">Dari :</label>
          <input type="date" name="from" class="form-control">
          <label for="to">Sampai :</label>
          <input type="date" name="to" class="form-control">
          </p>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary">Download Laporan</button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
        </form>
      </div>
    </div>
  </div>
<script type="text/javascript" src="/js/gudang.js"></script>
@endsection
