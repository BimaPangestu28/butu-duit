@extends('usahakumart/app')

@section('content')
<!-- Breadcrumb -->
<ol class="breadcrumb">
<li class="breadcrumb-item">Home</li>
<li class="breadcrumb-item">Admin</li>
<li class="breadcrumb-item active">Laporan Penjualan</li>

<!-- Breadcrumb Menu-->

</ol>

<div class="container-fluid">

  <div class="card">
    <div class="card-body">
      <div class="row">
        <div class="col-sm-12">
          <h4 class="card-title mb-0">Laporan Penjualan Produk</h4>
          <div class="small text-muted">{{date('d M Y')}}</div>
          @if(session('user-toko')->level == 1)
          <br>
          <button data-toggle="modal" data-target="#myModal" class="btn btn-warning">Download Laporan</button>

          <button data-toggle="modal" data-target="#myModalPeriode" class="btn btn-danger">Download Laporan Periode</button>
          @endif
          <br>
          <hr>
          <br>
          <form method="GET" action="/cashier/laporan/penjualan/search">
            <select name="kasir" class="form-control">
              <option value="">-- Pilih Nama Kasir --</option>
              @foreach($kasir as $k)
              <option value="{{ $k->id }}">{{ $k->nama }}</option>
              @endforeach
            </select><br>
            <input type="hidden" name="date" value="@if(app('request')->input('date')) {{ app('request')->input('date') }} @else {{ date('Y-m-d') }} @endif">
            <button type="submit" value="sortir" class="btn btn-primary form-control">Sortir Laporan Penjualan Berdasarkan Nama Kasir</button><!-- <br><br>
            <button type="submit" value="print" class="btn btn-danger form-control">Download Laporan Penjualan Berdasarkan Nama Kasir</button> -->
          </form>
        </div>
        <!--/.col-->
      </div>
      @if(session()->has('alert'))
      <div class="row">
      	<div class="alert alert-danger col-sm-12" style="margin-bottom: 0; margin-top: 25px;">
      			<p>{{ session()->get('alert') }}</p>
      	</div>
      </div>
      @endif
    </div>
  </div>
  <!--/.card-->

 <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          Usahaku Toserba - Toko Offline
        </div>
        <div class="card-body">
          <form method="GET" action="/cashier/laporan/penjualan/search" >
              <label for="from">Cari Pada Tanggal</label>
              <input type="date" class="form-control" name="date"><br>
              <button type="submit" class="btn btn-primary form-control">Search</button>
          </form>
          <br>
          <table class="table table-responsive-sm table-hover table-outline mb-0">
            <thead class="thead-light">
              <tr>
                <th>#</th>
                <th class="text-center">Tanggal</th>
                <th class="text-center">Pembeli</th>
                <th class="text-center">Nama Cashier</th>
                <th class="text-center">No Transaksi</th>
                <th class="text-center">Total Harga</th>
                <th class="text-center">Potongan</th>
                <th class="text-center">Total Pembayaran</th>
                <th class="text-center">Jumlah Dibayar</th>
                <th class="text-center">Kembalian</th>
                <th class="text-center">Jenis Pembayaran</th>
                @if(session('user-toko')->level == 1)
                <th class="text-center">Option</th>
                @endif
              </tr>
            </thead>
            <tbody id="body-data">
            @foreach ($datas as $no=>$view)
              <tr data-toggle="modal" data-id="{{ $view->id_order }}" class="tr" data-target="#myModal{{$view->id_order}}">
                <td class="text-center">{{$no+1}}</td>
                <td class="text-center">{{date('d F Y', strtotime($view['created_at']))}} {{$view['created_at']->hour}}:{{$view['created_at']->minute}}</td>
                <td class="text-center">
                  @if($view->id_member == null)
                  Guest
                  @else
                  {{$view['nama_member']}}
                  @endif
                </td>
                <td class="text-center">{{ $view->kasir }}</td>
                <td class="text-center">
                  @if($view->no_transaksi != 0)
                    {{ $view->no_transaksi }}
                  @else
                    Tidak Ada Nomer Transaksi
                  @endif
                </td>
                <td class="text-center">Rp. {{ number_format($view['total_pembayaran'] + App\Http\Controllers\LaporanController::totalPotongan($view->id_order),0,',','.') }}</td>
                <td class="text-center">Rp. {{ number_format(App\Http\Controllers\LaporanController::totalPotongan($view->id_order),0,',','.') }}</td>
                <td class="text-center">Rp. {{ number_format($view['total_pembayaran'],0,',','.') }}</td>
                <td class="text-center">Rp. {{ number_format($view['pembayaran'],0,',','.') }}</td>
                <td class="text-center">Rp. {{ number_format($view['kembalian'],0,',','.') }}</td>
                <td class="text-center">{{ $view->type }}</td>
                @if(session('user-toko')->level == 1)
                <th class="text-center">
                  <a href="/cashier/penjualan/{{ $view->id_order }}/cancel" class="btn btn-danger" onclick="return confirm('Apakah anda yakin?')">Cancel</a> <br><br>
                </th>
                @endif
              </tr>
            @endforeach
            </tbody>
          </table>
          <br>

          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header">
                    Laporan Perolehan Kasir Harian
                </div>
                <div class="card-body">
                    @if(app('request')->input('date'))
                    <ul class="list-group list-group-flush" style="font-size:17px;">
                        <li class="list-group-item">Omset <span class="float-right">Rp. {{ number_format(App\Http\Controllers\LaporanController::totalHari('pembayaran', app('request')->input('date'), app('request')->input('kasir')),0,',','.') }}</span></li>
                        <li class="list-group-item">Modal atau Total HPP <span class="float-right">Rp. {{ number_format(App\Http\Controllers\LaporanController::totalHari('hpp', app('request')->input('date'), app('request')->input('kasir')),0,',','.') }}</span></li>
                        <li class="list-group-item">Total Potongan <span class="float-right">Rp. {{ number_format(App\Http\Controllers\LaporanController::totalHari('diskon', app('request')->input('date'), app('request')->input('kasir')),0,',','.') }}</span></li>
                        
                        <li class="list-group-item">Affiliasi <span class="float-right">Rp. {{ number_format(App\Http\Controllers\LaporanController::totalHari('affiliasi', app('request')->input('date'), app('request')->input('kasir')),0,',','.') }}</span></li>
                        <li class="list-group-item">Net Profit <strong class="float-right">Rp. {{ number_format(App\Http\Controllers\LaporanController::totalHari('pendapatan bersih', app('request')->input('date'), app('request')->input('kasir')),0,',','.') }}</strong></li>
                    </ul>
                    @else
                    <ul class="list-group list-group-flush" style="font-size:17px;">
                        <li class="list-group-item">Omset <span class="float-right">Rp. {{ number_format(App\Http\Controllers\LaporanController::totalHari('pembayaran', app('request')->input('kasir')),0,',','.') }}</span></li>
                        <li class="list-group-item">Modal atau Total HPP <span class="float-right">Rp. {{ number_format(App\Http\Controllers\LaporanController::totalHari('hpp', app('request')->input('kasir')),0,',','.') }}</span></li>
                        <li class="list-group-item">Total Potongan <span class="float-right">Rp. {{ number_format(App\Http\Controllers\LaporanController::totalHari('diskon', app('request')->input('kasir')),0,',','.') }}</span></li>
                        <li class="list-group-item">Affiliasi <span class="float-right">Rp. {{ number_format(App\Http\Controllers\LaporanController::totalHari('affiliasi', app('request')->input('kasir')),0,',','.') }}</span></li>
                        <li class="list-group-item">Net Profit <strong class="float-right">Rp. {{ number_format(App\Http\Controllers\LaporanController::totalHari('pendapatan bersih', app('request')->input('kasir')),0,',','.') }}</strong></li>
                    </ul>
                    @endif
                </div>
              </div>
            </div>
            <!--/.col-->
          </div> <br>

          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header">
                    Laporan Perolehan Omset Kasir Harian
                </div>
                <div class="card-body">
                    @foreach($kasir as $k)
                      @if(app('request')->input('date'))
                        @if(App\Http\Controllers\LaporanController::totalHari('pembayaran', app('request')->input('date'), $k->id) > 0)
                        <ul class="list-group list-group-flush" style="font-size:17px;">
                            <li class="list-group-item">{{ $k->nama }} <span class="float-right">Rp. {{ number_format(App\Http\Controllers\LaporanController::totalHari('pembayaran', app('request')->input('date'), $k->id),0,',','.') }}</span></li>
                        </ul>
                        @endif
                      @else
                        @if(App\Http\Controllers\LaporanController::totalHari('pembayaran', $k->id) > 0)
                        <ul class="list-group list-group-flush" style="font-size:17px;">
                            <li class="list-group-item">{{ $k->nama }} <span class="float-right">Rp. {{ number_format(App\Http\Controllers\LaporanController::totalHari('pembayaran', $k->id),0,',','.') }}</span></li>
                        </ul>
                        @endif
                      @endif
                    @endforeach
                </div>
              </div>
            </div>
            <!--/.col-->
          </div>

        </div>
      </div>
    </div>
  </div>

</div>

</div>

@foreach($datas as $view)
  <div class="modal" id="myModal{{$view->id_order}}" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document" style="max-width: 1300px;">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Daftar Barang Belanja</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body" id="modal-{{ $view->id_order }}">
          <table class="table table-responsive-md table-hover table-outline mb-0">
              <thead class="thead-light">
                <tr>
                  <th>#</th>
                  <th class="text-center">Nama</th>
                  <th class="text-center">HPP</th>
                  <th class="text-center">Harga Jual</th>
                  <th class="text-center">Barcode</th>
                  <th class="text-center">Jumlah</th>
                  <th class="text-center">Affiliasi</th>
                  <th class="text-center">Diskon</th>
                  <th class="text-center">Total Pembelian</th>
                  <th class="text-center">Total Potongan</th>
                  <th class="text-center">Total Akhir</th>
                </tr>
              </thead>
              <tbody id="body-data">
                @foreach(App\Http\Controllers\LaporanController::dataList($view->id_order) as $o)
                <tr>
                  <td data-id="{{ $o->created_at }}">#</td>
                  <td class="text-center">{{ $o->name }}</td>
                  <td class="text-center">Rp. {{ number_format($o->price,0,',','.') }}</td>
                  <td class="text-center">Rp. {{ number_format($o->harga_jual,0,',','.') }}</td>
                  <td class="text-center">{{ $o->barcode }}</td>
                  <td class="text-center">{{ App\Http\Controllers\LaporanController::jumlah($o->id_product, $o->order_id) }}</td>
                  <td class="text-center">
                    @if($view->id_member != null)
                    Rp. {{ ($o->affiliasi * (25 / 100)) * App\Http\Controllers\LaporanController::jumlah($o->id_product, $o->order_id) }}
                    @else
                    Rp. 0
                    @endif
                  </td>
                  <td class="text-center">{{ $o->diskon }}%</td>
                  <td class="text-center">Rp. {{ number_format(App\Http\Controllers\LaporanController::jumlah($o->id_product, $o->order_id) * $o->harga_jual,0,',','.') }}</td>
                  <td class="text-center">Rp. {{ number_format(App\Http\Controllers\LaporanController::potongan($o->id_product, $o->order_id),0,',','.') }}</td>
                  <td class="text-center">Rp. {{ number_format((App\Http\Controllers\LaporanController::jumlah($o->id_product, $o->order_id) * $o->harga_jual) - App\Http\Controllers\LaporanController::potongan($o->id_product, $o->order_id),0,',','.') }}</td>
                </tr>
                @endforeach
              </tbody>
            </table>
            <br>
            <!-- <h6>Total HPP : Rp. {{ number_format(App\Http\Controllers\LaporanController::total($view->id_order, 'hpp', app('request')->input('kasir')),0,',','.') }}</h6> -->
            <h6>Total Pembayaran : Rp. {{ number_format(App\Http\Controllers\LaporanController::total($view->id_order, 'pembayaran', app('request')->input('kasir')),0,',','.') }}</h6>
            <h6>Total Diskon : Rp. {{ number_format(App\Http\Controllers\LaporanController::total($view->id_order, 'diskon', app('request')->input('kasir')),0,',','.') }}</h6>
            <h6>Total Potongan : Rp. {{ number_format(App\Http\Controllers\LaporanController::total($view->id_order, 'diskon', app('request')->input('kasir')),0,',','.') }}</h6>
            <h6>Pembayaran Akhir : Rp. {{ number_format(App\Http\Controllers\LaporanController::total($view->id_order, 'pembayaran akhir', app('request')->input('kasir')),0,',','.') }}</h6>
            <h6>Total Affiliasi : Rp. {{ number_format(App\Http\Controllers\LaporanController::total($view->id_order, 'affiliasi', app('request')->input('kasir')),0,',','.') }}</h6>

            <br><br>
            <h6>Print Daftar Belanja</h6>
            <form action="/cashier/laporan/penjualan/print-daftar" target="_blank">
              {{ csrf_field() }}
              <input type="hidden" name="id" value="{{ $view->id_order }}"> 
              <button type="submit" class="btn btn-primary form-control" name="type" value="print">Print Daftar Belanja</button><br><br>
            </form>
            <form action="/cashier/laporan/penjualan/cancel-daftar">
              {{ csrf_field() }}
              <div class="form-group">
                <label>Password Master</label>
                <input type="password" name="master" class="form-control">
              </div>
              <input type="hidden" name="id" value="{{ $view->id_order }}"> 
              <button type="submit" class="btn btn-danger form-control" name="type" value="cancel">Cancel Transaksi</button>
            </form>
        </div>

        <div class="modal-footer">
          <!-- <button type="submit" class="btn btn-primary">Download Laporan</button> -->
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
@endforeach

<!-- Modal -->
<div class="modal" id="myModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Download Laporan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="/cashier/laporan/penjualan/print">
      <div class="modal-body">
        <p>
        <label for="to">Download Laporan Pada Tanggal</label>
        <input type="date" name="date" class="form-control">
        </p>
        <p>
        <label for="to">Download Dengan Nama Kasir</label>
        <select name="kasir" class="form-control">
          <option value="">-- Semua Kasir --</option>
          @foreach($kasir as $k)
          <option value="{{ $k->id }}">{{ $k->nama }}</option>
          @endforeach
        </select>
        </p>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Download Laporan</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
      </form>
    </div>
  </div>
</div>

<div class="modal" id="myModalPeriode" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Download Laporan Per Periode</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="/cashier/laporan/penjualan/print-periode">
      <div class="modal-body">
        <p>
        <label for="to">Download Laporan Dari Tanggal</label>
        <input type="date" name="dari" class="form-control">
        </p>
        <p>
        <label for="to">Download Laporan Sampai Tanggal</label>
        <input type="date" name="sampai" class="form-control">
        </p>
        <p>
        <label for="to">Download Dengan Nama Kasir</label>
        <select name="kasir" class="form-control">
          <option value="">-- Semua Kasir --</option>
          @foreach($kasir as $k)
          <option value="{{ $k->id }}">{{ $k->nama }}</option>
          @endforeach
        </select>
        </p>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Download Laporan</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
      </form>
    </div>
  </div>
</div>

<script type="text/javascript" src="/js/laporan_jualan.js"></script>
@endsection
