 @extends('usahakumart/app')

@section('content')
<!-- Breadcrumb -->
<ol class="breadcrumb">
<li class="breadcrumb-item">Home</li>
<li class="breadcrumb-item">Admin</li>
<li class="breadcrumb-item active">Laporan Pembelian Produk</li>

<!-- Breadcrumb Menu-->

</ol>

<div class="container-fluid">

  <div class="card">
    <div class="card-body">
      <div class="row">
        <div class="col-sm-10">
          <h4 class="card-title mb-0">Laporan Pembelian Produk</h4>
          <div class="small text-muted">{{date('d M Y')}}</div><br>
          
          <a href="/cashier/laporan/pembelian-produk/tambah" class="btn btn-primary btn-sm">Buat Laporan Pembelian Produk</a>

          @if(session('user-toko')->level == 1)
          <a href="/cashier/laporan/pembelian-produk/print?date=sekarang" class="btn btn-warning btn-sm">Download Laporan Hari Ini</a>
          <a href="/cashier/laporan/pembelian-produk/print?date=bulanan" class="btn btn-warning btn-sm">Download Laporan Bulan Ini</a>
          @endif

          <!-- <br><br><hr><br>

          <form method="POST" enctype="multipart/form-data" action="/cashier/laporan/pembelian-produk/upload">
            {{ csrf_field() }}

            <input type="file" name="file" class="form-control"><br>
            <button class="btn btn-primary btn-sm" type="submit">Upload File Excel</button>
          </form> -->
        </div>
        <!--/.col-->
      </div>
      @if(session()->has('alert'))
      <div class="row">
      	<div class="alert alert-danger col-sm-12" style="margin-bottom: 0; margin-top: 25px;">
      			<p>{{ session()->get('alert') }}</p>
      	</div>
      </div>
      @endif
    </div>
  </div>
  <!--/.card-->


  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          Usahaku Toserba - Toko Offline
        </div>
        <div class="card-body">
          <form method="GET" action="/cashier/laporan/pembelian-produk/search" >
              <input class="form-control" type="date" placeholder="Type to search" name="q"></input><br>
              <input class="form-control" type="text" placeholder="Isi dengan Nama, Supplier atau Kode Barang" name="q2"></input><br>
              <button type="submit" class="btn btn-primary form-control">Search</button>
          </form>
          <br>
          <table class="table table-responsive-sm table-hover table-outline mb-0">
            <thead class="thead-light">
              <tr>
                <th>#</th>
                <th class="text-center">Kode Barang</th>
                <th class="text-center">Nama Barang</th>
                <th class="text-center">Supplier</th>
                <th class="text-center">Jumlah Barang</th>
                <th class="text-center">Tipe</th>
                <th class="text-center">Total Harga</th>
                <th class="text-center">Tanggal</th>
                @if(session('user-toko')->level == 1)
                <th class="text-center">Option</th>
                @endif
              </tr>
            </thead>
            <tbody>
              @foreach ($datas as $no=>$view)
              <tr>
                <td class="text-center">{{$no+1}}</td>
                <td class="text-left">{{$view['barcode']}}</td>
                <td class="text-left">{{$view['produk']}}</td>
                <td class="text-center">{{$view['supplier']}}</td>
                <td class="text-center">{{$view['jumlah_barang']}} {{$view['tipe_jumlah']}}</td>
                <td class="text-center">{{$view['tipe']}}</td>
                <td class="text-center">Rp. {{ number_format($view->total_harga,0,',','.') }}</td>
                <td class="text-center">{{ date('d F Y', strtotime($view->created_at)) }}</td>
                @if(session('user-toko')->level == 1)
                <td class="text-center">
                  <a href="/cashier/laporan/pembelian-produk/edit/{{ $view['id'] }}" class="btn btn-warning" style="display: inline-block;">Edit</a><br><br>
                  <form method="post" action="/cashier/laporan/pembelian-produk/delete/{{$view->id}}" style="display: inline-block;">
                    {{csrf_field()}}
                    <button type="submit" class="btn btn-danger">Hapus</button>
                  </form>
                </td>
                @endif
              </tr>
              @endforeach
            </tbody>
          </table>
          <br>
          @if(!Request::get('q'))
          {!!$datas->render()!!}
          @endif
        </div>
      </div>
    </div>
    <!--/.col-->
  </div>
  <!--/.row-->
</div>

</div>
<script type="text/javascript" src="/js/gudang.js"></script>
@endsection
