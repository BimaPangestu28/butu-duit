@extends('usahakumart/app')

@section('content')
<!-- Breadcrumb -->
<ol class="breadcrumb">
<li class="breadcrumb-item">Home</li>
<li class="breadcrumb-item">Admin</li>
<li class="breadcrumb-item active">Laporan Pembelian Produk</li>

<!-- Breadcrumb Menu-->

</ol>

<div class="container-fluid">

  <div class="card">
    <div class="card-body">
      <div class="row">
        <div class="col-sm-10">
          <h4 class="card-title mb-0">Laporan Pembelian Produk</h4>
          <div class="small text-muted">{{date('d M Y')}}</div><br>
        </div>
        <!--/.col-->
      </div>
      @if(session()->has('alert'))
      <div class="row">
      	<div class="alert alert-danger col-sm-12" style="margin-bottom: 0; margin-top: 25px;">
      			<p>{{ session()->get('alert') }}</p>
      	</div>
      </div>
      @endif
    </div>
  </div>
  <!--/.card-->


  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
            Laporan Perolehan Kasir Harian
        </div>
        <div class="card-body">
            <ul class="list-group list-group-flush" style="font-size:17px;">
                <li class="list-group-item">Omset <span class="float-right">Rp. {{ number_format(App\Http\Controllers\LaporanController::totalHari('pembayaran'),0,',','.') }}</span></li>
                <li class="list-group-item">Modal <span class="float-right">Rp. {{ number_format(App\Http\Controllers\LaporanController::totalHari('hpp'),0,',','.') }}</span></li>
                <li class="list-group-item">Diskon <span class="float-right">Rp. {{ number_format(App\Http\Controllers\LaporanController::totalHari('diskon'),0,',','.') }}</span></li>
                <li class="list-group-item">Affiliasi <span class="float-right">Rp. {{ number_format(App\Http\Controllers\LaporanController::totalHari('affiliasi'),0,',','.') }}</span></li>
                <li class="list-group-item">Net Profit <strong class="float-right">Rp. {{ number_format(App\Http\Controllers\LaporanController::totalHari('pendapatan bersih'),0,',','.') }}</strong></li>
            </ul>
        </div>
      </div>
    </div>
    <!--/.col-->
  </div>
  <!--/.row-->
</div>

</div>
<script type="text/javascript" src="/js/gudang.js"></script>
@endsection
