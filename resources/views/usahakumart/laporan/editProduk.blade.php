@extends('usahakumart/app')

@section('content')
<!-- Breadcrumb -->
<ol class="breadcrumb">
<li class="breadcrumb-item">Home</li>
<li class="breadcrumb-item">Admin</li>
<li class="breadcrumb-item active">Edit Laporan</li>

<!-- Breadcrumb Menu-->

</ol>

<div class="container-fluid">

  <div class="card">
    <div class="card-body">
      <div class="row">
        <div class="col-sm-5">
          <h4 class="card-title mb-0">Edit Laporan {{$product['produk']}}</h4>
          <div class="small text-muted">{{date('d M Y')}}</div>
          <br>
          <a href="/cashier/laporan/pembelian-produk" class="btn btn-primary btn-sm">Lihat Semua Laporan Pembelian Produk</a>
        </div>
        <!--/.col-->
        <div class="col-sm-7 d-none d-md-block">
          <button type="button" class="btn btn-primary float-right"><i class="icon-cloud-download"></i></button>
          <div class="btn-toolbar float-right" role="toolbar" aria-label="Toolbar with button groups">
          </div>
        </div>
        <!--/.col-->
      </div>
      @if($errors->count() > 0 || session()->has('alert'))
      <div class="row">
      	<div class="alert alert-danger col-sm-12" style="margin-bottom: 0; margin-top: 25px;">
      		@if($errors->count() > 0)
      			@foreach($errors->all() as $error)
      			<p>{{ $error }}</p>
      			@endforeach
      		@else
      			<p>{{ session()->get('alert') }}</p>
      		@endif
      	</div>
      </div>
      @endif
    </div>
  </div>
  <!--/.card-->


  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          Usahaku Toserba - Toko Offline
        </div>
        <div class="card-body">
          <form method="POST" action="/cashier/laporan/pembelian-produk/update/{{ $product->id }}" enctype="multipart/form-data">
          	{{ csrf_field() }}

            <input type="hidden" name="id" value="{{ $product->id }}">

            <div class="form-group">
          		<strong>No Transaksi</strong><br>
          		<input type="text" name="no_transaksi" class="form-control" style="margin-top: 10px;" placeholder="*) Isi Sesuai dengan no transaksi di nota" value="{{ $product['no_transaksi'] }}">
          	</div>

          	<div class="form-group">
              <strong>Kode Barang</strong><br>
              <input type="number" min="0" name="barcode" class="form-control" style="margin-top: 10px;" placeholder="*) Sesuaikan Dengan Barcode Produk" value="{{ $product['barcode'] }}">
            </div>            

            <div class="form-group">
              <strong>Nama Barang</strong><br>
              <input type="text" name="produk" class="form-control" style="margin-top: 10px;" placeholder="*) Sesuaikan Dengan Nama Produk" value="{{ $product['produk'] }}">
            </div>

            <div class="form-group">
              <strong>Nama Supplier</strong><br>
              <select name="supplier" class="form-control">
                <option value="{{ $product->supplier }}">{{ $product->supplier }}</option>
                @foreach($supplier as $s)
                  <option value="{{ $s->name }}"  @if($s->name == $product->supplier) selected='selected' @endif >{{ $s->name }}</option>
                @endforeach
              </select>
            </div> 

            <div class="form-group">
              <strong>Jumlah Barang</strong><br>
              <input type="number" name="jumlah_barang" class="form-control" style="margin-top: 10px;" placeholder="*) Isi Dengan Angka, Jangan Pake Symbol Atau Huruf" value="{{ $product['jumlah_barang'] }}" id="modal">
            </div>

            <div class="form-group">
              <strong>Tipe Jumlah</strong><br>
              <select name="tipe_jumlah" class="form-control">
                <!-- <option value="lusin" @if($product->tipe_jumlah == 'lusin') selected='selected' @endif>Lusin</option>
                <option value="gross" @if($product->tipe_jumlah == 'gross') selected='selected' @endif>Gross</option> -->
                <option value="banded" @if($product->tipe_jumlah == 'banded') selected='selected' @endif>Banded</option>
                <!-- <option value="rim" @if($product->tipe_jumlah == 'rim') selected='selected' @endif>Rim</option> -->
                <option value="pcs" @if($product->tipe_jumlah == 'pcs') selected='selected' @endif>Pcs</option>
                <option value="dus" @if($product->tipe_jumlah == 'dus') selected='selected' @endif>dus</option>
              </select>
            </div>

            <div class="form-group">
              <strong>Tipe</strong><br>
              <select class="form-control" name="tipe">
                <!-- <option value="Utang" @if($product->tipe == 'Utang') selected='selected' @endif>Utang</option>   -->
                <option value="Tunai" @if($product->tipe == 'Tunai') selected='selected' @endif>Tunai</option>  
                <option value="Kredit" @if($product->tipe == 'Kredit') selected='selected' @endif>Kredit</option>  
                <option value="Titipan" @if($product->tipe == 'Titipan') selected='selected' @endif>Titipan</option>  
              </select>
            </div>

            <div class="form-group">
              <strong>Total Harga</strong><br>
              <input type="number" name="total_harga" class="form-control" style="margin-top: 10px;" placeholder="*) Isi Dengan Angka, Jangan Pake Symbol Atau Huruf" value="{{ $product['total_harga'] }}">
            </div>

            <div class="form-group">
          		<strong>Diskon</strong><br>
          		<input type="number" name="diskon" class="form-control" style="margin-top: 10px;" placeholder="*) Isi Dengan Angka, Jangan Pake Symbol Atau Huruf" value="{{ $product['diskon'] }}">
          	</div>

            <div class="form-group">
          		<strong>Pajak</strong><br>
          		<input type="number" name="pajak" class="form-control" style="margin-top: 10px;" placeholder="*) Isi Dengan Angka, Jangan Pake Symbol Atau Huruf" value="{{ $product['pajak'] }}">
          	</div>

            <br><br>

            <input type="submit" class="btn btn-primary form-control" value="Tambah Laporan Pembelian Barang">
          </form>
        </div>
      </div>
    </div>
    <!--/.col-->
  </div>
  <!--/.row-->
</div>

</div>

<script type="text/javascript">
  // $(document).ready(function(){
  //   setInterval(function() {
  //     let modal     = $("#modal").val(),
  //         jual      = $("#jual").val();

  //     if (modal != '' && jual != '') {
  //       $("#keuntungan").val(jual - modal);
  //     }
  //   }, 1000);
  // })
</script>
@endsection
