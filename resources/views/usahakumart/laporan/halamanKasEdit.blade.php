@extends('usahakumart/app')

@section('content')
<!-- Breadcrumb -->
<ol class="breadcrumb">
<li class="breadcrumb-item">Home</li>
<li class="breadcrumb-item">Admin</li>
<li class="breadcrumb-item active">Edit Laporan</li>

<!-- Breadcrumb Menu-->

</ol>

<div class="container-fluid">

  <div class="card">
    <div class="card-body">
      <div class="row">
        <div class="col-sm-5">
          <h4 class="card-title mb-0">
            Edit Kas 
            @if($kas['tipe'] == 1)
            Masuk
            @elseif($kas['tipe'] == 2)
            Keluar
            @elseif($kas['tipe'] == 3)
            Mutasi Masuk
            @elseif($kas['tipe'] == 4)
            Mutasi Keluar
            @endif
            No. {{$kas['no_transaksi']}}
           </h4>
          <div class="small text-muted">{{date('d M Y')}}</div>
          <br>
          <a href="/cashier/laporan/kas" class="btn btn-primary btn-sm">Lihat Semua Laporan Kas</a>
        </div>
        <!--/.col-->
        <div class="col-sm-7 d-none d-md-block">
          <button type="button" class="btn btn-primary float-right"><i class="icon-cloud-download"></i></button>
          <div class="btn-toolbar float-right" role="toolbar" aria-label="Toolbar with button groups">
          </div>
        </div>
        <!--/.col-->
      </div>
      @if($errors->count() > 0 || session()->has('alert'))
      <div class="row">
      	<div class="alert alert-danger col-sm-12" style="margin-bottom: 0; margin-top: 25px;">
      		@if($errors->count() > 0)
      			@foreach($errors->all() as $error)
      			<p>{{ $error }}</p>
      			@endforeach
      		@else
      			<p>{{ session()->get('alert') }}</p>
      		@endif
      	</div>
      </div>
      @endif
    </div>
  </div>
  <!--/.card-->


  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          Usahaku Toserba - Toko Offline
        </div>
        <div class="card-body">
          <form method="POST" action="/cashier/laporan/kas/update/{{ $kas->id }}">
          	{{ csrf_field() }}

            <div class="form-group">
              <strong>No Transaksi</strong><br>
              <input type="text" min="0" name="no_transaksi" class="form-control" style="margin-top: 10px;" value="{{ $kas['no_transaksi'] }}">
            </div>            

          	<div class="form-group">
          		<strong>Jenis Kas</strong><br>
          		<input type="text" name="jenis" class="form-control" style="margin-top: 10px;" value="{{ $kas['jenis'] }}">
          	</div>

            <div class="form-group">
              <strong>Ke Kas</strong><br>
              <input type="text" name="ke_kas" class="form-control" style="margin-top: 10px;" value="{{ $kas['ke_kas'] }}">
            </div>

          	<div class="form-group">
          		<strong>Keterangan</strong><br>
          		<textarea name="keterangan" class="form-control" cols="30" rows="10">{{$kas['keterangan']}}</textarea>
          	</div>

            <div class="form-group">
              <strong>Total</strong><br>
              <input type="number" name="total" class="form-control" style="margin-top: 10px;" placeholder="*) Isi Dengan Angka, Jangan Pake Symbol Atau Huruf" value="{{$kas['total']}}">
            </div>

          	<div class="form-group">
          		<strong>Tipe</strong><br>
          		<select class="form-control" name="tipe">
                <option value="1">Kas Masuk</option>  
                <option value="2">Kas Keluar</option>  
                <option value="3">Kas Mutasi Masuk</option>  
                <option value="4">Kas Mutasi Keluar</option>  
              </select>
          	</div>
          	<br><br>

          	<input type="submit" class="btn btn-primary form-control" value="Update Kas">
          </form>
        </div>
      </div>
    </div>
    <!--/.col-->
  </div>
  <!--/.row-->
</div>

</div>

<script type="text/javascript">
  // $(document).ready(function(){
  //   setInterval(function() {
  //     let modal     = $("#modal").val(),
  //         jual      = $("#jual").val();

  //     if (modal != '' && jual != '') {
  //       $("#keuntungan").val(jual - modal);
  //     }
  //   }, 1000);
  // })
</script>
@endsection
