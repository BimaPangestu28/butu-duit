<!DOCTYPE html>
<html>
	<head>
		<title>Laporan Stok Kartu</title>
		<style type="text/css">
			.center {
				width: 90%;
				margin: 0 auto;
			}

			body {
				padding: 15px;
				font-family: 'Arial';
			}

			.title {
				text-align: center;
			}

			.title h3 {
				font-size: 24px;
				text-transform: uppercase;
				margin-bottom: 0;
			}

			.title h4 {
				margin-top: 0px;
				font-size: 20px;
			}

			table {
			    border-collapse: collapse;
			    width: 100%;
			}

			th, td {
			    text-align: left;
			    padding: 15px;
			    border: 1px solid black;
			}
		</style>
	</head>
	<body>
		<div class="center">
			<div class="title">
				<h3>Laporan Stok Kartu</h3>
				<h4>{{ Auth::user()->name }}</h4>
				<span>
					@if(date('N') == 1)
						Senin, 
					@elseif(date('N') == 2)
						Selasa, 
					@elseif(date('N') == 3)
						Rabu, 
					@elseif(date('N') == 4)
						Kamis, 
					@elseif(date('N') == 5)
						Jum'at, 
					@elseif(date('N') == 6)
						Sabtu, 
					@elseif(date('N') == 7)
						Minggu, 
					@endif
					{{ date(' d - F - Y') }}
				</span>
			</div>

			<br><br>

			<div class="barang-keluar">
				<h3 style="text-align: center;">
					Stok Kartu
				</h3>

				<table class="table table-responsive-sm table-hover table-outline mb-0">
	            <thead class="thead-light">
	              <tr>
	                <th>Jumlah Keseluruhan Kartu</th>
	                <th>Jumlah Kartu Yang Telah Digunakan</th>
	                <th>Jumlah Kartu Yang Belum Digunakan</th>
	              </tr>
	            </thead>
	            <tbody>
	              <tr>
	                <td>{{ $jumlah_keseluruhan }}</td>
	                <td>{{ $jumlah_terpakai }}</td>
	                <td>{{ $jumlah_belum }}</td>
	              </tr>
	            </tbody>
	          </table>
			</div>
		</div>
	</body>
</html>