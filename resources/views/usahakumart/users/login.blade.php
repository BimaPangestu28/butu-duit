<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="shortcut icon" href="/admin/img/favicon.png">
  <title>UsahaMart Administrator</title>

  <!-- Icons -->
  <link href="/admin/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="/admin/simple-line-icons/css/simple-line-icons.css" rel="stylesheet">

  <!-- Main styles for this application -->
  <link href="/admin/css/style.css" rel="stylesheet">
  <!-- Styles required by this views -->

</head>
<body class="app flex-row align-items-center">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-8">
        <div class="card-group">
          <div class="card p-4">
          	<form method="POST" action="/cashier/login">
          	{{ csrf_field() }}
            <div class="card-body">
              <h1>Login</h1>
              <p class="text-muted">Sign In to your account</p>
              <div class="input-group mb-3">
                <span class="input-group-addon"><i class="icon-user"></i></span>
                <input type="text" class="form-control" name="username" placeholder="Username">
              </div>
              <div class="input-group mb-4">
                <span class="input-group-addon"><i class="icon-lock"></i></span>
                <input type="password" name="password" class="form-control" placeholder="Password">
              </div>
              @if($errors->count() > 0 || session()->has('alert'))
              <div class="alert alert-danger">
              	@if($errors->count())
              		@foreach($errors->all() as $error)
	            	<span>{{ $error }}</span><br>
	            	@endforeach
	            @elseif(session()->has('alert'))
	            	<span>{{ session()->get('alert') }}</span><br>
              	@endif
              </div>
              @endif
              <div class="row">
                <div class="col-5">
                  <button type="submit" class="btn btn-primary px-4">Login</button>
                </div>
                <div class="col-7 text-right">
                  <span class="text-primary">Lupa password ?</span> <br>
                  <span class="text-secondary">Tanya sama developer</span>
                </div>
              </div>
            </div>
            </form>
          </div>
          <div class="card text-white bg-primary py-5 d-md-down-none" style="width:44%">
            <div class="card-body text-center">
              <div>
                <h2>Perhatian</h2>
                <p>Jadilah administrator yang baik, jangan menyalahgunakan sesuatu yang dapat merugikan orang lain jika kita tidak ingin dirugikan</p>
                <button type="button" class="btn btn-primary active mt-3" style="cursor: no-drop;">Tidak Ada Pendaftaran</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

</body>

</html>