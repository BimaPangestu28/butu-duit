@extends('usahakumart/app')

@section('content')
<!-- Breadcrumb -->
<ol class="breadcrumb">
<li class="breadcrumb-item">Home</li>
<li class="breadcrumb-item">Admin</li>
<li class="breadcrumb-item active">Daftar Users Toko</li>

<!-- Breadcrumb Menu-->

</ol>

<div class="container-fluid">

  <div class="card">
    <div class="card-body">
      <div class="row">
        <div class="col-sm-5">
          <h4 class="card-title mb-0">Daftar Users Toko</h4>
          <div class="small text-muted">{{date('d M Y')}}</div><br>
        </div>
        <!--/.col-->
        <div class="col-sm-7 d-none d-md-block">
          <button type="button" class="btn btn-primary float-right"><i class="icon-cloud-download"></i></button>
          <div class="btn-toolbar float-right" role="toolbar" aria-label="Toolbar with button groups">
          </div>
        </div>
        <!--/.col-->
      </div>
      @if(session()->has('alert'))
      <div class="row">
      	<div class="alert alert-danger col-sm-12" style="margin-bottom: 0; margin-top: 25px;">
      			<p>{{ session()->get('alert') }}</p>
      	</div>
      </div>
      @endif
    </div>
  </div>
  <!--/.card-->


  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          Usahaku Toserba - Toko Offline <a href="/cashier/user-toko/create" class="btn btn-primary btn-sm">Tambah User</a>
        </div>
        <div class="card-body">
          <!-- <form method="GET" action="/cashier/user-toko/search">
              <input class="form-control" type="text" placeholder="Type to search" style="width: 93.5%; display: inline-block;" name="q"></input>
              <button type="submit" class="btn btn-primary" style="margin-left: -4px; margin-top: -5px;">Search</button>
          </form>
          <br> -->
          <table class="table table-responsive-sm table-hover table-outline mb-0">
            <thead class="thead-light">
              <tr>
                <th class="text-center">#</th>
                <th class="text-center">Nama</th>
                <th class="text-center">Username</th>
                <th class="text-center">Level</th>
                <th class="text-center">Status</th>
                <th class="text-center">Option</th>
              </tr>
            </thead>
            <tbody>
            @foreach($users as $no => $s)
              <tr>
                <td class="text-center">{{ $no += 1 }}</td>
                <td class="text-center">{{ $s->nama }}</td>
                <td class="text-center">{{ $s->username }}</td>
                <td class="text-center">
                  @if($s->level == 1)
                    Master
                  @elseif($s->level == 2)
                    Input Data
                  @else
                    Kasir
                  @endif
                </td>
                <td>@if($s->status == 1) Aktif @else Non Aktif @endif</td>
                <td class="text-center">
                  <a href="/cashier/user-toko/aktivitas/{{ $s->id }}" class="btn btn-primary">Lihat Aktivitas</a>
                  <a href="/cashier/user-toko/edit/{{ $s->id }}" class="btn btn-warning">Edit</a>
                  {{-- <a href="/cashier/user-toko/delete/{{ $s->id }}" class="btn btn-danger" onclick="return confirm('Apakah kamu yakin?')">Delete</a> --}}
                  @if($s->status == 0)
                  <a href="/cashier/user-toko/status/aktif/{{ $s->id }}" class="btn btn-primary" onclick="return confirm('Apakah kamu yakin?')">Aktifkan</a>
                  @else
                  <a href="/cashier/user-toko/status/nonaktif/{{ $s->id }}" class="btn btn-danger" onclick="return confirm('Apakah kamu yakin?')">Non Aktifkan</a>
                  @endif
                </td>
              </tr>
            @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <!--/.col-->
  </div>
  <!--/.row-->
</div>

</div>
@endsection
