@extends('usahakumart/app')

@section('content')
<!-- Breadcrumb -->
<ol class="breadcrumb">
<li class="breadcrumb-item">Home</li>
<li class="breadcrumb-item">Admin</li>
<li class="breadcrumb-item active">Tambah User Toko</li>

<!-- Breadcrumb Menu-->

</ol>

<div class="container-fluid">

  <div class="card">
    <div class="card-body">
      <div class="row">
        <div class="col-sm-5">
          <h4 class="card-title mb-0">Tambah User Toko</h4>
          <div class="small text-muted">{{date('d M Y')}}</div>
        </div>
        <!--/.col-->
        <div class="col-sm-7 d-none d-md-block">
          <button type="button" class="btn btn-primary float-right"><i class="icon-cloud-download"></i></button>
          <div class="btn-toolbar float-right" role="toolbar" aria-label="Toolbar with button groups">
          </div>
        </div>
        <!--/.col-->
      </div>
      @if($errors->count() > 0 || session()->has('alert'))
      <div class="row">
      	<div class="alert alert-danger col-sm-12" style="margin-bottom: 0; margin-top: 25px;">
      		@if($errors->count() > 0)
      			@foreach($errors->all() as $error)
      			<p>{{ $error }}</p>
      			@endforeach
      		@else
      			<p>{{ session()->get('alert') }}</p>
      		@endif
      	</div>
      </div>
      @endif
    </div>
  </div>
  <!--/.card-->


  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          Usahaku Toserba - Toko Offline
        </div>
        <div class="card-body">
          <form method="POST" action="/cashier/user-toko/create" enctype="multipart/form-data">
          	{{ csrf_field() }}

          	<div class="form-group">
          		<strong>Nama</strong><br>
          		<input type="text" name="nama" class="form-control" style="margin-top: 10px;" value="{{ old('nama') }}">
          	</div>

            <div class="form-group">
              <strong>Username</strong><br>
              <input type="text" name="username" class="form-control" style="margin-top: 10px;" value="{{ old('username') }}">
            </div>

            <div class="form-group">
              <strong>Level</strong><br>
              <select class="form-control" name="level">
                <option value="1">Master</option>
                <option value="2">Input Data</option>
                <option value="3">Kasir</option>
              </select>
            </div>

            <div class="form-group">
              <strong>Password</strong><br>
              <input type="Password" name="password" class="form-control" style="margin-top: 10px;">
            </div>            

          	<br><br>

          	<input type="submit" class="btn btn-primary form-control" value="Tambah User Toko">
          </form>
        </div>
      </div>
    </div>
    <!--/.col-->
  </div>
  <!--/.row-->
</div>

</div>

@endsection
