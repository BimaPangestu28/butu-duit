@extends('usahakumart/app')

@section('content')
<!-- Breadcrumb -->
<ol class="breadcrumb">
<li class="breadcrumb-item">Home</li>
<li class="breadcrumb-item">Admin</li>
<li class="breadcrumb-item active">Tambah Pola Perilaku Barang</li>

<!-- Breadcrumb Menu-->

</ol>

<div class="container-fluid">

  <div class="card">
    <div class="card-body">
      <div class="row">
        <div class="col-sm-5">
          <h4 class="card-title mb-0">Tambah Pola Perilaku Barang</h4>
          <div class="small text-muted">{{date('d M Y')}}</div>
        </div>
        <!--/.col-->
        <div class="col-sm-7 d-none d-md-block">
          <button type="button" class="btn btn-primary float-right"><i class="icon-cloud-download"></i></button>
          <div class="btn-toolbar float-right" role="toolbar" aria-label="Toolbar with button groups">
          </div>
        </div>
        <!--/.col-->
      </div>
      @if($errors->count() > 0 || session()->has('alert'))
      <div class="row">
      	<div class="alert alert-danger col-sm-12" style="margin-bottom: 0; margin-top: 25px;">
      		@if($errors->count() > 0)
      			@foreach($errors->all() as $error)
      			<p>{{ $error }}</p>
      			@endforeach
      		@else
      			<p>{{ session()->get('alert') }}</p>
      		@endif
      	</div>
      </div>
      @endif
    </div>
  </div>
  <!--/.card-->


  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          Usahaku Toserba - Toko Offline
        </div>
        <div class="card-body">
          <form method="POST" action="/cashier/pola-perilaku-harga/create" enctype="multipart/form-data">
          	{{ csrf_field() }}

          	<div class="form-group">
          		<strong>Barang Yang Dibeli</strong><br>
          		<input type="text" name="beli_barang" class="form-control" style="margin-top: 10px;" value="{{ old('beli_barang') }}" id="beli" list="beliList">
              <datalist id="beliList"></datalist>
          	</div>

            <div class="form-group">
              <strong>Total Yang Dibeli</strong><br>
              <input type="number" name="total_beli" class="form-control" style="margin-top: 10px;" value="{{ old('total_beli') }}">
            </div>

            <div class="form-group">
              <strong>Option</strong><br>
              <select class="form-control" name="option" id="option">
                <option value="">-- Pilih Option --</option>
                <option value="1">Gratis</option>
                <option value="2">Potongan</option>
              </select>
            </div>

            <div class="form-group potongan" style="display: none;">
              <strong>Potongan</strong><br>
              <input type="number" name="potongan" class="form-control" style="margin-top: 10px;" value="{{ old('potongan') }}">
            </div>     

            <div class="form-group gratis" style="display: none;">
              <strong>Bonus Barang</strong><br>
              <input type="text" name="bonus_barang" class="form-control" style="margin-top: 10px;" value="{{ old('bonus_barang') }}" id="bonus" list="bonusList">
              <datalist id="bonusList"></datalist>
            </div>       

            <div class="form-group gratis" style="display: none;">
              <strong>Bonus Barang Jumlah</strong><br>
              <input type="number" name="bonus_barang_jumlah" class="form-control" style="margin-top: 10px;" value="{{ old('bonus_barang_jumlah') }}">
            </div>

          	<br><br>

          	<input type="submit" class="btn btn-primary form-control" value="Tambah Pola Perilaku Barang">
          </form>
        </div>
      </div>
    </div>
    <!--/.col-->
  </div>
  <!--/.row-->
</div>

</div>

<script type="text/javascript" src="/js/pola.js"></script>

@endsection
