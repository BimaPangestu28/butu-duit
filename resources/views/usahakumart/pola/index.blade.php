@extends('usahakumart/app')

@section('content')
<!-- Breadcrumb -->
<ol class="breadcrumb">
<li class="breadcrumb-item">Home</li>
<li class="breadcrumb-item">Admin</li>
<li class="breadcrumb-item active">Pola Perilaku Barang</li>

<!-- Breadcrumb Menu-->

</ol>

<div class="container-fluid">

  <div class="card">
    <div class="card-body">
      <div class="row">
        <div class="col-sm-5">
          <h4 class="card-title mb-0">Pola Perilaku Barang</h4>
          <div class="small text-muted">{{date('d M Y')}}</div><br>
        </div>
        <!--/.col-->
        <div class="col-sm-7 d-none d-md-block">
          <button type="button" class="btn btn-primary float-right"><i class="icon-cloud-download"></i></button>
          <div class="btn-toolbar float-right" role="toolbar" aria-label="Toolbar with button groups">
          </div>
        </div>
        <!--/.col-->
      </div>
      @if(session()->has('alert'))
      <div class="row">
      	<div class="alert alert-danger col-sm-12" style="margin-bottom: 0; margin-top: 25px;">
      			<p>{{ session()->get('alert') }}</p>
      	</div>
      </div>
      @endif
    </div>
  </div>
  <!--/.card-->


  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          Usahaku Toserba - Toko Offline &nbsp; &nbsp; <a href="/cashier/pola-perilaku-harga/create" class="btn btn-primary btn-sm">Tambah Pola Perilaku Harga</a>
        </div>
        <div class="card-body">
          <!-- <form method="GET" action="/cashier/daftar-rak/search-rak" >
              <select class="form-control" type="text" placeholder="Type to search" style="width: 93.5%; display: inline-block;" name="q">
                @for($i = 1; $i <= 10; $i++)
                  <option value="{{ $i }}">{{ $i }}</option>
                @endfor
              </select>
              <button type="submit" class="btn btn-primary" style="margin-left: -4px; margin-top: -5px;">Search</button>
          </form>
          <br> -->
          <table class="table table-responsive-sm table-hover table-outline mb-0">
            <thead class="thead-light">
              <tr>
                <th>#</th>
                <th class="text-center">Barang Yang Dibeli</th>
                <th class="text-center">Total Beli</th>
                <th class="text-center">Option</th>
                <th class="text-center">Potongan</th>
                <th class="text-center">Bonus Barang</th>
                <th class="text-center">Jumlah Bonus Barang</th>
                <th class="text-center">Action</th>
              </tr>
            </thead>
            <tbody>
            @foreach($data as $no => $t)
              <tr @if($t->aktif == 0) style="background: gray;" @endif>
                <td>{{ $no += 1 }}</td>
                <td class="text-center">{{ $t->name }}</td>
                <td class="text-center">{{ $t->total_beli }}</td>
                <td class="text-center">
                  @if($t->option == 1)
                    Bonus Produk
                  @else
                    Potongan Harga
                  @endif
                </td>
                <td class="text-center">Rp. {{ $t->potongan }}</td>
                <td class="text-center">{{ \App\Http\Controllers\PolaPerilakuHargaController::bonus($t->bonus_barang)->name }}</td>
                <td class="text-center">{{ $t->bonus_barang_jumlah }}</td>
                <td class="text-center">
                  <a href="/cashier/pola-perilaku-harga/edit/{{ $t->id }}" class="btn btn-md btn-primary">Edit</a>
                  <a href="/cashier/pola-perilaku-harga/delete/{{ $t->id }}" class="btn btn-md btn-warning" onclick="return confirm('Apakah anda yakin?')">Hapus</a>
                  @if($t->aktif == 1)
                  <a href="/cashier/pola-perilaku-harga/nonaktif/{{ $t->id }}" class="btn btn-md btn-danger" onclick="return confirm('Apakah anda yakin?')">Non Aktifkan</a>
                  @else
                  <a href="/cashier/pola-perilaku-harga/aktif/{{ $t->id }}" class="btn btn-md btn-danger" onclick="return confirm('Apakah anda yakin?')">Aktifkan</a>
                  @endif
                </td>
              </tr>
            @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <!--/.col-->
  </div>
  <!--/.row-->
</div>

</div>
<script type="text/javascript" src="/js/gudang.js"></script>
@endsection
