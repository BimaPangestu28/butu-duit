@extends('usahakumart/app')

@section('content')
<!-- Breadcrumb -->
<ol class="breadcrumb">
<li class="breadcrumb-item">Home</li>
<li class="breadcrumb-item">Admin</li>
<li class="breadcrumb-item active">Edit Potongan Barang</li>

<!-- Breadcrumb Menu-->

</ol>

<div class="container-fluid">

  <div class="card">
    <div class="card-body">
      <div class="row">
        <div class="col-sm-5">
          <h4 class="card-title mb-0">Edit Potongan Barang</h4>
          <div class="small text-muted">{{date('d M Y')}}</div>
          <a href="/cashier/notifikasi-barang" class="btn btn-primary">Lihat Daftar Notifikasi Barang</a>
        </div>
        <!--/.col-->
        <div class="col-sm-7 d-none d-md-block">
          <button type="button" class="btn btn-primary float-right"><i class="icon-cloud-download"></i></button>
          <div class="btn-toolbar float-right" role="toolbar" aria-label="Toolbar with button groups">
          </div>
        </div>
        <!--/.col-->
      </div>
      @if($errors->count() > 0 || session()->has('alert'))
      <div class="row">
      	<div class="alert alert-danger col-sm-12" style="margin-bottom: 0; margin-top: 25px;">
      		@if($errors->count() > 0)
      			@foreach($errors->all() as $error)
      			<p>{{ $error }}</p>
      			@endforeach
      		@else
      			<p>{{ session()->get('alert') }}</p>
      		@endif
      	</div>
      </div>
      @endif
    </div>
  </div>
  <!--/.card-->


  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          Usahaku Toserba - Toko Offline
        </div>
        <div class="card-body">
          <form method="POST" action="/cashier/notifikasi-barang/update" enctype="multipart/form-data">
          	{{ csrf_field() }}

            <input type="hidden" name="id" value="{{ $data->id }}">

          	<div class="form-group">
          		<strong>Nomer Barcode</strong><br>
          		<input type="number" id="barcode" name="barcode" class="form-control" style="margin-top: 10px;">
          	</div>

            <div class="form-group">
              <strong>Nama Barang</strong><br>
              <input type="text" id="nama_barang" name="nama_barang" class="form-control" style="margin-top: 10px;" value="{{ $data->name }}">
              <input type="hidden" id="id_product" name="id_product" class="form-control" style="margin-top: 10px;" value="{{ $data->id_p }}">
            </div>

            <div class="form-group">
              <strong>Jumlah Minimal</strong><br>
              <input type="text" name="jumlah_minimum" class="form-control" style="margin-top: 10px;" value="{{ $data->jumlah_minimum }}">
            </div>
          	<br><br>

          	<input type="submit" id="submit_button" class="btn btn-primary form-control" value="Edit Potongan Barang">
          </form>
        </div>
      </div>
    </div>
    <!--/.col-->
  </div>
  <!--/.row-->
</div>

</div>

<script type="text/javascript" src="/js/potongan.js"></script>

@endsection
