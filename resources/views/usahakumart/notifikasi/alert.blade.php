@extends('usahakumart/app')

@section('content')
<!-- Breadcrumb -->
<ol class="breadcrumb">
<li class="breadcrumb-item">Home</li>
<li class="breadcrumb-item">Admin</li>
<li class="breadcrumb-item active">Peringatan Stok Barang</li>

<!-- Breadcrumb Menu-->

</ol>

<div class="container-fluid">

  <div class="card">
    <div class="card-body">
      <div class="row">
        <div class="col-sm-5">
          <h4 class="card-title mb-0">Peringatan Stok Barang</h4>
          <div class="small text-muted">{{date('d M Y')}}</div><br>
        </div>
        <!--/.col-->
        <div class="col-sm-7 d-none d-md-block">
          <button type="button" class="btn btn-primary float-right"><i class="icon-cloud-download"></i></button>
          <div class="btn-toolbar float-right" role="toolbar" aria-label="Toolbar with button groups">
          </div>
        </div>
        <!--/.col-->
      </div>
      @if(session()->has('alert'))
      <div class="row">
      	<div class="alert alert-danger col-sm-12" style="margin-bottom: 0; margin-top: 25px;">
      			<p>{{ session()->get('alert') }}</p>
      	</div>
      </div>
      @endif
    </div>
  </div>
  <!--/.card-->


  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          Usahaku Toserba - Toko Offline
        </div>
        <div class="card-body">
          <form method="GET" action="/cashier/daftar-rak/search-rak" >
              <input class="form-control" type="text" placeholder="Type to search" style="width: 93.5%; display: inline-block;" name="q">
              <button type="submit" class="btn btn-primary" style="margin-left: -4px; margin-top: -5px;">Search</button>
          </form>
          <br>
          <table class="table table-responsive-sm table-hover table-outline mb-0">
            <thead class="thead-light">
              <tr>
                <th>#</th>
                <th class="text-center">Barcode Barang</th>
                <th class="text-center">Nama Barang</th>
                <th class="text-center">Jumlah Minimal</th>
                <th class="text-center">Sisa Stok</th>
              </tr>
            </thead>
            <tbody>
              @foreach($datas as $no => $d)
              <tr>
                <td class="text-center">{{ $no += 1 }}</td>
                <td class="text-center">{{ $d->barcode }}</td>
                <td class="text-center">{{ $d->name }}</td>
                <td class="text-center">{{ $d->jumlah_minimum }}</td>
                <td class="text-center">{{ $d->stok }}</td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <!--/.col-->
  </div>
  <!--/.row-->
</div>

</div>
<script type="text/javascript" src="/js/gudang.js"></script>
@endsection
