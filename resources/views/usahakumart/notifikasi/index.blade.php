@extends('usahakumart/app')

@section('content')
<!-- Breadcrumb -->
<ol class="breadcrumb">
<li class="breadcrumb-item">Home</li>
<li class="breadcrumb-item">Admin</li>
<li class="breadcrumb-item active">Notifikasi Barang</li>

<!-- Breadcrumb Menu-->

</ol>

<div class="container-fluid">

  <div class="card">
    <div class="card-body">
      <div class="row">
        <div class="col-sm-12">
          <h4 class="card-title mb-0">Notifikasi Barang</h4>
          <div class="small text-muted">{{date('d M Y')}}</div><br>
          <form action="/cashier/notifikasi-barang">
            <select class="form-control" name="supplier">
              @foreach($sup as $s)
              <option value="{{ $s->name }}">{{ $s->name }}</option>
              @endforeach
            </select>
            <br>
            <button class="btn btn-primary form-control" type="submit">Cari Berdasarkan Supplier</button>
          </form>
        </div>
        <!--/.col-->
        <div class="col-sm-7 d-none d-md-block">
        </div>
        <!--/.col-->
      </div>
      @if(session()->has('alert'))
      <div class="row">
      	<div class="alert alert-danger col-sm-12" style="margin-bottom: 0; margin-top: 25px;">
      			<p>{{ session()->get('alert') }}</p>
      	</div>
      </div>
      @endif
    </div>
  </div>
  <!--/.card-->


  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          Usahaku Toserba - Toko Offline
        </div>
        <div class="card-body">
          <form method="GET" action="/cashier/daftar-rak/search-rak" >
              <input class="form-control" type="text" placeholder="Type to search" style="width: 93.5%; display: inline-block;" name="q">
              <button type="submit" class="btn btn-primary" style="margin-left: -4px; margin-top: -5px;">Search</button>
          </form>
          <br>
          <table class="table table-responsive-sm table-hover table-outline mb-0">
            <thead class="thead-light">
              <tr>
                <th>#</th>
                <th class="text-center">Barcode Barang</th>
                <th class="text-center">Nama Barang</th>
                <th class="text-center">Jumlah Minimal</th>
                <th class="text-center">Option</th>
              </tr>
            </thead>
            <tbody>
              @foreach($datas as $no => $d)
              <tr>
                <td class="text-center">{{ $no += 1 }}</td>
                <td class="text-center">{{ $d->barcode }}</td>
                <td class="text-center">{{ $d->name }}</td>
                <td class="text-center">
                  @if($d->jumlah_minimum == '')
                    0
                  @else
                    {{ $d->jumlah_minimum }}
                  @endif
                </td>
                <td class="text-center">
                  <form method="POST" action="/cashier/notifikasi-barang/created">
                    {{ csrf_field() }}
                    <input type="hidden" name="id" value="{{ $d->id }}">
                    <input type="hidden" name="id_product" value="{{ $d->id_p }}">
                    <input type="number" name="jumlah_minimum" class="form-control" placeholder="Update Jumlah Minimal"><br>
                    <button type="submit" class="button-confirm btn btn-sm btn-primary form-control">Konfirmasi</button>
                  </form>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <!--/.col-->
  </div>
  <!--/.row-->
</div>

</div>
<script type="text/javascript" src="/js/notifikasi-barang.js"></script>
@endsection
