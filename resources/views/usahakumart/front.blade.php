<!DOCTYPE html>
<html>
<head>
	<title>Selamat Datang Di Usahaku Toserba</title>
	<link rel="stylesheet" type="text/css" href="/assets/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="/assets/bootstrap/css/style.css">
	<link rel="stylesheet" type="text/css" href="/assets/css/front.css">
</head>
<body>

	<div id="top">
		<img src="/assets/img/logo.png">
	</div>

	<!-- <div id="body">
		<div class="left">
			<div class="judul">
				<h3>DAFTAR BELANJA</h3>
			</div>

			<div>
				<table class="barang">
					<tr id="2">
	                    <td class="judul">FAIR &amp; LOVELY MVIT FC FOAM 12X12X9G</td>
	                    <td id="stok-2">2</td>
	                    <td id="harga_jual-2" data-id="5000">Rp. 5.000</td>
	                    <td id="affiliasi-2" data-id="12.25">Rp. 12.25</td>
	                    <td>0%</td>
	                 </tr>
             	</table>
			</div>
		</div>
		<div class="right">
			
		</div>
	</div> -->

	<div class="row">
		<div class="container" style="max-width: 1300px;">
			<div class="card ren-mar">
	          <div class="card-body">
	            <div class="row">
	              <div class="col-sm-5">
	                <h4 class="card-title mb-0">Usahaku Toserba</h4>
	                <div class="small text-muted">{{ Auth::user()->name }}</div>
	              </div>
	              <div class="col-sm-7 d-none d-md-block">
	              </div>
	            </div>
	          </div>
	        </div>

	        <div class="clear"></div>

	        <div class="col-lg-6">
	        	<div class="card">
	                <div class="card-header">
	                  <i class="fa fa-align-justify"></i> Keterangan Belanja
	                </div>
	                <div class="card-body">
	                  <table class="table table-striped">
	                    <thead>
	                      <tr>
	                        <th>Nama</th>
	                        <th>Jumlah</th>
	                        <th>Harga</th>
	                        <th>Affiliasi</th>
	                      </tr>
	                    </thead>
	                    <tbody id="barang">

	                    </tbody>
	                  </table>
	                </div>
              </div>
            </div>

            <div class="col-lg-6">
            	<div id="member">	</div>
            	<h3 id="pembayaran"></h3>
            	<h3 id="total">Total Harga Rp. 0</h3>
            	<br>
            	<hr>
            	<br>
            	<h3 id="kembalian"></h3>
            </div>
		</div>
	</div>
	<script type="text/javascript" src="/admin/js/jquery.min.js"></script>
	<script type="text/javascript" src="/admin/js/jquery.cookie.js"></script>
	<script type="text/javascript" src="/admin/js/front.js"></script>
</body>
</html>
