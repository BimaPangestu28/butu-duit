@extends('usahakumart/app')

@section('content')
<!-- Breadcrumb -->
<ol class="breadcrumb">
<li class="breadcrumb-item">Home</li>
<li class="breadcrumb-item">Admin</li>
<li class="breadcrumb-item active">Jumlah Barang Pada Rak</li>

<!-- Breadcrumb Menu-->

</ol>

<div class="container-fluid">

  <div class="card">
    <div class="card-body">
      <div class="row">
        <div class="col-sm-5">
          <h4 class="card-title mb-0">Jumlah Barang Pada Rak</h4>
          <div class="small text-muted">{{date('d M Y')}}</div><br>
        </div>
        <!--/.col-->
        <div class="col-sm-7 d-none d-md-block">
          <button type="button" class="btn btn-primary float-right"><i class="icon-cloud-download"></i></button>
          <div class="btn-toolbar float-right" role="toolbar" aria-label="Toolbar with button groups">
          </div>
        </div>
        <!--/.col-->
      </div>
      @if(session()->has('alert'))
      <div class="row">
      	<div class="alert alert-danger col-sm-12" style="margin-bottom: 0; margin-top: 25px;">
      			<p>{{ session()->get('alert') }}</p>
      	</div>
      </div>
      @endif
    </div>
  </div>
  <!--/.card-->


  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          Usahaku Toserba - Toko Offline
        </div>
        <div class="card-body">
          <form method="GET" action="/cashier/daftar-rak/search-rak" >
              <select class="form-control" type="text" placeholder="Type to search" style="width: 93.5%; display: inline-block;" name="q">
                @for($i = 1; $i <= 10; $i++)
                  <option value="{{ $i }}">{{ $i }}</option>
                @endfor
              </select>
              <button type="submit" class="btn btn-primary" style="margin-left: -4px; margin-top: -5px;">Search</button>
          </form>
          <br>
          <table class="table table-responsive-sm table-hover table-outline mb-0">
            <thead class="thead-light">
              <tr>
                <th>#</th>
                <th>Nama Barang</th>
                <th class="text-center">Stok Di Gudang</th>
                <!-- <th class="text-center">Stok Di Rak</th>
                <th class="text-center">Stok Keseluruhan</th> -->
                <!-- <th class="text-center">Option</th> -->
              </tr>
            </thead>
            <tbody>
            @foreach($data as $no => $t)
              <tr>
                <td>{{ $no += 1 }}</td>
                <td><a href="/cashier/edit-barang?id={{ $t->id }}" target="_blank">{{ $t->name }}</a></td>
                <!-- <td class="text-center">{{ $t->stok - $t->jumlah_pada_rak }}</td>
                <td class="text-center">{{ $t->jumlah_pada_rak }}</td> -->
                <td class="text-center">{{ $t->stok }}</td>
                <!-- <td class="text-center"><a class="btn btn-primary" href="/cashier/daftar-rak/update/{{ $t->id }}">Tambah Jumlah</a></td> -->
              </tr>
            @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <!--/.col-->
  </div>
  <!--/.row-->
</div>

</div>
<script type="text/javascript" src="/js/gudang.js"></script>
@endsection
