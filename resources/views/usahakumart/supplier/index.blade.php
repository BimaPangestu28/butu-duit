@extends('usahakumart/app')

@section('content')
<!-- Breadcrumb -->
<ol class="breadcrumb">
<li class="breadcrumb-item">Home</li>
<li class="breadcrumb-item">Admin</li>
<li class="breadcrumb-item active">Daftar Supplier Barang</li>

<!-- Breadcrumb Menu-->

</ol>

<div class="container-fluid">

  <div class="card">
    <div class="card-body">
      <div class="row">
        <div class="col-sm-5">
          <h4 class="card-title mb-0">Daftar Supplier Barang</h4>
          <div class="small text-muted">{{date('d M Y')}}</div><br>
        </div>
        <!--/.col-->
        <div class="col-sm-7 d-none d-md-block">
          <button type="button" class="btn btn-primary float-right"><i class="icon-cloud-download"></i></button>
          <div class="btn-toolbar float-right" role="toolbar" aria-label="Toolbar with button groups">
          </div>
        </div>
        <!--/.col-->
      </div>
      @if(session()->has('alert'))
      <div class="row">
      	<div class="alert alert-danger col-sm-12" style="margin-bottom: 0; margin-top: 25px;">
      			<p>{{ session()->get('alert') }}</p>
      	</div>
      </div>
      @endif
    </div>
  </div>
  <!--/.card-->


  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          Usahaku Toserba - Toko Offline <a href="/cashier/supplier/create" class="btn btn-primary btn-sm">Tambah Supplier</a>
        </div>
        <div class="card-body">
          <form method="GET" action="/cashier/supplier/search">
              <input class="form-control" type="text" placeholder="Type to search" style="width: 93.5%; display: inline-block;" name="q"></input>
              <button type="submit" class="btn btn-primary" style="margin-left: -4px; margin-top: -5px;">Search</button>
          </form>
          <br>
          <table class="table table-responsive-sm table-hover table-outline mb-0">
            <thead class="thead-light">
              <tr>
                <th class="text-center">#</th>
                <th class="text-center">Nama Supplier</th>
                @if(session('user-toko')->level == 1)
                <th class="text-center">Option</th>
                @endif
              </tr>
            </thead>
            <tbody>
            @foreach($supplier as $no => $s)
              <tr>
                <td class="text-center">{{ $no += 1 }}</td>
                <td class="text-center">{{ $s->name }}</td>
                @if(session('user-toko')->level == 1)
                <td class="text-center">
                  <a href="/cashier/supplier/edit/{{ $s->id }}" class="btn btn-warning">Edit Supplier</a>
                  <a href="/cashier/supplier/delete/{{ $s->id }}" class="btn btn-danger" onclick="return confirm('Apakah kamu yakin?')">Delete Supplier</a>
                </td>
                @endif
              </tr>
            @endforeach
            </tbody>
          </table>

          @if(!Request::get('q'))
          {{$supplier->render()}}
          @endif
        </div>
      </div>
    </div>
    <!--/.col-->
  </div>
  <!--/.row-->
</div>

</div>
@endsection
