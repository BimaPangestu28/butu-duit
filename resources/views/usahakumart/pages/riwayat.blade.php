@extends('usahakumart/app')

@section('content')
<!-- Breadcrumb -->
<ol class="breadcrumb">
<li class="breadcrumb-item">Home</li>
<li class="breadcrumb-item">Admin</li>
<li class="breadcrumb-item active">Riwayat Transaksi</li>

<!-- Breadcrumb Menu-->

</ol>

<div class="container-fluid">

  <div class="card">
    <div class="card-body">
      <div class="row">
        <div class="col-sm-5">
          <h4 class="card-title mb-0">Riwayat Transaksi</h4>
          <div class="small text-muted">{{date('d M Y')}}</div>
          <br>
          <h5>Total Pendapatan : Rp. {{number_format($pendapatan,0,',','.')}}</h5>
          <h5>Total Pendapatan Hari Ini : Rp. {{number_format($pendapatHari,0,',','.')}}</h5>
        </div>
        <!--/.col-->
        <div class="col-sm-7 d-none d-md-block">
          <button type="button" class="btn btn-primary float-right"><i class="icon-cloud-download"></i></button>
          <div class="btn-toolbar float-right" role="toolbar" aria-label="Toolbar with button groups">
          </div>
        </div>
        <!--/.col-->
      </div>
      @if(session()->has('alert'))
      <div class="row">
      	<div class="alert alert-danger col-sm-12" style="margin-bottom: 0; margin-top: 25px;">
      			<p>{{ session()->get('alert') }}</p>
      	</div>
      </div>
      @endif
    </div>
  </div>
  <!--/.card-->


  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          Usahaku Toserba - Toko Offline
        </div>
        <div class="card-body">
          <table class="table table-responsive-sm table-hover table-outline mb-0">
            <thead class="thead-light">
              <tr>
                <th>Total Pembayaran</th>
                <th class="text-center">Tanggal</th>
              </tr>
            </thead>
            <tbody>
            @foreach($order as $t)
              <tr>
                <td>
                  Rp. {{ number_format($t->total_pembayaran,0,',','.') }}
                </td>
                <td class="text-center">
                  {{ $t->created_at }}
                </td>
              </tr>
            @endforeach
            </tbody>
          </table>

          {{$order->render()}}
        </div>
      </div>
    </div>
    <!--/.col-->
  </div>
  <!--/.row-->
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          Usahaku Toserba - Transaksi Online
        </div>
        <div class="card-body">
          <table class="table table-responsive-sm table-hover table-outline mb-0">
            <thead class="thead-light">
              <tr>
                <th>Tanggal</th>
                <th>Nama Penerima</th>
                <th>Alamat Penerima</th>
                <th>Nomor Hp</th>
                <th class="text-center">Nomor Transaksi</th>
                <th class="text-center">Total Pembayaran</th>
              </tr>
            </thead>
            <tbody>
              @foreach ($trans as $view)
                <tr>
                  <td>{{$view->created_at}}</td>
                  <td>{{$view->penerima}}</td>
                  <td>{{$view->alamat}}</td>
                  <td>{{$view->phone}}</td>
                  <td class="text-center">{{$view->id}}</td>
                  <td class="text-center">Rp. {{ number_format($view->total_belanja,0,',','.') }}</td>
                </tr>
              @endforeach
            </tbody>
          </table>

          {{$trans->render()}}
        </div>
      </div>
    </div>
    <!--/.col-->
  </div>
  <!--/.row-->
</div>

</div>
@endsection
