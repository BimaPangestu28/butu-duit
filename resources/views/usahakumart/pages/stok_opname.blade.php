@extends('usahakumart/app')

@section('content')
<!-- Breadcrumb -->
<ol class="breadcrumb">
<li class="breadcrumb-item">Home</li>
<li class="breadcrumb-item">Admin</li>
<li class="breadcrumb-item active">Gudang barang</li>

<!-- Breadcrumb Menu-->

</ol>

<div class="container-fluid">

  <div class="card">
    <div class="card-body">
      <div class="row">
        <div class="col-sm-5">
          <h4 class="card-title mb-0">Stok Opname
              @if(app('request')->input('key'))
                Barang abjad {{ app('request')->input('key') }}
              @endif
          </h4>
          <div class="small text-muted">{{date('d M Y')}}</div><br>
        </div>
        <!--/.col-->
        <div class="col-sm-7 d-none d-md-block">
          <button type="button" class="btn btn-primary float-right"><i class="icon-cloud-download"></i></button>
          <div class="btn-toolbar float-right" role="toolbar" aria-label="Toolbar with button groups">
          </div>
        </div>
        <!--/.col-->
        <div class="col">
            <h6>Pilih Barang Berdasarkan Abjad</h6>
            <a href="/cashier/stok-opname?key=A" class="btn btn-danger" style="margin:5px 2px;font-size:10px;">A</a>
            <a href="/cashier/stok-opname?key=B" class="btn btn-danger" style="margin:5px 2px;font-size:10px;">B</a>
            <a href="/cashier/stok-opname?key=C" class="btn btn-danger" style="margin:5px 2px;font-size:10px;">C</a>
            <a href="/cashier/stok-opname?key=D" class="btn btn-danger" style="margin:5px 2px;font-size:10px;">D</a>
            <a href="/cashier/stok-opname?key=E" class="btn btn-danger" style="margin:5px 2px;font-size:10px;">E</a>
            <a href="/cashier/stok-opname?key=F" class="btn btn-danger" style="margin:5px 2px;font-size:10px;">F</a>
            <a href="/cashier/stok-opname?key=G" class="btn btn-danger" style="margin:5px 2px;font-size:10px;">G</a>
            <a href="/cashier/stok-opname?key=H" class="btn btn-danger" style="margin:5px 2px;font-size:10px;">H</a>
            <a href="/cashier/stok-opname?key=I" class="btn btn-danger" style="margin:5px 2px;font-size:10px;">I</a>
            <a href="/cashier/stok-opname?key=J" class="btn btn-danger" style="margin:5px 2px;font-size:10px;">J</a>
            <a href="/cashier/stok-opname?key=K" class="btn btn-danger" style="margin:5px 2px;font-size:10px;">K</a>
            <a href="/cashier/stok-opname?key=L" class="btn btn-danger" style="margin:5px 2px;font-size:10px;">L</a>
            <a href="/cashier/stok-opname?key=M" class="btn btn-danger" style="margin:5px 2px;font-size:10px;">M</a>
            <a href="/cashier/stok-opname?key=N" class="btn btn-danger" style="margin:5px 2px;font-size:10px;">N</a>
            <a href="/cashier/stok-opname?key=O" class="btn btn-danger" style="margin:5px 2px;font-size:10px;">O</a>
            <a href="/cashier/stok-opname?key=P" class="btn btn-danger" style="margin:5px 2px;font-size:10px;">P</a>
            <a href="/cashier/stok-opname?key=Q" class="btn btn-danger" style="margin:5px 2px;font-size:10px;">Q</a>
            <a href="/cashier/stok-opname?key=R" class="btn btn-danger" style="margin:5px 2px;font-size:10px;">R</a>
            <a href="/cashier/stok-opname?key=S" class="btn btn-danger" style="margin:5px 2px;font-size:10px;">S</a>
            <a href="/cashier/stok-opname?key=T" class="btn btn-danger" style="margin:5px 2px;font-size:10px;">T</a>
            <a href="/cashier/stok-opname?key=U" class="btn btn-danger" style="margin:5px 2px;font-size:10px;">U</a>
            <a href="/cashier/stok-opname?key=V" class="btn btn-danger" style="margin:5px 2px;font-size:10px;">V</a>
            <a href="/cashier/stok-opname?key=W" class="btn btn-danger" style="margin:5px 2px;font-size:10px;">W</a>
            <a href="/cashier/stok-opname?key=X" class="btn btn-danger" style="margin:5px 2px;font-size:10px;">X</a>
            <a href="/cashier/stok-opname?key=Y" class="btn btn-danger" style="margin:5px 2px;font-size:10px;">Y</a>
            <a href="/cashier/stok-opname?key=Z" class="btn btn-danger" style="margin:5px 2px;font-size:10px;">Z</a>
        </div>
      </div>
      @if(session()->has('alert'))
      <div class="row">
      	<div class="alert alert-danger col-sm-12" style="margin-bottom: 0; margin-top: 25px;">
      			<p>{{ session()->get('alert') }}</p>
      	</div>
      </div>
      @endif
    </div>
  </div>
  <!--/.card-->


  <input type="hidden" id="id_ses" value="{{ session('user-toko')->level }}">

  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          Usahaku Toserba - Daftar Barang SO
          <button data-toggle="modal" data-target="#alfabetThing" class="btn btn-warning btn-sm">Cetak Barang SO</button>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Barcode</th>
                            <th>Nama Produk</th>
                            <th>Harga Satuan</th>
                            <th>Jumlah SO</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($product as $no => $view)
                        <tr>
                            <td>{{ $no+1 }}</td>
                            <td>{{ $view->barcode }}</td>
                            <td>{{ $view->name }}</td>
                            <td>Rp. {{ number_format($view->price,0,',','.') }}</td>
                            <td>
                                <input type="number" name="stok" class="stok_opname" data-opname="{{ $view->id }}" value="{{ $view->stok_opname }}">
                                <span class="text-success" id="status-opname-{{ $view->id }}" style="display:none;">success</span>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>

                @if(app('request')->input('key'))
                  <a href="/cashier/stok-opname/opname?key={{ app('request')->input('key') }}" target="_blank" class="btn btn-warning">Cetak Selisih</a>

                  <form method="POST" action="/cashier/stok-opname/stok-update" style="display:inline-block;">
                    {{csrf_field()}}
                    <input type="hidden" name="key" value="{{ app('request')->input('key') }}">
                    <button type="submit" class="btn btn-danger">Update Data Stok</button>
                  </form>
                  <form method="POST" action="/cashier/stok-opname/stok-adjust" style="display:inline-block;">
                    {{csrf_field()}}
                    <input type="hidden" name="key" value="{{ app('request')->input('key') }}">
                    <button type="submit" class="btn btn-success">Adjustment Data</button>
                  </form>
                @endif
            </div>
        </div>
      </div>
    </div>
    <!--/.col-->
  </div>
  <!--/.row-->
</div>

</div>

<!-- Modal Print -->
<div class="modal" id="alfabetThing" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
          <div class="modal-title">Print Barang Menurut Abjad</div>
      </div>
      <div class="modal-body">
          <a href="/cashier/gudang-barang/print?key=A" target="_blank" class="btn btn-danger" style="margin:10px 5px;">A</a>
          <a href="/cashier/gudang-barang/print?key=B" target="_blank" class="btn btn-danger" style="margin:10px 5px;">B</a>
          <a href="/cashier/gudang-barang/print?key=C" target="_blank" class="btn btn-danger" style="margin:10px 5px;">C</a>
          <a href="/cashier/gudang-barang/print?key=D" target="_blank" class="btn btn-danger" style="margin:10px 5px;">D</a>
          <a href="/cashier/gudang-barang/print?key=E" target="_blank" class="btn btn-danger" style="margin:10px 5px;">E</a>
          <a href="/cashier/gudang-barang/print?key=F" target="_blank" class="btn btn-danger" style="margin:10px 5px;">F</a>
          <a href="/cashier/gudang-barang/print?key=G" target="_blank" class="btn btn-danger" style="margin:10px 5px;">G</a>
          <a href="/cashier/gudang-barang/print?key=H" target="_blank" class="btn btn-danger" style="margin:10px 5px;">H</a>
          <a href="/cashier/gudang-barang/print?key=I" target="_blank" class="btn btn-danger" style="margin:10px 5px;">I</a>
          <a href="/cashier/gudang-barang/print?key=J" target="_blank" class="btn btn-danger" style="margin:10px 5px;">J</a>
          <a href="/cashier/gudang-barang/print?key=K" target="_blank" class="btn btn-danger" style="margin:10px 5px;">K</a>
          <a href="/cashier/gudang-barang/print?key=L" target="_blank" class="btn btn-danger" style="margin:10px 5px;">L</a>
          <a href="/cashier/gudang-barang/print?key=M" target="_blank" class="btn btn-danger" style="margin:10px 5px;">M</a>
          <a href="/cashier/gudang-barang/print?key=N" target="_blank" class="btn btn-danger" style="margin:10px 5px;">N</a>
          <a href="/cashier/gudang-barang/print?key=O" target="_blank" class="btn btn-danger" style="margin:10px 5px;">O</a>
          <a href="/cashier/gudang-barang/print?key=P" target="_blank" class="btn btn-danger" style="margin:10px 5px;">P</a>
          <a href="/cashier/gudang-barang/print?key=Q" target="_blank" class="btn btn-danger" style="margin:10px 5px;">Q</a>
          <a href="/cashier/gudang-barang/print?key=R" target="_blank" class="btn btn-danger" style="margin:10px 5px;">R</a>
          <a href="/cashier/gudang-barang/print?key=S" target="_blank" class="btn btn-danger" style="margin:10px 5px;">S</a>
          <a href="/cashier/gudang-barang/print?key=T" target="_blank" class="btn btn-danger" style="margin:10px 5px;">T</a>
          <a href="/cashier/gudang-barang/print?key=U" target="_blank" class="btn btn-danger" style="margin:10px 5px;">U</a>
          <a href="/cashier/gudang-barang/print?key=V" target="_blank" class="btn btn-danger" style="margin:10px 5px;">V</a>
          <a href="/cashier/gudang-barang/print?key=W" target="_blank" class="btn btn-danger" style="margin:10px 5px;">W</a>
          <a href="/cashier/gudang-barang/print?key=X" target="_blank" class="btn btn-danger" style="margin:10px 5px;">X</a>
          <a href="/cashier/gudang-barang/print?key=Y" target="_blank" class="btn btn-danger" style="margin:10px 5px;">Y</a>
          <a href="/cashier/gudang-barang/print?key=Z" target="_blank" class="btn btn-danger" style="margin:10px 5px;">Z</a>
      </div>
    </div>
  </div>
</div>


<script type="text/javascript" src="/js/opname.js"></script>
@endsection
