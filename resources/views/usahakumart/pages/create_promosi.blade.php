@extends('usahakumart/app')

@section('content')
<!-- Breadcrumb -->
<ol class="breadcrumb">
<li class="breadcrumb-item">Home</li>
<li class="breadcrumb-item">Admin</li>
<li class="breadcrumb-item active">Tambah Promo</li>

<!-- Breadcrumb Menu-->

</ol>

<div class="container-fluid">

  <div class="card">
    <div class="card-body">
      <div class="row">
        <div class="col-sm-5">
          <h4 class="card-title mb-0">Tambah Promo</h4>
          <div class="small text-muted">{{date('d M Y')}}</div>
        </div>
        <!--/.col-->
        <div class="col-sm-7 d-none d-md-block">
          <button type="button" class="btn btn-primary float-right"><i class="icon-cloud-download"></i></button>
          <div class="btn-toolbar float-right" role="toolbar" aria-label="Toolbar with button groups">
          </div>
        </div>
        <!--/.col-->
      </div>
      @if($errors->count() > 0 || session()->has('alert'))
      <div class="row">
      	<div class="alert alert-danger col-sm-12" style="margin-bottom: 0; margin-top: 25px;">
      		@if($errors->count() > 0)
      			@foreach($errors->all() as $error)
      			<p>{{ $error }}</p>
      			@endforeach
      		@else
      			<p>{{ session()->get('alert') }}</p>
      		@endif
      	</div>
      </div>
      @endif
    </div>
  </div>
  <!--/.card-->


  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          Usahaku Toserba - Toko Offline
        </div>
        <div class="card-body">
          <form method="POST" action="/cashier/promosi-barang/tambah" enctype="multipart/form-data">
          	{{ csrf_field() }}

          	<div class="form-group">
          		<strong>Judul Promo</strong><br>
          		<input type="text" name="judul_promo" class="form-control" style="margin-top: 10px;" value="{{ old('judul_promo') }}">
          	</div>

            <div class="form-group">
              <strong>Diskon Yang Diberikan (Berlaku Kepada Semua Barang) berupa angka 1-100 tanpa persen (%)</strong><br>
              <input type="number" name="diskon" class="form-control" style="margin-top: 10px;" value="{{ old('diskon') }}" min="0" max="100">
            </div>

            <div class="form-group">
              <strong>Banner Promo</strong><br>
              <input type="file" name="gambare" class="form-control" style="margin-top: 10px;">
            </div>

            <div class="form-group">
              <strong>Barang Yang Akan Dipromosikan (Ceklist Barang Yang Dipilih)</strong><br><br>

              <input type="text" id="product_text" class="form-control" placeholder="Ketik untuk mencari produk"><br>
              <table class="table table-responsive-sm table-hover table-outline mb-0">
            <thead class="thead-light">
              <tr>
                <th>#</th>
                <th>Nama Barang</th>
                <th class="text-center">Harga Modal</th>
                <th class="text-center">Harga Jual</th>
                <th class="text-center">Affiliasi</th>
                <th class="text-center">Barcode</th>
                <th class="text-center">Stok Barang</th>
              </tr>
            </thead>
            <tbody id="search">
            @foreach($product as $t)
              <tr id="{{ $t->name }}">
                <td><input type="checkbox" name="id_product[]" value="{{ $t->id }}"></td>
                <td>{{ $t->name }}</td>
                <td class="text-center">Rp. {{ number_format($t->price,0,',','.') }}</td>
                <td class="text-center">Rp. {{ number_format($t->harga_jual,0,',','.') }}</td>
                <td class="text-center">Rp. {{ number_format($t->affiliasi,0,',','.') }}</td>
                <td class="text-center">{{ $t->barcode }}</td>
                <td class="text-center">{{ $t->stok }}</td>
              </tr>
            @endforeach
            </tbody>
          </table>
            </div>

          	<br><br>

          	<input type="submit" class="btn btn-primary form-control" value="Tambah Promo">
          </form>
        </div>
      </div>
    </div>
    <!--/.col-->
  </div>
  <!--/.row-->
</div>

</div>

<script type="text/javascript" src="/js/promosi.js"></script>

@endsection
