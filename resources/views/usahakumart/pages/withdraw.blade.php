@extends('usahakumart/app')

@section('content')
<!-- Breadcrumb -->
<ol class="breadcrumb">
<li class="breadcrumb-item">Home</li>
<li class="breadcrumb-item">Admin</li>
<li class="breadcrumb-item active">Request Withdraw</li>

<!-- Breadcrumb Menu-->

</ol>

<div class="container-fluid">

  <div class="card">
    <div class="card-body">
      <div class="row">
        <div class="col-sm-5">
          <h4 class="card-title mb-0">Request Withdraw</h4>
          <div class="small text-muted">{{date('d M Y')}}</div>
        </div>
        <!--/.col-->
        <div class="col-sm-7 d-none d-md-block">
          <button type="button" class="btn btn-primary float-right"><i class="icon-cloud-download"></i></button>
          <div class="btn-toolbar float-right" role="toolbar" aria-label="Toolbar with button groups">
          </div>
        </div>
        <!--/.col-->
      </div>
      @if(session()->has('alert'))
      <div class="row">
      	<div class="alert alert-danger col-sm-12" style="margin-bottom: 0; margin-top: 25px;">
      			<p>{{ session()->get('alert') }}</p>
      	</div>
      </div>
      @endif
    </div>
  </div>
  <!--/.card-->


  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          Usahaku Toserba - Toko Offline
        </div>
        <div class="card-body">
          <form method="POST" action="/cashier/request-withdraw">
            {{ csrf_field() }}

            <div class="form-group">
              <h5>TOTAL SALDO WITHDRAW  &nbsp; &nbsp; : &nbsp; &nbsp;   Rp. {{ number_format($saldo->saldo_withdraw,0,',','.') }}</h5>
            </div>

            <input type="submit" class="btn btn-primary form-control" value="Request Withdraw">
          </form>
          <br>
          <table class="table table-responsive-sm table-hover table-outline mb-0">
            <thead class="thead-light">
              <tr>
                <th class="text-center">Tanggal</th>
                <th class="text-center">Jumlah Withdraw</th>
                <th class="text-center">Status</th>
              </tr>
            </thead>
            <tbody>
              @foreach($withdraw as $w)
              <tr>
                <td class="text-center">{{ date('d-m-Y', strtotime($w->created_at)) }}</td>
                <td class="text-center">{{ $w->jumlah_withdraw }}</td>
                <td class="text-center">
                  @if($w->status == 0)
                    Menunggu Konfirmasi Admin
                  @else
                    Request Telah Dikonfirmasi
                  @endif
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <!--/.col-->
  </div>
  <!--/.row-->
</div>

</div>
@endsection
