@extends('usahakumart/app')

@section('content')
<!-- Breadcrumb -->
<ol class="breadcrumb">
<li class="breadcrumb-item">Home</li>
<li class="breadcrumb-item">Admin</li>
<li class="breadcrumb-item active">Request & Stok Kartu</li>

<!-- Breadcrumb Menu-->

</ol>

<div class="container-fluid">

  <div class="card">
    <div class="card-body">
      <div class="row">
        <div class="col-sm-5">
          <h4 class="card-title mb-0">Request & Stok Kartu</h4>
          <div class="small text-muted">{{date('d M Y')}}</div>
        </div>
        <!--/.col-->
        <div class="col-sm-7 d-none d-md-block">
          <button type="button" class="btn btn-primary float-right"><i class="icon-cloud-download"></i></button>
          <div class="btn-toolbar float-right" role="toolbar" aria-label="Toolbar with button groups">
          </div>
        </div>
        <!--/.col-->
      </div>
      @if(session()->has('alert'))
      <div class="row">
      	<div class="alert alert-danger col-sm-12" style="margin-bottom: 0; margin-top: 25px;">
      			<p>{{ session()->get('alert') }}</p>
      	</div>
      </div>
      @endif
    </div>
  </div>
  <!--/.card-->


  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          Usahaku Toserba - Toko Offline
        </div>
        <div class="card-body">
          <form method="POST" action="/cashier/stok-kartu" enctype="multipart/form-data">
            @if(Auth::user()->id_tipe == 2)
            <strong>Untuk melakukan permintaan stok kartu anda silahkan transfer ke rekening PT.Usahaku Bisnis Nusantara.</strong> <br> <br>  
            <div style="width: 50%;">
                <a href="/images/bank.jpeg" target="_blank">
                  <img src="/images/bank.jpeg" style="width: 100%; height: 100%; object-fit: cover;">
                </a>
            </div>
            <br>  <br>  
            @endif
            {{ csrf_field() }}

            <div class="form-group">
              <strong>Jumlah Kartu (Stok Kartu Yang Tersisa Adalah {{ $sisa }})</strong><br>
              <input type="number" name="jumlah_kartu" class="form-control" style="margin-top: 10px;" value="{{ old('number_card') }}" max="{{ $sisa }}">
            </div>

            @if(Auth::user()->id_tipe == 2)
            <div class="form-group">
              <strong>Bukti Transfer</strong><br>
              <input type="file" name="gambare" class="form-control" style="margin-top: 10px;">
            </div>
            @endif

            <input type="submit" class="btn btn-primary form-control" value="Kirim Request Kartu">
          </form>
          <br>
          <hr>
          <br>
          <!-- <form method="GET" action="/usahakumart/search-barang" >
              <input class="form-control" type="text" placeholder="Type to search" style="width: 93.5%; display: inline-block;" name="q"></input>
              <button type="submit" class="btn btn-primary" style="margin-left: -4px; margin-top: -5px;">Search</button>
          </form>
          <br> -->
          <form method="GET" action="/cashier/search-kartu" >
              <input class="form-control" type="text" placeholder="Type to search" style="width: 93.5%; display: inline-block;" name="q"></input>
              <button type="submit" class="btn btn-primary" style="margin-left: -4px; margin-top: -5px;">Search</button>
          </form>
          <br>
          <table class="table table-responsive-sm table-hover table-outline mb-0">
            <thead class="thead-light">
              <tr>
                <th class="text-center">Nomer Kartu</th>
                <th class="text-center">Status Kartu</th>
              </tr>
            </thead>
            <tbody>
            @foreach($stok as $t)
              <tr>
                <td class="text-center">{{ $t->number_card }}</td>
                <td class="text-center">
                  @if($t->status == 1)
                    Aktif
                  @else
                    Tidak Aktif
                  @endif
                </td>
              </tr>
            @endforeach
            </tbody>
          </table>

          {{$stok->render()}}
        </div>
      </div>
    </div>
    <!--/.col-->
  </div>
  <!--/.row-->
</div>

</div>
@endsection
