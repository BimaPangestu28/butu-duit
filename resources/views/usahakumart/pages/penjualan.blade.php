@extends('usahakumart.app')
# @Author: Bima Pangestu
# @Date:   2018-05-29T02:59:22+07:00
# @Email:  bimapangestu280@gmail.com
# @Filename: penjualan.blade.php
# @Last modified by:   Bima Pangestu
# @Last modified time: 2018-05-29T11:19:24+07:00




@section('content')


<!-- Breadcrumb -->
<ol class="breadcrumb">
	@if(Auth::user()->id_tipe == 2)
	<b>Jumlah Saldo Deposit Rp. {{ number_format($data->saldo_deposit,0,',','.') }}</b>
	@endif
</ol>

@if(app('request')->input('type'))
<button class="btn btn-warning form-control" id="close-tab" style="color: white;">TUTUP TAB</button> <br><br>
@endif

@if(session('user-toko')->level == 1)
<a class="btn btn-primary form-control" style="color: white;" target="_blank" href="/cashier/penjualan?type=custom">CUSTOM KASIR</a><br><br>
@endif

<a class="btn btn-danger form-control" style="color: white;" target="_blank" href="/cashier/penjualan?type=hold">BUKA TAB BARU</a> <br><br>

<div class="col-lg-9" style="float: left;">
	<div class="card">
        <div class="card-header">
          <i class="fa fa-align-justify"></i> Keterangan Belanja
        </div>
        <div class="card-body" id="body-penjualan">
          <table class="table table-striped">
            <thead>
              <tr>
                <th>Nama</th>
                <th>Jumlah</th>
                <th>Harga</th>
                <th>Affiliasi</th>
                <th>Diskon</th>
                <th>Potongan</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody id="barang">

            </tbody>
          </table>
        </div>
  </div>
</div>
<!-- fade -->
<div class="modal fade" id="modalCart" tabindex="-1" role="dialog" aria-labelledby="modalCart" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Keranjang Belanja</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal" style="color: white;">Close</button>
        <a id="pembayaran" class="btn btn-primary" style="color: white;">Lanjut Pembayaran</a>
      </div>
    </div>
  </div>
</div>

<div class="loader-animation" style="display:none;">
  <img src="/loader/produk.gif">
</div>

<div class="col-lg-3" style="float: left;">
	<form method="POST" id="submit" autocomplete="off">
		{{ csrf_field() }}
		<input type="text" name="barcode" id="barcode" class="form-control" placeholder="Barcode atau Nama Produk">
		<div id="search-results">
			<div class="this-result"></div>
		</div>
	</form>
  <br>
  <form method="POST" id="number_cek" autocomplete="off">
    {{ csrf_field() }}
    <input type="text" name="member_card" id="member_card" class="form-control" placeholder="Nomer Kartu Member (Jika Memiliki)">
  </form>
  <br>
  <hr>
	<form method="POST" action="/cashier/beli" id="beli_langsung">
		{{ csrf_field() }}
    <input type="hidden" name="total_potongan" id="potongan" class="form-control" placeholder="Total Potongan" value="0"><br>
    @if(app('request')->input('type') == 'custom')
    <input type="date" name="created_at" id="created_at" class="form-control"><br>
    @endif
    <select class="form-control" name="type">
      <option value="tunai">Tunai</option>
      <option value="debet">Debet</option>
    </select><br>
    <input type="text" name="pembayaran" id="pembayaran_beli" class="form-control" placeholder="Total pembayaran">
		<input type="hidden" name="product_id" id="id"><br>
    <h3 id="total" style="font-size: 18px;">Total Harga Rp. 0</h3>
		<h3 id="total-potongan" style="font-size: 18px;">Total Potongan Harga Rp. 0</h3>
		<h3 style="font-size: 18px;">Total Kembalian Rp. <span id="total-kembalian-uang">0</span></h3>
    <input type="hidden" data-real="0" name="total" id="total-semua">
		<input type="hidden" name="barang_bonus" id="barang-bonus">
    <input type="hidden" name="total_affiliasi" id="total_affiliasi">
		<br>
		<input type="submit" value="BELI" class="form-control btn btn-primary" id="beli" onclick="return confirm('Apakah total pembayaran sudah benar?')">
	</form>
</div>


  <div id="listPenjualan" style="display: none">
    <div style="font-size:8px;">
      <center>
        <strong style="font-size:12px;">PT. USAHAKU BISNIS NUSANTARA</strong>
        <br>
        <span>Jl. Anwar Perum Metro Indah, Ganjar Asri, Metro Barat, Metro</span>
        <br>
        <span>NPWP : 82.216.058.6-321.000</span>
        <br>
        <b>USAHAKU TOSERBA</b> | <span style="text-transform:uppercase;">{{Auth::user()->name}}</span>
        <br>
        {{-- <span>{{Auth::user()->alamat}}</span> --}}
        <span>Jln. Raya Pekalongan, Kec. Pekalongan, Lampung Timur</span>
      </center>
      <br>
      <br>
    <div style="margin-bottom: 3px; margin-top: 0;"><span id="no_transaksi">No. Transaksi {{$no_tran}}</span><br>
    <span id="nama_kasir">Kasir : {{session('user-toko')->nama}}</span></div>
      <br>
      <br>
      <div id="list-barang" style="margin-bottom: 0">

      </div>
      <hr style="border:1px dotted black;">
      <div style="margin-bottom: 3px;">Total Belanja<span style="float:right;" id="total_semua_struk"></span></div>
      <div style="margin-bottom: 3px; margin-top: 0;">Total Potongan<span style="float:right;" id="total_semua_struk_potongan">0</span></div>
      <div style="margin-bottom: 3px; margin-top: 0;">Total Jumlah<span style="float:right;" id="total_semua_struk_jumlah"></span></div>
      <div style="margin-bottom: 3px; margin-top: 0;">Total Dibayar<span style="float:right;" id="total_semua_struk_dibayar"></span></div>
      <div style="margin-bottom: 3px; margin-top: 0;">Total Kembalian<span style="float:right;" id="total_semua_struk_kembalian"></span></div>
      <br>
      <span>Tanggal : {{date('d/m/Y h:i:s', strtotime(now()))}}</span>
      <div id="more"></div>
    </div>
    <br>
    <div style="border:2px dotted black; padding:7px; font-size:10px;">
      Terimakasih Telah Berbelanja
    </div>
    <br>
    <div style="font-size: 10px;">
    website : toserba.usahaku.co.id
    </div>
    <!-- <small style="font-size:2px; margin-top:30px; display:inline-block;">{{date(now())}}</small> -->
    <hr style="height:1px; width:1px; background:white; margin-top:50px;">
    <br>
  </div>

<script type="text/javascript" src="/admin/js/livesearch.js"></script>
<script>
  $(document).ready(function () {
		var msg = '{{Session::get('alert')}}';
	  var exist = '{{Session::has('alert')}}';
	  if(exist){
	    alert(msg);
    }

    $(document).on('click', '#thisOne', function () {
			var code = $(this).attr('barCodeList');
			$('#barcode').val(code);
    });

    $(document).on('keyup', '#pembayaran_beli, #barcode', function () {
       var dibayar = $('#pembayaran_beli').val();
       var total_belanja = $('#total').attr('total');
       var total_kembalian = parseInt(dibayar - total_belanja).toLocaleString();
       $('#total-kembalian-uang').text(total_kembalian);
    });

  });
</script>
@endsection
