@extends('usahakumart.app')

@section('content')


<!-- Breadcrumb -->
<ol class="breadcrumb">
<li class="breadcrumb-item">Home</li>
<li class="breadcrumb-item">Admin</li>
<li class="breadcrumb-item active">Dashboard</li>

<!-- Breadcrumb Menu-->

</ol>

<div class="container-fluid">

    @if($errors->count() > 0 || session()->has('alert'))
    <div class="row">
      <div class="alert alert-danger col-sm-12" style="margin-bottom: 0; margin-top: 25px;">
        @if($errors->count() > 0)
          @foreach($errors->all() as $error)
          <p>{{ $error }}</p>
          @endforeach
        @else
          <p>{{ session()->get('alert') }}</p>
        @endif
      </div>
    </div>
    <br>
    <br>
    @endif

  <div class="card">
    <div class="card-body">
      <div class="row">
        <div class="col-sm-5">
          <h4 class="card-title mb-0">Usahaku Toserba</h4>
          <div class="small text-muted">{{ $toko->name }}</div><br>
          <!-- <a href="/cashier/laporan-harian" class="btn btn-primary" style="color: white;">Download Laporan Hari Ini</a>
          <a href="/cashier/laporan-bulanan" class="btn btn-warning" style="color: white;">Download Laporan Bulan Ini</a> -->
        </div>
        <!--/.col-->
        <div class="col-sm-7 d-none d-md-block">
          <button type="button" class="btn btn-primary float-right"><i class="icon-cloud-download"></i></button>
          <div class="btn-toolbar float-right" role="toolbar" aria-label="Toolbar with button groups">
          </div>
        </div>
        <!--/.col-->
      </div>
    </div>
  </div>

  @if(session('user-toko')->level == 1)

  <div class="card">
    <div class="card-header">
      Pendapatan Dalam Seminggu
    </div>
    <div class="card-body">
      <table class="table table-responsive-sm table-hover table-outline mb-0">
        <thead class="thead-light">
          <tr>
            <th>Tanggal</th>
            <th>Jumlah Pendapatan</th>
          </tr>
        </thead>
        <tbody id="minggu">

        </tbody>
      </table>

      <h3 id="loading" style="text-align: center; margin-top: 25px;">Loading...</h3>
    </div>
  </div>

  <div class="card">
    <div class="card-header">
      Pendapatan Dalam Sebulan
    </div>
    <div class="card-body">
      <table class="table table-responsive-sm table-hover table-outline mb-0">
        <thead class="thead-light">
          <tr>
            <th>Tanggal</th>
            <th>Jumlah Pendapatan</th>
          </tr>
        </thead>
        <tbody id="bulan">

        </tbody>
      </table>

      <h3 id="loading-bulan" style="text-align: center; margin-top: 25px;">Loading...</h3>
    </div>
  </div>


  <div class="card">
    <div class="card-header">
      Produk Terpopuler ( Produk Terlaris )
    </div>
    <div class="card-body">
      <table class="table table-responsive-sm table-hover table-outline mb-0">
        <thead class="thead-light">
          <tr>
            <th>Nama Produk</th>
            <th>Terjual Hari Ini</th>
            <th>Terjual Bulan Ini</th>
            <th>Terjual Tahun Ini</th>
            <th>Jumlah Terjual</th>
            <th>Stok Barang</th>
            <th>Jumlah Total Harga Barang</th>
          </tr>
        </thead>
        <tbody>
        @foreach($posts as $t)
          <tr>
            <td>{{ $t->name }}</td>
            <td class="text-center">{{ \App\Http\Controllers\CashierController::today($t->id) }}</td>
            <td class="text-center">{{ \App\Http\Controllers\CashierController::month($t->id) }}</td>
            <td class="text-center">{{ \App\Http\Controllers\CashierController::year($t->id) }}</td>
            <td class="text-center">{{ $t->terjual }}</td>
            <td class="text-center">{{ $t->stok }}</td>
            <td class="text-center">Rp. {{ number_format($t->stok * $t->harga_jual,0,',','.') }}</td>
          </tr>
        @endforeach
        </tbody>
      </table>

      {{$posts->render()}}
    </div>
  </div>
  <!--/.card-->
  @endif

</div>
<!-- /.conainer-fluid -->

<script type="text/javascript" src="/js/dashboard.js"></script>

@endsection
