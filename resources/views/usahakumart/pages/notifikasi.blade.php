@extends('usahakumart/app')

@section('content')
<!-- Breadcrumb -->
<ol class="breadcrumb">
<li class="breadcrumb-item">Home</li>
<li class="breadcrumb-item">Admin</li>
<li class="breadcrumb-item active">Notifikasi</li>

<!-- Breadcrumb Menu-->

</ol>

<div class="container-fluid">

  @if(session()->has('alert'))
  <div class="card">
    <div class="card-body">
      <div class="row">
      	<div class="alert alert-danger col-sm-12" style="margin-bottom: 0; margin-top: 25px;">
      			<p>{{ session()->get('alert') }}</p>
      	</div>
      </div>
    </div>
  </div>
  @endif
  <!--/.card-->


  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          Usahaku Toserba - Notifikasi Pembeli Online
        </div>
        <div class="card-body">
          <table class="table table-responsive-sm table-hover table-outline mb-0">
            <thead class="thead-light">
              <tr>
                <th>Tanggal</th>
                <th>Nama Penerima</th>
                <th>Alamat Penerima</th>
                <th>Nomor Hp</th>
                <th class="text-center">Nomor Transaksi</th>
                <th class="text-center">Total Pembayaran</th>
                <th class="text-center">Metode Pembayaan</th>
                <th class="text-center">Option</th>
              </tr>
            </thead>
            <tbody>
              @foreach ($trans as $view)
                <tr>
                  <td>{{$view->created_at}}</td>
                  <td>{{$view->penerima}}</td>
                  <td>{{$view->alamat}}</td>
                  <td>{{$view->phone}}</td>
                  <td class="text-center">{{$view->id}}</td>
                  <td class="text-center">Rp. {{ number_format($view->total_belanja,0,',','.') }}</td>
                  <td class="text-center">{{ $view->metode_pembayaran }}</td>
                  <td>
                    <button type="button" href="#" class="btn btn-primary list" data-id="{{ $view->id }}" data-toggle="modal" data-target="#listModal">Lihat Daftar Belanja</button><br><br>
                    <a type="button" href="/cashier/notifikasi/confirm/{{ $view->id }}?type={{ $view->metode_pembayaran }}" style="color:white;" class="btn btn-warning" data-id="{{ $view->id }}" onclick="return confirm('Apakah anda yakin?')">Konfirmasi Pesanan</a>
                  </td>
                </tr>
              @endforeach
            </tbody>
          </table>

        </div>
      </div>
    </div>
    <!--/.col-->
  </div>
  <!--/.row-->
</div>

<div class="modal fade" id="listModal" tabindex="-1" role="dialog" aria-labelledby="listModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">List Pesanan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <table class="table">
				  <thead>
				    <tr>
				      <th>Nama Barang</th>
				      <th>Jumlah</th>
				    </tr>
				  </thead>
				  <tbody id="thisCart">

				  </tbody>
				</table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="print">Print Struk</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div id="listPenjualan" style="display:none;">
  <div style="font-size:12px;">
    <center>
      <strong style="font-size:16px;">PT. USAHAKU BISNIS NUSANTARA</strong>
      <br>
      <span>Jl. Anwar Perum Metro Indah, Ganjar Asri, Metro Barat, Metro</span>
      <br>
      <span>NPWP : 82.216.058.6-321.000</span>
      <br>
      <b>USAHAKU TOSERBA</b> | <span style="text-transform:uppercase;">{{Auth::user()->name}}</span>
      <br>
      {{-- <span>{{Auth::user()->alamat}}</span> --}}
      <span>Jln. Raya Pekalongan, Kec. Pekalongan, Lampung Timur</span>
    </center>
    <br>
    <br>
  <div style="margin-bottom: 3px; margin-top: 0;"><span id="no_transaksi">No. Transaksi <b id="number_trans"></b></span><br>
  <span id="nama_kasir">Kasir : {{session('user-toko')->nama}}</span></div>      
    <br>
    <br>
    <div id="list-barang" style="margin-bottom: 0">

    </div>
    <hr style="border:1px dotted black;">
    <div style="margin-bottom: 3px;">Total Belanja<span style="float:right;" id="total_semua_struk"></span></div>
    <div style="margin-bottom: 3px; margin-top: 0;">Total Potongan<span style="float:right;" id="total_semua_struk_potongan">0</span></div>
    <div style="margin-bottom: 3px; margin-top: 0;">Total Jumlah<span style="float:right;" id="total_semua_struk_jumlah"></span></div>
    <div style="margin-bottom: 3px; margin-top: 0;">Total Dibayar<span style="float:right;" id="total_semua_struk_dibayar"></span></div>
    <div style="margin-bottom: 3px; margin-top: 0;">Total Kembalian<span style="float:right;" id="total_semua_struk_kembalian">0</span></div>
    <br>
    <span>Tanggal : {{date('d/m/Y h:i:s', strtotime(now()))}}</span>
    <div id="more"></div>
  </div>  
  <br>
  <div style="border:2px dotted black; padding:10px; font-size:12px;">
    Terimakasih Telah Berbelanja
  </div>
  <br>
  <div>
  website : toserba.usahaku.co.id
  </div>
  <!-- <small style="font-size:2px; margin-top:30px; display:inline-block;">{{date(now())}}</small> -->
  <hr style="height:1px; width:1px; background:white; margin-top:50px;">
  <br>
</div>>

<script type="text/javascript" src="/js/notif.js"></script>
</div>
@endsection
