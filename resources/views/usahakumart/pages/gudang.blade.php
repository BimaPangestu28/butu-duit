@extends('usahakumart/app')

@section('content')
<!-- Breadcrumb -->
<ol class="breadcrumb">
<li class="breadcrumb-item">Home</li>
<li class="breadcrumb-item">Admin</li>
<li class="breadcrumb-item active">Gudang barang</li>

<!-- Breadcrumb Menu-->

</ol>

<div class="container-fluid">

  <div class="card">
    <div class="card-body">
      <div class="row">
        <div class="col-sm-5">
          <h4 class="card-title mb-0">Gudang barang</h4>
          <div class="small text-muted">{{date('d M Y')}}</div><br>
          
          <div>
            <h5>Total Nilai Barang Rp. {{ number_format($total_nilai_barang->tot,0,',','.') }}</h5>
          </div>

          <form action="/cashier/print-label">
            <input type="hidden" name="id_product" id="id">
            <select name="type" class="form-control" style="width: 200px; display: inline-block;">
              <option value="0">Label Kertas</option>
              <option value="1">Label Barcode</option>
              <option value="2">Label Barcode Kecil</option>
            </select>
            <select name="supplier" class="form-control" id="supplier" style="width: 210px; display: inline-block;">
              <option value="">-- Pilih Supplier --</option>
              @foreach($dataSup as $da)
              <option value="{{ $da->supplier }}">{{ $da->supplier }}</option>
              @endforeach
            </select><br><br>
            <button href="/cashier/print-label" class="btn btn-primary form-control">Download Label Product</button>  
          </form>
        </div>
        <!--/.col-->
        <div class="col-sm-7 d-none d-md-block">
          <button type="button" class="btn btn-primary float-right"><i class="icon-cloud-download"></i></button>
          <div class="btn-toolbar float-right" role="toolbar" aria-label="Toolbar with button groups">
          </div>
        </div>
        <!--/.col-->
      </div>
      @if(session()->has('alert'))
      <div class="row">
      	<div class="alert alert-danger col-sm-12" style="margin-bottom: 0; margin-top: 25px;">
      			<p>{{ session()->get('alert') }}</p>
      	</div>
      </div>
      @endif
    </div>
  </div>
  <!--/.card-->


  <input type="hidden" id="id_ses" value="{{ session('user-toko')->level }}">

  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          Usahaku Toserba - Toko Offline <button id="check-all" class="btn btn-primary btn-sm">Ceklist Semua</button>
          <button data-toggle="modal" data-target="#alfabetThing" class="btn btn-warning btn-sm">Cetak Barang</button>
          <a href="/cashier/gudang-barang/duplikat" class="btn btn-danger btn-sm">Cari Barang Duplikat</a>
        </div>
        <div class="card-body">
          <form method="GET" action="/cashier/search-barang" >
              <input class="form-control" type="text" placeholder="Type to search" style="width: 93.5%; display: inline-block;" name="q" id="product_text"></input>
              <button type="submit" class="btn btn-primary" style="margin-left: -4px; margin-top: -5px;">Search</button>
          </form>
          <br>
          <table class="table table-responsive-sm table-hover table-outline mb-0">
            <thead class="thead-light">
              <tr>
                <th>#</th>
                <th>Nama Barang</th>
                <th class="text-center">Harga Modal</th>
                <th class="text-center">Harga Jual</th>
                <th class="text-center">Affiliasi</th>
                <th class="text-center">Barcode</th>
                <th class="text-center">Stok Barang</th>
                @if(session('user-toko')->level == 1)
                <th class="text-center">Option</th>
                @endif
              </tr>
            </thead>
            <tbody id="barangApi">
            @foreach($product as $t)
              <tr id="{{ $t->name }}">
                <td><input type="checkbox" class="id_product" value="{{ $t->id }}"></td>
                <td>{{ $t->name }}</td>
                <td class="text-center">Rp. {{ number_format($t->price,0,',','.') }}</td>
                <td class="text-center">Rp. {{ number_format($t->harga_jual,0,',','.') }}</td>
                <td class="text-center">{{ $t->affiliasi }}</td>
                <td class="text-center">{{ $t->barcode }}</td>
                <td class="text-center">{{ $t->stok }}</td>
                @if(session('user-toko')->level == 1)
                <td class="text-center">
                  <a class="btn-danger btn" onclick="return confirm('Anda yakin?')" href="/cashier/hapus-barang?id={{ $t->id }}" style="color: white;">Hapus Barang</a><br><br>
                  <a class="btn-warning btn" href="/cashier/edit-barang?id={{ $t->id }}" style="color: white;">Edit Barang</a>
                </td>
                @endif
              </tr>
            @endforeach
            </tbody>
          </table>

          @if(!Request::get('q'))
          {{$product->render()}}
          @endif
        </div>
      </div>
    </div>
    <!--/.col-->
  </div>
  <!--/.row-->
</div>

</div>

<!-- Modal Print -->
<div class="modal" id="alfabetThing" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
          <div class="modal-title">Print Barang Menurut Abjad</div>
      </div>
      <div class="modal-body">
          <a href="/cashier/gudang-barang/print?key=A" class="btn btn-danger" style="margin:10px 5px;">A</a>
          <a href="/cashier/gudang-barang/print?key=B" class="btn btn-danger" style="margin:10px 5px;">B</a>
          <a href="/cashier/gudang-barang/print?key=C" class="btn btn-danger" style="margin:10px 5px;">C</a>
          <a href="/cashier/gudang-barang/print?key=D" class="btn btn-danger" style="margin:10px 5px;">D</a>
          <a href="/cashier/gudang-barang/print?key=E" class="btn btn-danger" style="margin:10px 5px;">E</a>
          <a href="/cashier/gudang-barang/print?key=F" class="btn btn-danger" style="margin:10px 5px;">F</a>
          <a href="/cashier/gudang-barang/print?key=G" class="btn btn-danger" style="margin:10px 5px;">G</a>
          <a href="/cashier/gudang-barang/print?key=H" class="btn btn-danger" style="margin:10px 5px;">H</a>
          <a href="/cashier/gudang-barang/print?key=I" class="btn btn-danger" style="margin:10px 5px;">I</a>
          <a href="/cashier/gudang-barang/print?key=J" class="btn btn-danger" style="margin:10px 5px;">J</a>
          <a href="/cashier/gudang-barang/print?key=K" class="btn btn-danger" style="margin:10px 5px;">K</a>
          <a href="/cashier/gudang-barang/print?key=L" class="btn btn-danger" style="margin:10px 5px;">L</a>
          <a href="/cashier/gudang-barang/print?key=M" class="btn btn-danger" style="margin:10px 5px;">M</a>
          <a href="/cashier/gudang-barang/print?key=N" class="btn btn-danger" style="margin:10px 5px;">N</a>
          <a href="/cashier/gudang-barang/print?key=O" class="btn btn-danger" style="margin:10px 5px;">O</a>
          <a href="/cashier/gudang-barang/print?key=P" class="btn btn-danger" style="margin:10px 5px;">P</a>
          <a href="/cashier/gudang-barang/print?key=Q" class="btn btn-danger" style="margin:10px 5px;">Q</a>
          <a href="/cashier/gudang-barang/print?key=R" class="btn btn-danger" style="margin:10px 5px;">R</a>
          <a href="/cashier/gudang-barang/print?key=S" class="btn btn-danger" style="margin:10px 5px;">S</a>
          <a href="/cashier/gudang-barang/print?key=T" class="btn btn-danger" style="margin:10px 5px;">T</a>
          <a href="/cashier/gudang-barang/print?key=U" class="btn btn-danger" style="margin:10px 5px;">U</a>
          <a href="/cashier/gudang-barang/print?key=V" class="btn btn-danger" style="margin:10px 5px;">V</a>
          <a href="/cashier/gudang-barang/print?key=W" class="btn btn-danger" style="margin:10px 5px;">W</a>
          <a href="/cashier/gudang-barang/print?key=X" class="btn btn-danger" style="margin:10px 5px;">X</a>
          <a href="/cashier/gudang-barang/print?key=Y" class="btn btn-danger" style="margin:10px 5px;">Y</a>
          <a href="/cashier/gudang-barang/print?key=Z" class="btn btn-danger" style="margin:10px 5px;">Z</a>
      </div>
    </div>
  </div>
</div>


<script type="text/javascript" src="/js/gudang.js"></script>
@endsection
