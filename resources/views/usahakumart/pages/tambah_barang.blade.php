@extends('usahakumart/app')

@section('content')
<!-- Breadcrumb -->
<ol class="breadcrumb">
<li class="breadcrumb-item">Home</li>
<li class="breadcrumb-item">Admin</li>
<li class="breadcrumb-item active">Tambah Barang</li>

<!-- Breadcrumb Menu-->

</ol>

<div class="container-fluid">

  <div class="card">
    <div class="card-body">
      <div class="row">
        <div class="col-sm-5">
          <h4 class="card-title mb-0">Tambah Barang</h4>
          <div class="small text-muted">{{date('d M Y')}}</div>
        </div>
        <!--/.col-->
        <div class="col-sm-7 d-none d-md-block">
          <button type="button" class="btn btn-primary float-right"><i class="icon-cloud-download"></i></button>
          <div class="btn-toolbar float-right" role="toolbar" aria-label="Toolbar with button groups">
          </div>
        </div>
        <!--/.col-->
      </div>
      @if($errors->count() > 0 || session()->has('alert'))
      <div class="row">
      	<div class="alert alert-danger col-sm-12" style="margin-bottom: 0; margin-top: 25px;">
      		@if($errors->count() > 0)
      			@foreach($errors->all() as $error)
      			<p>{{ $error }}</p>
      			@endforeach
      		@else
      			<p>{{ session()->get('alert') }}</p>
      		@endif
      	</div>
      </div>
      @endif
    </div>
  </div>
  <!--/.card-->


  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          Usahaku Toserba - Toko Offline
        </div>
        <div class="card-body">
          <form method="POST" action="/cashier/tambah-barang" enctype="multipart/form-data">
          	{{ csrf_field() }}

          	<div class="form-group">
          		<strong>Nama Barang</strong><br>
          		<input type="text" name="name" class="form-control" style="margin-top: 10px;" placeholder="*) Sesuaikan Dengan Nama Produk" value="{{ old('name') }}" id="produk" list="barangList">
              <datalist id="barangList"></datalist>
          	</div>

            <div class="form-group">
              <strong>Category Barang</strong><br>
              <select class="form-control" name="id_category">
                <option value="">-- Pilih Category --</option>
                @foreach($cat as $a)
                  <option value="{{ $a->id }}">{{ $a->category }}</option>
                @endforeach
              </select>
            </div>

            <div class="form-group">
              <strong>Rak</strong><br>
              <select class="form-control" name="rak">
                @for($i=1; $i <= 10; $i++)
                <option value="{{ $i }}">{{ $i }}</option>
                @endfor
              </select>
            </div>

            <!-- <div class="form-group">
              <strong>Jumlah Pada Rak</strong><br>
              <input type="text" name="jumlah_pada_rak" class="form-control" style="margin-top: 10px;" placeholder="*) Isi Dengan Angka, Jangan Pake Symbol Atau Huruf" value="{{ old('jumlah_pada_rak') }}">
            </div> -->

          	<div class="form-group">
          		<strong>Harga Modal</strong><br>
          		<input type="text" name="price" class="form-control" style="margin-top: 10px;" placeholder="*) Isi Dengan Angka, Jangan Pake Symbol Atau Huruf" value="{{ old('price') }}" id="modal">
          	</div>

            <div class="form-group">
              <strong>Harga Jual</strong><br>
              <input type="text" name="harga_jual" class="form-control" style="margin-top: 10px;" placeholder="*) Isi Dengan Angka, Jangan Pake Symbol Atau Huruf" value="{{ old('harga_jual') }}" id="jual">
            </div>

            <div class="form-group">
              <strong>Keuntungan</strong><br>
              <input type="text" readonly  name="keuntungan" class="form-control" style="margin-top: 10px;" id="keuntungan">
            </div>

          	<!-- <div class="form-group">
          		<strong>Affiliasi Barang</strong><br>
          		<input type="text" name="affiliasi" class="form-control" style="margin-top: 10px;" placeholder="*) Isi Dengan Angka, Jangan Pake Symbol Atau Huruf" value="{{ old('affiliasi') }}">
          	</div>
 -->
          	<div class="form-group">
          		<strong>Stok Barang</strong><br>
          		<input type="text" name="stok" class="form-control" style="margin-top: 10px;" placeholder="*) Isi Dengan Angka, Jangan Pake Symbol Atau Huruf" value="{{ old('stok') }}" id="stok">
          	</div>

          	<div class="form-group">
          		<strong>Barcode Barang</strong><br>
          		<input type="text" name="barcode" class="form-control" style="margin-top: 10px;" value="{{ old('barcode') }}" id="barcode_barang">
          	</div>

            <div class="form-group">
              <strong>Gambar Barang</strong><br>
              <input type="file" name="gambare" class="form-control" style="margin-top: 10px;">
            </div>

            <div class="form-group">
              <strong>Kondisi Barang</strong><br>
              <select class="form-control" name="kondisi">
                <option value="Baru">Baru</option>
                <option value="Bekas">Bekas</option>
              </select>
            </div>

            <div class="form-group">
              <strong>Keterangan Barang</strong><br>
              <textarea name="deskripsi" class="form-control"></textarea>
            </div>

          	<br><br>

          	<input type="submit" class="btn btn-primary form-control" value="Tambah Barang">
          </form>
        </div>
      </div>
    </div>
    <!--/.col-->
  </div>
  <!--/.row-->
</div>

</div>

<script type="text/javascript">
  $(document).ready(function(){
    setInterval(function() {
      let modal     = $("#modal").val(),
          jual      = $("#jual").val();

      if (modal != '' && jual != '') {
        $("#keuntungan").val(jual - modal);
      }
    }, 1000);
  })
</script>

<script type="text/javascript" src="/js/barang.js"></script>

@endsection
