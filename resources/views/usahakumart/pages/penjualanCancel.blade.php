@extends('usahakumart.app')

@section('content')


<!-- Breadcrumb -->
<ol class="breadcrumb">
	@if(Auth::user()->id_tipe == 2)
<b>Jumlah Saldo Deposit Rp. {{ number_format($data->saldo_deposit,0,',','.') }}</b>
@endif
</ol>

@if(app('request')->input('type'))
<button class="btn btn-warning form-control" id="close-tab" style="color: white;">TUTUP TAB</button> <br><br>
@endif

@if(session('user-toko')->level == 1)
<a class="btn btn-primary form-control" style="color: white;" target="_blank" href="/cashier/penjualan?type=custom">CUSTOM KASIR</a><br><br>
@endif

<a class="btn btn-danger form-control" style="color: white;" target="_blank" href="/cashier/penjualan?type=hold">BUKA TAB BARU</a> <br><br>

<div class="col-lg-9" style="float: left;">
	<div class="card">
        <div class="card-header">
          <i class="fa fa-align-justify"></i> Keterangan Belanja
        </div>
        <div class="card-body" id="body-penjualan">
          <table class="table table-striped">
            <thead>
              <tr>
                <th>Nama</th>
                <th>Jumlah</th>
                <th>Harga</th>
                <th>Affiliasi</th>
                <th>Diskon</th>
                <th>Potongan</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody id="barang">
              @foreach($item as $i)
              <tr id='{{ $i->id }}'>
                <td>{{ $i->name }}</td>
                <td><input type='number' min='1' max='{{ $i->stok }}' data-id='{{ $i->id }}' class='stok' id='stok-{{ $i->id }}'value='1'></td>
                <td id='harga_jual-{{ $i->id }}' class='harga_result' data-id='{{$i->harga_jual - ($i->harga_jual * ($i->diskon / 100))}}' data-single='${$i->harga_jual - ($i->harga_jual * ($i->diskon / 100))}'>Rp. {{ number_format(($i->harga_jual - ($i->harga_jual * ($i->diskon / 100))),0,',','.')}}</td>
                <td id='affiliasi-{{ $i->id }}' class='affiliasi' data-id='{{$i->affiliasi * (25 / 100)}}' data-single='{{$i->affiliasi * (25 / 100)}}'>Rp. {{ number_format(($i->affiliasi * (25 / 100)),0,',','.')}}</td>
                <td id='diskon-{{ $i->id }}' data-id='{{$i->diskon}}'>{{$i->diskon}}%</td>
                <td id='potongan-{{ $i->id }}' data-id='0' class='potongan'>0</td>
                <td><button id='hapus-{{ $i->id }}' data-id='{{ $i->id }}' class='btn btn-danger btn-sm delete'>X</button></td>
             </tr>
             @endforeach
            </tbody>
          </table>
        </div>
  </div>
</div>
<!-- fade -->
<div class="modal fade" id="modalCart" tabindex="-1" role="dialog" aria-labelledby="modalCart" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Keranjang Belanja</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal" style="color: white;">Close</button>
        <a id="pembayaran" class="btn btn-primary" style="color: white;">Lanjut Pembayaran</a>
      </div>
    </div>
  </div>
</div>

<!-- <div class="loader">
  <img src="/loader/produk.gif">
</div> -->
<div class="col-lg-3" style="float: left;">
	<form method="POST" id="submit" autocomplete="off">
		{{ csrf_field() }}
		<input type="text" name="barcode" id="barcode" class="form-control" placeholder="Barcode atau Nama Produk">
		<div id="search-results">
			<div class="this-result"></div>
		</div>
	</form>
  <br>
  <form method="POST" id="number_cek" autocomplete="off">
    {{ csrf_field() }}
    <input type="text" name="member_card" id="member_card" class="form-control" placeholder="Nomer Kartu Member (Jika Memiliki)">
  </form>
  <br>
  <hr>
	<form method="POST" action="/cashier/beli" id="beli_langsung">
		{{ csrf_field() }}
    <input type="number" name="total_potongan" id="potongan" class="form-control" placeholder="Total Potongan" value="0" disabled=""><br>
    @if(app('request')->type == 'custom')
    <input type="date" name="created_at" id="created_at" class="form-control"><br>
    @endif
    <input type="text" name="pembayaran" id="pembayaran_beli" class="form-control" placeholder="Total pembayaran">
		<input type="hidden" name="product_id" value="{{ $id_pro }}" id="id"><br>
    <h3 id="total" style="font-size: 18px;" total="">Total Harga Rp. 0</h3>
		<h3 id="total-potongan" style="font-size: 18px;">Total Potongan Harga Rp. 0</h3>
    <input type="hidden" data-real="0" name="total" id="total-semua">
		<input type="hidden" name="barang_bonus" id="barang-bonus">
		<br>
		<input type="submit" value="BELI" class="form-control btn btn-primary" id="beli" onclick="return confirm('Apakah total pembayaran sudah benar?')">
	</form>
</div>


  <div id="listPenjualan" style="display:none;">
    <div style="font-size:12px;">
      <center>
        <strong style="font-size:16px;">PT. USAHAKU BISNIS NUSANTARA</strong>
        <br>
        <span>Jl. Anwar Perum Metro Indah, Ganjar Asri, Metro Barat, Metro</span>
        <br>
        <span>NPWP : 82.216.058.6-321.000</span>
        <br>
        <b>USAHAKU TOSERBA</b> | <span style="text-transform:uppercase;">{{Auth::user()->name}}</span>
        <br>
        {{-- <span>{{Auth::user()->alamat}}</span> --}}
        <span>Jln. Raya Pekalongan, Kec. Pekalongan, Lampung Timur</span>
      </center>
      <br>
      <br>
    <div style="margin-bottom: 3px; margin-top: 0;"><span id="no_transaksi">No. Transaksi {{$no_tran}}</span><br>
    <span id="nama_kasir">Kasir : {{session('user-toko')->nama}}</span></div>      
      <br>
      <br>
      <div id="list-barang" style="margin-bottom: 0">

      </div>
      <hr style="border:1px dotted black;">
      <div style="margin-bottom: 3px;">Total Belanja<span style="float:right;" id="total_semua_struk"></span></div>
      <div style="margin-bottom: 3px; margin-top: 0;">Total Potongan<span style="float:right;" id="total_semua_struk_potongan">0</span></div>
      <div style="margin-bottom: 3px; margin-top: 0;">Total Jumlah<span style="float:right;" id="total_semua_struk_jumlah"></span></div>
      <div style="margin-bottom: 3px; margin-top: 0;">Total Dibayar<span style="float:right;" id="total_semua_struk_dibayar"></span></div>
      <div style="margin-bottom: 3px; margin-top: 0;">Total Kembalian<span style="float:right;" id="total_semua_struk_kembalian"></span></div>
      <br>
      <span>Tanggal : {{date('d/m/Y h:i:s', strtotime(now()))}}</span>
      <div id="more"></div>
    </div>  
    <br>
    <div style="border:2px dotted black; padding:10px; font-size:12px;">
      Terimakasih Telah Berbelanja
    </div>
    <br>
    <div>
    website : toserba.usahaku.co.id
    </div>
    <!-- <small style="font-size:2px; margin-top:30px; display:inline-block;">{{date(now())}}</small> -->
    <hr style="height:1px; width:1px; background:white; margin-top:50px;">
    <br>
  </div>

<script type="text/javascript" src="/admin/js/livesearch.js"></script>
<script>
  $(document).ready(function () {
		var msg = '{{Session::get('alert')}}';
	  var exist = '{{Session::has('alert')}}';
	  if(exist){
	    alert(msg);
	  }
  });
</script>
@endsection
