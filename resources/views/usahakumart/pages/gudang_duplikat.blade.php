@extends('usahakumart/app')

@section('content')
<!-- Breadcrumb -->
<ol class="breadcrumb">
<li class="breadcrumb-item">Home</li>
<li class="breadcrumb-item">Admin</li>
<li class="breadcrumb-item active">Gudang barang</li>

<!-- Breadcrumb Menu-->

</ol>

<div class="container-fluid">

  <div class="card">
    <div class="card-body">
      <div class="row">
        <div class="col-sm-5">
          <h4 class="card-title mb-0">Gudang barang duplikat</h4>
          <div class="small text-muted">{{date('d M Y')}}</div><br>
        </div>
        <!--/.col-->
        <div class="col-sm-7 d-none d-md-block">
          <button type="button" class="btn btn-primary float-right"><i class="icon-cloud-download"></i></button>
          <div class="btn-toolbar float-right" role="toolbar" aria-label="Toolbar with button groups">
          </div>
        </div>
        <!--/.col-->
      </div>
      @if(session()->has('alert'))
      <div class="row">
      	<div class="alert alert-danger col-sm-12" style="margin-bottom: 0; margin-top: 25px;">
      			<p>{{ session()->get('alert') }}</p>
      	</div>
      </div>
      @endif
    </div>
  </div>
  <!--/.card-->


  <input type="hidden" id="id_ses" value="{{ session('user-toko')->level }}">

  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
            <a href="/cashier/gudang-barang/duplikat?t=barcode" class="btn btn-danger btn-sm">Duplikat Barcode</a>
            <a href="/cashier/gudang-barang/duplikat?t=name" class="btn btn-danger btn-sm">Duplikat Nama</a>
        </div>
        <div class="card-body">
          <table class="table table-responsive-sm table-hover table-outline mb-0">
            <thead class="thead-light">
              <tr>
                <th>#</th>
                @if(app('request')->input('t') == 'barcode')
                <th>Barcode</th>
                @elseif (app('request')->input('t') == 'name')
                <th>Name</th>
                @else
                <th>Barcode</th>
                @endif
                <th>Banyak Duplikat</th>
                @if(session('user-toko')->level == 1)
                <th class="text-center">Option</th>
                @endif
              </tr>
            </thead>
            <tbody id="barangApi">
            @foreach($product as $no => $t)
              <tr id="{{ $t->name }}">
                <td>{{ $no+1 }}</td>
                @if(app('request')->input('t') == 'barcode')
                <td>{{ $t->barcode }}</td>
                @elseif (app('request')->input('t') == 'name')
                <td>{{ $t->name }}</td>
                @else
                <td>{{ $t->barcode }}</td>
                @endif
                <td>{{ $t->data_count }}</td>
                @if(session('user-toko')->level == 1)
                <td class="text-center">
                    @if(app('request')->input('t') == 'barcode')
                    <a class="btn-warning btn" target="_blank" href="/cashier/search-barang?q={{ $t->barcode }}" style="color: white;">Lihat Barang</a>
                    @elseif (app('request')->input('t') == 'name')
                    <a class="btn-warning btn" target="_blank" href="/cashier/search-barang?q={{ $t->name }}" style="color: white;">Lihat Barang</a>
                    @else
                    <a class="btn-warning btn" target="_blank" href="/cashier/search-barang?q={{ $t->barcode }}" style="color: white;">Lihat Barang</a>
                    @endif
                </td>
                @endif
              </tr>
            @endforeach
            </tbody>
          </table>

          @if(!Request::get('q'))
          {{$product->render()}}
          @endif
        </div>
      </div>
    </div>
    <!--/.col-->
  </div>
  <!--/.row-->
</div>

</div>

<script type="text/javascript" src="/js/gudang.js"></script>
@endsection
