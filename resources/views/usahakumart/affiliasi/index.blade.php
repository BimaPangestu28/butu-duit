@extends('usahakumart/app')

@section('content')
<!-- Breadcrumb -->
<ol class="breadcrumb">
<li class="breadcrumb-item">Home</li>
<li class="breadcrumb-item">Admin</li>
<li class="breadcrumb-item active">Laporan Kas</li>

<!-- Breadcrumb Menu-->

</ol>

<div class="container-fluid">

  <div class="card">
    <div class="card-body">
      <div class="row">
        <div class="col-sm-12">
          <h4 class="card-title mb-0">Laporan Affiliasi Masuk {{Auth::user()->name}}</h4>
          <div class="small text-muted">{{date('d M Y')}}</div><br>

          <div>
            <h5 style="display: inline-block; width:200px; margin-right:50px;">Affiliasi Perusahaan</h5> :
            <h5 style="display: inline-block; width:200px; margin-left:50px;">Rp. {{ number_format($total->affiliasi_perusahaan,0,',','.') }}</h5>
          </div>
          <div>
            <h5 style="display: inline-block; width:200px; margin-right:50px;">Affiliasi Member</h5> :
            <h5 style="display: inline-block; width:200px; margin-left:50px;">Rp. {{ number_format($total->affiliasi_member,0,',','.') }}</h5>
          </div>

        </div>
        <!--/.col-->
      </div>
    </div>
  </div>
  <!--/.card-->

    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">
            <b>Affiliasi Masuk Perusahaan Bulan {{ \Carbon\Carbon::now()->format('F Y') }}</b>
            <a button data-toggle="modal" data-target="#masukPerusahaan" class="btn btn-warning btn-sm text-white">Download Laporan Pemasukan</a>
          </div>
          <div class="card-body">

            <input type="month" id="search-mp" class="form-control">
            <br>
            <table class="table table-responsive-sm table-hover table-outline mb-0">
              <thead class="thead-light">
                <tr>
                  <th>#</th>
                  <th class="text-center">Jumlah Affiliasi Masuk</th>
                  <th class="text-center">Tanggal</th>
                  <th class="text-center">Option</th>
                </tr>
              </thead>
              <tbody id="mp">
                @forelse($masuk_perusahaan as $no => $mp)
                <tr>
                  <td>{{ $no += 1 }}</td>
                  <td class="text-center">Rp. {{ number_format($mp->jumlah,0,',','.') }}</td>
                  <td class="text-center">{{ date('d/m/Y', strtotime($mp->date)) }}</td>
                  <td class="text-center"><button data-id="{{ $mp->date }}" type="button" data-toggle="modal" data-target="#modalDetail" name="button" class="btn btn-primary detail">Lihat Detail</button></td>
                </tr>
                @empty
                <span style="text-align: center;">Tidak Ada Data</span>
                @endforelse

                @if(count($masuk_perusahaan) > 0)
                <tr>
                  <td></td>
                  <td class="text-center" id="jumlah-mp">Rp. {{ number_format($total_mp->jumlah,0,',','.') }}</td>
                  <td></td>
                  <td></td>
                </tr>
                @endif
              </tbody>
            </table>
            <br>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-12">
          <div class="card">
            <div class="card-header">
              <b>Affiliasi Masuk Member Bulan {{ \Carbon\Carbon::now()->format('F Y') }}</b>
              <a button data-toggle="modal" data-target="#masukMember" class="btn btn-warning btn-sm text-white">Download Laporan Pemasukan</a>
            </div>
            <div class="card-body">

              <input type="month" id="search-mm" class="form-control">
              <br>
              <table class="table table-responsive-sm table-hover table-outline mb-0">
                <thead class="thead-light">
                  <tr>
                    <th>#</th>
                    <th class="text-center">Jumlah Affiliasi Masuk</th>
                    <th class="text-center">Tanggal</th>
                    <th class="text-center">Option</th>
                  </tr>
                </thead>

                <tbody id="mm">
                  @forelse($masuk_member as $no => $mm)
                  <tr>
                    <td>{{ $no += 1 }}</td>
                    <td class="text-center">Rp. {{ number_format($mm->jumlah,0,',','.') }}</td>
                    <td class="text-center">{{ date('d/m/Y', strtotime($mm->date)) }}</td>
                    <td class="text-center"><button data-id="{{ $mm->date }}" type="button" data-toggle="modal" data-target="#modalDetail" name="button" class="btn btn-primary detail">Lihat Detail</button></td>
                  </tr>
                  @empty
                  <span style="text-align: center;">Tidak Ada Data</span>
                  @endforelse

                  @if(count($masuk_member) > 0)
                  <tr>
                    <td></td>
                    <td class="text-center" id="jumlah-mm">Rp. {{ number_format($total_mm->jumlah,0,',','.') }}</td>
                    <td></td>
                    <td></td>
                  </tr>
                  @endif
                </tbody>
              </table>
              <br>
            </div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <b>Affiliasi Keluar Member Bulan {{ \Carbon\Carbon::now()->format('F Y') }}</b>
                <a button data-toggle="modal" data-target="#keluarMember" class="btn btn-warning btn-sm text-white">Download Laporan Pengeluaran</a>
              </div>
              <div class="card-body">

                <input type="month" id="search-km" class="form-control">
                <br>
                <table class="table table-responsive-sm table-hover table-outline mb-0">
                  <thead class="thead-light">
                    <tr>
                      <th>#</th>
                      <th class="text-center">Jumlah Affiliasi Keluar</th>
                      <th class="text-center">Tanggal</th>
                      <th class="text-center">Option</th>
                    </tr>
                  </thead>

                  <tbody id="km">
                    @forelse($keluar_member as $no => $km)
                    <tr>
                      <td>{{ $no += 1 }}</td>
                      <td class="text-center">Rp. {{ number_format($km->jumlah,0,',','.') }}</td>
                      <td class="text-center">{{ date('d/m/Y', strtotime($km->date)) }}</td>
                      <td class="text-center"><button data-id="{{ $km->date }}" type="button" data-toggle="modal" data-target="#modalDetail" name="button" class="btn btn-primary detail">Lihat Detail</button></td>
                    </tr>
                    @empty
                    <span style="text-align: center;">Tidak Ada Data</span>
                    @endforelse

                    @if(count($keluar_member) > 0)
                    <tr>
                      <td></td>
                      <td class="text-center" id="jumlah-mm">Rp. {{ number_format($total_km->jumlah,0,',','.') }}</td>
                      <td></td>
                      <td></td>
                    </tr>
                    @endif
                  </tbody>
                </table>
                <br>
              </div>
            </div>
          </div>
        </div>
  </div>

</div>


</div>

<!-- Modal Print -->
<div class="modal" id="masukPerusahaan" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Laporan Affiliasi Masuk Perusahaan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="/cashier/laporan/affiliasi/print">
      <div class="modal-body">
        <p>
        <label for="to">Bulan :</label>
        <input type="month" name="to" class="form-control">
        <input type="hidden" name="type" value="Masuk">
        <input type="hidden" name="pemilik" value="Perusahaan">
        </p>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Download</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
      </form>
    </div>
  </div>
</div>
<div class="modal" id="masukMember" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Laporan Affiliasi Masuk Perusahaan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="/cashier/laporan/affiliasi/print">
      <div class="modal-body">
        <p>
        <label for="to">Bulan :</label>
        <input type="month" name="to" class="form-control">
        <input type="hidden" name="type" value="Masuk">
        <input type="hidden" name="pemilik" value="Member">
        </p>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Download</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
      </form>
    </div>
  </div>
</div>
<div class="modal" id="keluarMember" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Laporan Affiliasi Masuk Perusahaan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="/cashier/laporan/affiliasi/print">
      <div class="modal-body">
        <p>
        <label for="to">Bulan :</label>
        <input type="month" name="to" class="form-control">
        <input type="hidden" name="type" value="Keluar">
        <input type="hidden" name="pemilik" value="Member">
        </p>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Download</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
      </form>
    </div>
  </div>
</div>

<div class="modal" id="modalDetail" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="body-detail">
        <div class="loading" style="text-align: center;">
          <span>Loading</span>
        </div>
        <div class="detail-body" style="display: none;">
          <table class="table table-responsive-sm table-hover table-outline mb-0">
            <thead class="thead-light">
              <tr>
                <th>#</th>
                <th class="text-center">Jumlah Affiliasi Masuk</th>
                <th class="text-center">Tanggal</th>
              </tr>
            </thead>
            <tbody>

            </tbody>
          </table>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div class="modal" id="modalKeluar" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Download Kas Keluar</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="/cashier/laporan/kas/print">
        <div class="modal-body">
          <p>
          <input type="hidden" name="data" value="keluar">
          <label for="from">Dari :</label>
          <input type="date" name="from" class="form-control">
          <label for="to">Sampai :</label>
          <input type="date" name="to" class="form-control">
          </p>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary">Download Laporan</button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
        </form>
      </div>
    </div>
  </div>
<script type="text/javascript" src="/js/gudang.js"></script>
@endsection

@section('script')
<script type="text/javascript" src="/js/affiliasi.js"></script>
@endsection
