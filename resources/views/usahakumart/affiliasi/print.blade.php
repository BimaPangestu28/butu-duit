<!DOCTYPE html>
<html>
	<head>
		<title>Laporan
		</title>
		<style type="text/css">
			.center {
				width: 90%;
				margin: 0 auto;
			}

			body {
				padding: 15px;
				font-family: 'Arial';
				font-size: 13px;
			}

			.title {
				text-align: left;
			}

			.title h3 {
				font-size: 15px;
				text-transform: uppercase;
				margin-bottom: 0;
			}

			.title h4 {
				margin-top: 0px;
				font-size: 13px;
			}

			table {
			    border-collapse: collapse;
			    width: 100%;
			}

			th, td {
			    text-align: left;
			    padding: 5px;
			    border: 1px solid black;
			}
		</style>
	</head>
	<body>
		<div class="center">
			<div class="title">
				<h3>Laporan Affiliasi {{ $type }} {{ $pemilik }}</h3>
				<h4>{{ Auth::user()->name }}</h4>
				<h4>Bulan : {{ $month }} / {{ $year }}</h4>
			</div>

			<br><br>

			<div class="barang-keluar">
				<table class="table table-responsive-sm table-hover table-outline mb-0">
	            <thead class="thead-light">
                    <tr>
                        <th>#</th>
                        <th class="text-center">Jumlah Affiliasi Masuk</th>
                        <th class="text-center">Tanggal</th>
                    </tr>
                    </thead>
                    <tbody>
                        @forelse($data as $no => $mp)
                        <tr>
                        <td>{{ $no += 1 }}</td>
                        <td class="text-center">Rp. {{ number_format($mp->jumlah,0,',','.') }}</td>
                        <td class="text-center">{{ date('d/m/Y', strtotime($mp->date)) }}</td>
                        </tr>
                        @empty
                        <span style="text-align: center;">Tidak Ada Data</span>
                        @endforelse
                    </tbody>
	          </table>
			</div>
            <br>
		</div>
	</body>
</html>