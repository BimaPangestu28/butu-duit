<div style="display: block; width: 100%;">
@if($counter->count() <= 0)
	Maaf, Hasil Pencarian tidak ada
@else
	@foreach ($products as $pr)
		<button  barCodeList='{{$pr->barcode}}' id="thisOne" type="submit" style="background: none; border: none;display: inline-block;width: 100%;padding: 0;outline: 0; text-align: left;">
			<div class="my-result-search card" style="list-style: none; padding: 0px 10px;">
				<li id="akuSuka">{{$pr->name}}</li>
				<li>{{$pr->barcode}}</li>
				<!-- <li>{{$keywords}}</li> -->
			</div>
		</button>
	@endforeach

	<script>
		$(document).on('click', '#thisOne', function () {
			var code = $(this).attr('barCodeList');
			$('#barcode').val(code);
		});
	</script>
	
@endif
</div>