<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
  	<script type="text/javascript" src="/admin/js/jquery.min.js"></script>
    <style media="screen">
    </style>
  </head>
  <body>

  <div id="listPenjualan">
    <div style="font-size:12px;">
      <center>
        <img src="/assets/img/logo-black.png" width="100px" alt="">
        <br>
        <span>Jl. Sakura No.31</span>
        <br>
        <span>Cab. Metro Timur</span>
      </center>
      <br>
      <br>
      <div class="list">
        <span style="width:50%;display:inline-block;margin-bottom:5px;">Iphone X Yakin asli gak boong </span>
        <span style="float:right;">Rp. 17.000.000</span>
      </div>
      <div class="list">
        <span style="width:50%;display:inline-block;margin-bottom:5px;">Macbook Pro beli 1 gratis 2 </span>
        <span style="float:right;">Rp. 21.000.000</span>
      </div>
      <div class="list">
        <span style="width:50%;display:inline-block;margin-bottom:5px;">Iphone X murah original lisensi cina </span>
        <span style="float:right;">Rp. 17.000.000</span>
      </div>
      <hr style="border:1px dotted black;">
      <h4>Total <span style="float:right;">Rp. 999.999.999</span></h4>
      <span>Tanggal : {{date(now())}}</span>
    </div>
    <br>
    <div style="border:2px dotted black; padding:10px; font-size:12px;">
      Terimakasih Telah Berbelanja
    </div>
    <!-- <small style="font-size:2px; margin-top:30px; display:inline-block;">{{date(now())}}</small> -->
    <hr style="height:1px; width:1px; background:white; margin-top:50px;">
    <br>
  </div>

  <button type="button" id="beli" name="button">Beli</button>

<script type="text/javascript">
  $('#beli').click(function () {
    var print_window = window,
    print_document = $('#listPenjualan').clone();
    print_window.document.open();
    print_window.document.write(print_document.html());
    print_window.document.close();
    print_window.print();
    print_window.close();
  });
</script>

  </body>
</html>
