<!DOCTYPE html>
<html>
	<head>
		<title>Laporan </title>
		<style type="text/css">
			.center {
				width: 90%;
				margin: 0 auto;
			}

			body {
				padding: 15px;
				font-family: 'Arial';
				font-size: 9px;
			}

			.title {
				text-align: left;
			}

			.title h3 {
				font-size: 15px;
				text-transform: uppercase;
				margin-bottom: 0;
			}

			.title h4 {
				margin-top: 0px;
				font-size: 13px;
			}

			table {
			    border-collapse: collapse;
			    width: 100%;
			}

			th, td {
			    text-align: left;
			    padding: 5px;
			    border: 1px solid black;
			}
		</style>
	</head>
	<body>
		<div class="center">
			<div class="title">
				<h3>Laporan Laba Rugi</h3>
				<h4>{{ Auth::user()->name }}</h4>
			</div>

			<div class="barang-keluar">
				<h5>
					Tanggal : {{ date('d - F - Y', strtotime($start_date)) }} sampai {{ date('d - F - Y', strtotime($end_date)) }}
				</h5>

				
				<table class="table table-responsive-sm table-hover table-outline mb-0">
		            <thead class="thead-light">
		              <tr>
		                <th>#</th>
		                <th class="text-center">Tanggal</th>
		                <th class="text-center">Keterangan</th>
		                <th class="text-center">Pendapatan</th>
		                <th class="text-center">HPP</th>
		                <th class="text-center">Total Potongan</th>
		                <th class="text-center">Affiliasi</th>
		                <th class="text-center">Keterangan</th>
		                <th class="text-center">Laba Rugi</th>
		                <th class="text-center">Operator</th>
		              </tr>
		            </thead>
		            <tbody id="body-data">
		              @foreach($period as $p)
		                <tr>
		                  <td>{{ $no += 1 }}</td>
		                  <td>{{ $p->format('d/m/Y') }}</td>
		                  <td>Pendapatan Penjualan</td>
		                  <td style="display: none;">{{ $pendapatan += (App\Http\Controllers\LaporanController::totalPeriode($p->format('Y-m-d'), null, 'omset') + App\Http\Controllers\LaporanController::totalPeriode($p->format('Y-m-d'), null, 'potongan')) }}</td>
		                  <td>Rp. {{ number_format(App\Http\Controllers\LaporanController::totalPeriode($p->format('Y-m-d'), null, 'omset') + App\Http\Controllers\LaporanController::totalPeriode($p->format('Y-m-d'), null, 'potongan'),0,',','.') }}</td>
		                  <td style="display: none;">{{ $hpp += App\Http\Controllers\LaporanController::totalPeriode($p->format('Y-m-d'), null, 'hpp') }}</td>
		                  <td>Rp. {{ number_format(App\Http\Controllers\LaporanController::totalPeriode($p->format('Y-m-d'), null, 'hpp'),0,',','.') }}</td>
		                  <td style="display: none;">{{ $potongan += App\Http\Controllers\LaporanController::totalPeriode($p->format('Y-m-d'), null, 'potongan') }}</td>
		                  <td>Rp. {{ number_format(App\Http\Controllers\LaporanController::totalPeriode($p->format('Y-m-d'), null, 'potongan'),0,',','.') }}</td>
		                  <td style="display: none;">{{ $affiliasi += App\Http\Controllers\LaporanController::totalPeriode($p->format('Y-m-d'), null, 'affiliasi') }}</td>
		                  <td>Rp. {{ number_format(App\Http\Controllers\LaporanController::totalPeriode($p->format('Y-m-d'), null, 'affiliasi'),0,',','.') }}</td>
		                  <td>Kas Masuk</td>
		                  <td>
		                    Rp. {{ 
		                      number_format($laba += 
		                      (App\Http\Controllers\LaporanController::totalPeriode($p->format('Y-m-d'), null, 'omset') - App\Http\Controllers\LaporanController::totalPeriode($p->format('Y-m-d'), null, 'hpp') - App\Http\Controllers\LaporanController::totalPeriode($p->format('Y-m-d'), null, 'affiliasi')),0,',','.')
		                    }}
		                  </td>
		                  <td>
		                    @foreach($user as $u)
		                      {{ $u->nama }} <br>
		                    @endforeach
		                  </td>
		                </tr>
		                @foreach(App\Http\Controllers\LaporanKasController::kasDate($p->format('Y-m-d')) as $k)
		                <tr>
		                  <td>{{ $no += 1 }}</td>
		                  <td></td>
		                  <td>{{ $k->jenis }}</td>
		                  <td style="display: none">
		                    @if($k->tipe == 1)
		                    {{ $pendapatan += $k->total }}
		                    @elseif($k->tipe == 2)
		                    {{ $pendapatan -= $k->total }}
		                    @elseif($k->tipe == 3)
		                    {{ $pendapatan += $k->total }}
		                    @elseif($k->tipe == 4)
		                    {{ $pendapatan -= $k->total }}
		                    @endif
		                  </td>
		                  <td>Rp. {{ number_format($k->total,0,',','.') }}</td>
		                  <td>Rp. 0</td>
		                  <td>Rp. 0</td>
		                  <td>Rp. 0</td>
		                  <td>
		                    @if($k->tipe == 1)
		                      Kas Masuk
		                    @elseif($k->tipe == 2)
		                      Kas Keluar
		                    @elseif($k->tipe == 3)
		                      Kas Mutasi Masuk
		                    @elseif($k->tipe == 4)
		                      Kas Mutasi Keluar
		                    @endif
		                  </td>
		                  <td>Rp.
		                    @if($k->tipe == 1)
		                      {{ number_format($laba += $k->total,0,',','.') }}
		                    @elseif($k->tipe == 2)
		                      {{ number_format($laba -= $k->total,0,',','.') }}
		                    @elseif($k->tipe == 3)
		                      {{ number_format($laba += $k->total,0,',','.') }}
		                    @elseif($k->tipe == 4)
		                      {{ number_format($laba -= $k->total,0,',','.') }}
		                    @endif
		                  </td>
		                  <td>
		                    @foreach($user as $u)
		                      {{ $u->nama }} <br>
		                    @endforeach
		                  </td>
		                </tr>
		                @endforeach
		              @endforeach
		              <tr>
		                <td></td>
		                <td>Total</td>
		                <td></td>
		                <td>Rp. {{ number_format($pendapatan,0,',','.') }}</td>
		                <td>Rp. {{ number_format($hpp,0,',','.') }}</td>
		                <td>Rp. {{ number_format($potongan,0,',','.') }}</td>
		                <td>Rp. {{ number_format($affiliasi,0,',','.') }}</td>
		                <td></td>
		                <td>Rp. {{ number_format($laba,0,',','.') }}</td>
		                <td></td>
		              </tr>
		            </tbody>
		          </table>
			</div>
            <br>
		</div>
	</body>
</html>