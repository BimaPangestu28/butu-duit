<!DOCTYPE html>
<html>
	<head>
		<title>Laporan </title>
		<style type="text/css">
			.center {
				width: 90%;
				margin: 0 auto;
			}

			body {
				padding: 15px;
				font-family: 'Arial';
			}

			.title {
				text-align: center;
			}

			.title h3 {
				font-size: 24px;
				text-transform: uppercase;
				margin-bottom: 0;
			}

			.title h4 {
				margin-top: 0px;
				font-size: 20px;
			}

			table {
			    border-collapse: collapse;
			    width: 100%;
			}

			th, td {
			    text-align: left;
			    padding: 5px;
			    border: 1px solid black;
			}
		</style>
	</head>
	<body>
		<div class="center">
			<div class="title">
				<h3>Laporan Pembelian Produk {{$no_transaksi}}</h3>
				<h4>{{ Auth::user()->name }}</h4>
			</div>

			<br><br>

			<div class="barang-keluar">
				<h4>
					Nomor Transaksi : {{$no_transaksi}}
				</h4>

				<table class="table table-responsive-sm table-hover table-outline mb-0">
	            <thead class="thead-light">
                    <tr>
                        <th>#</th>
                        <th class="text-center">Kode Barang</th>
                        <th class="text-center">Nama Barang</th>
                        <th class="text-center">Supplier</th>
                        <th class="text-center">Jumlah Barang</th>
                        <th class="text-center">Jumlah Satuan</th>
                        <th class="text-center">Tipe</th>
                        <th class="text-center">Total Harga</th>
                        <th class="text-center">Tanggal</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($datas as $no=>$view)
                    <tr>
                        <td class="text-center">{{$no+1}}</td>
                        <td class="text-left">{{$view['barcode']}}</td>
                        <td class="text-left">{{$view['produk']}}</td>
                        <td class="text-center">{{$view['supplier']}}</td>
                        <td class="text-center">{{$view['jumlah_barang']}} {{$view['tipe_jumlah']}}</td>
                        <td class="text-center">{{$view['jumlah_satuan']}}</td>
                        <td class="text-center">{{$view['tipe']}}</td>
                        <td class="text-center">Rp. {{ number_format($view->total_harga,0,',','.') }}</td>
                        <td class="text-center">{{ date('d F Y', strtotime($view->created_at)) }}</td>
                    </tr>
                    @endforeach
                    </tbody>
	          </table>
			</div>
            <br>
            <br>
            <h3>Total Pembayaran : Rp. {{number_format($dataTotal,0,',','.')}}</h3>
            <h3>Total Pajak : Rp. {{number_format($dataPajak,0,',','.')}}</h3>
            <h3>Total Diskon : Rp. {{number_format($dataDiskon,0,',','.')}}</h3>
            <h3>Total Hutang : Rp. {{number_format($utang,0,',','.')}}</h3>
		</div>
	</body>
</html>