<!DOCTYPE html>
<html>
	<head>
		<title>Laporan </title>
		<style type="text/css">
			.center {
				width: 90%;
				margin: 0 auto;
			}

			body {
				padding: 15px;
				font-family: 'Arial';
				font-size: 13px;
			}

			.title {
				text-align: left;
			}

			.title h3 {
				font-size: 15px;
				text-transform: uppercase;
				margin-bottom: 0;
			}

			.title h4 {
				margin-top: 0px;
				font-size: 13px;
			}

			table {
			    border-collapse: collapse;
			    width: 100%;
			}

			th, td {
			    text-align: left;
			    padding: 5px;
			    border: 1px solid black;
			}
            .text-success {
                color: green;
                font-size: 15px;
            }
            .text-danger {
                color: red;
                font-size: 15px;
            }
            .text-center {
                text-align: center;
            }
		</style>
	</head>
	<body>
		<div class="center">
			<div class="title">
				<h3>Stok Opname Produk Abjad {{$key}}</h3>
				<h4>{{ Auth::user()->name }}</h4>
			</div>

			<br><br>

			<div class="barang-keluar">

          <table class="table table-responsive-sm table-hover table-outline mb-0">
                <thead class="thead-light">
                <tr>
                    <th>#</th>
                    <th>Nama Produk</th>
                    <th>Barcode</th>
                    <th>Harga Satuan</th>
                    <th width="15%">Stok Opname Terakhir</th>
                    <th width="10%">Keterangan</th>
                </tr>
                </thead>
                <tbody>
                @foreach($product as $no=>$t)
                <tr>
                    <td>{{$no+1}}</td>
                    <td>{{ $t->name }}</td>
                    <td>{{ $t->barcode }}</td>
                    <td>Rp. {{ number_format($t->price,0,',','.') }}</td>
                    <td>{{ $t->stok_opname }}</td>
					<td class="text-center">
						<b>Rp. {{ number_format(($t->price * $t->stok) - ($t->price * $t->stok_opname) ,0,',','.') }}</b>
                    </td>
                </tr>
                @endforeach
                </tbody>
			</table>
            <br>
			Total Selisih : Rp. {{ number_format($total_awal->tot - $total_akhir->tot,0,',','.') }}
			<br>
			Total Selisih Keseluruhan Abjad : Rp. {{ number_format($total_semua,0,',','.') }}
			</div>
            <br>
		</div>
	</body>
</html>