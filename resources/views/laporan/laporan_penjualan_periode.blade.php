<!DOCTYPE html>
<html>
	<head>
		<title>Laporan </title>
		<style type="text/css">
			.center {
				width: 90%;
				margin: 0 auto;
			}

			body {
				padding: 15px;
				font-family: 'Arial';
				font-size: 9px;
			}

			.title {
				text-align: left;
			}

			.title h3 {
				font-size: 15px;
				text-transform: uppercase;
				margin-bottom: 0;
			}

			.title h4 {
				margin-top: 0px;
				font-size: 13px;
			}

			table {
			    border-collapse: collapse;
			    width: 100%;
			}

			th, td {
			    text-align: left;
			    padding: 5px;
			    border: 1px solid black;
			}
		</style>
	</head>
	<body>
		<div class="center">
			<div class="title">
				<h3>Laporan Penjualan Harian</h3>
				<h4>{{ Auth::user()->name }}</h4>
			</div>

			<div class="barang-keluar">
				<h5>
					Tanggal : {{ date('d - F - Y', strtotime($dari)) }} sampai {{ date('d - F - Y', strtotime($sampai)) }}
				</h5>

				
				<table class="table table-responsive-sm table-hover table-outline mb-0">
					<thead class="thead-light">
					<tr>
						<th>#</th>
						<th class="text-center">Tanggal</th>
						<th class="text-center">Omset</th>
						<th class="text-center">Potongan</th>
						<th class="text-center">Total Harga</th>
						<th class="text-center">Kembalian</th>
						<th class="text-center">HPP</th>
						<th class="text-center">Affiliasi</th>
						<th class="text-center">Presentase Keuntungan</th>
						<th class="text-center">Net Profit</th>
					</tr>
					</thead>
					<tbody id="body-data">
					@foreach ($datas as $no => $view)
					<tr data-toggle="modal" data-target="#myModal{{$view->id_order}}" data-id="{{ $view->id_order }}" class="tr">
						<td class="text-center">{{$no+1}}</td>
						<td class="text-center">{{date('d F Y', strtotime($view->date))}}</td>
						<td class="text-center">Rp. {{ number_format($view['total_pembayaran'] + $view->potongan,0,',','.') }}</td>
		                <td class="text-center">Rp. {{ number_format($view->potongan,0,',','.') }}</td>
						<td class="text-center">Rp. {{ number_format($view['total_pembayaran'],0,',','.') }}</td>
						<td class="text-center">Rp. {{ number_format($view['kembalian'],0,',','.') }}</td>
						<td class="text-center">Rp. {{ number_format(app\Http\Controllers\LaporanController::totalPeriode($view->date, $kasir, 'hpp'),0,',','.') }}</td>
						<td class="text-center">Rp. {{ number_format(app\Http\Controllers\LaporanController::totalPeriode($view->date, $kasir, 'aff'),0,',','.') }}</td>
						<td class="text-center">{{ 
							($view['total_pembayaran'] - app\Http\Controllers\LaporanController::totalPeriode($view->date, $kasir, 'hpp') - app\Http\Controllers\LaporanController::totalPeriode($view->date, $kasir, 'aff')) / ($view['total_pembayaran'] + $view->potongan) * 100 
						}}%</td>
						<td class="text-center">Rp. {{ number_format($view['total_pembayaran'] - app\Http\Controllers\LaporanController::totalPeriode($view->date, $kasir, 'hpp') - app\Http\Controllers\LaporanController::totalPeriode($view->date, $kasir, 'aff'),0,',','.') }}</td>
					</tr>
					@endforeach
					<tr data-toggle="modal" data-target="#myModal{{$view->id_order}}" data-id="{{ $view->id_order }}" class="tr">
						<td class="text-center"></td>
						<td class="text-center"></td>
						<td class="text-center">
							Rp. {{ number_format($total_pembayaran + $potongan,0,',','.') }}
						</td>
						<td class="text-center">
							Rp. {{ number_format($potongan,0,',','.') }}
						</td>
						<td class="text-center">
							Rp. {{ number_format($total_pembayaran,0,',','.') }}
						</td>
						<td class="text-center">
							Rp. {{ number_format($kembalian,0,',','.') }}
						</td>
						<td class="text-center">
							Rp. {{ number_format(app\Http\Controllers\LaporanController::totalPeriodeSemua($dari, $sampai, $kasir, 'hpp'),0,',','.') }}
						</td>
						<td class="text-center">
							Rp. {{ number_format(app\Http\Controllers\LaporanController::totalPeriodeSemua($dari, $sampai, $kasir, 'aff'),0,',','.') }}
						</td>
						<td class="text-center">
							{{ 
								($total_pembayaran - app\Http\Controllers\LaporanController::totalPeriodeSemua($dari, $sampai, $kasir, 'hpp') - app\Http\Controllers\LaporanController::totalPeriodeSemua($dari, $sampai, $kasir, 'aff')) / ($total_pembayaran + $potongan) * 100	
							}}%
						</td>
						<td class="text-center">
							Rp. {{ number_format($total_pembayaran - app\Http\Controllers\LaporanController::totalPeriodeSemua($dari, $sampai, $kasir, 'hpp') - app\Http\Controllers\LaporanController::totalPeriodeSemua($dari, $sampai, $kasir, 'aff'),0,',','.') }}
						</td>
					</tr>
					</tbody>
				</table>
			</div>
            <br>
		</div>
	</body>
</html>