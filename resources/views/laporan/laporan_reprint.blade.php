<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
</head>
<body onload="printMas()">
	<div id="listPenjualan">
		<div style="font-size:12px;">
			<center>
			<strong style="font-size:16px;">PT. USAHAKU BISNIS NUSANTARA</strong>
			<br>
			<span>Jl. Anwar Perum Metro Indah, Ganjar Asri, Metro Barat, Metro</span>
			<br>
			<span>NPWP : 82.216.058.6-321.000</span>
			<br>
			<b>USAHAKU TOSERBA</b> | <span style="text-transform:uppercase;">{{Auth::user()->name}}</span>
			<br>
			{{-- <span>{{Auth::user()->alamat}}</span> --}}
			<span>Jln. Raya Pekalongan, Kec. Pekalongan, Lampung Timur</span>
			</center>
			<br>
			<br>
		<div style="margin-bottom: 3px; margin-top: 0;"><span id="no_transaksi">No. Transaksi {{ $single->no_transaksi }}</span><br>
		<span id="nama_kasir">Kasir : {{session('user-toko')->nama}}</span></div>      
			<br>
			<br>
			<div id="list-barang" style="margin-bottom: 0">
			
				@foreach ($data as $no => $o)
				<div class="list">
					<span style="width:50%;display:inline-block;margin-bottom:5px;">{{ $o->name }} (<span>{{ App\Http\Controllers\LaporanController::jumlah($o->id_product, $o->order_id) }}</span>)</span>
					<span style="float:right;">Rp. {{ number_format(App\Http\Controllers\LaporanController::jumlah($o->id_product, $o->order_id) * $o->harga_jual,0,',','.') }}</span>
				</div>
				@endforeach
			</div>
			<hr style="border:1px dotted black;">
			<div style="margin-bottom: 3px;">Total Belanja<span style="float:right;" id="total_semua_struk">Rp. {{ number_format(App\Http\Controllers\LaporanController::total($single->order_id, 'pembayaran'),0,',','.') }}</span></div>
			<div style="margin-bottom: 3px; margin-top: 0;">Total Diskon<span style="float:right;" id="total_semua_struk_diskon">Rp. {{ number_format(App\Http\Controllers\LaporanController::total($single->order_id, 'diskon'),0,',','.') }}</span></div>
			<div style="margin-bottom: 3px; margin-top: 0;">Total Potongan<span style="float:right;" id="total_semua_struk_potongan">Rp. {{ number_format(App\Http\Controllers\LaporanController::totalPotongan($single->id_order),0,',','.') }}</span></div>
			<div style="margin-bottom: 3px; margin-top: 0;">Total Jumlah<span style="float:right;" id="total_semua_struk_jumlah">Rp. {{ number_format(App\Http\Controllers\LaporanController::total($single->order_id, 'pembayaran akhir'),0,',','.') }}</span></div>
			<div style="margin-bottom: 3px; margin-top: 0;">Total Dibayar<span style="float:right;" id="total_semua_struk_dibayar">Rp. {{ number_format($single->pembayaran,0,',','.') }}</span></div>
			<div style="margin-bottom: 3px; margin-top: 0;">Total Kembalian<span style="float:right;" id="total_semua_struk_kembalian">Rp. {{ number_format($single->kembalian,0,',','.') }}</span></div>
			<br>
			<span>Tanggal : {{date('d/m/Y h:i:s', strtotime(now()))}}</span>
			<div id="more"></div>
		</div>  
		<br>
		<div style="border:2px dotted black; padding:10px; font-size:12px;">
			Terimakasih Telah Berbelanja
		</div>
		<br>
		<div>
		website : toserba.usahaku.co.id
		</div>
		<!-- <small style="font-size:2px; margin-top:30px; display:inline-block;">{{date(now())}}</small> -->
		<hr style="height:1px; width:1px; background:white; margin-top:50px;">
		<br>
	</div>
	
</body>

<script>
	function printMas(){
		window.print();
		window.close();
	}
</script>
</html>