<!DOCTYPE html>
<html>
	<head>
		<title>Laporan</title>
		<style type="text/css">
			.center {
				width: 90%;
				margin: 0 auto;
			}

			body {
				padding: 15px;
				font-family: 'Arial';
				font-size: 13px;
			}

			.title {
				text-align: left;
			}

			.title h3 {
				font-size: 15px;
				text-transform: uppercase;
				margin-bottom: 0;
			}

			.title h4 {
				margin-top: 0px;
				font-size: 13px;
			}

			table {
			    border-collapse: collapse;
			    width: 100%;
			}

			th, td {
			    text-align: left;
			    padding: 5px;
			    border: 1px solid black;
			}
		</style>
	</head>
	<body>
		<div class="center">
			<div class="title">
				<h3>Laporan Kas</h3>
				<h4>{{ Auth::user()->name }}</h4>
				<span>Periode : {{date('d F Y', strtotime(app('request')->input('from'))) }} - {{date('d F Y', strtotime(app('request')->input('to'))) }} </span>
				<br>
				<span>Tanggal Download : {{date('d F Y', strtotime(now())) }} </span>
			</div>

			<br><br>

			<div class="barang-keluar">
				<h3>
					@if(app('request')->input('data') == 'masuk')
					Kas Masuk
					@elseif(app('request')->input('data') == 'keluar')
					Kas Keluar
					@endif
				</h3>

				<table class="table table-responsive-sm table-hover table-outline mb-0">
	            <thead class="thead-light">
                    <tr>
                        <th>#</th>
                        <th class="text-center">Tanggal</th>
                        <th class="text-center">No Transaksi</th>
                        <th class="text-center">Jenis Kas</th>
                        <th class="text-center">Keterangan</th>
                        <th class="text-center">Total</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($kases as $no=>$view)
                        <tr>
                            <td>{{$no+1}}</td>
                            <td>{{$view['created_at']}}</td>
                            <td>{{$view['no_transaksi']}}</td>
                            <td>{{$view['jenis']}}</td>
                            <td>{{$view['keterangan']}}</td>
                            <td>Rp. {{ number_format($view->total,0,',','.') }}</td>
                        </tr>
						@endforeach

						@if(app('request')->input('data') == 'masuk' && $totalJualan > 0)
						<tr>
							<td>~</td>
							<td>{{ app('request')->input('from') }} - {{app('request')->input('to') }}</td>
							<td>-</td>
							<td>Penjualan Barang</td>
							<td>Penjualan barang dari toko</td>
							<td>Rp. {{ number_format($totalJualan,0,',','.') }} </td>
						</tr>

						@elseif(app('request')->input('data') == 'keluar' && $totalSupplierHarga > 0)
						<tr>
							<td>~</td>
							<td>{{ app('request')->input('from') }} - {{app('request')->input('to') }}</td>
							<td>-</td>
							<td>Pembelian Produk</td>
							<td>Pembelian barang dari supplier</td>
							<td>Rp. {{ number_format($totalSupplierHarga,0,',','.') }} </td>
						</tr>
						@endif
					</tbody>
	          </table>
			</div>
			<br>
			@if(app('request')->input('data') == 'masuk')
			<span>Total : <strong> Rp. {{ number_format($kases->sum('total') + $totalJualan,0,',','.') }} </strong></span>
			@elseif(app('request')->input('data') == 'keluar')
			<span>Total : <strong> Rp. {{ number_format($kases->sum('total') + $totalSupplierHarga,0,',','.') }} </strong></span>
			@endif

		</div>
	</body>
</html>