<!DOCTYPE html>
<html>
	<head>
		<title>Laporan </title>
		<style type="text/css">
			.center {
				width: 90%;
				margin: 0 auto;
			}

			body {
				padding: 15px;
				font-family: 'Arial';
				font-size: 13px;
			}

			.title {
				text-align: left;
			}

			.title h3 {
				font-size: 15px;
				text-transform: uppercase;
				margin-bottom: 0;
			}

			.title h4 {
				margin-top: 0px;
				font-size: 13px;
			}

			table {
			    border-collapse: collapse;
			    width: 100%;
			}

			th, td {
			    text-align: left;
			    padding: 5px;
			    border: 1px solid black;
			}
		</style>
	</head>
	<body>
		<div class="center">
			<div class="title">
				<h3>Produk Abjad {{$key}}</h3>
				<h4>{{ Auth::user()->name }}</h4>
			</div>

			<br><br>

			<div class="barang-keluar">

          <table class="table table-responsive-sm table-hover table-outline mb-0">
                <thead class="thead-light">
                <tr>
                    <th>#</th>
                    <th>Nama Produk</th>
                    <th>Barcode</th>
                    <th>Harga Satuan</th>
                    <th width="10%">Stok</th>
                </tr>
                </thead>
                <tbody>
                @foreach($product as $no=>$t)
                <tr>
                    <td>{{$no+1}}</td>
                    <td>{{ $t->name }}</td>
                    <td class="text-center">{{ $t->barcode }}</td>
					<td class="text-center">Rp. {{ number_format($t->price,0,',','.') }}</td>
					<td class="text-center"></td>
                </tr>
                @endforeach
                </tbody>
			</table>
			<br>
			</div>
            <br>
		</div>
	</body>
</html>