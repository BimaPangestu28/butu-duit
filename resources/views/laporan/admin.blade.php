<!DOCTYPE html>
<html>
	<head>
		<title>Laporan Admin</title>
		<style type="text/css">
			.center {
				width: 90%;
				margin: 0 auto;
			}

			body {
				padding: 15px;
				font-family: 'Arial';
			}

			.title {
				text-align: center;
			}

			.title h3 {
				font-size: 24px;
				text-transform: uppercase;
			}

			.title h4 {
				font-size: 20px;
			}

			table {
			    border-collapse: collapse;
			    width: 100%;
			}

			th, td {
			    text-align: left;
			    padding: 5px;
			    border: 1px solid black;
			}
		</style>
	</head>
	<body>
		<div class="center">
			<div class="title">
				<h3>Laporan {{ $tipe }} UsahakuMart <br> <b>{{ $toko->name }}</b></h3>
				<h4>{{ Auth::user()->name }}</h4>
				<span>
					@if(date('N') == 1)
						Senin, 
					@elseif(date('N') == 2)
						Selasa, 
					@elseif(date('N') == 3)
						Rabu, 
					@elseif(date('N') == 4)
						Kamis, 
					@elseif(date('N') == 5)
						Jum'at, 
					@elseif(date('N') == 6)
						Sabtu, 
					@elseif(date('N') == 7)
						Minggu, 
					@endif
					{{ date(' d - F - Y') }}
				</span>
			</div>
			<br><br><br><br>
			<table class="table table-responsive-sm table-hover table-outline mb-0">
            <thead class="thead-light">
              <tr>
                <th>Nama Pembeli</th>
                <th>Total Harga Barang</th>
                <th>Total Uang Pembayaran</th>
                <th>Total Uang Kembalian</th>
                <th class="text-center">Tanggal</th>
              </tr>
            </thead>
            <tbody>
            @foreach($order as $t)
              <tr>
                <td>
                  @if($t->id_member != null)
                    {{$t->nama}}
                  @else
                    Guest
                  @endif
                </td>
                <td>  
                  Rp. {{ number_format($t->total_pembayaran,0,',','.') }}
                </td>
                <td>  
                  Rp. {{ number_format($t->pembayaran,0,',','.') }}
                </td>
                <td>  
                  Rp. {{ number_format($t->kembalian,0,',','.') }}
                </td>
                <td class="text-center">
                  {{ $t->created_at }}
                </td>
              </tr>
            @endforeach
            <tr>
            	<td><b>Total</b></td>
            	<td>Rp. {{ number_format($harga,0,',','.') }}</td>
            	<td>Rp. {{ number_format($bayar,0,',','.') }}</td>
            	<td colspan="2">Rp. {{ number_format($kembali,0,',','.') }}</td>
            </tr>
            </tbody>
          </table>
		</div>
	</body>
</html>