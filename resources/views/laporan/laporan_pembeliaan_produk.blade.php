<!DOCTYPE html>
<html>
	<head>
		<title>Laporan 
		<?php 
		if($_GET['date']=='bulanan') {
			echo "bulanan";
		}else {
			echo "harian";
		} ?> 
		</title>
		<style type="text/css">
			.center {
				width: 90%;
				margin: 0 auto;
			}

			body {
				padding: 15px;
				font-family: 'Arial';
			}

			.title {
				text-align: center;
			}

			.title h3 {
				font-size: 24px;
				text-transform: uppercase;
				margin-bottom: 0;
			}

			.title h4 {
				margin-top: 0px;
				font-size: 20px;
			}

			table {
			    border-collapse: collapse;
			    width: 100%;
			}

			th, td {
			    text-align: left;
			    padding: 5px;
			    border: 1px solid black;
			}
		</style>
	</head>
	<body>
		<div class="center">
			<div class="title">
				<h3>Laporan Pembelian Produk Dari Supplier</h3>
				<h4>{{ Auth::user()->name }}</h4>
				<span>
				<?php 
				if($_GET['date']=='bulanan') {
				?>
					@if(date('m') == 1)
						Bulan Januari
					@elseif(date('m') == 2)
						Bulan Februari 
					@elseif(date('m') == 3)
						Bulan Maret
					@elseif(date('m') == 4)
						Bulan April
					@elseif(date('m') == 5)
						Bulan Mei
					@elseif(date('m') == 6)
						Bulan Juni
					@elseif(date('m') == 7)
						Bulan Juli
					@elseif(date('m') == 8)
						Bulan Agustus
					@elseif(date('m') == 9)
						Bulan September
					@elseif(date('m') == 10)
						Bulan Oktober
					@elseif(date('m') == 11)
						Bulan Novermber
					@elseif(date('m') == 12)
						Bulan Desember
					@endif
				<?php
				}else {
				?>
					@if(date('N') == 1)
						Senin, 
					@elseif(date('N') == 2)
						Selasa, 
					@elseif(date('N') == 3)
						Rabu, 
					@elseif(date('N') == 4)
						Kamis, 
					@elseif(date('N') == 5)
						Jum'at, 
					@elseif(date('N') == 6)
						Sabtu, 
					@elseif(date('N') == 7)
						Minggu, 
					@endif
					{{ date(' d - F - Y') }}
				<?php
				}
				?>
				</span>
			</div>

			<br><br>

			<div class="barang-keluar">
				<h3 style="text-align: center;">
					Barang Masuk
				</h3>

				<table class="table table-responsive-sm table-hover table-outline mb-0">
	            <thead class="thead-light">
                    <tr>
                        <th>#</th>
                        <th class="text-center">Kode Barang</th>
                        <th class="text-center">Nama Barang</th>
                        <th class="text-center">Supplier</th>
                        <th class="text-center">Jumlah Barang</th>
                        <th class="text-center">Jumlah Satuan</th>
                        <th class="text-center">Tipe</th>
                        <th class="text-center">Total Harga</th>
                        <th class="text-center">Tanggal</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($data as $no=>$view)
                    <tr>
                        <td class="text-center">{{$no+1}}</td>
                        <td class="text-left">{{$view['barcode']}}</td>
                        <td class="text-left">{{$view['produk']}}</td>
                        <td class="text-center">{{$view['supplier']}}</td>
                        <td class="text-center">{{$view['jumlah_barang']}} {{$view['tipe_jumlah']}}</td>
                        <td class="text-center">{{$view['jumlah_satuan']}}</td>
                        <td class="text-center">{{$view['tipe']}}</td>
                        <td class="text-center">Rp. {{ number_format($view->total_harga,0,',','.') }}</td>
                        <td class="text-center">{{ date('d F Y', strtotime($view->created_at)) }}</td>
                    </tr>
                    @endforeach
                    </tbody>
	          </table>
			</div>
            <br>
            <br>
            <h3>Total Pembayaran : Rp. {{number_format($count,0,',','.')}}</h3>
            <h3>Total Hutang : Rp. {{number_format($utang,0,',','.')}}</h3>
		</div>
	</body>
</html>