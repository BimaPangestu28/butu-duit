<!DOCTYPE html>
<html>
	<head>
		<title>Laporan </title>
		<style type="text/css">
			.center {
				width: 90%;
				margin: 0 auto;
			}

			body {
				padding: 15px;
				font-family: 'Arial';
				font-size: 13px;
			}

			.title {
				text-align: left;
			}

			.title h3 {
				font-size: 15px;
				text-transform: uppercase;
				margin-bottom: 0;
			}

			.title h4 {
				margin-top: 0px;
				font-size: 13px;
			}

			table {
			    border-collapse: collapse;
			    width: 100%;
			}

			th, td {
			    text-align: left;
			    padding: 5px;
			    border: 1px solid black;
			}
		</style>
	</head>
	<body>
		<div class="center">
			<div class="title">
				<h3>Laporan Produk Terpopuler {{$lot}} Besar</h3>
				<h4>{{ Auth::user()->name }}</h4>
			</div>

			<br><br>

			<div class="barang-keluar">

          <table class="table table-responsive-sm table-hover table-outline mb-0">
                <thead class="thead-light">
                <tr>
                    <th>#</th>
                    <th>Nama Produk</th>
                    <th>Terjual Hari Ini</th>
                    <th>Terjual Bulan Ini</th>
                    <th>Terjual Tahun Ini</th>
                    <th>Jumlah Terjual</th>
                    <th>Stok Barang</th>
                    <th>Jumlah Total Harga Barang</th>
                </tr>
                </thead>
                <tbody>
                @foreach($datas as $no=>$t)
                <tr>
                    <td>{{$no+1}}</td>
                    <td>{{ $t->name }}</td>
                    <td class="text-center">{{ \App\Http\Controllers\CashierController::today($t->id) }}</td>
                    <td class="text-center">{{ \App\Http\Controllers\CashierController::month($t->id) }}</td>
                    <td class="text-center">{{ \App\Http\Controllers\CashierController::year($t->id) }}</td>
                    <td class="text-center">{{ $t->terjual }}</td>
                    <td class="text-center">{{ $t->stok }}</td>
                    <td class="text-center">Rp. {{ number_format($t->stok * $t->harga_jual,0,',','.') }}</td>
                </tr>
                @endforeach
                </tbody>
            </table>
			</div>
            <br>
		</div>
	</body>
</html>