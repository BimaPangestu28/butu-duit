<!DOCTYPE html>
<html>
	<head>
		<title>Laporan </title>
		<style type="text/css">
			.center {
				width: 90%;
				margin: 0 auto;
			}

			body {
				padding: 15px;
				font-family: 'Arial';
				font-size: 13px;
			}

			.title {
				text-align: left;
			}

			.title h3 {
				font-size: 15px;
				text-transform: uppercase;
				margin-bottom: 0;
			}

			.title h4 {
				margin-top: 0px;
				font-size: 13px;
			}

			table {
			    border-collapse: collapse;
			    width: 100%;
			}

			th, td {
			    text-align: left;
			    padding: 5px;
			    border: 1px solid black;
			}
		</style>
	</head>
	<body>
		<div class="center">
			<div class="title">
				<h3>Laporan Pembelian Produk</h3>
				<h4>{{ Auth::user()->name }}</h4>
			</div>

			<br><br>

			<div class="barang-keluar">
				<h5>
					Tanggal : {{date('d F Y', strtotime($from)) }} - {{date('d F Y', strtotime($to)) }} <br>
					Tanggal Print : {{date('d F Y', strtotime(now())) }}
				</h5>

				<table class="table table-responsive-sm table-hover table-outline mb-0">
	            <thead class="thead-light">
                    <tr>
                        <th>#</th>
                        <th class="text-center">Nama Supplier</th>
                        <th class="text-center">Total Biaya Kredit</th>
                        <th class="text-center">Total Biaya Titipan</th>
                        <th class="text-center">Total Biaya Tunai</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($datas as $no=>$view)
                    <tr>
                        <td class="text-center">{{$no+1}}</td>
                        <td class="text-left">{{$view['supplier']}}</td>
                        <td class="text-left">Rp. {{ number_format(App\Http\Controllers\LaporanController::hargaLaporan($view->supplier, 'kredit'),0,',','.') }}</td>
                        <td class="text-left">Rp. {{ number_format(App\Http\Controllers\LaporanController::hargaLaporan($view->supplier, 'titipan'),0,',','.') }}</td>
                        <td class="text-left">Rp. {{ number_format(App\Http\Controllers\LaporanController::hargaLaporan($view->supplier, 'tunai'),0,',','.') }}</td>
                    </tr>
                    @endforeach
                    </tbody>
	          </table>
            </div>
            <h4>Total Pembayaran Tunai : Rp. {{ number_format($total_tunai,0,',','.') }}</h4>
            <h4>Total Pembayaran Kredit : Rp. {{ number_format($total_kredit,0,',','.') }}</h4>
            <h4>Total Pembayaran Titipan : Rp. {{ number_format($total_titipan,0,',','.') }}</h4>
            <h4>Total Pembayaran : Rp. {{ number_format($total_semua,0,',','.') }}</h4>
		</div>
	</body>
</html>