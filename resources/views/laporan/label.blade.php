<!DOCTYPE html>
<html>
<head>
	<title>Print Label</title>
	<style type="text/css">
		body {
			font-family: 'Arial';
			margin: 0;
		}

		.label {
			display: inline-block;
			padding: 10px;
			box-sizing: border-box;
			page-break-after: always;
			margin-bottom: 10px;
		}

		.border {
			border-top: 2px solid blue;
			border-right: 2px solid blue;
			border-left: 2px solid yellow;
			border-bottom: 2px solid yellow;
		}

		h3 {
			margin: 0;
			font-weight: normal;
			min-height: 12px;
			font-size: 14px;
		}

		h2 {
			margin: 0;
			color: white;
			font-weight: normal;
			background: blue;
			box-sizing: border-box;
			padding: 10px;
			margin: 10px 0;
			font-size: 21px;
			margin-bottom: 0;
		}

		h5 {
			margin: 0;
			font-weight: normal;
			text-transform: uppercase;
			font-size: 11px;
		}

		#bod {
			margin-right: 2.5cm;
			margin-left: 0.2cm;
		}

		.lab-1 {
			display: inline-block;
			margin-bottom: 0.2cm;
		}

		.yellow {
			background: yellow;
			width: 100%;
			height: 5px;
			margin-bottom: 20px;
		}
	</style>
</head>
<body>
	@if($type == 0)
	@foreach($product as $pro)
	<span style="display: none;">{{$count++}}</span>
	<div class="label border" style="width: {{ $size[1] }}cm; height: {{ $size[0] }}cm;">
		<div class="title">
			<h3 @if(strlen($pro->name) > 20) style="font-size: 10px;" @endif>{{ $pro->name }}</h3>
			<h3>{{ $pro->barcode }}</h3>
			<h2>Harga &nbsp; Rp. {{ number_format($pro->harga_jual,0,',','.') }}</h2>
			<div class="yellow"></div>
			<h5>Affiliasi Pembeli &nbsp; <b>Rp. {{ number_format($pro->affiliasi * (25 / 100),0,',','.') }}</b></h5>
			<h5 class="affiliasi">Affiliasi &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp; <b style="margin-left: 2px;">Rp. {{ number_format($pro->affiliasi * (75 / 100),0,',','.') }}</b></h5>
		</div>
	</div>
	@if($count % 4 == 0)
	<br><br><br>
	@endif
	@endforeach
	@elseif($type == 1)
	<div id="bod">
		@foreach($product as $pro)
		<span style="display: none;">{{$count++}}</span>
		<div class="label lab-1" style="width: {{ $size[1] }}cm; height: {{ $size[0] }}cm; border: 1px solid black;">
			<div class="title">
				<h3 @if(strlen($pro->name) > 20) style="font-size: 8px;" @endif>{{ $pro->name }}</h3>
				{!! \App\Http\Controllers\KartuController::barcode($pro->barcode) !!}
				<span style="font-size: 10px; margin-top: -20px;">{{ $pro->barcode }}</span>
				<h5 style="font-size: 10px; margin-top: 0;">Harga &nbsp; <b>Rp. {{ number_format($pro->harga_jual,0,',','.') }}</b></h5>
				<h5 style="font-size: 8px;">Affiliasi Pembeli &nbsp; <b>Rp. {{ number_format($pro->affiliasi * (25 / 100),0,',','.') }}</b></h5>
				<h5 class="affiliasi" style="font-size: 8px;">Affiliasi &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp; <b style="margin-left: 2px;">Rp. {{ number_format($pro->affiliasi * (75 / 100),0,',','.') }}</b></h5>
			</div>
		</div>
		@if($count % 4 == 0)
		<br><br><br>
		@endif
		@endforeach
	</div>	
	@else
	<div id="bod">
		@foreach($product as $pro)
		<span style="display: none;">{{$count++}}</span>
		<div class="label lab-1" style="width: {{ $size[1] }}cm; height: {{ $size[0] }}cm; border: 1px solid black;">
			<div class="title">
				<h3 style="font-size: 10px;">{{ $pro->name }}</h3>
				<span style="font-size: 12px; margin-top: -20px;">{{ $pro->barcode }}</span>
				<h5 style="font-size: 10px; margin-top: 0;">Harga &nbsp; <b>Rp. {{ number_format($pro->harga_jual,0,',','.') }}</b></h5>
				<!-- <h5 style="font-size: 8px;">Affiliasi Pembeli &nbsp; <b>Rp. {{ number_format($pro->affiliasi * (25 / 100),0,',','.') }}</b></h5>
				<h5 class="affiliasi" style="font-size: 8px;">Affiliasi &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp; <b style="margin-left: 2px;">Rp. {{ number_format($pro->affiliasi * (75 / 100),0,',','.') }}</b></h5> -->
			</div>
		</div>
		@if($count % 7 == 0)
		<br>
		@elseif($count == 41 || $count == 69 || $count == 97 || $count == 125 || $count == 153 || $count == 181 || $count == 209 || $count == 237 || $count == 265 || $count == 293 || $count == 321)
		<br><br><br><br><br>
		@endif
		@endforeach
	</div>
	@endif
</body>
</html>