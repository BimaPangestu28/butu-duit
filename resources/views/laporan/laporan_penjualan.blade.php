<!DOCTYPE html>
<html>
	<head>
		<title>Laporan </title>
		<style type="text/css">
			.center {
				width: 90%;
				margin: 0 auto;
			}

			body {
				padding: 15px;
				font-family: 'Arial';
				font-size: 9px;
			}

			.title {
				text-align: left;
			}

			.title h3 {
				font-size: 15px;
				text-transform: uppercase;
				margin-bottom: 0;
			}

			.title h4 {
				margin-top: 0px;
				font-size: 13px;
			}

			table {
			    border-collapse: collapse;
			    width: 100%;
			}

			th, td {
			    text-align: left;
			    padding: 5px;
			    border: 1px solid black;
			}
		</style>
	</head>
	<body>
		<div class="center">
			<div class="title">
				<h3>Laporan Penjualan Harian</h3>
				<h4>{{ Auth::user()->name }}</h4>
			</div>

			<div class="barang-keluar">
				<h5>
					Tanggal : {{date('d F Y', strtotime($date)) }}
				</h5>

				
				<table class="table table-responsive-sm table-hover table-outline mb-0">
					<thead class="thead-light">
					<tr>
						<th>#</th>
						<th class="text-center">Tanggal</th>
						<th class="text-center">Pembeli</th>
						<th class="text-center">Nama Cashier</th>
						<th class="text-center">No Transaksi</th>
						<th class="text-center">Total Harga</th>
						<th class="text-center">Potongan</th>
						<th class="text-center">Total Pembayaran</th>
						<th class="text-center">Tunai</th>
						<th class="text-center">Affiliasi</th>
						<th class="text-center">Debet</th>
						<th class="text-center">Kembalian</th>
						<th class="text-center">HPP</th>
					</tr>
					</thead>
					<tbody id="body-data">
					@foreach ($datas as $no => $view)
					<tr data-toggle="modal" data-target="#myModal{{$view->id_order}}" data-id="{{ $view->id_order }}" class="tr">
						<td class="text-center">{{$no+1}}</td>
						<td class="text-center">{{date('d F Y', strtotime($view['created_at']))}} {{$view['created_at']->hour}}:{{$view['created_at']->minute}}</td>
						<td class="text-center">
						@if($view->id_member == null)
						Guest
						@else
						{{$view['nama_member']}}
						@endif
						</td>
						<td class="text-center">{{ $view->kasir }}</td>
						<td class="text-center">
						@if($view->no_transaksi != 0)
							{{ $view->no_transaksi }}
						@else
							Tidak Ada Nomer Transaksi
						@endif
						</td>
						<td class="text-center">Rp. {{ number_format($view['total_pembayaran'] + App\Http\Controllers\LaporanController::totalPotongan($view->id_order),0,',','.') }}</td>
		                <td class="text-center">Rp. {{ number_format(App\Http\Controllers\LaporanController::totalPotongan($view->id_order),0,',','.') }}</td>
						<td class="text-center">Rp. {{ number_format($view['total_pembayaran'],0,',','.') }}</td>
						<td class="text-center">
							@if($view->type == 'tunai')
								Rp. {{ number_format($view['pembayaran'],0,',','.') }}
							@else
								-
							@endif
						</td>
						<td class="text-center">
							@if($view->type == 'affiliasi')
								Rp. {{ number_format($view['pembayaran'],0,',','.') }}
							@else
								-
							@endif
						</td>
						<td class="text-center">
							@if($view->type == 'debet')
								Rp. {{ number_format($view['pembayaran'],0,',','.') }}
							@else
								-
							@endif
						</td>
						<td class="text-center">Rp. {{ number_format($view['kembalian'],0,',','.') }}</td>
						<td class="text-center">
							Rp. {{ number_format(App\Http\Controllers\LaporanController::total($view->id_order, 'hpp'),0,',','.') }}
						</td>
					</tr>
					@endforeach
					</tbody>
				</table>
				<h3>
					<span style="width: 20%; display: inline-block;">Omset</span> : 
					<span style="width: 40%; margin-left: 50px;">Rp. {{ number_format(App\Http\Controllers\LaporanController::totalHari('pembayaran', app('request')->input('date'), app('request')->input('kasir')),0,',','.') }}</span>
				</h3>
				<h3>
					<span style="width: 20%; display: inline-block;">Modal atau Total HPP</span> : 
					<span style="width: 40%; margin-left: 50px;">Rp. {{ number_format(App\Http\Controllers\LaporanController::totalHari('hpp', app('request')->input('date'), app('request')->input('kasir')),0,',','.') }}</span>
				</h3>
				<h3>
					<span style="width: 20%; display: inline-block;">Total Potongan</span> : 
					<span style="width: 40%; margin-left: 50px;">Rp. {{ number_format(App\Http\Controllers\LaporanController::totalHari('diskon', app('request')->input('date'), app('request')->input('kasir')),0,',','.') }}</span>
				</h3>
				<h3>
					<span style="width: 20%; display: inline-block;">Total Affiliasi</span> : 
					<span style="width: 40%; margin-left: 50px;">Rp. {{ number_format(App\Http\Controllers\LaporanController::totalHari('affiliasi', app('request')->input('date'), app('request')->input('kasir')),0,',','.') }}</span>
				</h3>
	            <h3>
	            	<span style="width: 20%; display: inline-block;">Net Profit</span> : 
	            	<span style="width: 40%; margin-left: 50px;">Rp. {{ number_format(App\Http\Controllers\LaporanController::totalHari('pendapatan bersih', app('request')->input('date'), app('request')->input('kasir')),0,',','.') }}</span>
	            </h3>
			</div>
            <br>
		</div>
	</body>
</html>