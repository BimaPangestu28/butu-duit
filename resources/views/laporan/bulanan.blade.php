<!DOCTYPE html>
<html>
	<head>
		<title>Laporan Bulanan</title>
		<style type="text/css">
			.center {
				width: 90%;
				margin: 0 auto;
			}

			body {
				padding: 15px;
				font-family: 'Arial';
			}

			.title {
				text-align: center;
			}

			.title h3 {
				font-size: 24px;
				text-transform: uppercase;
				margin-bottom: 0;
			}

			.title h4 {
				margin-top: 0px;
				font-size: 20px;
			}

			table {
			    border-collapse: collapse;
			    width: 100%;
			}

			th, td {
			    text-align: left;
			    padding: 5px;
			    border: 1px solid black;
			}
		</style>
	</head>
	<body>
		<div class="center">
			<div class="title">
				<h3>Laporan Bulanan UsahakuMart</h3>
				<h4>{{ Auth::user()->name }}</h4>
				<span>
					@if(date('N') == 1)
						Senin, 
					@elseif(date('N') == 2)
						Selasa, 
					@elseif(date('N') == 3)
						Rabu, 
					@elseif(date('N') == 4)
						Kamis, 
					@elseif(date('N') == 5)
						Jum'at, 
					@elseif(date('N') == 6)
						Sabtu, 
					@elseif(date('N') == 7)
						Minggu, 
					@endif
					{{ date(' d - F - Y') }}
				</span>
			</div>

			<br><br>

			<div class="barang-keluar">
				<h3 style="text-align: center;">
					Barang Masuk
				</h3>

				<table class="table table-responsive-sm table-hover table-outline mb-0">
	            <thead class="thead-light">
	              <tr>
	                <th>Kode Barang</th>
	                <th>Nama Barang</th>
	                <th>Harga Modal</th>
	                <th>Harga Jual</th>
	                <th>Jumlah Awal</th>
	                <th>Stok Ditambahkan</th>
	                <th>Total Stok</th>
	                <th>Total Harga Barang</th>
	              </tr>
	            </thead>
	            <tbody>
	              @foreach($arrA as $out)
	              <tr>
	                <td>{{ $out->barcode }}</td>
	                <td>{{ $out->name }}</td>
	                <td>Rp. {{ number_format($out->price,0,',','.') }}</td>
	                <td>Rp. {{ number_format($out->harga_jual,0,',','.') }}</td>
	                <td>{{ \App\Http\Controllers\CashierController::monthAwal($out->id_product) }}</td>
	                <td>{{ \App\Http\Controllers\CashierController::monthIn($out->id_product) }}</td>
	                <td>{{ 
	                		\App\Http\Controllers\CashierController::monthAwal($out->id_product) +
	                	   \App\Http\Controllers\CashierController::monthIn($out->id_product) 
	                	}}
	                </td>
	                <td>Rp. {{ number_format(\App\Http\Controllers\CashierController::monthIn($out->id_product) * $out->harga_jual,0,',','.') }}</td>
	              </tr>
	              @endforeach
	            </tbody>
	          </table>
			</div>

			<br><br>

			<div class="barang-keluar">
				<h3 style="text-align: center;">
					Barang Keluar
				</h3>

				<table class="table table-responsive-sm table-hover table-outline mb-0">
	            <thead class="thead-light">
	              <tr>
	                <th>Kode Barang</th>
	                <th>Nama Barang</th>
	                <th>Harga Modal</th>
	                <th>Harga Jual</th>
	                <th>Jumlah Awal</th>
	                <th>Terjual</th>
	                <th>Sisa Stok</th>
	                <th>Kas Masuk</th>
	                <th>Laba</th>
	              </tr>
	            </thead>
	            <tbody>
	              @foreach($arr as $out)
	              <tr>
	                <td>{{ $out->barcode }}</td>
	                <td>{{ $out->name }}</td>
	                <td>Rp. {{ number_format($out->price,0,',','.') }}</td>
	                <td>Rp. {{ number_format($out->harga_jual,0,',','.') }}</td>
	                <td>{{ $out->stok + \App\Http\Controllers\CashierController::month($out->id_product) }}</td>
	                <td>{{ \App\Http\Controllers\CashierController::month($out->id_product) }}</td>
	                <td>{{ $out->stok }}</td>
	                <td>Rp. {{ number_format(\App\Http\Controllers\CashierController::month($out->id_product) * $out->harga_jual,0,',','.') }}</td>
	                <td>
	                	Rp. {{ number_format(\App\Http\Controllers\CashierController::month($out->id_product) * ($out->keuntungan - $out->affiliasi),0,',','.') }}
	                </td>
	              </tr>
	              @endforeach
	              <tr>
	              	<td colspan="6"><b>OMSET</b></td>
	              	<td colspan="1"><b>Rp. {{ number_format($omset,0,',','.') }}</b></td>
	              	<td colspan="1"><b>Rp. {{ number_format($laba,0,',','.') }}</b></td>
	              </tr>
	            </tbody>
	          </table>
			</div>
		</div>
	</body>
</html>