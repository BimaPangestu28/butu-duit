@extends('administrator/index')

@section('content')
<!-- Breadcrumb -->
<ol class="breadcrumb">
<li class="breadcrumb-item">Home</li>
<li class="breadcrumb-item">Admin</li>
<li class="breadcrumb-item active">Konfirmasi Deposit User</li>

<!-- Breadcrumb Menu-->

</ol>

<div class="container-fluid">

  <div class="card">
    <div class="card-body">
      <div class="row">
        <div class="col-sm-5">
          <h4 class="card-title mb-0">Konfirmasi Deposit User</h4>
          <div class="small text-muted">{{date('d M Y')}}</div>
        </div>
        <!--/.col-->
        <div class="col-sm-7 d-none d-md-block">
          <button type="button" class="btn btn-primary float-right"><i class="icon-cloud-download"></i></button>
          <div class="btn-toolbar float-right" role="toolbar" aria-label="Toolbar with button groups">
          </div>
        </div>
        <!--/.col-->
      </div>
      @if($errors->count() > 0 || session()->has('alert'))
      <div class="row">
      	<div class="alert alert-danger col-sm-12" style="margin-bottom: 0; margin-top: 25px;">
      		@if($errors->count() > 0)
      			@foreach($errors->all() as $error)
      			<p>{{ $error }}</p>
      			@endforeach
      		@else
      			<p>{{ session()->get('alert') }}</p>
      		@endif
      	</div>
      </div>
      @endif
    </div>
  </div>
  <!--/.card-->


  <div class="row">
    <div class="col-md-12">

      <div class="card">
        <div class="card-header">
          Usahaku Toserba - Toko Offline
        </div>
        <div class="card-body">
          <table class="table table-responsive-sm table-outline mb-0">
            <thead class="thead-light">
              <tr>
                <th class="text-center">Tanggal</th>
                <th class="text-center">Jumlah Transfer</th>
                <th class="text-center">Bukti Transfer</th>
                <th class="text-center">Option</th>
              </tr>
            </thead>
            <tbody>
              @foreach($deposit as $w)
              <tr>
                <td class="text-center">{{ date('d-m-Y H:m:s', strtotime($w->created_at)) }}</td>
                <td class="text-center">Rp. {{ number_format($w->jumlah,0,',','.') }}</td>
                <td class="text-center">
                  <a href="/gambar/{{ $w->gambar }}" target="_blank">
                    <img src="/gambar/{{ $w->gambar }}" width="200px" height="200px" style="object-fit: cover;">
                  </a>
                </td>
                <td class="text-center">
                  <a href="/usahakumart/request-deposit-user/tolak?id={{ $w->id }}" onclick="return confirm('Apakah anda yakin?')" class="btn btn-primary">Tolak Request</a>
                  <a href="/usahakumart/request-deposit-user/konfirmasi?id={{ $w->id }}" onclick="return confirm('Apakah anda yakin?')" class="btn btn-danger">Konfirmasi Request</a>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <!--/.col-->
  </div>
  <!--/.row-->
</div>

</div>

@endsection
