@extends('administrator/index')

@section('js')
	<script type="text/javascript" src="/admin/js/dashboard.js"></script>
@endsection

@section('content')
<main class="main">

<!-- Breadcrumb -->
<ol class="breadcrumb">
<li class="breadcrumb-item">Home</li>
<li class="breadcrumb-item">Admin</li>
<li class="breadcrumb-item active">Dashboard</li>

<!-- Breadcrumb Menu-->

</ol>

<div class="container-fluid">

  <div class="card">
    <div class="card-body">
      <div class="row">
        <div class="col-sm-5">
          <h4 class="card-title mb-0">Dashboard</h4>
          <div class="small text-muted">{{date('d M Y')}}</div>
        </div>
        <!--/.col-->
        <div class="col-sm-7 d-none d-md-block">
          <button type="button" class="btn btn-primary float-right"><i class="icon-cloud-download"></i></button>
          <div class="btn-toolbar float-right" role="toolbar" aria-label="Toolbar with button groups">
          </div>
        </div>
        <!--/.col-->
      </div>
    </div>
  </div>
  <!--/.card-->


  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          Traffic &amp; Sales
        </div>
        <div class="card-body">
          <div class="row">
            <div class="col-sm-12 col-lg-4">
              <div class="row">
                <div class="col-sm-6">
                  <div class="callout callout-info">
                    <small class="text-muted">Member Baru</small>
                    <br>
                    <strong class="h4" id="anggota-baru"></strong>
                    <div class="chart-wrapper">
                      <canvas id="sparkline-chart-1" width="100" height="30"></canvas>
                    </div>
                  </div>
                </div>
                <!--/.col-->
                <div class="col-sm-6">
                  <div class="callout callout-danger">
                    <small class="text-muted">Total Member</small>
                    <br>
                    <strong class="h4" id="total-anggota"></strong>
                    <div class="chart-wrapper">
                      <canvas id="sparkline-chart-2" width="100" height="30"></canvas>
                    </div>
                  </div>
                </div>
                <!--/.col-->
              </div>
              <!--/.row-->
              <hr class="mt-0">
            </div>
            <!--/.col-->
            <div class="col-sm-6 col-lg-4">
              <div class="row">
                <div class="col-sm-6">
                  <div class="callout callout-warning">
                    <small class="text-muted">Stok Kartu</small>
                    <br>
                    <strong class="h4" id="kartu"></strong>
                    <div class="chart-wrapper">
                      <canvas id="sparkline-chart-3" width="100" height="30"></canvas>
                    </div>
                  </div>
                </div>
                <!--/.col-->
                <div class="col-sm-6">
                  <div class="callout callout-success">
                    <small class="text-muted">Jumlah Toko UMart</small>
                    <br>
                    <strong class="h4" id="toko"></strong>
                    <div class="chart-wrapper">
                      <canvas id="sparkline-chart-4" width="100" height="30"></canvas>
                    </div>
                  </div>
                </div>
                <!--/.col-->
              </div>
              <!--/.row-->
              <hr class="mt-0">
            </div>
            <!--/.col-->
            <div class="col-sm-6 col-lg-4">
              <div class="row">
                <div class="col-sm-6">
                  <div class="callout">
                    <small class="text-muted">Member Aktif</small>
                    <br>
                    <strong class="h4" id="aktif"></strong>
                    <div class="chart-wrapper">
                      <canvas id="sparkline-chart-5" width="100" height="30"></canvas>
                    </div>
                  </div>
                </div>
                <!--/.col-->
                <div class="col-sm-6">
                  <div class="callout callout-primary">
                    <small class="text-muted">Member Tidak Aktif</small>
                    <br>
                    <strong class="h4" id="tidak-aktif"></strong>
                    <div class="chart-wrapper">
                      <canvas id="sparkline-chart-6" width="100" height="30"></canvas>
                    </div>
                  </div>
                </div>
                <!--/.col-->
              </div>
              <!--/.row-->
              <hr class="mt-0">
            </div>
            <!--/.col-->
          </div>
          <!--/.row-->
          <br>
          <table class="table table-responsive-sm table-hover table-outline mb-0">
            <thead class="thead-light">
              <tr>
                <th class="text-center"><i class="icon-people"></i></th>
                <th>Nama Toko</th>
                <th>Wilayah</th>
                <th class="text-center">Status Toko</th>
              </tr>
            </thead>
            <tbody>
            @foreach($toko as $t)
              <tr>
                <td class="text-center">
                  <div class="avatar">
                    <img src="/admin/img/logo-symbol.png" class="img-avatar">
                  </div>
                </td>
                <td>
                  <div>{{ $t->name }}</div>
                  <div class="small text-muted">
                    Terdaftar: {{ date('d F Y', strtotime($t->created_at)) }}
                  </div>
                </td>
                <td>
                  <div class="clearfix">
                    {{ $t->provinsi }}, {{ $t->kota }}, {{ $t->kecamatan }}
                  </div>  
                </td>
                <td class="text-center">
                  @if($t->aktif == 1)
                  Aktif
                  @else
                  Tidak Aktif
                  @endif
                </td>
              </tr>
            @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <!--/.col-->
  </div>
  <!--/.row-->
</div>

</div>
<!-- /.conainer-fluid -->
</main>
@endsection