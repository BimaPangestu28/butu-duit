@extends('administrator/index')

@section('js')
<script type="text/javascript" src="/js/alamat.js"></script>
@endsection

@section('content')
<main class="main">

<!-- Breadcrumb -->
<ol class="breadcrumb">
<li class="breadcrumb-item">Home</li>
<li class="breadcrumb-item">Admin</li>
<li class="breadcrumb-item active">Dashboard</li>

<!-- Breadcrumb Menu-->

</ol>

<div class="container-fluid">

  <div class="card">
    <div class="card-body">
      <div class="row">
        <div class="col-sm-5">
          <h4 class="card-title mb-0">Tambah Alamat</h4>
          <div class="small text-muted">{{date('d M Y')}}</div>
        </div>
        <!--/.col-->
        <div class="col-sm-7 d-none d-md-block">
          <button type="button" class="btn btn-primary float-right"><i class="icon-cloud-download"></i></button>
          <div class="btn-toolbar float-right" role="toolbar" aria-label="Toolbar with button groups">
          </div>
        </div>
        <!--/.col-->
      </div>
      @if($errors->count() > 0 || session()->has('alert'))
      <div class="row">
      	<div class="alert alert-danger col-sm-12" style="margin-bottom: 0; margin-top: 25px;">
      		@if($errors->count() > 0)
      			@foreach($errors->all() as $error)
      			<p>{{ $error }}</p>
      			@endforeach
      		@else
      			<p>{{ session()->get('alert') }}</p>
      		@endif
      	</div>
      </div>
      @endif
    </div>
  </div>
  <!--/.card-->


  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          Usahaku Toserba - Tambah Provinsi
        </div>
        <div class="card-body">
          <table class="table table-hover">
            <thead>
              <tr>
                <th width="5%" class="text-center">No</th>
                <th width="75%" class="text-center">Nama Provinsi</th>
                <th width="20%" class="text-center">Action</th>
              </tr>
            </thead>
            <tbody id="table-provinsi">
              @foreach($provinsi as $no => $p)
              <tr id="provinsi-{{ $p->id }}">
                <td class="text-center">{{ $no += 1 }}</td>
                <td class="text-center" id="nama-provinsi-{{ $p->id }}">{{ $p->provinsi }}</td>
                <td class="text-center">
                  <button type="button" id="edit" data-id="{{ $p->id }}" class="btn btn-warning edit-provinsi" style="color: white;">Edit</button>
                  <button type="button" id="hapus" data-id="{{ $p->id }}" class="btn btn-danger hapus-provinsi">Hapus</button>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>

          <form method="POST" id="form-provinsi">
          	{{ csrf_field() }}
            <input type="hidden" id="provinsi-id" value="">

          	<div class="form-group">
          		<strong>Tambah Provinsi</strong><br>
          		<input type="text" id="provinsi" class="form-control" style="margin-top: 10px;" placeholder="">
          	</div>

          	<input type="submit" class="btn btn-primary form-control" id="button-provinsi" value="Tambah Provinsi">
          </form>
        </div>
      </div>
      <div class="card">
        <div class="card-header">
          Usahaku Toserba - Tambah Kota
        </div>
        <div class="card-body">
          <table class="table table-hover">
            <thead>
              <tr>
                <th width="5%" class="text-center">No</th>
                <th width="35%" class="text-center">Nama Provinsi</th>
                <th width="35%" class="text-center">Nama Kota</th>
                <th width="20%" class="text-center">Action</th>
              </tr>
            </thead>
            <tbody id="table-kota">
              @foreach($kota as $no => $k)
              <tr id="kota-{{ $k->id }}">
                <td class="text-center">{{ $no += 1 }}</td>
                <td class="text-center" data-id="{{ $k->id_provinsi }}" id="nama-kota-provinsi-{{ $k->id }}">{{ $k->provinsi }}</td>
                <td class="text-center" id="nama-kota-{{ $k->id }}">{{ $k->kota }}</td>
                <td class="text-center">
                  <button type="button" data-id="{{ $k->id }}" class="btn btn-warning edit-kota" style="color: white;">Edit</button>
                  <button type="button" data-id="{{ $k->id }}" class="btn btn-danger hapus-kota">Hapus</button>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>

          <form method="POST" id="form-kota">
            {{ csrf_field() }}
            <input type="hidden" id="kota-id" value="">

            <div class="form-group">
              <strong>Pilih Provinsi</strong><br>
              <select id="kota-id-provinsi" class="form-control" style="margin-top: 10px;">
                @foreach($provinsi as $p)
                <option value="{{ $p->id }}">{{ $p->provinsi }}</option>
                @endforeach
              </select>
            </div>

            <div class="form-group">
              <strong>Tambah Kota</strong><br>
              <input type="text" id="kota" class="form-control" style="margin-top: 10px;" placeholder="">
            </div>

            <input type="submit" class="btn btn-primary form-control" id="button-kota" value="Tambah Kota">
          </form>
        </div>
      </div>
      <div class="card">
        <div class="card-header">
          Usahaku Toserba - Tambah Kecamatan
        </div>
        <div class="card-body">
          <table class="table table-hover">
            <thead>
              <tr>
                <th width="5%" class="text-center">No</th>
                <th width="25%" class="text-center">Nama Kota</th>
                <th width="25%" class="text-center">Nama Kecamatan</th>
                <th width="20%" class="text-center">Action</th>
              </tr>
            </thead>
            <tbody id="table-kecamatan">
              @foreach($kecamatan as $no => $k)
              <tr id="kecamatan-{{ $k->id }}">
                <td class="text-center">{{ $no += 1 }}</td>
                <td class="text-center" data-id="{{ $k->id_kota }}" id="nama-kecamatan-kota-{{ $k->id }}">{{ $k->kota }}</td>
                <td class="text-center" id="nama-kecamatan-{{ $k->id }}">{{ $k->kecamatan }}</td>
                <td class="text-center">
                  <button type="button" data-id="{{ $k->id }}" class="btn btn-warning edit-kecamatan" style="color: white;">Edit</button>
                  <button type="button" data-id="{{ $k->id }}" class="btn btn-danger hapus-kecamatan">Hapus</button>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>

          <form method="POST" id="form-kecamatan">
            {{ csrf_field() }}

            <input type="hidden" id="kecamatan-id" value="">

            <div class="form-group">
              <strong>Pilih Kota</strong><br>
              <select id="kecamatan-id-kota" class="form-control" style="margin-top: 10px;">
                @foreach($kota as $k)
                <option value="{{ $k->id }}">{{ $k->kota }}</option>
                @endforeach
              </select>
            </div>

            <div class="form-group">
              <strong>Tambah Kecamatan</strong><br>
              <input type="text" id="kecamatan" class="form-control" style="margin-top: 10px;" placeholder="">
            </div>

            <input type="submit" class="btn btn-primary form-control" id="button-kecamatan" value="Tambah Kecamatan">
          </form>
        </div>
      </div>
    </div>
    <!--/.col-->
  </div>
  <!--/.row-->
</div>

</div>
<!-- /.conainer-fluid -->
</main>
@endsection
