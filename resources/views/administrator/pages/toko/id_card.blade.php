<!DOCTYPE html>
<html>
<head>
	<title></title>
	<style type="text/css">
		body {
			margin: 0 auto;
		}

		.card {
			width: 8.5cm;
			height: 5.4cm;
			display: inline-block;
		}

		.card img {
			object-fit: cover;
		}

		.card .gambar {
			height: 100%;
			width: 100%;
		}

		.barcode {
			margin-top: -40px; 
			margin-left: 10px;
			text-align: left;
		}

		.barcode-2 {
			margin-top: -61.5px; 
			margin-left: 30px;
			letter-spacing: 2px;
		}

		#box {
			margin-bottom: 10px;
		}
	</style>
</head>
<body>
	@if($submit == 'Cetak Kartu Depan')
		@for($i = 0; $i < count($arr); $i++)
			<div class="card" style="position: relative; @if($i % 2 == 0) margin-left: .2cm; @endif">
				<img src="{{ public_path() }}/kartu/kartu.jpg" class="gambar">
				<div class="barcode">
					{!! $arr[$i] !!}
					<span style="font-size: 9px; display: block;">{{ $range[$i] }}</span><br><br>
				</div>
			</div>
			<?php $no = 1 ?>
			<span style="display: none;">{{ $no++ }}</span>
			@if($i % 2 != 0)
			<br><br>
			@endif
		@endfor
	@else
		<div style="float: right;">
		@for($i = 0; $i < count($arr); $i++)
			<div class="card" style="position: relative;">
				<img src="{{ public_path() }}/kartu/belakang.jpg" class="gambar">
			</div>
			<?php $no = 1 ?>
			<span style="display: none;">{{ $no++ }}</span>
			@if($i % 2 != 0)
			<br><br>
			@endif
		@endfor
		</div>
	@endif
</body>
</html>