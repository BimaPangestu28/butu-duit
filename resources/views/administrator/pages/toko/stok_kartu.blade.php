@extends('administrator/index')

@section('js')
  <script type="text/javascript" src="/admin/js/toko.js"></script>
@endsection

@section('content')
<main class="main">

<!-- Breadcrumb -->
<ol class="breadcrumb">
<li class="breadcrumb-item">Home</li>
<li class="breadcrumb-item">Admin</li>
<li class="breadcrumb-item active">Daftar Stok Kartu</li>

<!-- Breadcrumb Menu-->

</ol>

<div class="container-fluid">

  <div class="card">
    <div class="card-body">
      <div class="row">
        <div class="col-sm-5">
          <h4 class="card-title mb-0">Daftar Stok Kartu</h4>
          <div class="small text-muted">{{date('d M Y')}}</div>
        </div>
        <!--/.col-->
        <div class="col-sm-7 d-none d-md-block">
          <button type="button" class="btn btn-primary float-right"><i class="icon-cloud-download"></i></button>
          <div class="btn-toolbar float-right" role="toolbar" aria-label="Toolbar with button groups">
          </div>
        </div>
        <!--/.col-->
      </div>
      @if(session()->has('alert'))
      <div class="row">
        <div class="alert alert-danger col-sm-12" style="margin-bottom: 0; margin-top: 25px;">
            <p>{{ session()->get('alert') }}</p>
        </div>
      </div>
      @endif

      @if($errors->count() > 0 )
      <div class="row">
        <div class="alert alert-danger col-sm-12" style="margin-bottom: 0; margin-top: 25px;">
            @foreach ($errors->all() as $error)
              <p>{{$error}}</p>
            @endforeach
        </div>
      </div>
      @endif
    </div>
  </div>
  <!--/.card-->


  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          Usahaku Toserba - Toko Offline
        </div>
        <div class="card-body">
          <table class="table table-responsive-sm table-hover table-outline mb-0">
            <thead class="thead-light">
              <tr>
                <th class="text-center">Jumlah Kartu</th>
                <th class="text-center">Jumlah Kartu Yang Telah Diorder</th>
                <th class="text-center">Jumlah Kartu Yang Belum Diorder</th>
              </tr>
            </thead>
            <tbody> 
              <tr>
                <td class="text-center">{{ $jumlah }}</td>
                <td class="text-center">{{ $sudah }}</td>
                <td class="text-center">{{ $belum }}</td>
              </tr>
            </tbody>
          </table>
      </div>

      <div class="card">
        <div class="card-header">
          Usahaku Toserba - Toko Offline
        </div>
        <div class="card-body">
          <form method="POST" action="/usahakumart/stok-kartu/tambah" >
              {{ csrf_field() }}
              <input class="form-control" type="text" placeholder="Masukkan Nomer Awal" name="min_card"></input><br>
              <input class="form-control" type="text" placeholder="Masukkan Nomer Akhir" name="max_card"></input><br>
              <button type="submit" class="btn btn-primary form-control"">Tambah Stok Kartu</button>
          </form>
          <br>
          <table class="table table-responsive-sm table-hover table-outline mb-0">
            <thead class="thead-light">
              <tr>
                <th>Nomer Kartu</th>
                <th class="text-center">Action</th>
              </tr>
            </thead>
            <tbody>
              @foreach($card as $c)
              <tr>
                <td>{{ $c->number_card }}</td>
                <td class="text-center">
                  <a href="/usahakumart/stok-kartu/hapus?id={{ $c->id }}" class="btn btn-danger" onclick="return confirm('Apakah Anda Yakin?')">Hapus Kartu</a>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
          {{ $card->render() }}

        </div>
      </div>
    </div>
    <!--/.col-->
  </div>
  <!--/.row-->
</div>

</div>
<!-- /.conainer-fluid -->
</main>
@endsection
