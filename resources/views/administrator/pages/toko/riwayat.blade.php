@extends('administrator/index')

@section('js')
  <script type="text/javascript" src="/admin/js/toko.js"></script>
@endsection

@section('content')
<main class="main">

<!-- Breadcrumb -->
<ol class="breadcrumb">
<li class="breadcrumb-item">Home</li>
<li class="breadcrumb-item">Admin</li>
<li class="breadcrumb-item active">History Toko</li>

<!-- Breadcrumb Menu-->

</ol>

<div class="container-fluid">

  <div class="card">
    <div class="card-body">
      <div class="row">
        <div class="col-sm-5">
          <h4 class="card-title mb-0">Detail Toko</h4>
          <div class="small text-muted">{{date('d M Y')}}</div>
        </div>
        <!--/.col-->
        <div class="col-sm-7 d-none d-md-block">
          <button type="button" class="btn btn-primary float-right"><i class="icon-cloud-download"></i></button>
          <div class="btn-toolbar float-right" role="toolbar" aria-label="Toolbar with button groups">
          </div>
        </div>
        <!--/.col-->
      </div>
      @if(session()->has('alert'))
      <div class="row">
        <div class="alert alert-danger col-sm-12" style="margin-bottom: 0; margin-top: 25px;">
            <p>{{ session()->get('alert') }}</p>
        </div>
      </div>
      @endif
    </div>
  </div>
  <!--/.card-->


  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          Usahaku Toserba - Riwayat Pembelian Toko Hari Ini
        </div>
        <div class="card-body">
          <table class="table table-responsive-sm table-hover table-outline mb-0">
            <thead class="thead-light">
              <tr>
                <th>Nama Pembeli</th>
                <th>Total Harga Barang</th>
                <th>Total Uang Pembayaran</th>
                <th>Total Uang Kembalian</th>
                <th class="text-center">Tanggal</th>
              </tr>
            </thead>
            <tbody>
            @foreach($order as $t)
              <tr>
                <td>
                  @if($t->id_member != null)
                    {{$t->nama}}
                  @else
                    Guest
                  @endif
                </td>
                <td>
                  Rp. {{ number_format($t->total_pembayaran,0,',','.') }}
                </td>
                <td>
                  Rp. {{ number_format($t->pembayaran,0,',','.') }}
                </td>
                <td>
                  Rp. {{ number_format($t->kembalian,0,',','.') }}
                </td>
                <td class="text-center">
                  {{ $t->created_at }}
                </td>
              </tr>
            @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <!--/.col-->
  </div>
  <!--/.row-->
</div>

</div>
<!-- /.conainer-fluid -->
</main>
@endsection
