@extends('administrator/index')

@section('js')
	<script type="text/javascript" src="/admin/js/toko.js"></script>
@endsection

@section('content')
<main class="main">

<!-- Breadcrumb -->
<ol class="breadcrumb">
<li class="breadcrumb-item">Home</li>
<li class="breadcrumb-item">Admin</li>
<li class="breadcrumb-item active">Dashboard</li>

<!-- Breadcrumb Menu-->

</ol>


<div class="loader">
  <img src="/loader/produk.gif">
</div>

<div class="container-fluid">

  <div class="card">
    <div class="card-body">
      <div class="row">
        <div class="col-sm-5">
          <h4 class="card-title mb-0">Edit data</h4>
          <div class="small text-muted">{{date('d M Y')}}</div>
        </div>
        <!--/.col-->
        <div class="col-sm-7 d-none d-md-block">
          <button type="button" class="btn btn-primary float-right"><i class="icon-cloud-download"></i></button>
          <div class="btn-toolbar float-right" role="toolbar" aria-label="Toolbar with button groups">
          </div>
        </div>
        <!--/.col-->
      </div>
      @if($errors->count() > 0 || session()->has('alert'))
      <div class="row">
      	<div class="alert alert-danger col-sm-12" style="margin-bottom: 0; margin-top: 25px;">
      		@if($errors->count() > 0)
      			@foreach($errors->all() as $error)
      			<p>{{ $error }}</p>
      			@endforeach
      		@else
      			<p>{{ session()->get('alert') }}</p>
      		@endif
      	</div>
      </div>
      @endif
    </div>
  </div>
  <!--/.card-->


  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          Usahaku Toserba - data Offline
        </div>
        <div class="card-body">
          <form method="POST" action="/usahakumart/update-toko/{{ $data->id_toko }}">
          	{{ csrf_field() }}

            <input type="hidden" name="oldpass" value="{{ bcrypt('$data->password') }}">

          	<div class="form-group">
          		<strong>Nama Toko</strong><br>
          		<input type="text" name="name" class="form-control" style="margin-top: 10px;" placeholder="Nama data" value="{{ $data->name }}">
          	</div>

            <div class="form-group">
              <strong>Username Toko</strong><br>
              <input type="text" name="username" class="form-control" style="margin-top: 10px;" placeholder="Username data" value="{{ $data->user_toko }}">
            </div>

          	<div class="form-group">
          		<strong>Password Toko</strong><br>
          		<input type="password" name="password" class="form-control" style="margin-top: 10px;" placeholder="Password data">
          	</div>

          	<div class="form-group">
          		<strong>Provinsi</strong><br>
          		<select class="form-control" name="id_provinsi" style="margin-top: 10px;" id="provinsi">
                <option value="{{ $data->id_provinsi }}">{{ $data->provinsi }}</option>
          			@foreach($provinsi as $p)
          			<option value="{{ $p->id }}">{{ $p->provinsi }}</option>
          			@endforeach
          		</select>
          	</div>

          	<div class="form-group">
          		<strong>Kota</strong><br>
          		<select class="form-control" name="id_kota" style="margin-top: 10px;" id="kota">
                <option value="{{ $data->id_kota }}">{{ $data->kota }}</option>
          			@foreach($kota as $k)
          			<option value="{{ $k->id }}">{{ $k->kota }}</option>
          			@endforeach
          		</select>
          	</div>

          	<div class="form-group">
          		<strong>Kecamatan</strong><br>
          		<select class="form-control" name="id_kecamatan" style="margin-top: 10px;" id="kecamatan">
                <option value="{{ $data->id_kecamatan }}">{{ $data->kecamatan }}</option>
          			@foreach($kecamatan as $k)
          			<option value="{{ $k->id }}">{{ $k->kecamatan }}</option>
          			@endforeach
          		</select>
          	</div>

            <div class="form-group">
              <strong>Alamat Toko</strong><br>
              <input type="text" name="alamat" class="form-control" style="margin-top: 10px;" placeholder="Alamat data" value="{{ $data->alamat_toko }}">
            </div>

            <div class="form-group">
              <strong>Kategori Toko</strong><br>
              <select class="form-control" name="id_kategori" style="margin-top: 10px;">
                <option value="{{ $data->id_kategori }}">{{ $data->kategori }}</option>
                @foreach($kategori as $k)
                <option value="{{ $k->id }}">{{ $k->kategori }}</option>
                @endforeach
              </select>
            </div>

            <div class="form-group">
              <strong>Tipe Toko</strong><br>
              <select class="form-control" name="id_tipe" style="margin-top: 10px;">
                <option value="{{ $data->id_tipe }}">{{ $data->tipe }}</option>
                @foreach($tipe as $k)
                <option value="{{ $k->id }}">{{ $k->tipe }}</option>
                @endforeach
              </select>
            </div>

            <div class="form-group">
              <strong>Pemilik toko</strong><br>
              <select class="form-control" name="id_pemilik" style="margin-top: 10px;">
                <option value="{{ $data->id_pemilik }}">{{ $data->id_pemilik }} - {{ $data->nama }}</option>
                @foreach($member as $k)
                <option value="{{ $k->id_member }}">{{ $k->id_member}} - {{ $k->nama }}</option>
                @endforeach
              </select>
            </div>

          	<br><br>

          	<input type="submit" class="btn btn-primary form-control" value="Tambah data">
          </form>
        </div>
      </div>
    </div>
    <!--/.col-->
  </div>
  <!--/.row-->
</div>

</div>
<!-- /.conainer-fluid -->
</main>
@endsection
