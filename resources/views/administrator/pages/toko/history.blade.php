@extends('administrator/index')

@section('js')
	<script type="text/javascript" src="/admin/js/toko.js"></script>
@endsection

@section('content')
<main class="main">

<!-- Breadcrumb -->
<ol class="breadcrumb">
<li class="breadcrumb-item">Home</li>
<li class="breadcrumb-item">Admin</li>
<li class="breadcrumb-item active">History Toko</li>

<!-- Breadcrumb Menu-->

</ol>

<div class="container-fluid">

  <div class="card">
    <div class="card-body">
      <div class="row">
        <div class="col-sm-5">
          <h4 class="card-title mb-0">Detail Toko</h4>
          <div class="small text-muted">{{date('d M Y')}}</div>
        </div>
        <!--/.col-->
        <div class="col-sm-7 d-none d-md-block">
          <button type="button" class="btn btn-primary float-right"><i class="icon-cloud-download"></i></button>
          <div class="btn-toolbar float-right" role="toolbar" aria-label="Toolbar with button groups">
          </div>
        </div>
        <!--/.col-->
      </div>
      @if(session()->has('alert'))
      <div class="row">
      	<div class="alert alert-danger col-sm-12" style="margin-bottom: 0; margin-top: 25px;">
      			<p>{{ session()->get('alert') }}</p>
      	</div>
      </div>
      @endif
    </div>
  </div>
  <!--/.card-->


  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          Usahaku Toserba - Produk Terpopuler Di Toko
        </div>
        <div class="card-body">
          <table class="table table-responsive-sm table-hover table-outline mb-0">
            <thead class="thead-light">
              <tr>
                <th>Nama Produk</th>
                <th>Terjual Hari Ini</th>
                <th>Terjual Bulan Ini</th>
                <th>Terjual Tahun Ini</th>
                <th>Jumlah Terjual</th>
                <th>Stok Barang</th>
                <th>Jumlah Total Harga Barang</th>
              </tr>
            </thead>
            <tbody>
            @foreach($populer as $t)
              <tr>
                <td>{{ $t->name }}</td>
                <td class="text-center">{{ \App\Http\Controllers\CashierController::today($t->id) }}</td>
                <td class="text-center">{{ \App\Http\Controllers\CashierController::month($t->id) }}</td>
                <td class="text-center">{{ \App\Http\Controllers\CashierController::year($t->id) }}</td>
                <td class="text-center">{{ $t->terjual }}</td>
                <td class="text-center">{{ $t->stok }}</td>
                <td class="text-center">Rp. {{ number_format($t->stok * $t->harga_jual,0,',','.') }}</td>
              </tr>
            @endforeach
            </tbody>
          </table>
          <br><br>
          <a href="/usahakumart/detail-toko/{{ $id }}/produk-terpopuler" class="btn btn-primary form-control">Lihat Semuanya</a>
        </div>
      </div>

      <div class="card">
        <div class="card-header">
          Usahaku Toserba - Stok Barang Di Toko
        </div>
        <div class="card-body">
          <table class="table table-responsive-sm table-hover table-outline mb-0">
            <thead class="thead-light">
              <tr>
                <th>Nama Barang</th>
                <th class="text-center">Harga</th>
                <th class="text-center">Affiliasi</th>
                <th class="text-center">Barcode</th>
                <th class="text-center">Stok Barang</th>
                <th class="text-center">Jumlah Total Harga Barang</th>
                <th class="text-center">Status Barang</th>
              </tr>
            </thead>
            <tbody>
            @foreach($produk as $t)
              <tr>
                <td>{{ $t->name }}</td>
                <td class="text-center">Rp. {{ number_format($t->price,0,',','.') }}</td>
                <td class="text-center">Rp. {{ number_format($t->affiliasi,0,',','.') }}</td>
                <td class="text-center">{{ $t->barcode }}</td>
                <td class="text-center">{{ $t->stok }}</td>
                <td class="text-center">Rp. {{ number_format($t->stok * $t->harga_jual,0,',','.') }}</td>
                <td class="text-center">
                  @if($t->stok <= 30 && $t->stok > 20)
                  <button class="btn" style="background: green; width: 100%;"></button>
                  @elseif($t->stok <= 20 && $t->stok > 10)
                  <button class="btn" style="background: yellow; width: 100%;"></button>
                  @elseif(($t->stok <= 10))
                  <button class="btn" style="background: red; width: 100%;"></button>
                  @else
                  <button class="btn" style="background: blue; width: 100%;"></button>
                  @endif
                </td>
              </tr>
            @endforeach
            </tbody>
          </table>
          <br><br>
          <a href="/usahakumart/detail-toko/{{ $id }}/gudang" class="btn btn-primary form-control">Lihat Semuanya</a>
        </div>
      </div>

      <div class="card">
        <div class="card-header">
          Usahaku Toserba - Riwayat Pembelian Toko Hari Ini
          <a href="/usahakumart/detail-toko/{{ $id }}/hari/print" style="margin-top: 1px;" class="btn btn-primary">Print Riwayat Hari Ini</a>
        </div>
        <div class="card-body">
          <table class="table table-responsive-sm table-hover table-outline mb-0">
            <thead class="thead-light">
              <tr>
                <th>Nama Pembeli</th>
                <th>Total Harga Barang</th>
                <th>Total Uang Pembayaran</th>
                <th>Total Uang Kembalian</th>
                <th class="text-center">Tanggal</th>
              </tr>
            </thead>
            <tbody>
            @foreach($order as $t)
              <tr>
                <td>
                  @if($t->id_member != null)
                    {{$t->nama}}
                  @else
                    Guest
                  @endif
                </td>
                <td>
                  Rp. {{ number_format($t->total_pembayaran,0,',','.') }}
                </td>
                <td>
                  Rp. {{ number_format($t->pembayaran,0,',','.') }}
                </td>
                <td>
                  Rp. {{ number_format($t->kembalian,0,',','.') }}
                </td>
                <td class="text-center">
                  {{ $t->created_at }}
                </td>
              </tr>
            @endforeach
            </tbody>
          </table>
          <br><br>
          <a href="/usahakumart/detail-toko/{{ $id }}/hari/riwayat-pembelian" class="btn btn-primary form-control">Lihat Semuanya</a>
        </div>
      </div>

      <div class="card">
        <div class="card-header">
          Usahaku Toserba - Riwayat Pembelian Toko Bulan Ini
          <a href="/usahakumart/detail-toko/{{ $id }}/bulan/print" style="margin-top: 1px;" class="btn btn-primary">Print Riwayat Bulan Ini</a>
        </div>
        <div class="card-body">
          <table class="table table-responsive-sm table-hover table-outline mb-0">
            <thead class="thead-light">
              <tr>
                <th>Nama Pembeli</th>
                <th>Total Harga Barang</th>
                <th>Total Uang Pembayaran</th>
                <th>Total Uang Kembalian</th>
                <th class="text-center">Tanggal</th>
              </tr>
            </thead>
            <tbody>
            @foreach($month as $t)
              <tr>
                <td>
                  @if($t->id_member != null)
                    {{$t->nama}}
                  @else
                    Guest
                  @endif
                </td>
                <td>
                  Rp. {{ number_format($t->total_pembayaran,0,',','.') }}
                </td>
                <td>
                  Rp. {{ number_format($t->pembayaran,0,',','.') }}
                </td>
                <td>
                  Rp. {{ number_format($t->kembalian,0,',','.') }}
                </td>
                <td class="text-center">
                  {{ $t->created_at }}
                </td>
              </tr>
            @endforeach
            </tbody>
          </table>
          <br><br>
          <a href="/usahakumart/detail-toko/{{ $id }}/bulan/riwayat-pembelian" class="btn btn-primary form-control">Lihat Semuanya</a>
        </div>
      </div>
    </div>
    <!--/.col-->
  </div>
  <!--/.row-->
</div>

</div>
<!-- /.conainer-fluid -->
</main>
@endsection
