@extends('administrator/index')

@section('js')
  <script type="text/javascript" src="/admin/js/toko.js"></script>
@endsection

@section('content')
<main class="main">

<!-- Breadcrumb -->
<ol class="breadcrumb">
<li class="breadcrumb-item">Home</li>
<li class="breadcrumb-item">Admin</li>
<li class="breadcrumb-item active">History Toko</li>

<!-- Breadcrumb Menu-->

</ol>

<div class="container-fluid">

  <div class="card">
    <div class="card-body">
      <div class="row">
        <div class="col-sm-5">
          <h4 class="card-title mb-0">Detail Toko</h4>
          <div class="small text-muted">{{date('d M Y')}}</div>
        </div>
        <!--/.col-->
        <div class="col-sm-7 d-none d-md-block">
          <button type="button" class="btn btn-primary float-right"><i class="icon-cloud-download"></i></button>
          <div class="btn-toolbar float-right" role="toolbar" aria-label="Toolbar with button groups">
          </div>
        </div>
        <!--/.col-->
      </div>
      @if(session()->has('alert'))
      <div class="row">
        <div class="alert alert-danger col-sm-12" style="margin-bottom: 0; margin-top: 25px;">
            <p>{{ session()->get('alert') }}</p>
        </div>
      </div>
      @endif
    </div>
  </div>
  <!--/.card-->


  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          Usahaku Toserba - Stok Barang Di Toko
        </div>
        <div class="card-body">
          <table class="table table-responsive-sm table-hover table-outline mb-0">
            <thead class="thead-light">
              <tr>
                <th>Nama Barang</th>
                <th class="text-center">Harga</th>
                <th class="text-center">Affiliasi</th>
                <th class="text-center">Barcode</th>
                <th class="text-center">Stok Barang</th>
                <th class="text-center">Jumlah Total Harga Barang</th>
                <th class="text-center">Status Barang</th>
              </tr>
            </thead>
            <tbody>
            @foreach($produk as $t)
              <tr>
                <td>{{ $t->name }}</td>
                <td class="text-center">Rp. {{ number_format($t->price,0,',','.') }}</td>
                <td class="text-center">Rp. {{ number_format($t->affiliasi,0,',','.') }}</td>
                <td class="text-center">{{ $t->barcode }}</td>
                <td class="text-center">{{ $t->stok }}</td>
                <td class="text-center">Rp. {{ number_format($t->stok * $t->harga_jual,0,',','.') }}</td>
                <td class="text-center">
                  @if($t->stok <= 30 && $t->stok > 20)
                  <button class="btn" style="background: green; width: 100%;"></button>
                  @elseif($t->stok <= 20 && $t->stok > 10)
                  <button class="btn" style="background: yellow; width: 100%;"></button>
                  @elseif(($t->stok <= 10))
                  <button class="btn" style="background: red; width: 100%;"></button>
                  @else
                  <button class="btn" style="background: blue; width: 100%;"></button>
                  @endif
                </td>
              </tr>
            @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <!--/.col-->
  </div>
  <!--/.row-->
</div>

</div>
<!-- /.conainer-fluid -->
</main>
@endsection
