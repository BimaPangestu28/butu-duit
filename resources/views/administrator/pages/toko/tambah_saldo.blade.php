@extends('administrator/index')

@section('js')
  <script type="text/javascript" src="/admin/js/toko.js"></script>
@endsection

@section('content')
<main class="main">

<!-- Breadcrumb -->
<ol class="breadcrumb">
<li class="breadcrumb-item">Home</li>
<li class="breadcrumb-item">Admin</li>
<li class="breadcrumb-item active">Request Saldo Deposit Toko</li>

<!-- Breadcrumb Menu-->

</ol>

<div class="container-fluid">

  <div class="card">
    <div class="card-body">
      <div class="row">
        <div class="col-sm-5">
          <h4 class="card-title mb-0">Tambah Saldo Toko</h4>
          <div class="small text-muted">{{date('d M Y')}}</div>
        </div>
        <!--/.col-->
        <div class="col-sm-7 d-none d-md-block">
          <button type="button" class="btn btn-primary float-right"><i class="icon-cloud-download"></i></button>
          <div class="btn-toolbar float-right" role="toolbar" aria-label="Toolbar with button groups">
          </div>
        </div>
        <!--/.col-->
      </div>
      @if(session()->has('alert'))
      <div class="row">
        <div class="alert alert-danger col-sm-12" style="margin-bottom: 0; margin-top: 25px;">
            <p>{{ session()->get('alert') }}</p>
        </div>
      </div>
      @endif
    </div>
  </div>
  <!--/.card-->


  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          Usahaku Toserba - Toko Offline
        </div>
        <div class="card-body">
          <table class="table table-responsive-sm table-hover table-outline mb-0">
            <thead class="thead-light">
              <tr>
                <th>Nama Toko</th>
                <th>Jumlah Transfer</th>
                <th>Bukti Transfer</th>
                <th>Option</th>
              </tr>
            </thead>
            <tbody> 
              @foreach($deposit as $k)
              <tr>
                <td>{{ $k->name }}</td>
                <td>Rp. {{ number_format($k->jumlah_transfer,0,',','.') }}</td>
                <td>
                  <a href="/gambar/{{ $k->gambar }}" target="_blank">
                    <img src="/gambar/{{ $k->gambar }}" width="200px" height="200px" style="object-fit: cover;">
                  </a>
                </td>
                <td><a href="/usahakumart/deposit/konfirm/{{ $k->id_deposit }}" class="btn btn-warning" style="color: white;" onclick="return confirm('Apakah anda yakin?')">Konfirmasi Deposit</a></td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
      <div class="card">
        <div class="card-header">
          Usahaku Toserba - Toko Offline
        </div>
        <div class="card-body">
          <table class="table table-responsive-sm table-hover table-outline mb-0">
            <thead class="thead-light">
              <tr>
                <th>Nama Toko</th>
                <th>Saldo Deposit</th>
                <th>Saldo Withdraw</th>
              </tr>
            </thead>
            <tbody> 
              @foreach($toko as $k)
              <tr>
                <td>{{ $k->name }}</td>
                <td>Rp. {{ number_format($k->saldo_deposit,0,',','.') }}</td>
                <td>Rp. {{ number_format($k->saldo_withdraw,0,',','.') }}</td>
              </tr>
              @endforeach
            </tbody>
          </table>

        </div>
      </div>
    </div>
    <!--/.col-->
  </div>
  <!--/.row-->
</div>

</div>
<!-- /.conainer-fluid -->
</main>
@endsection