<!DOCTYPE html>
<html>
<head>
	<title></title>
	<style type="text/css">
		.card {
			width: 8.5cm;
			height: 5.4cm;
			display: inline-block;
		}

		.card .gambar {
			height: 100%;
			width: 100%;
		}

		.barcode {
			margin-top: -40px; 
			margin-left: 50px;
		}

		.barcode-2 {
			margin-top: -61.5px; 
			margin-left: 30px;
			letter-spacing: 2px;
		}

		#box {
			margin-bottom: 10px;
		}
	</style>
</head>
<body>
	@for($i = 0; $i < count($arr); $i++)
		<div id="box">
			<div class="card" style="position: relative;">
				<img src="{{ public_path() }}/kartu/kartu.jpg" class="gambar">
				<div class="barcode" >
					<div>{!! $arr[$i] !!}</div>
					<span style="font-size: 12px;">{{ $range[$i] }}</span><br><br>
				</div>
			</div>

			<div class="card" style="position: relative;">
				<img src="{{ public_path() }}/kartu/belakang.jpg" class="gambar">
				<div class="barcode-2" >
					<span style="font-size: 12px;">{{ $range[$i] }}</span><br><br>
				</div>
			</div>
		</div>
		<?php $no = 1 ?>
		<span style="display: none;">{{ $no++ }}</span>
		@if($no == 7)
		<h3>break</h3>
		<h3>break</h3>
		<h3>break</h3>
		<h3>break</h3>
		<h3>break</h3>
		@elseif($no == 13 || $no == 19 || $no == 25 || $no == 31 || $no == 37 || $no == 43 || $no == 49)
		<h3>break</h3>
		<h3>break</h3>
		<h3>break</h3>
		@endif
	@endfor
</body>
</html>