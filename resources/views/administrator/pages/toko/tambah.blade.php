@extends('administrator/index')

@section('js')
	<script type="text/javascript" src="/admin/js/toko.js"></script>
@endsection

@section('content')
<main class="main">

<!-- Breadcrumb -->
<ol class="breadcrumb">
<li class="breadcrumb-item">Home</li>
<li class="breadcrumb-item">Admin</li>
<li class="breadcrumb-item active">Dashboard</li>

<!-- Breadcrumb Menu-->

</ol>


<div class="loader">
  <img src="/loader/produk.gif">
</div>

<div class="container-fluid">

  <div class="card">
    <div class="card-body">
      <div class="row">
        <div class="col-sm-5">
          <h4 class="card-title mb-0">Tambah Toko</h4>
          <div class="small text-muted">{{date('d M Y')}}</div>
        </div>
        <!--/.col-->
        <div class="col-sm-7 d-none d-md-block">
          <button type="button" class="btn btn-primary float-right"><i class="icon-cloud-download"></i></button>
          <div class="btn-toolbar float-right" role="toolbar" aria-label="Toolbar with button groups">
          </div>
        </div>
        <!--/.col-->
      </div>
      @if($errors->count() > 0 || session()->has('alert'))
      <div class="row">
      	<div class="alert alert-danger col-sm-12" style="margin-bottom: 0; margin-top: 25px;">
      		@if($errors->count() > 0)
      			@foreach($errors->all() as $error)
      			<p>{{ $error }}</p>
      			@endforeach
      		@else
      			<p>{{ session()->get('alert') }}</p>
      		@endif
      	</div>
      </div>
      @endif
    </div>
  </div>
  <!--/.card-->


  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          Usahaku Toserba - Toko Offline
        </div>
        <div class="card-body">
          <form method="POST" action="/usahakumart/tambah-toko">
          	{{ csrf_field() }}

          	<div class="form-group">
          		<strong>Nama Toko</strong><br>
          		<input type="text" name="name" class="form-control" style="margin-top: 10px;" placeholder="Nama Toko">
          	</div>

            <div class="form-group">
              <strong>Username Toko</strong><br>
              <input type="text" name="username" class="form-control" style="margin-top: 10px;" placeholder="Username Toko">
            </div>

          	<div class="form-group">
          		<strong>Password Toko</strong><br>
          		<input type="password" name="password" class="form-control" style="margin-top: 10px;" placeholder="Password Toko">
          	</div>

          	<div class="form-group">
          		<strong>Provinsi</strong><br>
          		<select class="form-control" name="id_provinsi" style="margin-top: 10px;" id="provinsi">
          			@foreach($provinsi as $p)
          			<option value="{{ $p->id }}">{{ $p->provinsi }}</option>
          			@endforeach
          		</select>
          	</div>

          	<div class="form-group">
          		<strong>Kota</strong><br>
          		<select class="form-control" name="id_kota" style="margin-top: 10px;" id="kota">
          			@foreach($kota as $k)
          			<option value="{{ $k->id }}">{{ $k->kota }}</option>
          			@endforeach
          		</select>
          	</div>

          	<div class="form-group">
          		<strong>Kecamatan</strong><br>
          		<select class="form-control" name="id_kecamatan" style="margin-top: 10px;" id="kecamatan">
          			@foreach($kecamatan as $k)
          			<option value="{{ $k->id }}">{{ $k->kecamatan }}</option>
          			@endforeach
          		</select>
          	</div>

            <div class="form-group">
              <strong>Alamat Toko</strong><br>
              <input type="text" name="alamat" class="form-control" style="margin-top: 10px;" placeholder="Alamat Toko">
            </div>

            <div class="form-group">
              <strong>Kategori Toko</strong><br>
              <select class="form-control" name="id_kategori" style="margin-top: 10px;">
                @foreach($kategori as $k)
                <option value="{{ $k->id }}">{{ $k->kategori }}</option>
                @endforeach
              </select>
            </div>

            <div class="form-group">
              <strong>Tipe Toko</strong><br>
              <select class="form-control" name="id_tipe" style="margin-top: 10px;">
                @foreach($tipe as $k)
                <option value="{{ $k->id }}">{{ $k->tipe }}</option>
                @endforeach
              </select>
            </div>

            <div class="form-group">
              <strong>Pemilik Toko</strong><br>
              <select class="form-control" name="id_pemilik" style="margin-top: 10px;">
                @foreach($member as $k)
                <option value="{{ $k->id_member }}">{{ $k->id_member}} - {{ $k->nama }}</option>
                @endforeach
              </select>
            </div>

						<div class="form-group">
              <strong>Level Toko</strong><br>
              <select class="form-control" name="toko_level" style="margin-top: 10px;">
								<option value="">Toko Usahaku Toserba</option>
								<option value="1">1</option>
								<option value="2">2</option>
                <option value="3">3</option>
              </select>
            </div>

          	<br><br>

          	<input type="submit" class="btn btn-primary form-control" value="Tambah Toko">
          </form>
        </div>
      </div>
    </div>
    <!--/.col-->
  </div>
  <!--/.row-->
</div>

</div>
<!-- /.conainer-fluid -->
</main>
@endsection
