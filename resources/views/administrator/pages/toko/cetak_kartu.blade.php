@extends('administrator/index')

@section('js')
	<script type="text/javascript" src="/admin/js/toko.js"></script>
@endsection

@section('content')
<main class="main">

<!-- Breadcrumb -->
<ol class="breadcrumb">
<li class="breadcrumb-item">Home</li>
<li class="breadcrumb-item">Admin</li>
<li class="breadcrumb-item active">Dashboard</li>

<!-- Breadcrumb Menu-->

</ol>


<div class="loader">
  <img src="/loader/produk.gif">
</div>

<div class="container-fluid">

  <div class="card">
    <div class="card-body">
      <div class="row">
        <div class="col-sm-5">
          <h4 class="card-title mb-0">Tambah Toko</h4>
          <div class="small text-muted">{{date('d M Y')}}</div>
        </div>
        <!--/.col-->
        <div class="col-sm-7 d-none d-md-block">
          <button type="button" class="btn btn-primary float-right"><i class="icon-cloud-download"></i></button>
          <div class="btn-toolbar float-right" role="toolbar" aria-label="Toolbar with button groups">
          </div>
        </div>
        <!--/.col-->
      </div>
      @if($errors->count() > 0 || session()->has('alert'))
      <div class="row">
      	<div class="alert alert-danger col-sm-12" style="margin-bottom: 0; margin-top: 25px;">
      		@if($errors->count() > 0)
      			@foreach($errors->all() as $error)
      			<p>{{ $error }}</p>
      			@endforeach
      		@else
      			<p>{{ session()->get('alert') }}</p>
      		@endif
      	</div>
      </div>
      @endif
    </div>
  </div>
  <!--/.card-->


  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          Usahaku Toserba - Toko Offline
        </div>
        <div class="card-body">
          <form method="POST" action="/usahakumart/cetak-kartu">
          	{{ csrf_field() }}

          	<div class="form-group">
          		<strong>Nomer Mulai</strong><br>
          		<input type="number" name="mulai" min="0" class="form-control" style="margin-top: 10px;" placeholder="Nomer Mulai">
          	</div>

            <div class="form-group">
              <strong>Nomer Berakhir</strong><br>
              <input type="number" name="berakhir" class="form-control" style="margin-top: 10px;" placeholder="Nomer Berakhir">
            </div>

          	<br><br>

            <input type="submit" class="btn btn-primary form-control" name="submit" value="Cetak Kartu Depan"><br><br>
          	<input type="submit" class="btn btn-primary form-control" name="submit" value="Cetak Kartu Belakang">
          </form>
        </div>
      </div>
    </div>
    <!--/.col-->
  </div>
  <!--/.row-->
</div>

</div>
<!-- /.conainer-fluid -->
</main>
@endsection
