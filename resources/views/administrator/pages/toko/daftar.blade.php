@extends('administrator/index')

@section('js')
	<script type="text/javascript" src="/admin/js/toko.js"></script>
@endsection

@section('content')
<main class="main">

<!-- Breadcrumb -->
<ol class="breadcrumb">
<li class="breadcrumb-item">Home</li>
<li class="breadcrumb-item">Admin</li>
<li class="breadcrumb-item active">Daftar Toko</li>

<!-- Breadcrumb Menu-->

</ol>

<div class="container-fluid">

  <div class="card">
    <div class="card-body">
      <div class="row">
        <div class="col-sm-12">
          <h4 class="card-title mb-0">Daftar Toko</h4>
          <div class="small text-muted">{{date('d M Y')}}</div>
          <br>
            <a href="/usahakumart/daftar-toko" class="btn btn-primary">Semua Toko</a>
          @foreach($tipe as $t)
            <a href="/usahakumart/daftar-toko?tipe={{ $t->id }}" class="btn btn-primary">Toko {{ $t->tipe }}</a>
          @endforeach
        </div>
        <!--/.col-->
      </div>
      @if(session()->has('alert'))
      <div class="row">
      	<div class="alert alert-danger col-sm-12" style="margin-bottom: 0; margin-top: 25px;">
      			<p>{{ session()->get('alert') }}</p>
      	</div>
      </div>
      @endif
    </div>
  </div>
  <!--/.card-->


  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          Usahaku Toserba - Toko Offline
        </div>
        <div class="card-body">
          <form method="GET" action="/usahakumart/toko-search" >
              <input class="form-control" type="text" placeholder="Type to search" style="width: 93.5%; display: inline-block;" name="q"></input>
              <button type="submit" class="btn btn-primary" style="margin-left: -4px; margin-top: -5px;">Search</button>
          </form>
          <br>
          <table class="table table-responsive-sm table-hover table-outline mb-0">
            <thead class="thead-light">
              <tr>
                <th class="text-center"><i class="icon-people"></i></th>
                <th>Nama Toko</th>
                <th>Wilayah</th>
                <th class="text-center">Status Toko</th>
								<th class="text-center" width="10%">Durasi Toko</th>
                <th class="text-center">Option</th>
                <th class="text-center">Action</th>
              </tr>
            </thead>
            <tbody>
            @foreach($toko as $t)
              <tr>
                <td class="text-center">
                  <div class="avatar">
                    <img src="/admin/img/logo-symbol.png" class="img-avatar">
                  </div>
                </td>
                <td>
                  <div style="text-transform: uppercase;">{{$t->name}}</div>
                  <div class="small text-muted">
                    Terdaftar: {{ date('d F Y', strtotime($t->created_at)) }}
                  </div>
                </td>
                <td>
                  <div class="clearfix">
                    {{ $t->provinsi }}, {{ $t->kota }}, {{ $t->kecamatan }}
                  </div>
                </td>
                <td class="text-center">
                  @if($t->aktif == 1)
                  Aktif
                  @else
                  Tidak Aktif
                  @endif
                </td>
								<td class="text-center" id="durasi-{{ $t->id }}">{{ $t->duration_toko }}</td>
                <td class="text-center">
                  @if($t->aktif == 1)
                  <a class="btn-danger btn" onclick="return confirm('Anda yakin?')" href="/usahakumart/nonaktif-toko?id={{ $t->id }}" style="color: white;">Non Aktifkan</a>
                  @else
                  <a class="btn-primary btn" onclick="return confirm('Anda yakin?')" href="/usahakumart/aktif-toko?id={{ $t->id }}" style="color: white;">Aktifkan</a>
                  @endif
									<br>
									<br>
                  <a class="btn-success btn" style="color: white;" href="/usahakumart/detail-toko?id={{ $t->id }}">Detail Toko</a>
									<br>
									<br>
                  <button class="btn-primary btn akun-option" data-id="{{ $t->id }}" data-toggle="modal" data-target="#modalKeluar" style="color: white;">Durasi Toko</button>
                </td>
                <td class="text-center">
                  <a class="btn-danger btn" onclick="return confirm('Anda yakin?')" href="/usahakumart/hapus-toko/{{ $t->username }}" style="color: white;">Hapus Toko</a>
                  <br>
                  <br>
                  <a class="btn-success btn" style="color: white;" href="/usahakumart/edit-toko/{{ $t->username }}">Edit Toko</a>
                  <br>
                  <br>

                  @if($t->permission == 0)
                  <a class="btn-primary btn" onclick="return confirm('Anda yakin?')" style="color: white;" href="/usahakumart/allow-toko/{{ $t->id }}">Izinkan Toko</a>
                  @else
                  <a class="btn-primary btn" onclick="return confirm('Anda yakin?')" style="color: white;" href="/usahakumart/deny-toko/{{ $t->id }}">Batal Izinkan Toko</a>
                  @endif
                </td>
              </tr>
            @endforeach
            </tbody>
          </table>

          {{$toko->render()}}
        </div>
      </div>
    </div>
    <!--/.col-->
  </div>
  <!--/.row-->
</div>

<div class="modal" id="modalKeluar" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Akun Toko</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <p>
          <input type="hidden" id="id-toko" value="">
          <label for="to">Tambah Durasi Toko :</label>
					<select class="form-control" name="" id="durasi-toko">
						@for($i = 1; $i <= 12; $i++)
							<option value="{{ $i }}">{{ $i }} Bulan</option>
						@endfor
					</select>
          </p>
        </div>
        <div class="modal-footer">
          <button type="button" id="update-toko" class="btn btn-primary" data-dismiss="modal">Update Toko</button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>

</div>
<!-- /.conainer-fluid -->
</main>
@endsection
