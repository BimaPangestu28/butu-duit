@extends('administrator/index')

@section('js')
  <script type="text/javascript" src="/admin/js/toko.js"></script>
@endsection

@section('content')
<main class="main">

<!-- Breadcrumb -->
<ol class="breadcrumb">
<li class="breadcrumb-item">Home</li>
<li class="breadcrumb-item">Admin</li>
<li class="breadcrumb-item active">Request Stok Kartu Toko</li>

<!-- Breadcrumb Menu-->

</ol>

<div class="container-fluid">

  <div class="card">
    <div class="card-body">
      <div class="row">
        <div class="col-sm-5">
          <h4 class="card-title mb-0">Request Stok Kartu Toko</h4>
          <div class="small text-muted">{{date('d M Y')}}</div>
        </div>
        <!--/.col-->
        <div class="col-sm-7 d-none d-md-block">
          <button type="button" class="btn btn-primary float-right"><i class="icon-cloud-download"></i></button>
          <div class="btn-toolbar float-right" role="toolbar" aria-label="Toolbar with button groups">
          </div>
        </div>
        <!--/.col-->
      </div>
      @if(session()->has('alert'))
      <div class="row">
        <div class="alert alert-danger col-sm-12" style="margin-bottom: 0; margin-top: 25px;">
            <p>{{ session()->get('alert') }}</p>
        </div>
      </div>
      @endif
    </div>
  </div>
  <!--/.card-->


  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          Usahaku Toserba - Toko Offline
        </div>
        <div class="card-body">
          <table class="table table-responsive-sm table-hover table-outline mb-0">
            <thead class="thead-light">
              <tr>
                <th>Nama Toko</th>
                <th>Jumlah Kartu</th>
                <th>Bukti Transfer</th>
                <th>Option</th>
              </tr>
            </thead>
            <tbody> 
              @foreach($kartu as $k)
              <tr>
                <td>{{ $k->name }}</td>
                <td>{{ number_format($k->jumlah_kartu,0,',','.') }}</td>
                <td>
                  <a href="/gambar/{{ $k->gambar }}" target="_blank">
                    <img src="/gambar/{{ $k->gambar }}" width="200px" height="200px" style="object-fit: cover;">
                  </a>
                </td>
                <td><a href="/usahakumart/kartu/konfirm/{{ $k->id_request }}?jumlah={{ $k->jumlah_kartu }}" class="btn btn-warning" style="color: white;" onclick="return confirm('Apakah anda yakin?')">Konfirmasi Request</a></td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
      <div class="card">
        <div class="card-header">
          Usahaku Toserba - Toko Offline
        </div>
        <div class="card-body">
          <table class="table table-responsive-sm table-hover table-outline mb-0">
            <thead class="thead-light">
              <tr>
                <th>Nama Toko</th>
                <th class="text-center">Jumlah Kartu</th>
                <th class="text-center">Jumlah Kartu Yang Telah Digunakan</th>
                <th class="text-center">Jumlah Kartu Yang Belum Digunakan</th>
              </tr>
            </thead>
            <tbody> 
              @foreach($toko as $k)
              <tr>
                <td>{{ $k->name }}</td>
                <td class="text-center">{{ \App\Http\Controllers\AdminController::jumlahKartu($k->id) }}</td>
                <td class="text-center">{{ \App\Http\Controllers\AdminController::jumlahKartuDigunakan($k->id) }}</td>
                <td class="text-center">{{ \App\Http\Controllers\AdminController::jumlahKartuBelum($k->id) }}</td>
              </tr>
              @endforeach
            </tbody>
          </table>

        </div>
      </div>
    </div>
    <!--/.col-->
  </div>
  <!--/.row-->
</div>

</div>
<!-- /.conainer-fluid -->
</main>
@endsection