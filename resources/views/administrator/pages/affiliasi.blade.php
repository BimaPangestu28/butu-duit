@extends('administrator/index')

@section('js')
  <script type="text/javascript" src="/admin/js/toko.js"></script>
@endsection

@section('content')
<main class="main">

<!-- Breadcrumb -->
<ol class="breadcrumb">
<li class="breadcrumb-item">Home</li>
<li class="breadcrumb-item">Admin</li>
<li class="breadcrumb-item active">Daftar Request Withdraw</li>

<!-- Breadcrumb Menu-->

</ol>

<div class="container-fluid">

  <div class="card">
    <div class="card-body">
      <div class="row">
        <div class="col-sm-5">
          <h4 class="card-title mb-0">Daftar Request Withdraw</h4>
          <div class="small text-muted">{{date('d M Y')}}</div>
        </div>
        <!--/.col-->
        <div class="col-sm-7 d-none d-md-block">
          <button type="button" class="btn btn-primary float-right"><i class="icon-cloud-download"></i></button>
          <div class="btn-toolbar float-right" role="toolbar" aria-label="Toolbar with button groups">
          </div>
        </div>
        <!--/.col-->
      </div>
      @if(session()->has('alert'))
      <div class="row">
        <div class="alert alert-danger col-sm-12" style="margin-bottom: 0; margin-top: 25px;">
            <p>{{ session()->get('alert') }}</p>
        </div>
      </div>
      @endif
    </div>
  </div>
  <!--/.card-->


  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          Usahaku Toserba - Stok Barang Di Toko
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>Tanggal</th>
                  <th>Nama Toko</th>
                  <th>Total Withdraw</th>
                  <th>Option</th>
                </tr>
              </thead>
              <tbody>
                @foreach($payout as $t)
                <tr>
                  <td>{{ $t->created_at }}</td>
                  <td>{{ $t->name }}</td>
                  <td>Rp. {{ number_format($t->jumlah_withdraw,0,',','.') }}</td>
                  <td><a href="/usahakumart/payout/konfirm?id={{ $t->idw }}" onclick="return confirm('Apakah anda yakin?')" class="btn btn-primary">Konfirmasi Request</a></td>
                </tr>
                @endforeach

                {{ $payout->render() }}
              </tbody>
            </table>
        </div>
        </div>
      </div>
    </div>
    <!--/.col-->
  </div>
  <!--/.row-->
</div>

</div>
<!-- /.conainer-fluid -->
</main>
@endsection
