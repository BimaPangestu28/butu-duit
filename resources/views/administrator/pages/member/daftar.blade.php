@extends('administrator/index')

@section('js')
	<script type="text/javascript" src="/admin/js/toko.js"></script>
@endsection

@section('content')
<main class="main">

<!-- Breadcrumb -->
<ol class="breadcrumb">
<li class="breadcrumb-item">Home</li>
<li class="breadcrumb-item">Admin</li>
<li class="breadcrumb-item active">Daftar Member</li>

<!-- Breadcrumb Menu-->

</ol>

<div class="container-fluid">

  <div class="card">
    <div class="card-body">
      <div class="row">
        <div class="col-sm-5">
          <h4 class="card-title mb-0">Daftar Member</h4>
          <div class="small text-muted">{{date('d M Y')}}</div>
        </div>
        <!--/.col-->
        <div class="col-sm-7 d-none d-md-block">
          <button type="button" class="btn btn-primary float-right"><i class="icon-cloud-download"></i></button>
          <div class="btn-toolbar float-right" role="toolbar" aria-label="Toolbar with button groups">
          </div>
        </div>
        <!--/.col-->
      </div>
      @if(session()->has('alert'))
      <div class="row">
      	<div class="alert alert-danger col-sm-12" style="margin-bottom: 0; margin-top: 25px;">
      			<p>{{ session()->get('alert') }}</p>
      	</div>
      </div>
      @endif
    </div>
  </div>
  <!--/.card-->


  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          Usahaku Toserba - Toko Offline
        </div>
        <div class="card-body">
					<form action="/usahakumart/daftar-member" method="get">
            <input type="text" name="q" placeholder="Ketik nama" class="form-control"><br>
            <button type="submit" class="btn btn-primary form-control">Search</button>
          </form>
          <br><br>

          <table class="table table-responsive-sm table-hover table-outline mb-0">
            <thead class="thead-light">
              <tr>
                <th>Nama</th>
                <th>Nomer Kartu</th>
                <th>Kode Affiliasi</th>
                <th class="text-center">Status</th>
                <th class="text-center">Option</th>
              </tr>
            </thead>
            <tbody>
            @foreach($member as $m)
              <tr>
                <td>
                  <div>{{ $m->nama }}</div>
                  <div class="small text-muted">
                    Terdaftar: {{ date('d F Y', strtotime($m->created_at)) }}
                  </div>
                </td>
                <td>
                  <div class="clearfix">
                    {{ $m->number_card }}
                  </div>
                </td>
                <td>
                  <div class="clearfix">
                    {{ $m->code_affiliasi }}
                  </div>
                </td>
                <td class="text-center">
                  @if($m->status == 1)
                  Aktif
                  @else
                  Tidak Aktif
                  @endif
                </td>
                <td class="text-center">
                  @if($m->status == 1)
                  <a class="btn-danger btn" onclick="return confirm('Anda yakin?')" href="/usahakumart/nonaktif-member?id={{ $m->id }}" style="color: white;">Non Aktifkan</a>
                  @else
                  <a class="btn-primary btn" onclick="return confirm('Anda yakin?')" href="/usahakumart/aktif-member?id={{ $m->id }}" style="color: white;">Aktifkan</a>
                  @endif

									<a class="btn-warning btn" onclick="return confirm('Anda yakin?')" href="/usahakumart/hapus-member/{{ $m->id }}" style="color: white;">Hapus</a>
                </td>
              </tr>
            @endforeach
            </tbody>
          </table>

          {{ $member->render() }}
        </div>
      </div>
    </div>
    <!--/.col-->
  </div>
  <!--/.row-->
</div>

</div>
<!-- /.conainer-fluid -->
</main>
@endsection
