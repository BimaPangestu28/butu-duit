<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="shortcut icon" href="/admin/img/favicon.png">
  <title>UsahaMart Administrator</title>

  <!-- Icons -->
  <link href="/admin/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="/admin/simple-line-icons/css/simple-line-icons.css" rel="stylesheet">

  <!-- Main styles for this application -->
  <link href="/admin/css/style.css" rel="stylesheet">
  <!-- Styles required by this views -->

</head>

<body class="app header-fixed sidebar-fixed aside-menu-fixed aside-menu-hidden">
  <header class="app-header navbar">
    <button class="navbar-toggler mobile-sidebar-toggler d-lg-none mr-auto" type="button">
      <span class="navbar-toggler-icon"></span>
    </button>
    <a class="navbar-brand" href="#"></a>
    <button class="navbar-toggler sidebar-toggler d-md-down-none" type="button">
      <span class="navbar-toggler-icon"></span>
    </button>

    <ul class="nav navbar-nav d-md-down-none">
      <li class="nav-item px-3">
        <a class="nav-link" href="#">Dashboard</a>
      </li>
      <li class="nav-item px-3">
        <a class="nav-link" href="#">Users</a>
      </li>
      <li class="nav-item px-3">
        <a class="nav-link" href="#">Settings</a>
      </li>
    </ul>
    <ul class="nav navbar-nav ml-auto">
      <li class="nav-item px-3">Hallo, {{Auth::user()->username}}</li>
    </ul>

  </header>

  <div class="app-body">
    <div class="sidebar">
      <nav class="sidebar-nav">
        <ul class="nav">
          <li class="nav-item">
            <a class="nav-link" href="/usahakumart/dashboard"><i class="icon-speedometer"></i> Dashboard</a>
          </li>

          <li class="nav-title">
            Pengaturan Utama
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/usahakumart/cetak-kartu"><i class="icon-speedometer"></i> Cetak Kartu</a>
          </li>
          <li class="nav-item nav-dropdown">
            <a class="nav-link nav-dropdown-toggle" href="#"><i class="icon-puzzle"></i> Usahaku Toserba</a>
            <ul class="nav-dropdown-items">
              <li class="nav-item">
                <a class="nav-link" href="/usahakumart/tambah-toko"><i class="icon-puzzle"></i> Tambah Toko</a>
              </li>
              <!-- <li class="nav-item">
                <a class="nav-link" href="/usahakumart/tambah-saldo-toko"><i class="icon-puzzle"></i> Tambah Saldo Toko</a>
              </li> -->
              <li class="nav-item">
                <a class="nav-link" href="/usahakumart/kategori-toko"><i class="icon-puzzle"></i> Kategori Toko</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="/usahakumart/tipe-toko"><i class="icon-puzzle"></i> Tipe Toko</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="/usahakumart/daftar-toko"><i class="icon-puzzle"></i> Daftar Toko</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="/usahakumart/alamat-toko"><i class="icon-puzzle"></i> Tambah Alamat Toko</a>
              </li>
            </ul>
          </li>
          <li class="nav-item nav-dropdown">
            <a class="nav-link nav-dropdown-toggle" href="#"><i class="icon-star"></i> Member</a>
            <ul class="nav-dropdown-items">
              <li class="nav-item">
                <a class="nav-link" href="/usahakumart/daftar-member"><i class="icon-star"></i> Daftar Member</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="/usahakumart/upgrade-member"><i class="icon-star"></i> Upgrade Member</a>
              </li>
              <!-- <li class="nav-item">
                <a class="nav-link" href="icons-simple-line-icons.html"><i class="icon-star"></i> Statistik Member</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="icons-simple-line-icons.html"><i class="icon-star"></i> Aktifitas Member</a>
              </li> -->
            </ul>
          </li><!--
          <li class="nav-item">
            <a class="nav-link" href="widgets.html"><i class="icon-calculator"></i> Affiliasi</a>
          </li> -->
          <!-- <li class="nav-item">
            <a class="nav-link" href="charts.html"><i class="icon-pie-chart"></i> Artikel UMart</a>
          </li> -->
          <li class="nav-item">
            <a class="nav-link" href="/usahakumart/payout"><i class="icon-star"></i> Request Payout <span class="badge badge-pill badge-danger" id="notifyBadge">0</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/usahakumart/stok-kartu"><i class="icon-star"></i> Stok Kartu</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/usahakumart/deposit"><i class="icon-star"></i> Request Deposit <span class="badge badge-pill badge-danger" id="notifyBadgeDeposit">0</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/usahakumart/kartu"><i class="icon-star"></i> Request Kartu <span class="badge badge-pill badge-danger" id="notifyBadgeKartu">0</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/usahakumart/request-deposit-user"><i class="icon-star"></i> Request User <span class="badge badge-pill badge-danger" id="notifyBadgeDepo">0</span></a>
          </li>
          <li class="divider"></li>
          <li class="nav-title">
            Pengaturan Lainnya
          </li>
          <li class="nav-item nav-dropdown">
            <li class="nav-item">
              <a class="nav-link" href="/usahakumart/pengaturan"><i class="icon-logout"></i> Pengaturan</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="/usahakumart-administrator/logout"><i class="icon-logout"></i> Logout</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#"><i class="icon-star"></i> Lihat Website</a>
            </li>
          </li>

        </ul>
      </nav>
      <button class="sidebar-minimizer brand-minimizer" type="button"></button>
    </div>

    <!-- Main content -->
    @yield('content')

  </div>

  <footer class="app-footer">
    <span>Usahaku © 2017 usahaku.co.id.</span>
  </footer>

  <!-- CoreUI main scripts -->
  <script type="text/javascript" src="/admin/js/jquery.min.js"></script>

  <script src="/admin/js/app.js"></script>
  <script src="/admin/js/notif.js"></script>

  <!-- Plugins and scripts required by this views -->

  <!-- Custom scripts required by this view -->
  <script src="/admin/js/views/main.js"></script>
  <script type="text/javascript" src="/admin/js/jquery.min.js"></script>
	<script type="text/javascript" src="/assets/bootstrap/js/bootstrap.bundle.min.js"></script>
  @yield('js')

</body>
</html>
