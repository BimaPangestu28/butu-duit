<!DOCTYPE html>
<html>
<head>
  <title>Affiliasi Usahaku</title>
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" type="text/css" href="/assets/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="/admin/font-awesome/css/font-awesome.min.css">
  <script type="text/javascript" src="/admin/js/jquery.min.js"></script>
  <link rel="stylesheet" type="text/css" href="/assets/css/toko.css">
</head>
<body>


    <main role="main">

      <div id="banner">
        <section class="jumbotron text-center">
          <div class="container">
            <img src="/assets/img/logo.png" width="150">
            <br>
            <br>
            <br>
            <h1 class="jumbotron-heading">Hallo, {{ session()->get('member')->nama }}</h1>
            <p class="text-dark col-sm-9" style="display:inline-block;">Lihat tabel affiliasi dari hasil belanja kamu dan jaringan kamu di Usahaku Toserba maupun di usahaku marketplace serta perolehan affiliasi kamu disini.</p>
            <div>
              @if($id != null)
              <a href="/history/{{ $id }}" class="btn btn-primary" style="margin:5px 0;">Saldo Affiliasi : Rp. {{ number_format($total->total_affiliasi,0,',','.') }}</a> &nbsp;
              @else
              <a href="/history" class="btn btn-primary" style="margin:5px 0;">Saldo Affiliasi : Rp. {{ number_format($total->total_affiliasi,0,',','.') }}</a> &nbsp;
              @endif
              <a href="#" class="btn btn-primary" style="margin:5px 0;">Saldo Deposit : Rp. {{ number_format($deposit->saldo,0,',','.') }}</a> &nbsp;
              @if($id != null)
              <a href="/toko-usahakumart/{{ $id }}" class="btn btn-primary" style="margin:5px 0;">Kembali Ke Toko</a>
              @else
              <a href="/logout" class="btn btn-primary" style="margin:5px 0;">Logout</a>
              @endif <br> <br>
              <a href="/profile/pengaturan" class="btn btn-warning" style="margin:5px 0;">Pengaturan Akun</a>&nbsp;
              <a class="btn btn-warning" style="margin:5px 0;" id="tambah" style="cursor: pointer; color: white;">Tambah Saldo Deposit</a>&nbsp;
              <a href="/" class="btn btn-danger" style="margin:5px 0;">Cari Toko</a>
            </div><br>
          </div>
        </section>
      </div>

      <div class="modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Modal title</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <form method="POST" action="/saldo-deposit/tambah" enctype="multipart/form-data">
              {{ csrf_field() }}
              <div class="modal-body">
                <input type="number" name="jumlah" placeholder="Jumlah Transfer" class="form-control"><br>
                <span>Bukti Transfer</span><br>
                <input type="file" name="bukti">
              </div>
              <div class="modal-footer">
                <button type="submit" class="btn-nav btn-primary">Request Tambah Saldo Deposit</button>
                <button type="button" class="btn-nav btn-secondary" class="close">Close</button>
              </div>
            </form>
          </div>
        </div>
      </div>

      <div class="album text-muted">
        <div class="container">
        @if($i == 1)
        <h5>TABEL AFFILIASI | JARINGAN LINE</h5> <a class="btn-nav-primary btn" href="/affiliasi/{{ $id }}">LIHAT JARINGANKU</a>
        @else
        <h5>TABEL AFFILIASI | JARINGANKU</h5>
        @endif
        <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>No.</th>
                  <th>Nama</th>
                  <th>Total Affiliasi</th>
                  <th>Status Member</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody>
                @foreach($tree as $t)
                <tr>
                  <td>{{ $no++ }}</td>
                  <td>{{ $t->nama }}</td>
                  <td>
                    {{ $t->child + $t->child_2 + $t->child_3 + $t->child_4 + $t->child_5 + $t->child_6 +
                       $t->child_7 + $t->child_8 + $t->child_9 + $t->child_10  }}
                    Orang
                  </td>
                  <td>{{ \App\Http\Controllers\AffiliasiController::cek($t->id_mem) }}</td>
                  <td><a href="?id_line={{ $t->id }}">lihat affiliasi</a></td>
                </tr>
                @endforeach
              </tbody>
            </table>
        </div>

        </div>
      </div>

      @if($i != 1)
      <div class="album text-muted">
        <div class="container">
        <h5>Riwayat Request Deposit</h5>
        <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>No.</th>
                  <th>Jumlah</th>
                  <th>Status</th>
                  <th>Tanggal</th>
                </tr>
              </thead>
              <tbody>
                @foreach($depositA as $t)
                <tr>
                  <td>{{ $no++ }}</td>
                  <td>Rp. {{ number_format($t->jumlah,0,',','.') }}</td>
                  <td>
                    @if($t->status == 1)
                      Request Ditolak
                    @elseif($t->status == 2)
                      Request Telah Dikonfirmasi
                    @else
                      Menunggu Konfirmasi
                    @endif
                  </td>
                  <td>{{ $t->created_at }}</td>
                </tr>
                @endforeach
              </tbody>
            </table>
        </div>
        </div>
      </div>
      @endif

    </main>

    <footer id="footer" class="text-muted">
      <div class="container text-center">
        <a href="#" class="btn-nav btn-primary">Anggaran Toko Premium : Rp. {{ number_format($member->premium_cash,0,',','.') }}</a> &nbsp;
        <br><br>
        <p>
					<a href="#" class="btn-nav btn-warning uniquePadding">Tentang</a>
					<a href="#" class="btn-nav btn-warning uniquePadding">Kontak</a>
					<a href="#" class="btn-nav btn-warning uniquePadding">Frequent Question</a>
				</p>
				<p class="copyright">Usahaku Toserba &copy; Usahaku.co.id, 2018</p>
      </div>
    </footer>

<script>
  
  var msg = '{{Session::get('notif')}}';
  var exist = '{{Session::has('notif')}}';
  if(exist){
    alert(msg);
  }

  $("#tambah").on('click', function(){
    $(".modal").css('display', 'inline-block');
  });

  $(".close").on('click', function(){
    $(".modal").css('display', 'none');
  });

</script>
</body>

</html>
