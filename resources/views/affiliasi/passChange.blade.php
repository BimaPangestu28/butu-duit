<!DOCTYPE html>
<html>
<head>
  <title>Pengaturan Member Usahaku</title>
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" type="text/css" href="/assets/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="/admin/font-awesome/css/font-awesome.min.css">
  <script type="text/javascript" src="/admin/js/jquery.min.js"></script>
  <link rel="stylesheet" type="text/css" href="/assets/css/toko.css">
</head>
<body>


    <main role="main">

      <div id="banner">
        <section class="jumbotron text-center">
          <div class="container">
            <img src="/assets/img/logo.png" width="150">
            <br>
            <br>
            <br>
            <h1 class="jumbotron-heading">Hallo, {{ session()->get('member')->nama }}</h1>
            <p class="lead text-dark col-sm-9" style="display:inline-block;">Disini kamu bisa mengatur pengaturan akun milik mu.</p>
            <p>
              <a href="#" class="btn btn-primary" style="margin:5px 0;">Saldo Affiliasi : Rp. {{ number_format($total->total_affiliasi,0,',','.') }}</a> &nbsp;
              <a href="#" class="btn btn-primary" style="margin:5px 0;">Saldo Deposit : Rp. {{ number_format($deposit->saldo,0,',','.') }}</a> &nbsp;
              @if(session()->has('member'))
              <a href="/logout" class="btn btn-primary" style="margin:5px 0;">Logout</a>
              @endif <br> <br>
              <a href="/profile/pengaturan" class="btn btn-warning" style="margin:5px 0;">Pengaturan Akun</a>&nbsp;
              <a class="btn btn-warning" style="margin:5px 0;" id="tambah" style="cursor: pointer; color: white;">Tambah Saldo Deposit</a>&nbsp;
              <a href="/" class="btn btn-danger" style="margin:5px 0;">Cari Toko</a>
            </p><br>
          </div>
        </section>
      </div>

      @if($errors->count() > 0 || session()->has('alert'))
      <div class="row">
      	<div class="alert alert-danger col-sm-12" style="margin-bottom: 0; margin-top: 25px;">
      		@if($errors->count() > 0)
      			@foreach($errors->all() as $error)
      			<p>{{ $error }}</p>
      			@endforeach
      		@else
      			<p>{{ session()->get('alert') }}</p>
      		@endif
      	</div>
      </div>
      @endif

      <div class="modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Modal title</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <form method="POST" action="/saldo-deposit/tambah" enctype="multipart/form-data">
              {{ csrf_field() }}
              <div class="modal-body">
                <input type="number" name="jumlah" placeholder="Jumlah Transfer" class="form-control"><br>
                <span>Bukti Transfer</span><br>
                <input type="file" name="bukti">
              </div>
              <div class="modal-footer">
                <button type="submit" class="btn-nav btn-primary">Request Tambah Saldo Deposit</button>
                <button type="button" class="btn-nav btn-secondary" class="close">Close</button>
              </div>
            </form>
          </div>
        </div>
      </div>

      <div class="album text-muted">
        <div class="container">
          <h5>Ganti Password</h5>
          <br>
          <br>
          <form method="POST" action="/profile/password/update">
            {{ csrf_field() }}
            <div class="form-group">
              <div>
                <span>Password Lama</span>
                <input type="password" name="old_pass" id="oldPass" class="form-control">
                <small>Masukkan password yang saat ini digunakan</small>
              </div><br>
              <div>
                <span>Password Baru</span>
                <input type="password" name="password" id="newPass" class="form-control">
                <small>Masukkan password baru, lebih dari 8 karakter</small>
              </div><br>
              <div>
                <span>Ketik Ulang Password Baru</span>
                <input type="password" name="confirm_pass" id="conPass" class="form-control">
                <small id="error_pass">Masukkan password baru sekali lagi</small>
              </div><br>
              <div>
                <button type="submit" id="btn_submit" class="btn btn-primary form-control">Update Data</button>
              </div>
            </div>
          </form>
        </div>
      </div>

    </main>

    <footer id="footer" class="text-muted">
      <div class="container text-center">
        <p>
					<a href="#" class="btn-nav btn-warning uniquePadding">Tentang</a>
					<a href="#" class="btn-nav btn-warning uniquePadding">Kontak</a>
					<a href="#" class="btn-nav btn-warning uniquePadding">Frequent Question</a>
				</p>
				<p class="copyright">Usahaku Toserba &copy; Usahaku.co.id, 2018</p>
      </div>
    </footer>

<script>

  var msg = '{{Session::get('notif')}}';
  var exist = '{{Session::has('notif')}}';
  if(exist){
    alert(msg);
  }

  $("#tambah").on('click', function(){
    $(".modal").css('display', 'inline-block');
  }); 

  $(".close").on('click', function(){
    $(".modal").css('display', 'none');
  }); 

  $(document).ready(function(){
      $('#conPass, #newPass').keyup(function () {
         var newPass = $('#newPass').val(),
            oldPass = $('#conPass').val()
            
            if (newPass != oldPass) {
                $('#btn_submit').attr('disabled', 'disabled');
                $('#error_pass').html("<b class='text-danger'>Password tidak sama</b>");
            } else {
                $('#btn_submit').removeAttr('disabled');
                $('#error_pass').html("<b class='text-success'>Password benar</b>");
            }
      });
  });
  
</script>
</body>

</html>
