<!DOCTYPE html>
<html>
<head>
  <title>Affiliasi Usahaku</title>
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" type="text/css" href="/assets/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="/admin/font-awesome/css/font-awesome.min.css">
  <script type="text/javascript" src="/admin/js/jquery.min.js"></script>
  <link rel="stylesheet" type="text/css" href="/assets/css/toko.css">
</head>
<body>


    <main role="main">

      <div id="banner">
        <section class="jumbotron text-center">
          <div class="container">
            <img src="/assets/img/logo.png" width="150">
            <br>
            <br>
            <br>
            <h1 class="jumbotron-heading">Hallo, {{ session()->get('member')->nama }}</h1>
            <p class="lead text-dark col-sm-9" style="display:inline-block;">Lihat tabel affiliasi dari hasil belanja kamu dan jaringan kamu di Usahaku Toserba maupun di usahaku marketplace serta perolehan affiliasi kamu disini.</p>
            <p>
              @if($id != null)
              <a href="/history/{{ $id }}" class="btn btn-primary" style="margin:5px 0;">Saldo Affiliasi : Rp. {{ number_format($total->total_affiliasi,0,',','.') }}</a> &nbsp;
              @else
              <a href="/history" class="btn btn-primary" style="margin:5px 0;">Saldo Affiliasi : Rp. {{ number_format($total->total_affiliasi,0,',','.') }}</a> &nbsp;
              @endif
              <a href="#" class="btn btn-primary" style="margin:5px 0;">Saldo Deposit : Rp. {{ number_format($deposit->saldo,0,',','.') }}</a> &nbsp;
              @if($id != null)
              <a href="/toko-usahakumart/{{ $id }}" class="btn btn-primary" style="margin:5px 0;">Kembali Ke Toko</a>
              @else
              <a href="/logout" class="btn btn-primary" style="margin:5px 0;">Logout</a>
              @endif <br> <br>
              <a href="/pengaturan/1" class="btn btn-warning" style="margin:5px 0;">Pengaturan Akun</a>&nbsp;
              <a class="btn btn-warning" style="margin:5px 0;" id="tambah" style="cursor: pointer; color: white;">Tambah Saldo Deposit</a>&nbsp;
              <a href="/" class="btn btn-danger" style="margin:5px 0;">Cari Toko</a>
            </p><br>
          </div>
        </section>
      </div>

      <div class="modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Modal title</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <form method="POST" action="/saldo-deposit/tambah" enctype="multipart/form-data">
              {{ csrf_field() }}
              <div class="modal-body">
                <input type="number" name="jumlah" placeholder="Jumlah Transfer" class="form-control"><br>
                <span>Bukti Transfer</span><br>
                <input type="file" name="bukti">
              </div>
              <div class="modal-footer">
                <button type="submit" class="btn-nav btn-primary">Request Tambah Saldo Deposit</button>
                <button type="button" class="btn-nav btn-secondary" class="close">Close</button>
              </div>
            </form>
          </div>
        </div>
      </div>

      <div class="album text-muted">
        <div class="container">
        <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>No.</th>
                  <th>Tanggal</th>
                  <th>Pendapatan Affiliasi</th>
                </tr>
              </thead>
              <tbody>
                @foreach($history as $no => $t)
                <tr>
                  <td>{{ $no += 1 }}</td>
                  <td>{{ $t->created_at }}</td>
                  <td>Rp. {{ number_format($t->total_affiliasi,0,',','.') }}</td>
                </tr>
                @endforeach
              </tbody>
            </table>

            {{ $history->render() }}
        </div>

        </div>
      </div>

    </main>

    <footer id="footer" class="text-muted">
      <div class="container text-center">
        <a href="#" class="btn-nav btn-primary">Anggaran Toko Premium : Rp. {{ number_format($member->premium_cash,0,',','.') }}</a> &nbsp;
        <br><br>
        <p>
					<a href="#" class="btn-nav btn-warning uniquePadding">Tentang</a>
					<a href="#" class="btn-nav btn-warning uniquePadding">Kontak</a>
					<a href="#" class="btn-nav btn-warning uniquePadding">Frequent Question</a>
				</p>
				<p class="copyright">Usahaku Toserba &copy; Usahaku.co.id, 2018</p>
      </div>
    </footer>

<script>

  var msg = '{{Session::get('notif')}}';
  var exist = '{{Session::has('notif')}}';
  if(exist){
    alert(msg);
  }

  $("#tambah").on('click', function(){
    $(".modal").css('display', 'inline-block');
  });

  $(".close").on('click', function(){
    $(".modal").css('display', 'none');
  });
  
</script>
</body>

</html>
