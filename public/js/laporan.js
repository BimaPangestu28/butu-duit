$(document).ready(function() {
	var produk 				=	$("#produk").val();
		

	$("#total_harga").on('keyup', function() {
		let tipe_jumlah			=	$("#tipe_jumlah").val(),
			total_harga			=	$("#total_harga").val(),
			jumlah_barang		=	$("#jumlah_barang").val();
		if (tipe_jumlah != '' && total_harga != '' && jumlah_barang != '') {
			$("#modal").val(total_harga / jumlah_barang);
		}
	});

	$("#barcode_barang").on('keypress', function(e) {
		if (event.keyCode === 10 || event.keyCode === 13) 
        event.preventDefault();
	});

	$("#submit_button").on('click', function() {
		$("#form").submit();
	});

	$("#tipe").on('change', function() {
		let value 		=		$(this).val();

		if (value == 'Kredit') {
			$("#tanggal_pembayaran").show();
		} else {
			$("#tanggal_pembayaran").hide();
		}
	});

	$("#presentase").on('change keyup', function() {
		let persen 		=	parseInt($(this).val()),
			modal 		=	parseInt($("#modal").val());

		$("#jual").val(modal + (modal * (persen/100)));
	});

	$("#jual").on('change keyup', function() {
		let jual 		=	parseInt($(this).val()),
			modal 		=	parseInt($("#modal").val());

		$("#presentase").val(Math.floor((jual - modal) / modal * 100));
	});

	$("#barcode_barang").on('keyup', function(e) {
		let barcode_barang	=	$("#barcode_barang").val();

		if(e.keyCode == 13) {
			$.ajax({
				url: '/cashier/laporan/pembelian-produk/api/search',
				method: 'GET',
				data: {
					barcode: barcode_barang
				},
				success: function(result) {
					if(result != "null") {
						$("#keAda").show();
						$("body").css('background', 'green');
						$("#produk").val(result.produk);
						$("#supplier").prepend(`<option value="${ result.supplier }" selected="">${ result.supplier }</option>`);
						$("#category").prepend(`<option value="${ result.id_category }" selected="">${ result.category }</option>`);
						$("#jual").val(result.harga_jual);
						$("#affiliasi").val(result.affiliasi);
						$("#stok").hide();
						$("#gambar").append(`
							<br><img src="/gambar/${ result.gambar }" width="400" height="300" style="object-fit: cover;">
							<input type="hidden" value="1" name="restok">
						`);
						$("#rak").prepend(`<option value="${ result.rak }">${ result.rak }</option>`);
					} else {
						$("body").css('background', 'red');						
						$("#keAda").hide();
					}
				}
			});
		}
	})
});