$(document).ready(function() {
	$("#product_text").on('keyup', function(e) {
		e.preventDefault();

		let filter = $(this).val().toUpperCase();
		
		$("#search").find("tr").hide();
		$("tr[id*='" + filter + "']").show();

		if (filter == '') {
			$("#search").find("tr").show();
		}
	});

	$("#search tr").on('click', function() {
		if ($(this).find('input').attr('checked') != 'checked') {
			$(this).find('input').attr('checked', 'checked');
		} else {
			$(this).find('input').removeAttr('checked', 'checked');
		}
	});
});