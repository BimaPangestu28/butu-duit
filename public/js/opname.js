$(document).ready(function () {

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

   $(document).on('change', '.stok_opname', function () {
      var id = $(this).attr('data-opname'),
          msg = $(this).val();

      $.ajax({
          method: "post",
          url: "/cashier/stok-opname/update",
          data: {
              id: id,
              stok_opname: msg
          }, success:function (data) {
              $("#status-opname-"+id).show();
          }
      })
   });
});