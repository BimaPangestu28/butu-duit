$(document).ready(function() {
	/*$("#name").on('change', function(){
		let name 	=	$("#name").val();

		$.ajax({
			url: '/cashier/api/tambah-barang',
			method: 'GET',
			data: {
				name: name
			},
			success: function(result) {
				$("#modal").val(result.total_harga / result.jumlah_satuan);
				$("#barcode_barang").val(result.barcode);
			}
		});
	});*/

	let produk 	=	$("#produk").val();

	$("#produk").on('keyup', function() {

		$("#barangList").empty();

		$.ajax({
			url: '/cashier/laporan/pembelian-produk/api/search',
			method: 'GET',
			data: {
				produk: produk
			},
			success: function(result) {
				$(result).each(function(key,value){
		            $("#barangList").append(`
		            	<option value="${value.produk}">${value.produk}</option>
		            `);
		        });
			}
		});
	});

	$("#produk").on('input', function() {
		let option 	=	this.value;

		$("#barangList").find('option').filter(function() {
			$.ajax({
				url: '/cashier/laporan/pembelian-produk/api/search',
				method: 'GET',
				data: {
					option: option					
				},
				success: function(result) {
					$("#modal").val(result.total_harga / result.jumlah_satuan);
					$("#barcode_barang").val(result.barcode);
					$("#stok").val(result.jumlah_satuan);
				}
			});	
		});

		/*$.ajax({
			url: '/cashier/laporan/pembelian-produk/api/searchSingle',
			method: 'GET',
			data: {
				barang: option					
			},
			success: function(result) {
				console.log(result);
			}
		});*/
	});
});