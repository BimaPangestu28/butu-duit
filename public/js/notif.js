$(document).ready(function() {
	function rupiah(bilangan) {
		let 	reverse 	= bilangan.toString().split('').reverse().join(''),
				ribuan 		= reverse.match(/\d{1,3}/g);
				ribuan		= ribuan.join('.').split('').reverse().join('');

		return ribuan;
	}

	$(".list").on('click', function() {
		var id 		=	$(this).attr('data-id');

		$.ajax({
			url: '/cashier/api/notifikasi',
	        type: 'GET',
	        dataType: 'json',
	        data: { id: id },
	        success: function(result) {
				$("#thisCart").empty();
				$("#list-barang").empty();

				$("#more").append(`
					<span>Affiliasi : Rp. ${rupiah(result[3])}</span><br>
					<span>Nomer Kartu : ${result[2].number_card}</span>
				`);

	        	$.each(result[0], function(i) {
            		$("#thisCart").append(
            			`<tr>
			              <td>${ result[0][i].name }</td>
			              <td>${ result[0][i].jumlah_barang }</td>
			            </tr>`
            		);

            		$("#list-barang").append(`
						<div class="list">
				          <span style="width:50%;display:inline-block;margin-bottom:5px;">${result[0][i].name} (<span>${result[0][i].jumlah_barang}</span>)</span>
				          <span style="float:right;">Rp. ${rupiah((result[0][i].harga_jual - (result[0][i].harga_jual * (result[0][i].diskon / 100))) * result[0][i].jumlah_barang)}</span>
				        </div>
					`);

					$("#total_semua_struk_potongan").html(rupiah(result[1].potongan));

					$("#total_semua_struk_jumlah").html(rupiah(result[1].total_belanja - result[1].potongan));

					$("#number_trans").html(result[4]);

					$("#total_semua_struk").html('Rp ' + rupiah(result[1].total_belanja));
					$("#total_dibayar").html('Rp ' + rupiah(result[1].total_belanja));
            	});
	        }
		})
	});

	$("#print").on('click', function() {
		var print_window = window,
	    print_document = $('#listPenjualan').clone();
	    print_window.document.open();
	    print_window.document.write(print_document.html());
	    print_window.document.close();
	    print_window.print();
	    print_window.location.reload();
	});
});
