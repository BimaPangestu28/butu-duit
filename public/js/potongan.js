$(document).ready(function() {

	$("#barcode").on('keyup keypress', function(e) {
		let barcode_barang	=	$("#barcode").val();

		if(e.keyCode == 13) {
			$.ajax({
				url: '/cashier/potongan-barang/api',
				method: 'GET',
				data: {
					barcode: barcode_barang
				},
				success: function(result) {
					$("#nama_barang").val(result.name);
					$("#id_product").val(result.id);
				}
			});
		}
	})

	$("#barcode").on('keypress', function(e) {
		if (event.keyCode === 10 || event.keyCode === 13) 
        event.preventDefault();
	});

	$("#submit_button").on('click', function() {
		$("#form").submit();
	});

});