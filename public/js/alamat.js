$(document).ready(function() {
  $("#form-provinsi").on('submit', function(e) {
    e.preventDefault();

    let provinsi    =   $("#provinsi").val().trim(),
        id          =   $(this).children("#provinsi-id").val();

    if (provinsi) {
      if (!id) {
        $.ajax({
          url: '/usahakumart/tambah-alamat',
          method: 'GET',
          data: {
            type: 'provinsi',
            provinsi: provinsi
          },
          success: function(data) {
            if (data.success) {
              let number    =   $("#table-provinsi tr").length;

              $("#table-provinsi").append(`
                <tr id="provinsi-${ data.id }">
                  <td class="text-center">${ number += 1 }</td>
                  <td class="text-center" id="nama-provinsi-${ data.id }">${ provinsi }</td>
                  <td class="text-center">
                    <button type="button" id="edit" data-id="${ data.id }" class="btn btn-warning edit-provinsi" style="color: white;">Edit</button>
                    <button type="button" id="hapus" data-id="${ data.id }" class="btn btn-danger hapus-provinsi">Hapus</button>
                  </td>
                </tr>
              `);

              $("#provinsi").val('');

              alert(data.success);
            } else {
              alert(data.error);
            }
          }
        });
      } else {
        $.ajax({
          url: '/usahakumart/update-alamat',
          method: 'GET',
          data: {
            type: 'provinsi',
            provinsi: provinsi,
            id: id
          },
          success: function(data) {
            if (data.success) {
              $("#nama-provinsi-" + id).html(provinsi);

              alert(data.success);
            } else {
              alert(data.warning);
            }

            $("#provinsi").val('');
            $("#provinsi-id").val('');
            $("#button-provinsi").val('Tambah Provinsi');
          }
        });
      }
    } else {
      alert('Isi form yang disediakan');
    }
  });

  $(document).on('click', '.hapus-provinsi', function() {
    let id    =   $(this).attr('data-id');

    if (confirm('Apakah anda yakin?')) {
      $.ajax({
        url: '/usahakumart/hapus-alamat',
        method: 'GET',
        data: {
          type: 'provinsi',
          id: id
        },
        success: function(response) {
          if (response.success) {
            $("#provinsi-" + id).fadeOut();

            alert(response.success);
          } else {
            alert(response.warning);
          }
        }
      });
    } else {
      return false;
    }
  });

  $(document).on('click', '.edit-provinsi', function() {
    let id          =     $(this).attr('data-id'),
        provinsi    =     $("#nama-provinsi-" + id).html();

    $("#provinsi").val(provinsi);
    $("#provinsi-id").val(id);
    $("#button-provinsi").val('Update Provinsi');
  });

  $("#form-kota").on('submit', function(e) {
    e.preventDefault();

    let kota            =   $("#kota").val().trim(),
        id_provinsi     =   $("#kota-id-provinsi").val(),
        provinsi        =   $("#kota-id-provinsi option:selected").text(),
        id              =   $(this).children("#kota-id").val();

    if (kota) {
      if (!id) {
        $.ajax({
          url: '/usahakumart/tambah-alamat',
          method: 'GET',
          data: {
            type       : 'kota',
            kota       : kota,
            id_provinsi: id_provinsi
          },
          success: function(data) {
            if (data.success) {
              let number    =   $("#table-kota tr").length;

              $("#table-kota").append(`
                <tr id="kota-${ data.id }">
                  <td class="text-center">${ number += 1 }</td>
                  <td class="text-center" data-id="${ data.id_provinsi }" id="nama-kota-provinsi-${ data.id }">${ data.provinsi }</td>
                  <td class="text-center" id="nama-kota-${ data.id }">${ kota }</td>
                  <td class="text-center">
                    <button type="button" id="edit" data-id="${ data.id }" class="btn btn-warning edit-kota" style="color: white;">Edit</button>
                    <button type="button" id="hapus" data-id="${ data.id }" class="btn btn-danger hapus-kota">Hapus</button>
                  </td>
                </tr>
              `);

              $("#kota").val('');

              alert(data.success);
            } else {
              alert(data.error);
            }
          }
        });
      } else {
        $.ajax({
          url: '/usahakumart/update-alamat',
          method: 'GET',
          data: {
            type        : 'kota',
            kota        : kota,
            id          : id,
            id_provinsi : id_provinsi
          },
          success: function(data) {
            if (data.success) {
              $("#nama-kota-" + id).html(kota);
              $("#nama-kota-provinsi-" + id).html(provinsi);
              $("#nama-kota-provinsi-" + id).attr('data-id', id_provinsi);

              alert(data.success);
            } else {
              alert(data.warning);
            }

            $("#kota").val('');
            $("#kota-id").val('');
            $("#button-kota").val('Tambah Kota');
          }
        });
      }
    } else {
      alert('Isi form yang disediakan');
    }
  });

  $(document).on('click', '.hapus-kota', function() {
    let id    =   $(this).attr('data-id');

    if (confirm('Apakah anda yakin?')) {
      $.ajax({
        url: '/usahakumart/hapus-alamat',
        method: 'GET',
        data: {
          type: 'kota',
          id: id
        },
        success: function(response) {
          if (response.success) {
            $("#kota-" + id).fadeOut();

            alert(response.success);
          } else {
            alert(response.warning);
          }
        }
      });
    } else {
      return false;
    }
  });

  $(document).on('click', '.edit-kota', function() {
    let id              =     $(this).attr('data-id'),
        kota            =     $("#nama-kota-" + id).html();
        id_provinsi     =     $("#nama-kota-provinsi-" + id).attr("data-id"),
        provinsi        =     $("#nama-kota-provinsi-" + id).html();

    $("#kota-id-provinsi").append(`
      <option selected="selected" value="${ id_provinsi }">${ provinsi }</option>
    `);

    $("#kota").val(kota);
    $("#kota-id").val(id);
    $("#button-kota").val('Update Kota');
  });

  $("#form-kecamatan").on('submit', function(e) {
    e.preventDefault();

    let kecamatan   =   $("#kecamatan").val().trim(),
        id_kota     =   $("#kecamatan-id-kota").val(),
        kota        =   $("#kecamatan-id-kota option:selected").text(),
        id          =   $(this).children("#kecamatan-id").val();

    if (kecamatan) {
      if (!id) {
        $.ajax({
          url: '/usahakumart/tambah-alamat',
          method: 'GET',
          data: {
            type       : 'kecamatan',
            kecamatan  : kecamatan,
            id_kota    : id_kota
          },
          success: function(data) {
            if (data.success) {
              let number    =   $("#table-kecamatan tr").length;

              $("#table-kecamatan").append(`
                <tr id="kecamatan-${ data.id }">
                  <td class="text-center">${ number += 1 }</td>
                  <td class="text-center" data-id="${ data.id_kota }" id="nama-kecamatan-kota-${ data.id }">${ data.kota }</td>
                  <td class="text-center" id="nama-kecamatan-${ data.id }">${ kecamatan }</td>
                  <td class="text-center">
                    <button type="button" id="edit" data-id="${ data.id }" class="btn btn-warning edit-kecamatan" style="color: white;">Edit</button>
                    <button type="button" id="hapus" data-id="${ data.id }" class="btn btn-danger hapus-kecamatan">Hapus</button>
                  </td>
                </tr>
              `);

              $("#kecamatan").val('');

              alert(data.success);
            } else {
              alert(data.error);
            }
          }
        });
      } else {
        $.ajax({
          url: '/usahakumart/update-alamat',
          method: 'GET',
          data: {
            type        : 'kecamatan',
            kecamatan   : kecamatan,
            id          : id,
            id_kota     : id_kota
          },
          success: function(data) {
            if (data.success) {
              $("#nama-kecamatan-" + id).html(kecamatan);
              $("#nama-kecamatan-kota-" + id).html(kota);
              $("#nama-kecamatan-kota-" + id).attr('data-id', id_kota);

              alert(data.success);
            } else {
              alert(data.warning);
            }

            $("#kecamatan").val('');
            $("#kecamatan-id").val('');
            $("#button-kecamatan").val('Tambah Kecamatan');
          }
        });
      }
    } else {
      alert('Isi form yang disediakan');
    }
  });

  $(document).on('click', '.hapus-kecamatan', function() {
    let id    =   $(this).attr('data-id');

    if (confirm('Apakah anda yakin?')) {
      $.ajax({
        url: '/usahakumart/hapus-alamat',
        method: 'GET',
        data: {
          type: 'kecamatan',
          id: id
        },
        success: function(response) {
          if (response.success) {
            $("#kecamatan-" + id).fadeOut();

            alert(response.success);
          } else {
            alert(response.warning);
          }
        }
      });
    } else {
      return false;
    }
  });

  $(document).on('click', '.edit-kecamatan', function() {
    let id              =     $(this).attr('data-id'),
        kecamatan       =     $("#nama-kecamatan-" + id).html();
        id_kota         =     $("#nama-kecamatan-kota-" + id).attr("data-id"),
        kota            =     $("#nama-kecamatan-kota-" + id).html();

    $("#kecamatan-id-kota").append(`
      <option selected="selected" value="${ id_kota }">${ kota }</option>
    `);

    $("#kecamatan").val(kecamatan);
    $("#kecamatan-id").val(id);
    $("#button-kecamatan").val('Update Kecamatan');
  });
});
