$(document).ready(function() {
	function rupiah(bilangan) {
		let 	reverse 	= bilangan.toString().split('').reverse().join(''),
				ribuan 		= reverse.match(/\d{1,3}/g);
				ribuan		= ribuan.join('.').split('').reverse().join('');

		return ribuan;
	}

	// $(".id_product").on('click', function() {
	// 	let id_product 		=	 	$(this).val();
	// 		id 		 		=	 	$("#id").val(),
	// 		cek 			=		$(this).attr('checked');
			
	// 	if(cek != 'checked') {
	// 		if (id == '') {
	// 			$("#id").val(id + id_product);
	// 		} else {
	// 			$("#id").val(id + ',' + id_product);
	// 		}
	// 	} else {
	// 		let data 		=		$(".id_product[checked='checked']");

	// 		$("#id").val('');

	// 		$.each(data, function(i) {
	// 			if (id == '') {
	// 				$("#id").val(id + data.val());
	// 			} else {
	// 				$("#id").val(id + ',' + data.val());
	// 			}
	// 		});
	// 	}
	// });

	$("#check-all").on('click', function() {
		if ($("#check-all").html() == "Ceklist Semua") {
			$(".id_product").attr('checked', 'checked');

			$("#check-all").html('Unceklist Semua');
		} else {
			$(".id_product").removeAttr('checked', 'checked');

			$("#check-all").html('Ceklist Semua');
		}
	});

	$("#product_text").on('keyup', function(e) {
		e.preventDefault();

		let filter = $(this).val().toUpperCase();
		
		$("#barangApi").find("tr").hide();
		$("tr[id*='" + filter + "']").show();

		if (filter == '') {
			$("#barangApi").find("tr").show();
		}
	});

	$("#barangApi tr").on('click', function() {
		if ($(this).find('input').attr('checked') != 'checked') {
			$(this).find('input').attr('checked', 'checked');

			let id_product 		=	 	$(this).find('input').val();
				id 		 		=	 	$("#id").val();
		
			if (id == '') {
				$("#id").val(id + id_product);
			} else {
				$("#id").val(id + ',' + id_product);
			}
		} else {
			$(this).find('input').removeAttr('checked', 'checked');

			$("#id").val('');

			let data 			=		$(".id_product[checked='checked']");

			$.each(data, function(i) {
				let id 		 		=	 	$("#id").val();
				
				if (id == '') {
					$("#id").val(id + data[i].value);
				} else {
					$("#id").val(id + ',' + data[i].value);
				}
				console.log(data[i].value)
			});
		}
	});

	$("#supplier").on('change', function() {
		let supplier	=	$("#supplier").val(),
			session		=	$("#id_ses").val();

		$("#barangApi").empty();

		$.ajax({
			url: '/cashier/gudang-barang/api',
			method: 'GET',
			data: {
				supplier: supplier
			},
			success: function(result) {
				if (session == 1) {
					$(result).each(function(key,value){
			            $("#barangApi").append(`
			              <tr>
			                <td><input type="checkbox" class="id_product" value="${ value.id }"></td>
			                <td>${ value.name }</td>
			                <td class="text-center">Rp. ${ rupiah(value.price) }</td>
			                <td class="text-center">Rp. ${ rupiah(value.harga_jual) }</td>
			                <td class="text-center">${ value.affiliasi }</td>
			                <td class="text-center">${ value.barcode }</td>
			                <td class="text-center">${ value.stok }</td>
			                <td class="text-center">
			                  <a class="btn-danger btn" onclick="return confirm('Anda yakin?')" href="/cashier/hapus-barang?id=${ value.id }" style="color: white;">Hapus Barang</a> <br> <br>
			                  <a class="btn-warning btn" href="/cashier/edit-barang?id=${ value.id }" style="color: white;">Edit Barang</a>
			                </td>
			              </tr>
			            `);
			        });
		        } else {
		        	$(result).each(function(key,value){
			            $("#barangApi").append(`
			            	<tr>
			                <td><input type="checkbox" class="id_product" value="${ value.id }"></td>
			                <td>${ value.name }</td>
			                <td class="text-center">Rp. ${ rupiah(value.price) }</td>
			                <td class="text-center">Rp. ${ rupiah(value.harga_jual) }</td>
			                <td class="text-center">${ value.affiliasi }</td>
			                <td class="text-center">${ value.barcode }</td>
			                <td class="text-center">${ value.stok }</td>
			              </tr>
			            `);
			        });
		        }
			}
		})
	});
});	