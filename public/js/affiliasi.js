$(document).ready(function() {
  function rupiah(bilangan) {
		if (bilangan != null) {
			let 	reverse 	= bilangan.toString().split('').reverse().join(''),
					ribuan 		= reverse.match(/\d{1,3}/g);
					ribuan		= ribuan.join('.').split('').reverse().join('');

			return ribuan;
		}
	}

  $(document).on('click', '.detail', function() {
      $(".loading").show();
      $(".detail-body").hide();

      let id    =   $(this).attr('data-id');

      $.ajax({
        url: '/cashier/laporan/affiliasi/api',
        method: 'GET',
        data: {
          id: id
        },
        success: function(data) {
          $(".loading").fadeOut();
          $(".detail-body").fadeIn();

          $(".detail-body tbody").empty();

          if (data != '' || data != null) {
            $.each(data, function(i, v) {
              $(".detail-body tbody").append(`
                <tr>
                  <td>${ i += 1 }</td>
                  <td class="text-center">Rp. ${ rupiah(v.jumlah) }</td>
                  <td class="text-center">${ v.created_at }</td>
                </tr>
              `)
            })
          } else {
            $(".detail-body tbody").append(`<span style="text-align: center;">Tidak Ada Data</span>`);
          }

        }
      })
  })

  $("#search-mp").on('change', function() {
    search($(this).val(), 'Masuk', 'Perusahaan', 'mp');
  });

  $("#search-mm").on('change', function() {
    search($(this).val(), 'Masuk', 'Member', 'mm');
  });

  $("#search-km").on('change', function() {
    search($(this).val(), 'Keluar', 'Member', 'km');
  });

  function search(tanggal, type, pemilik, table) {
    let bulan   =   tanggal.split("-")[1],
        tahun   =   tanggal.split("-")[0];

    $.ajax({
      url: '/cashier/laporan/affiliasi/api/search',
      method: 'GET',
      data: {
        bulan: bulan,
        tahun: tahun,
        type: type,
        pemilik: pemilik
      },
      success: function(data) {
        $("#" + table).empty();

        if (data[0] != null || data[0] != '') {
          $.each(data[0], function(i, v) {
            $("#" + table).append(`
              <tr>
                <td>${ i += 1 }</td>
                <td class="text-center">Rp. ${ rupiah(v.jumlah) }</td>
                <td class="text-center">${ v.date }</td>
                <td class="text-center"><button data-id="${ v.date }" type="button" data-toggle="modal" data-target="#modalDetail" name="button" class="btn btn-primary detail">Lihat Detail</button></td>
              </tr>
            `)
          })

          $("#" + table).append(`
            <tr>
              <td></td>
              <td class="text-center">Rp. ${ rupiah(data[1].jumlah) }</td>
              <td class="text-center"></td>
              <td class="text-center"></td>
            </tr>
          `)
        } else {
          $("#" + table).append(`<span style="text-align: center;">Tidak Ada Data</span>`);
        }
      }
    });
  }
});
