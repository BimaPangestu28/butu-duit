$(document).ready(function() {
	function rupiah(bilangan) {
		let 	reverse 	= bilangan.toString().split('').reverse().join(''),
				ribuan 		= reverse.match(/\d{1,3}/g);
				ribuan		= ribuan.join('.').split('').reverse().join('');

		return ribuan;
	}

	$.ajax({
        url: '/cashier/api/minggu',
        type: 'GET',
        dataType: 'json',
        success: function(result) {
        	$("#loading").hide();

       		$(result).each(function(key,value){
	            $("#minggu").append(`
	            	<tr>
	            		<td>${value.date}</td>
	            		<td>Rp. ${rupiah(value.count)}</td>
	            	</tr>
	            `);
	        });
        }
    });

    $.ajax({
        url: '/cashier/api/bulan',
        type: 'GET',
        dataType: 'json',
        success: function(result) {
        	$("#loading-bulan").hide();

       		$(result).each(function(key,value){
	            $("#bulan").append(`
	            	<tr>
	            		<td>${value.date}</td>
	            		<td>Rp. ${rupiah(value.count)}</td>
	            	</tr>
	            `);
	        });
        }
    });
});