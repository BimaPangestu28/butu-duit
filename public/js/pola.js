$(document).ready(function() {
		$("#option").on('change', function() {
			let option 		=		this.value;
			
			if (option == 2) {
				$(".potongan").fadeIn();
				$(".gratis").fadeOut();
			} else {
				$(".potongan").fadeOut();
				$(".gratis").fadeIn();
			}
		});

		$("#beli").on('keyup', function() {
			let data 	=	this.value;

			$("#beliList").empty();

			$.ajax({
				url: '/cashier/pola-perilaku-harga/api',
				method: 'GET',
				data: {
					data: data
				},
				success: function(result) {
					$(result).each(function(key,value){
			            $("#beliList").append(`
			            	<option value="${value.name}">${value.name}</option>
			            `);
			        });
				}
			});
		});

		$("#bonus").on('keyup', function() {
			let data 	=	this.value;

			$("#bonusList").empty();

			$.ajax({
				url: '/cashier/pola-perilaku-harga/api',
				method: 'GET',
				data: {
					data: data
				},
				success: function(result) {
					$(result).each(function(key,value){
			            $("#bonusList").append(`
			            	<option value="${value.name}">${value.name}</option>
			            `);
			        });
				}
			});
		});
});