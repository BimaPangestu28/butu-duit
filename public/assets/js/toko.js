$(document).ready(function () {

  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });

  function rupiah(bilangan) {
    let   reverse   = bilangan.toString().split('').reverse().join(''),
          ribuan    = reverse.match(/\d{1,3}/g);
          ribuan    = ribuan.join('.').split('').reverse().join('');

    return ribuan;
  }

  // Beli Barang
  $(document).on('click', '.beliBarang', function () {
    var id_barang     = $(this).attr('id-barang');
    var id_toko       = $('meta[name="toko-identity"]').attr('content');
    var nama_barang   = $('#card_' + id_barang + ' ' + '.judul-barang').text();
    var harga_barang  = $('#card_' + id_barang + ' ' + '.my-price').attr('data-id');
    var stok          = $('#jumlah_' + id_barang).attr('max');
    var tag_tr        = $('#HanyaList_' + id_barang).length;
    var aff           = $("#affiliasi_pembeli_" + id_barang).html();

    if ($('#card_' + id_barang + ' ' + '.judul-barang').length > 1) {
      nama_barang = $('#card_' + id_barang + ' ' + '.judul-barang').first().text();
    }

    $.ajax({
      type: "POST",
      url: '/toko-usahakumart/cart/' + id_barang,
      data: {toko : id_toko, barang : id_barang, nama : nama_barang, harga : harga_barang},
      typeData: 'json',
      success:function (data) {

        if (tag_tr > 0) { //gak tau if condition nya
          $("#jumlah_" + id_barang).val(data.jumlah_barang);
          $("#harga_" + id_barang).html('Rp. ' + rupiah(data.jumlah_barang * harga_barang));
          $("#affiliasi_" + id_barang).html('Rp. ' + rupiah(data.jumlah_barang * aff.replace('.', '')));
        } else {
          $('#thisCart').prepend(
            "<tr id="+'HanyaList_'+data.id_barang+">" +
              '<td>'+nama_barang+'</td>'+
              '<td><input id="jumlah_'+ data.id_barang +'" id-number="'+data.id_barang+'" class="form-control add-core" type="number" min="1" max="'+stok+'" value="'+data.jumlah_barang+'"></input></td>'+
              '<td>'+'Rp.'+ rupiah(harga_barang) +'</td>'+
              '<td id="harga_'+ data.id_barang +'">Rp. '+ rupiah(harga_barang) +'</td>'+
              '<td id="affiliasi_'+ data.id_barang +'">Rp. '+ rupiah(aff.replace('.', '')) +'</td>'+
              '<td><button type="button" data-hapus="'+data.id_barang+'" class="hapus_barang btn btn-danger"> <i class="fa fa-trash"></i> </button></td>'+
            '</tr>'
          );
        }

        $('#cart_offline').click();
      }
    });
  });

  // Hapus barang cart
  $(document).on('click', '.hapus_barang', function () {
    var id = $(this).attr('data-hapus');

    $.ajax({
      type: 'POST',
      url: '/toko-usahakumart/cart/delete/' + id,
      data: {id_barang : id},
      success:function (data) {
        $('#HanyaList_'+id).fadeOut();
        $('#HanyaList_'+id).remove();
      }
    });
  });

  $(document).on('change', '.add-core',function () {
    var jumlah = $(this).val();
    var id = $(this).attr('id-number');
    var harga_barang  = $('#card_' + id + ' ' + '.my-price').attr('data-id');
    var aff           = $("#affiliasi_pembeli_" + id).html();

    $.ajax({
      type: 'POST',
      url: '/toko-usahakumart/jumlah/' + id,
      data: {jumlah_barang: jumlah, id_produk: id},
      success:function (data) {
        $("#harga_" + id).html('Rp. ' + rupiah(data.jumlah_barang * harga_barang));
        $("#affiliasi_" + id).html('Rp. ' + rupiah(data.jumlah_barang * aff.replace('.', '')));
      }
    })
  });

  $('#mySearch').keyup(function () {
    var isi     = $('#mySearch').val();
    var id_toko = $('meta[name="toko-identity"]').attr('content');
    $.ajax({
      type: 'GET',
      url: '/toko-usahakumart/search/'+ id_toko,
      data: {search : isi},
      success:function (data) {
          if (isi.length > 0) {
            $('#searchResult').html(data);
          }else {
            $('#searchResult').empty();
          }
      }
    });
  });

});
