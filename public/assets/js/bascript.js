$(document).ready(function () {


	$('#provinsi').on('change', function (e) {


		var provinsi_id = e.target.value;


		$.get('/ajax-kota?id_prov=' + provinsi_id, function (data) {
			// console.log(data);
			$('#kota').empty();
			$('#kecamatan').empty();
			$.each(data, function (index, kotaObj) {
				$('#kota').append('<option value="'+kotaObj.id+'">'+kotaObj.kota+'</option>')
			});
		});

		$.get('/ajax-kota-barang?id_prov=' + provinsi_id, function (data) {
			// console.log(data);
			$('#list-toko').empty();
			$.each(data, function (index, dataObj) {
				$('#list-toko').append(`<a href="/toko-usahakumart/${dataObj.id}"><li class="list-group-item"><span>${dataObj.name}</span><br><small>${dataObj.kecamatan}, ${dataObj.kota}, ${dataObj.provinsi}</small></li></a>`)
			});
		});
	});

	$('#kota').on('change', function (e) {


		var kota_id = e.target.value;


		$.get('/ajax-kecamatan?id_kota=' + kota_id, function (data) {
			// console.log(data);
			$('#kecamatan').empty();
			$.each(data, function (index, kecObj) {
				$('#kecamatan').append('<option value="'+kecObj.id+'">'+kecObj.kecamatan+'</option>')
			});
		});

		$.get('/ajax-kecamatan-barang?id_kota=' + kota_id, function (data) {
			// console.log(data);
			$('#list-toko').empty();
			$.each(data, function (index, dataObj) {
				$('#list-toko').append(`<a href="/toko-usahakumart/${dataObj.id}"><li class="list-group-item"><span>${dataObj.name}</span><br><small>${dataObj.alamat}</small></li></a>`)
			});
		});
	});

	$('#kecamatan').on('change', function (e) {
		var kecamatan_id = e.target.value;

		$.get('/ajax-final-barang?id_kecamatan=' + kecamatan_id, function (data) {
			// console.log(data);
			$('#list-toko').empty();
			$.each(data, function (index, dataObj) {
				$('#list-toko').append(`<a href="/toko-usahakumart/${dataObj.id}"><li class="list-group-item"><span>${dataObj.name}</span><br><small>${dataObj.alamat}</small></li></a>`)
			});
		});
	});
	
});
