$(document).ready(function() {
	$("#cari").on('submit', function(e) {
		e.preventDefault();

		let provinsi 		=	$("#provinsi").val(),
			kota	 		=	$("#kota").val(),
			kecamatan	 	=	$("#kecamatan").val(),
			kategori	 	=	$("#kategori").val();

		$.ajax({
		  type: 'GET',
	      url: '/api/searchToko',
	      data: {
	      	provinsi: provinsi,
	      	kota: kota,
	      	kecamatan: kecamatan,
	      	kategori: kategori
	      },
	      beforeSend: function() {
	      	$(".loader").css('display', 'flex');
	      },
	      success:function (result) {
	      	$(".loader").css('display', 'none');	
	        $("#list-toko").empty();
        	$.each(result, function(i) {
        		$("#list-toko").append(
        			`<a href="/toko-usahakumart/${ result[i].id }">
						<li class="list-item">
							<span>${ result[i].name }</span> <br>
							<small>Alamat : ${ result[i].alamat }</small>
						</li>
					</a>`
        		);
        	});
	      }
		})
	});

	$("#provinsi").on('change', function() {
		let id	=	$(this).val();

		$.ajax({
		  type: 'GET',
	      url: '/api/provinsi',
	      data: {
	      	id: id
	      },
	      success:function (result) {
	      	$("#kota").empty();
	        $.each(result[0], function(i) {
        		$("#kota").append(
        			`<option value="${ result[0][i].id }">${ result[0][i].kota }</option>`
        		);
        	});

	        $("#kecamatan").empty();
        	$.each(result[1], function(i) {
        		$("#kecamatan").append(
        			`<option value="${ result[1][i].id }">${ result[1][i].kecamatan }</option>`
        		);
        	});
	      }
		})
	});

	$("#kota").on('change', function() {
		let id	=	$(this).val();

		$.ajax({
		  type: 'GET',
	      url: '/api/kota',
	      data: {
	      	id: id
	      },
	      success:function (result) {
	        $("#kecamatan").empty();
        	$.each(result, function(i) {
        		$("#kecamatan").append(
        			`<option value="${ result[i].id }">${ result[i].kecamatan }</option>`
        		);
        	});
	      }
		})
	});

	$("#query-search").on('keyup', function() {
		let query 		=	$("#query-search").val(),
			provinsi 	=	$("#provinsi").val(),
			kecamatan	=	$("#kecamatan").val(),
			kota 		=	$("#kota").val();
			kategori	=	$("#kategori").val();

		$.ajax({
		  type: 'GET',
	      url: '/api/searchToko',
	      data: {
	      	queryS: query,
	      	provinsi: provinsi,
	      	kecamatan: kecamatan,
	      	kota: kota,
	      	kategori: kategori
	      },
	      beforeSend: function() {
	      	$(".loader").css('display', 'flex');
	      },
	      success:function (result) {
	      	$(".loader").css('display', 'none');	
	        $("#list-toko").empty();
	        console.log(result);
        	$.each(result, function(i) {
        		$("#list-toko").append(
        			`<a href="/toko-usahakumart/${ result[i].id }">
						<li class="list-item">
							<span>${ result[i].name }</span> <br>
							<small>Alamat : ${ result[i].alamat }</small>
						</li>
					</a>`
        		);
        	});
	      }
		})
	});
});