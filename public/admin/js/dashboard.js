$(document).ready(function() {
	window.onload = function() {
		function anggota() {
			$.ajax({
		        url: '/api-dashboard/anggota-baru',
		        type: 'GET',
		        dataType: 'json',
		        beforeSend: function()
	            {
	            	$("#anggota-baru").text('Loading...');
	            },
	            success: function(result) {
	           		$("#anggota-baru").text(result); 	
	            }
		    });
		}

		function totalAnggota() {
			$.ajax({
		        url: '/api-dashboard/total-anggota',
		        type: 'GET',
		        dataType: 'json',
		        beforeSend: function()
	            {
	            	$("#total-anggota").text('Loading...');
	            },
	            success: function(result) {
	           		$("#total-anggota").text(result); 	
	            }
		    });
		}

		function kartu() {
			$.ajax({
		        url: '/api-dashboard/kartu',
		        type: 'GET',
		        dataType: 'json',
		        beforeSend: function()
	            {
	            	$("#kartu").text('Loading...');
	            },
	            success: function(result) {
	           		$("#kartu").text(result); 	
	            }
		    });
		}

		function toko() {
			$.ajax({
		        url: '/api-dashboard/toko',
		        type: 'GET',
		        dataType: 'json',
		        beforeSend: function()
	            {
	            	$("#toko").text('Loading...');
	            },
	            success: function(result) {
	           		$("#toko").text(result); 	
	            }
		    });
		}

		function aktif() {
			$.ajax({
		        url: '/api-dashboard/aktif',
		        type: 'GET',
		        dataType: 'json',
		        beforeSend: function()
	            {
	            	$("#aktif").text('Loading...');
	            },
	            success: function(result) {
	           		$("#aktif").text(result); 	
	            }
		    });
		}

		function tidakAktif() {
			$.ajax({
		        url: '/api-dashboard/tidak-aktif',
		        type: 'GET',
		        dataType: 'json',
		        beforeSend: function()
	            {
	            	$("#tidak-aktif").text('Loading...');
	            },
	            success: function(result) {
	           		$("#tidak-aktif").text(result); 	
	            }
		    });
		}

		anggota();
		totalAnggota();
		kartu();
		toko();
		aktif();
		tidakAktif();
	}
});