$(document).ready(function() {

	// var timer;
	// $('#barcode').keyup(function () {
	// 	timer = setTimeout(function () {
	// 		var keywords = $('#barcode').val();
	// 		$('#search-results .this-result').show();

	// 		if (keywords.length > 5) {

	// 			$.get('http://localhost:8000/search/liveSearch', {keywords: keywords}, function (markup) {
	// 				$('#search-results .this-result').html(markup);
	// 			});
				
	// 		} else  {
	// 			$('#search-results .this-result').html("");
	// 		}
	// 	}, 500);
	// });


	var timer;

	$("#barcode").on('keyup', function() {
		timer = setTimeout(function () {
			let nama	=	$("#barcode").val();
			var words = $('#barcode').val().split(' ');

			$("#search-results .this-result").empty();

			if (words.length >= 2) {
				$("#search-results .this-result").show();

				$.ajax({
					url: '/search/liveSearch',
					method: 'GET',
					data: {
						nama: nama
					},
					success: function(result) {
						$(result).each(function(key, value){
							$("#search-results .this-result").append(`
								<button  barCodeList='${ value.barcode }' id="thisOne" type="submit" style="background: none; border: none;display: inline-block;width: 100%;padding: 0;outline: 0; text-align: left;">
									<div class="my-result-search card" style="list-style: none; padding: 0px;">
										<li id="akuSuka">${ value.name }</li>
										<li>${ value.barcode }</li>
									</div>
								</button>
							`);
						});
					}
				})
			} else {
				$("#search-results .this-result").empty();
			}

		}, 500);
	});

	$('#barcode').keydown(function () {
		clearTimeout(timer);
	});

	// var barcode = $('#barcode').val();

	// if (barcode == null) {
	// 	$('#search-results .this-result').hide();
	// }else {
	// 	$('#search-results .this-result').show();
	// }

});