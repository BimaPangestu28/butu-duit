$(document).ready(function() {
	$("#provinsi").on('change', function() {
		let id	=	this.value;

		$.ajax({
	        url: '/api-destination/kota?id_prov=' + id,
	        type: 'GET',
	        dataType: 'json',
	        beforeSend: function()
            {
            	$("#kota").empty();
            	$("#kecamatan").empty();
            },
            success: function(result) {
           		$(result[0]).each(function(key,value){
		            $("#kota").append(`
		            	<option value="${ value.id }">${ value.kota }</option>
		            `);
		        });

		        $(result[1]).each(function(key,value){
		            $("#kecamatan").append(`
		            	<option value="${ value.id }">${ value.kecamatan }</option>
		            `);
		        });
            }
	    });
	})

	$("#kota").on('change', function() {
		let id	=	this.value;

		$.ajax({
	        url: '/api-destination/kecamatan?id_kota=' + id,
	        type: 'GET',
	        dataType: 'json',
	        beforeSend: function()
            {
            	$("#kecamatan").empty();
            },
            success: function(result) {
           		$(result).each(function(key,value){
		            $("#kecamatan").append(`
		            	<option value="${ value.id }">${ value.kecamatan }</option>
		            `);
		        });
            }
	    });
	});

	$(document).on('click', '.akun-option', function() {
		let id 				=		$(this).attr('data-id');

		$("#id-toko").val(id);
	});

	$(document).on('click', '#update-toko', function() {
		let id 				=		$("#id-toko").val(),
				durasi 		=		$("#durasi-toko").val();

		if (confirm('Apakah anda yakin?')) {
			$.ajax({
				url: '/usahakumart/durasi-toko',
				method: 'GET',
				data: {
					id: id,
					durasi: durasi
				},
				success: function(respon) {
					if (respon.error) {
						alert(respon.error);
					} else {
						alert(respon.success);

						$("#durasi-" + id).html(respon.durasi);
					}
				}
			});
		} else {
			alert('Penambahan durasi dibatalkan');
		}
	});
});
