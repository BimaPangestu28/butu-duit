$(document).ready(function() {
	$("#barcode").focus();
	function rupiah(bilangan) {
		if (bilangan != null) {
			let 	reverse 	= bilangan.toString().split('').reverse().join(''),
					ribuan 		= reverse.match(/\d{1,3}/g);
					ribuan		= ribuan.join('.').split('').reverse().join('');

			return ribuan;
		}
	}

	$("#close-tab").on('click', function() {
		window.top.close();
	});

	window.onload = function() {
		$.removeCookie('harga', { path: '/cashier' }),
		$.removeCookie('update-data', { path: '/cashier' }),
		$.removeCookie('result', { path: '/cashier' });
		$.removeCookie('member', { path: '/cashier' });
		$.removeCookie('total', { path: '/cashier' });
	}

	$("#beli_langsung").on('submit', function(e) {
		let pembayaran 		=	$("#pembayaran_beli").val(),
			total 			=	$("#total-semua").val();

		if (pembayaran == '' || parseInt(pembayaran) < parseInt(total)) {
			e.preventDefault();

			alert('Isi Total Pembayaran Dengan Benar');
		} else {
			let produk 		=	$("#id").val();

			$("#total_semua_struk_dibayar").html('Rp. ' + rupiah(pembayaran));
			$("#total_semua_struk_kembalian").html('Rp. ' + rupiah(parseInt(pembayaran) - parseInt(total)));

			$.cookie('total', pembayaran);

			var print_window = window.open(),
		    print_document = $('#listPenjualan').clone();
		    print_window.document.open();
		    print_window.document.write(print_document.html());
		    print_window.document.close();
			print_window.print();
		    print_window.close();
		}
	});

	$(document).on('keyup change', '.stok', function() {
		let stok 	=	$(this).attr('max'),
			value 	=	$(this).val();

		if(parseInt(value) > parseInt(stok)) {
			$(this).val(parseInt(stok));
		} else if(value == '') {
			$(this).val(1);
		}
	})

	$("#potongan").on('change', function() {
		let potongan 	=	parseInt($("#potongan").val()),
			total 		=	$("#total").attr('total'),
			final 		=	total - potongan;
			/*console.log(total);*/

		$("#total-semua").val(final);
		$("#potongan").val(potongan);
		$("#total").html(`Total Harga Rp. ${rupiah(final)}`);
		$("#total-potongan").html(`Total Potongan Harga Rp. ${rupiah(potongan)}`);
	});

	$(document).on('mouseup keyup', '.potongan', function(){
		let id 			=	$(this).attr('data-id'),
			value 		=	$(this).val(),
			harga_jual 	=	parseInt($(`#harga_jual-${id}`).attr('data-id')),
			stok 	 	=	parseInt($(`#stok-${id}`).val());

		$(`#harga_jual-${id}`).html(`Rp. ${ rupiah(harga_jual - value) }`);
	});

	$(document).on('mouseup keyup', '.stok', function() {
		let id 				=	$(this).attr('id'),
			stok 		    =	$("#" + id).val(),
			id_pro 			=	$(this).attr('data-id'),
			harga_jual 		=	$("#harga_jual-" + id_pro).attr('data-id'),
			data_single 	=	$("#harga_jual-" + id_pro).attr('data-single'),
			affiliasi		=	$("#affiliasi-" + id_pro).attr('data-id'),
			data_aff		=	$("#affiliasi-" + id_pro).attr('data-single'),
			tstok			=	stok,
			diskon 			=	$("#diskon-" + id_pro).attr('data-id'),
			tharga_jual		=	parseInt(data_single), //diskon disini di hapus, karena ambil parameternya udah di diskon
			taffiliasi		=	parseInt(data_aff) * tstok,
			hasil			=	$("#total-semua").val(),
			harga			=	parseInt(hasil) + (data_single * tstok),
			potongan 		=	parseInt($(`#potongan-${id_pro}`).attr('data-id')),
			total 			=	(tstok * tharga_jual) - potongan,
			total_harga 	=	parseInt($("#total").attr('total'));

		$(this).attr('value', $(this).val());

		$.ajax({
			url: '/cashier/potongan-barang/cek',
			method: 'GET',
			data: {
				id: id_pro
			},
			success: function(result) {
				/*if ((stok % result.jumlah_dibeli) == 0) {
					$(`#potongan-${id_pro}`).html(`Rp. ${ rupiah((stok / result.jumlah_dibeli) * result.potongan) }`);
					$(`#potongan-${id_pro}`).attr('data-id', (stok / result.jumlah_dibeli) * result.potongan);

					$("#harga_jual-" + id_pro).attr('data-id', total - result.potongan);
					$("#harga_jual-" + id_pro).html('Rp. ' + rupiah(total - result.potongan));

					$("#total").html('Total Harga Rp. ' + rupiah((total_harga + parseInt(data_single)) - result.potongan));
					$("#total").attr('total', (total_harga + parseInt(data_single)) - result.potongan);
					$("#total_semua_struk_jumlah").html('Rp. ' + rupiah(total_harga - result.potongan));
					$("#total_semua_struk_potongan").html('Rp. ' + rupiah((stok / result.jumlah_dibeli) * result.potongan));
					$("#total-semua").val(total_harga - ((stok / result.jumlah_dibeli) * result.potongan));

					$("#harga_jual_struk-" + id_pro).attr('data-id', total - result.potongan);
					$("#harga_jual_struk-" + id_pro).html('Rp. ' + rupiah(total - result.potongan));
				} else {
					$("#harga_jual_struk-" + id_pro).attr('data-id', total);
					$("#harga_jual_struk-" + id_pro).html('Rp. ' + rupiah(total));
				}*/

				let potonganResult 	=	Math.floor(stok / result.jumlah_dibeli);

				if (result.length != 0) {
					$(`#potongan-${id_pro}`).html(`Rp. ${ rupiah((potonganResult) * result.potongan) }`);
					$(`#potongan-${id_pro}`).attr('data-id', (potonganResult) * result.potongan);

					$("#harga_jual-" + id_pro).attr('data-id', total - result.potongan);
					$("#harga_jual-" + id_pro).html('Rp. ' + rupiah(total - result.potongan));

					$("#total").html('Total Harga Rp. ' + rupiah((total_harga + parseInt(data_single)) - result.potongan));
					$("#total").attr('total', (total_harga + parseInt(data_single)) - result.potongan);
					$("#total_semua_struk_jumlah").html('Rp. ' + rupiah(total_harga - result.potongan));
					$("#total_semua_struk_potongan").html('Rp. ' + rupiah((potonganResult) * result.potongan));
					$("#total-semua").val(total_harga - ((potonganResult) * result.potongan));

					$("#harga_jual_struk-" + id_pro).attr('data-id', total - result.potongan);
					$("#harga_jual_struk-" + id_pro).html('Rp. ' + rupiah(total - result.potongan));

					total_update(id_pro);
				} else {
					let result 			=	$(".harga_result"),
						pot 					=	$(".potongan"),
						aff 					=	$(".affiliasi"),
						total_semua 	=	0,
						affiliasi 		=	0;

					$.each(result, function(i, v) {
						total_semua += parseInt($(this).attr('data-id'));
					});

					$.each(aff, function(i, v) {
						affiliasi += parseInt($(this).attr('data-id'));
					});

					$.each(pot, function(i, v) {
						potongan 	+= parseInt($(this).attr('data-id'));
					});

					$("#total").html('Total Harga Rp. ' + rupiah(total_semua));
					$("#total").attr('total', total_semua);
					$("#total-semua").attr('total', total_semua);
					$("#total-semua").val(total_semua);

					$("#total_semua_struk").html('Rp. ' + rupiah(total_semua + potongan));
					$("#total_semua_struk_jumlah").html('Rp. ' + rupiah(total_semua));

					$("#total_affiliasi").val(affiliasi);

					$("#affiliasi-struk").html(`Affiliasi : Rp. ${affiliasi}`);
					$("#affiliasi-struk").attr('data-id', affiliasi);
				}
			}
		});

		$("#harga_jual_struk-" + id_pro).attr('data-id', total);
		$("#harga_jual_struk-" + id_pro).html('Rp. ' + rupiah(total));

		$("#stok-" + id_pro).val(tstok);
		$("#jumlah_struk-" + id_pro).html(tstok);
		$("#harga_jual-" + id_pro).attr('data-id', total);
		$("#harga_jual-" + id_pro).html('Rp. ' + rupiah(total));

		$("#affiliasi-" + id_pro).attr('data-id', taffiliasi);
		$("#affiliasi-" + id_pro).html('Rp. ' + rupiah(taffiliasi));

		let tr 		=	$("tr");

		$("#id").val('');

		$.each(tr, function(i, v) {
			let barang 	=	$(this).attr('id'),
				stok 	=	$(`tr[id=${barang}] td .stok`).val();

			for (let index = 0; index < stok; index++) {
				let id 		=	$("#id").val();

				if (id == '') {
					$("#id").val(barang);
				} else {
					$("#id").val(id + ',' + barang);
				}
			}
		});
	});

	function total_update(id_pro) {
		let result 			=	$(".harga_result"),
			pot 			=	$(".potongan"),
			aff 			=	$(".affiliasi"),
			total_semua 	=	0,
			total_belanja 	=	0,
			potongan 		=	0,
			affiliasi 		=	0;

		$.each(result, function(i, v) {
			total_semua += parseInt($(this).attr('data-id'));
		});

		$.each(aff, function(i, v) {
			affiliasi += parseInt($(this).attr('data-id'));
		});

		$.each(result, function(i, v) {
			total_belanja += (parseInt($(this).attr('data-single') * $(`#stok-${id_pro}`).val()));
		});

		$.each(pot, function(i, v) {
			potongan 	+= parseInt($(this).attr('data-id'));
		});

		$("#potongan").attr('value', potongan);
		$("#total-potongan").html(`Total Potongan Harga Rp. ${rupiah(potongan)}`);
		$("#total").attr('total', total_semua);
		$("#total-semua").val(total_semua);
		$("#total").html('Total harga Rp. ' + rupiah(total_semua));
		$("#total_semua_struk").html('Rp. ' + rupiah(total_semua + potongan));
		$("#total_semua_struk_jumlah").html('Rp. ' + rupiah(total_semua));
		$("#affiliasi-struk").html(`Affiliasi : Rp. ${affiliasi}`);
		$("#affiliasi-struk").attr('data-id', affiliasi);

		$.cookie('harga', total_semua);
		$.cookie('update-data', '1');
	}

	$(document).on('click', '.delete', function() {
		let id 	=	$(this).attr('data-id');

		$(`tr[id='${id}']`).remove();
		$(`#print-${id}`).remove();

		total_update();

		let array	=	$("#id").val().split(","),
			counts 	=	[];

		// counts.splice( $.inArray(id, counts), 1 );

		// $.each(counts, function(i, v) {
		// 	final.push(v);
		// });

		// for(var i in counts){
		// 	if(counts[i] == id){
		// 		counts.splice(i,1);
		// 		break;
		// 	}
		// }

		// console.log(counts);

		let tr 		=	$("tr"),
			id_pro	=	null;

		$("#id").val('');

		$.each(tr, function(i, v) {
			let barang 	=	$(this).attr('id'),
				stok 	=	$(`tr[id=${barang}] td .stok`).val();

			for (let index = 0; index < stok; index++) {
				let id 		=	$("#id").val();

				if (id == '') {
					$("#id").val(barang);
				} else {
					$("#id").val(id + ',' + barang);
				}
			}
		});
	});

	$(document).on('submit', '#submit', function(e) {
		e.preventDefault();
		let barcode 	=	$("#barcode").val(),
			produk 		=	$("#id").val();

		$.ajax({
			url: '/cashier/get?barcode=' + barcode,
			method: 'GET',
			dataType: 'json',
			data: {
				produk: produk
			},
			success: function(result) {
				if (result.length == 0) {
					alert('Barcode atau Nama barang tidak ditemukan');
				} else if(result == 'alert') {
					alert('Stok barang habis');
				} else {

				$("#barcode").val('');
				$('#search-results .this-result').hide();

				let id 		=	$("#id").val(),
					total	=	$("#total-semua").val();

				if (total == '') {
					total = 0;
				}

				//harga	=	parseInt(total) + (result[0].harga_jual - (result[0].harga_jual * (result[0].diskon / 100)));
				harga		=	parseInt(total) + (result.harga_jual - (result.harga_jual * (result.diskon / 100)));
				potongan 	=	$("#potongan").val();

				$("#total").attr('total', harga);
				$("#total-semua").val(harga);
				$("#total").html('Total Harga Rp. ' + rupiah(harga));
				$("#total_semua_struk").html('Rp. ' + rupiah(harga + parseInt(potongan)));
				$("#total_semua_struk_jumlah").html('Rp. ' + rupiah(harga));

				$.cookie('harga', harga);
				$.cookie('update-data', '1');

				if (id == '') {
					$("#id").val(result.id);
				} else {
					$("#id").val(id + ',' + result.id);
				}

				if ($("#affiliasi-struk").length > 0) {
					let affstruk 	=	$("#affiliasi-struk").attr('data-id');

					$("#affiliasi-struk").html(`Affiliasi : Rp. ${parseInt(affstruk) + (result.affiliasi * (25 / 100))}`);

					$("#affiliasi-struk").attr('data-id', parseInt(affstruk) + (result.affiliasi * (25 / 100)));
				} else {
					$("#more").append(`
						<span id="affiliasi-struk" data-id="${result.affiliasi * (25 / 100)}">Affiliasi : Rp. ${rupiah(result.affiliasi * (25 / 100))} </span><br>
					`);
				}

				if ($("#" + result.id).length > 0) {
					let stok			=	$("#stok-" + result.id).val(),
						harga_jual 		=	$("#harga_jual-" + result.id).attr('data-id'),
						affiliasi		=	$("#affiliasi-" + result.id).attr('data-id'),
						tstok			=	parseInt(stok) + 1,
						tharga_jual		=	parseInt(harga_jual) + (result.harga_jual - (result.harga_jual * (result.diskon / 100))),
						taffiliasi		=	parseInt(affiliasi) + (result.affiliasi * (25 / 100));

					$("#stok-" + result.id).val(tstok);
					$("#jumlah_struk-" + result.id).html(tstok);

					$("#harga_jual-" + result.id).attr('data-id', tharga_jual);
					$("#harga_jual-" + result.id).html('Rp. ' + rupiah(tharga_jual));
					$("#harga_jual_struk-" + result.id).attr('data-id', tharga_jual);
					$("#harga_jual_struk-" + result.id).html('Rp. ' + rupiah(tharga_jual));

					$("#affiliasi-" + result.id).attr('data-id', taffiliasi);
					/*$("#affiliasi-" + result.id).attr('data-single', taffiliasi);*/
					$("#affiliasi-" + result.id).html('Rp. ' + rupiah(taffiliasi));
				} else {
					$("#barang").append(`
						<tr id='${result.id}'>
		                    <td>${result.name}</td>
		                    <td><input type='number' min='1' max='${result.stok}' data-id='${result.id}' class='stok' id='stok-${result.id}'value='1'></td>
		                    <td id='harga_jual-${result.id}' class='harga_result' data-id='${result.harga_jual - (result.harga_jual * (result.diskon / 100))}' data-single='${result.harga_jual - (result.harga_jual * (result.diskon / 100))}'>Rp. ${rupiah(result.harga_jual - (result.harga_jual * (result.diskon / 100)))}</td>
		                    <td id='affiliasi-${result.id}' class='affiliasi' data-id='${result.affiliasi * (25 / 100)}' data-single='${result.affiliasi * (25 / 100)}'>Rp. ${rupiah(result.affiliasi * (25 / 100))}</td>
		                    <td id='diskon-${result.id}' data-id='${result.diskon}'>${result.diskon}%</td>
		                    <td id='potongan-${result.id}' data-id='0' class='potongan'>0</td>
		                    <td><button id='hapus-${result.id}' data-id='${result.id}' class='btn btn-danger btn-sm delete'>X</button></td>
		                 </tr>
					`);

					$("#list-barang").append(
						'<div class="list" id=print-' + result.id + '>' +
				          '<span style="width:50%;display:inline-block;margin-bottom:5px;">' + result.name + (result.diskon != 0 ? " DISC. " + result.diskon + "%" : "") + '(<span id="jumlah_struk-' + result.id + '">1</span>)</span>' +
				          '<span style="float:right;" id="harga_jual_struk-' + result.id + '" data-id="' + (result.harga_jual - (result.harga_jual * (result.diskon / 100))) + '">Rp.' + (rupiah(result.harga_jual - (result.harga_jual * (result.diskon / 100)))) + '</span>' +
				        '</div>'
					);

					var height = 0;
					$('#barang tr').each(function(i, value){
					    height += parseInt($(this).height());
					});

					height += '';
					$('#body-penjualan').animate({scrollTop: height});
				}

			let id_pro_barcode 			=	result.id,
				harga_jual_barcode 		=	$("#harga_jual-" + id_pro_barcode).val();

			$.ajax({
				url: '/cashier/potongan-barang/cek',
				method: 'GET',
				data: {
					id: id_pro_barcode,
					jumlah: 1
				},
				success: function(hasil) {
					if (hasil.length != 0) {
						let potonganResult 	=	hasil.potongan;

						$(`#potongan-${id_pro_barcode}`).html(`Rp. ${ rupiah((potonganResult)) }`);
						$(`#potongan-${id_pro_barcode}`).attr('data-id', (potonganResult));

						$("#harga_jual-" + id_pro_barcode).attr('data-id', (result.harga_jual - (result.harga_jual * (result.diskon / 100))) - potonganResult);
						$("#harga_jual-" + id_pro_barcode).html('Rp. ' + rupiah((result.harga_jual - (result.harga_jual * (result.diskon / 100))) - potonganResult));

						let hasils 					=	$(".harga_result"),
							pots 					=	$(".potongan"),
							affs 					=	$(".affiliasi"),
							total_semuas 			=	0,
							total_belanjas 			=	0,
							potongans 				=	0,
							affiliasis				=	0;

						$.each(hasils, function(i, v) {
							total_semuas += parseInt($(this).attr('data-id'));
						});

						$.each(affs, function(i, v) {
							affiliasis += parseInt($(this).attr('data-id'));
						});

						$.each(hasils, function(i, v) {
							total_belanjas += (parseInt($(this).attr('data-single') * $(`#stok-${id_pro_barcode}`).val()));
						});

						$.each(pots, function(i, v) {
							potongans 	+= parseInt($(this).attr('data-id'));
						});

						$("#total").html('Total Harga Rp. ' + rupiah(total_semuas));
						$("#total").attr('total', total_semuas);
						$("#total_semua_struk_jumlah").html('Rp. ' + rupiah(total_semuas + potongans));
						$("#total_semua_struk_potongan").html('Rp. ' + rupiah(potongans));
						$("#total-semua").val(total_semuas);

						$("#harga_jual_struk-" + id_pro_barcode).html('Rp. ' + rupiah(result.harga_jual - hasil.potongan));

						//$("#harga_jual_struk-" + id_pro_barcode).attr('data-id', total_barcode - result.potongan);
						//$("#harga_jual_struk-" + id_pro_barcode).html('Rp. ' + rupiah(total_barcode - result.potongan));

						total_update(id_pro_barcode);
					}
				}
			});

			let total_affiliasi 		= $(".affiliasi"),
				total_semua_affiliasi	= 0;

			$.each(total_affiliasi, function(i, v) {
				total_semua_affiliasi += parseInt($(this).attr('data-id'));
			});

			$("#total_affiliasi").val(Math.floor(total_semua_affiliasi));

				let html 	=	$("#barang").html();

				$.ajax({
					url: '/cashier/text',
					method: 'GET',
					dataType: 'json',
					data: {
						html: html,
					},
					beforeSend: function() {
						$(".loader").css('display', 'flex');
					},
					success: function(result) {
						$(".loader").css('display', 'none');
						$.cookie('result', result);
					}
				})
			}
			}
		})
	})

	$("#number_cek").on('submit', function(e) {
		e.preventDefault();

		let number 	=	$("#member_card").val(),
			card 	=	number.split(' ').join('');

		$.ajax({
			url: '/cashier/api/cek_aff',
			method: 'GET',
			dataType: 'json',
			data: {
				card: card,
			},
			beforeSend: function() {
				$(".loader").css('display', 'flex');
			},
			success: function(result) {
				if(result.cekData != false) {
					$("#more").append(`
					<span>Nomer Kartu : ${card}</span>
					`);

					$(".loader").css('display', 'none');
					$("#modalCart").css('display', 'block');
					$("#modalCart").removeClass('fade');
					$('.modal-body').empty();
					$('.modal-body').append(`
						<div id="member-data">
							<span style="font-size: 16px;">Nama &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp; &nbsp; : &nbsp; &nbsp; &nbsp; ${result.nama}</span><br>
							<span style="font-size: 16px;" id="affiliasi_value" data-value="${result.total_affiliasi}">Jumlah Saldo Affiliasi &nbsp; &nbsp; &nbsp;&nbsp; : &nbsp; &nbsp; &nbsp; Rp. ${rupiah(result.total_affiliasi)}</span><br>
							<span style="font-size: 16px;" id="saldo_value" data-value="${result.saldo}">Jumlah Saldo Deposit &nbsp; &nbsp; &nbsp;&nbsp; : &nbsp; &nbsp; &nbsp; Rp. ${rupiah(result.saldo)}</span><br><br>
						</div>
						<input type="radio" name="cek" value="affiliasi"> <span style="font-size: 16px;">Pembayaran Menggunakan Affiliasi</span><br><span style="margin-left: 17px; font-weight: bold; color: red;">Saldo minimal Rp. 20.000</span><br><br>
						<input type="radio" name="cek" value="saldo"> <span style="font-size: 16px;">Pembayaran Menggunakan Saldo Deposit</span><br><span style="margin-left: 17px; font-weight: bold; color: red;">Saldo minimal Rp. 20.000</span><br><br>
						<input type="radio" name="cek" value="cash" checked=""> <span style="font-size: 16px;">Pembayaran Menggunakan Cash</span>
						<select class='form-control' id="type_member">
							<option value="tunai">Tunai</option>
							<option value="debet">Debet</option>
						</select>
						<input type="number" class="form-control" style="margin-top: 10px;" placeholder="Isi Jika Melakukan Pembayaran Menggunakan Cash" id="cash">
					`);

					$.cookie('member', [result.nama, result.total_affiliasi]);
				} else {
					alert(result.warning);
				}
			}
		});
	})

	$("button[data-dismiss=modal]").on('click', function() {
		$("#modalCart").css('display', 'none');
		$("#modalCart").addClass('fade');
	})

	$("#pembayaran").on('click', function(e) {
		e.preventDefault();

		let type 						=	$('input[name=cek]:checked').val(),
			cash 							=	$("#cash").val(),
			aff 							=	$("#affiliasi_value").attr('data-value'),
			saldo 						=	$("#saldo_value").attr('data-value'),
			total   					= $("#total-semua").val(),
			id 								=	$("#id").val(),
			number 						=	$("#member_card").val(),
			card 							=	number.split(' ').join(''),
			bonus 						=	$("#barang-bonus").val(),
			potongan 					=	$("#potongan").val(),
			total_affiliasi 	=	$("#total_affiliasi").val();

		if (type == "cash") {
			if (cash == '') {
				alert('Isi form yang tersedia');
			} else if (parseInt(total) > parseInt(cash)) {
				alert('Total belanja lebih besar daripada pembayaran');
			} else {
				$.ajax({
					url: '/cashier/api/pembayaran',
					method: 'GET',
					dataType: 'json',
					data: {
						product_id: id,
						total: total,
						pembayaran: cash,
						member_card: card,
						barang_bonus: bonus,
						total_potongan: potongan,
						type: $("#type_member").val(),
						total_affiliasi: total_affiliasi
					},
					beforeSend: function() {
						$(".loader-animation").css('display', 'flex');
					},
					success: function(result) {

						$("#total_semua_struk_dibayar").html('Rp. ' + rupiah(cash));
						$("#total_semua_struk_kembalian").html('Rp. ' + rupiah(result));

						$(".loader").css('display', 'none');
						$.cookie('total', cash);
						alert('Pembayaran berhasil. Total Kembalian Rp. ' + rupiah(result));

						$("#modalCart").css('display', 'none');
						$("#modalCart").addClass('fade');

						var print_window = window,
					    print_document = $('#listPenjualan').clone();
					    print_window.document.open();
					    print_window.document.write(print_document.html());
					    print_window.document.close();
						print_window.print();
					    print_window.close();
						print_window.location.reload();
					}
				})
			}
		} else if (type == "saldo") {
			if (parseInt(saldo) < 20000) {
				alert('Saldo minimal Rp. 20.000');
			} else if (parseInt(total) > parseInt(saldo)) {
				alert('Total belanja lebih besar daripada Saldo Deposit');
			} else {
				$.ajax({
					url: '/cashier/api/pembayaran',
					method: 'GET',
					dataType: 'json',
					data: {
						product_id: id,
						total: total,
						pembayaran: total,
						member_card: card,
						type: 'saldo',
						barang_bonus: bonus,
						total_potongan: potongan,
						total_affiliasi: total_affiliasi
					},
					beforeSend: function() {
						$(".loader-animation").css('display', 'flex');
					},
					success: function(result) {
						$("#total_semua_struk_dibayar").html('Rp. ' + rupiah(total));
						$("#total_semua_struk_kembalian").html('Rp. ' + rupiah(result));

						$(".loader").css('display', 'none');

						alert('Pembayaran berhasil. Total Kembalian Rp. ' + rupiah(result));

						$("#modalCart").css('display', 'none');
						$("#modalCart").addClass('fade');

						var print_window = window,
					    print_document = $('#listPenjualan').clone();
					    print_window.document.open();
					    print_window.document.write(print_document.html());
					    print_window.document.close();
						print_window.print();
					    print_window.close();
						print_window.location.reload();;
					}
				})
			}
		} else {
			if (parseInt(aff) < 20000) {
				alert('Saldo minimal Rp. 20.000');
			} else if (parseInt(total) > parseInt(aff)) {
				alert('Total belanja lebih besar daripada affiliasi');
			} else {
				$.ajax({
					url: '/cashier/api/pembayaran',
					method: 'GET',
					dataType: 'json',
					data: {
						product_id: id,
						total: total,
						pembayaran: total,
						member_card: card,
						type: 'aff',
						barang_bonus: bonus,
						total_potongan: potongan,
						type: 'affiliasi',
						total_affiliasi: total_affiliasi
					},
					beforeSend: function() {
						$(".loader-animation").css('display', 'flex');
					},
					success: function(result) {
						$("#total_semua_struk_dibayar").html('Rp. ' + rupiah(total));
						$("#total_semua_struk_kembalian").html('Rp. ' + rupiah(result));

						$(".loader").css('display', 'none');

						alert('Pembayaran berhasil. Total Kembalian Rp. ' + rupiah(result));

						$("#modalCart").css('display', 'none');
						$("#modalCart").addClass('fade');


						var print_window = window,
					    print_document = $('#listPenjualan').clone();
					    print_window.document.open();
					    print_window.document.write(print_document.html());
					    print_window.document.close();
							print_window.print();
					    print_window.close();
							print_window.location.reload();;
					}
				})
			}
		}
	});
});
