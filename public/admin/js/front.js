$(document).ready(function() {
	function rupiah(bilangan) {
		let 	reverse 	= bilangan.toString().split('').reverse().join(''),
				ribuan 		= reverse.match(/\d{1,3}/g);
				ribuan		= ribuan.join('.').split('').reverse().join('');

		return ribuan;
	}

	$.cookie('update-data', '1', { path: '/cashier' }); // => true

	setInterval(function(){
		let harga 	=	$.cookie('harga'),
			status 	=	$.cookie('update-data'),
			result 	=	$.cookie('result'),
			data 	=   $.cookie('member'),
			total 	=   $.cookie('total');
			
			if (data != undefined) {
				member	=	[data.split(",")];
			}

		if (harga != undefined) {
			$("#total").html('Total Harga Rp. ' + rupiah(harga));
		} else {
			$("#total").html('Total Harga Rp. 0');
		}

		if (status != 0) {
			if (result != undefined) {
				$("#barang").load(result);
			} else {
				$("#barang").empty();
			}
		}

		if (data != undefined) {
			$("#member").html(`
			<h5>Nama &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            	: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ${member[0][0]}</h5>
    		<h5>Saldo Affiliasi &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : 
    			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Rp. ${rupiah(member[0][1])}</h5>
    			<br>
    			<hr>
    			<br>
			`);
		} else {
			$("#member").empty();
		}
		console.log(total);
		if (total != undefined) {
			$("#pembayaran").html(`Total Dibayar	: 	Rp. ${rupiah(total)}`);
			$("#kembalian").html(`Total Kembalian	: 	Rp. ${rupiah(harga - total)}`);
		}
	}, 1000);
});