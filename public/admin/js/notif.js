$(document).ready(function() {
	setInterval(function() {
		$.ajax({
			url: '/usahakumart/request',
			method: 'GET',
			success: function(result) {
				$("#notifyBadge").html(result);
			}
		});

		$.ajax({
			url: '/usahakumart/requestDeposit',
			method: 'GET',
			success: function(result) {
				$("#notifyBadgeDeposit").html(result);
			}
		});

		$.ajax({
			url: '/usahakumart/requestKartu',
			method: 'GET',
			success: function(result) {
				$("#notifyBadgeKartu").html(result);
			}
		});

		$.ajax({
			url: '/usahakumart/requestDepo',
			method: 'GET',
			success: function(result) {
				$("#notifyBadgeDepo").html(result);
			}
		});
	}, 1000);
});