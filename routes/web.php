<?php

/*Route::get('tes', function() {
	echo DNS1D::getBarcodeSVG("4445645656", "PHARMA2T",3,33);
	echo DNS1D::getBarcodeHTML("4445645656", "PHARMA2T",3,33);
	echo '<img src="' . DNS1D::getBarcodePNG("4", "C39+",3,33) . '" alt="barcode"   />';
	echo DNS1D::getBarcodePNGPath("4445645656", "PHARMA2T",3,33);
	echo '<img src="data:image/png;base64,' . DNS1D::getBarcodePNG("4445645656", "C39+",1,33) . '" alt="barcode"/>';
});*/

Route::get('/fix/{option}', 'FixController@index');

Route::group(['prefix' => '/usahakumart-administrator'], function(){
	Route::group(['as' => 'login'], function() {
		Route::get('/login', 'AuthController@login');
		Route::post('/login', 'AuthController@postLogin');
		Route::get('/logout', 'AuthController@logout');
	});
});

// Live Search
Route::get('/search/liveSearch', 'SearchController@liveSearch');

Route::group(['middleware' => ['auth', 'level']], function() {
	Route::group(['prefix' => '/usahakumart'], function() {
		Route::get('/tambah-toko', 'AdminController@tambahToko');
		/*Route::get('/tambah-saldo-toko', 'AdminController@tambahSaldoToko');
		Route::post('/tambah-saldo-toko', 'AdminController@tambahSaldo');*/
		Route::post('/tambah-toko', 'AdminController@postTambahToko');
		Route::get('/kategori-toko', 'AdminController@kategoriToko');
		Route::get('/hapus-kategori/{id}', 'AdminController@hapusKategoriToko');
		Route::get('/edit-kategori/{id}', 'AdminController@editKategoriToko');
		Route::post('/edit-kategori', 'AdminController@updateKategoriToko');
		Route::post('/kategori-toko', 'AdminController@tambahKategoriToko');
		Route::get('/tipe-toko', 'AdminController@tipeToko');
		Route::get('/hapus-tipe/{id}', 'AdminController@hapusTipeToko');
		Route::get('/edit-tipe/{id}', 'AdminController@editTipeToko');
		Route::post('/edit-tipe', 'AdminController@updateTipeToko');
		Route::post('/tipe-toko', 'AdminController@tambahTipeToko');
		Route::get('/nonaktif-toko', 'AdminController@nonaktif');
		Route::get('/aktif-toko', 'AdminController@aktif');
		Route::get('/daftar-toko', 'AdminController@daftarToko');
		Route::get('/durasi-toko', 'AdminController@durasiToko');
		Route::get('/edit-toko/{nama}', 'AdminController@editToko');
		Route::post('/update-toko/{id}', 'AdminController@updateToko');
		Route::get('/hapus-toko/{nama}', 'AdminController@hapusToko');
		Route::get('/allow-toko/{id}', 'AdminController@allow');
		Route::get('/deny-toko/{id}', 'AdminController@deny');
		Route::get('/alamat-toko', 'AdminController@alamatToko');
		Route::get('/tambah-alamat', 'AdminController@tambahAlamat');
		Route::get('/hapus-alamat', 'AdminController@hapusAlamat');
		Route::get('/update-alamat', 'AdminController@updateAlamat');

		Route::get('/pengaturan', 'PengaturanController@index');
		Route::post('/pengaturan/{id}/{type}', 'PengaturanController@update');

		Route::get('/daftar-member', 'AdminController@daftarMember');
		Route::get('/nonaktif-member', 'AdminController@Membernonaktif');
		Route::get('/aktif-member', 'AdminController@Memberaktif');

		Route::get('/dashboard', 'AdminController@index');
		Route::get('/detail-toko', 'AdminController@historyToko');
		Route::get('/detail-toko/{id}/produk-terpopuler', 'AdminController@populerAll');
		Route::get('/detail-toko/{id}/gudang', 'AdminController@gudang');
		Route::get('/detail-toko/{id}/{jenis}/riwayat-pembelian', 'AdminController@riwayatPembelian');
		Route::get('/detail-toko/{id}/{jenis}/print', 'AdminController@printLaporan');
		Route::get('/toko-search', 'AdminController@search');

		Route::get('/payout', 'AffiliasiController@list');
		Route::get('/payout/konfirm', 'AffiliasiController@konfirm');

		Route::get('/deposit', 'AdminController@deposit');
		Route::get('/deposit/konfirm/{id}', 'AdminController@konfirmDeposit');

		Route::get('/stok-kartu', 'AdminController@stokKartu');
		Route::post('/stok-kartu/tambah', 'AdminController@stokKartuTambah');
		Route::get('/stok-kartu/hapus', 'AdminController@stokKartuHapus');
		Route::get('/kartu', 'AdminController@kartu');
		Route::get('/kartu/konfirm/{id}', 'AdminController@konfirmKartu');

		Route::get('/request', 'CashierController@countWithdraw');
		Route::get('/requestDeposit', 'CashierController@countDeposit');
		Route::get('/requestKartu', 'CashierController@countKartu');

		Route::get('/request-deposit-user', 'CashierController@depositUser');
		Route::get('/request-deposit-user/{type}', 'CashierController@depositUserKonf');
		Route::get('/requestDepo', 'CashierController@countDepo');

		Route::get('/hapus-member/{id}', 'MemberController@hapus');
		Route::get('/upgrade-member', 'AdminController@upgrade');
		Route::get('/upgrade-member/search', 'AdminController@searchUpgrade');
		Route::get('/upgrade-member/{id}', 'AdminController@upgradeAkun');
		Route::get('/upgrade-member/{id}/premium', 'AdminController@upgradeAkunPremium');

		Route::get('/cetak-kartu', 'KartuController@cetak');
		Route::post('/cetak-kartu', 'KartuController@cetakKartu');
	});

	Route::group(['prefix' => '/api-dashboard'], function() {
		Route::get('/anggota-baru', 'API\DashboardController@anggota');
		Route::get('/total-anggota', 'API\DashboardController@totalAnggota');
		Route::get('/kartu', 'API\DashboardController@kartu');
		Route::get('/toko', 'API\DashboardController@toko');
		Route::get('/tidak-aktif', 'API\DashboardController@tidakAktif');
		Route::get('/aktif', 'API\DashboardController@aktif');
	});
});

Route::post('/cashier/beli', 'BuyController@index');

Route::group(['middleware' => ['auth']], function() {
	Route::group(['prefix' => '/cashier'], function() {
		Route::get('/login', 'AuthController@loginCashier');
		Route::post('/login', 'AuthController@loginCashierPost');

		// Pengaturan Toko
		Route::group(['middleware' => ['iskasir:admin']], function () {
			Route::get('/pengaturan', 'PengaturanController@indexCashier');
			Route::post('/pengaturan/{id}', 'PengaturanController@updateToko');	
		});

		//Notifikasi Barang
		Route::group(['prefix' => '/notifikasi-barang', 'middleware' => ['iskasir:admin']], function() {
			Route::get('/', 'NotifikasiBarangController@index');
			Route::get('/created', 'NotifikasiBarangController@created');
			Route::post('/created', 'NotifikasiBarangController@store');
			Route::get('/{id}/edit', 'NotifikasiBarangController@edit');
			Route::post('/update', 'NotifikasiBarangController@update');
			Route::get('/{id}/delete', 'NotifikasiBarangController@delete');
			Route::get('/count', 'NotifikasiBarangController@count');
			Route::get('/alert', 'NotifikasiBarangController@alert');
		});

		//Daftar Rak
		Route::group(['prefix' => '/daftar-rak', 'middleware' => ['iskasir:adminTukang']], function() {
			Route::get('/', 'DaftarRakController@index');
			Route::get('/search-rak', 'DaftarRakController@search');
			Route::get('/update/{id}', 'DaftarRakController@update');
			Route::post('/update/{id}', 'DaftarRakController@updatePost');
		});

		//User Toko
		Route::group(['prefix' => '/user-toko', 'middleware' => ['iskasir:admin']], function() {
			Route::get('/', 'UserTokoController@index');
			Route::get('/create', 'UserTokoController@create');
			Route::post('/create', 'UserTokoController@store');
			Route::get('/edit/{id}', 'UserTokoController@edit');
			Route::post('/update/{id}', 'UserTokoController@update');
			Route::get('/delete/{id}', 'UserTokoController@delete');
			Route::get('/aktivitas/{id}', 'UserTokoController@aktivitas');
			Route::get('/status/{type}/{id}', 'UserTokoController@status');
		});

		//Pola Perlakuan Harga
		Route::group(['prefix' => '/pola-perilaku-harga', 'middleware' => ['iskasir:admin']], function() {
			Route::get('/', 'PolaPerilakuHargaController@index');
			Route::get('/create', 'PolaPerilakuHargaController@create');
			Route::post('/create', 'PolaPerilakuHargaController@store');
			Route::get('/edit/{id}', 'PolaPerilakuHargaController@edit');
			Route::get('/delete/{id}', 'PolaPerilakuHargaController@delete');
			Route::get('/aktif/{id}', 'PolaPerilakuHargaController@aktif');
			Route::get('/nonaktif/{id}', 'PolaPerilakuHargaController@nonaktif');

		});
		Route::get('/pola-perilaku-harga/api', 'PolaPerilakuHargaController@api');
		Route::get('/pola-perilaku-harga/api/count', 'PolaPerilakuHargaController@apiCount');

		Route::group(['prefix' => '/laporan'], function() {

			//Affiliasi
			Route::group(['prefix' => '/affiliasi', 'middleware' => ['iskasir:admin']], function()	{
				Route::get('/', 'LaporanAffiliasiController@index');
				Route::get('/print', 'LaporanAffiliasiController@affPrint');
				Route::get('/api', 'LaporanAffiliasiController@api');
				Route::get('/api/search', 'LaporanAffiliasiController@searchApi');
			});

			//Back Office
			Route::group(['prefix' => '/back-office', 'middleware' => ['iskasir:admin']], function() {
				Route::get('/', 'LaporanKasController@backOffice');
				Route::get('/print-periode', 'LaporanKasController@printPeriode');
			});

			// Laporan Pembelian Produk
			Route::group(['prefix' => '/pembelian-produk', 'middleware' => ['iskasir:admin']], function ()	{
				Route::get('/', 'LaporanController@pembelianProduk');
				Route::get('/search', 'LaporanController@searchProduk');
				Route::get('/tambah', 'LaporanController@pembelianProdukTambah');
				Route::post('/store', 'LaporanController@pembelianProdukStore');
				Route::post('/delete/{id}', 'LaporanController@deleteProduct');
				Route::get('/edit/{id}', 'LaporanController@editProduk');
				Route::post('/update/{id}', 'LaporanController@updateProduk');
				Route::get('/print', 'LaporanController@pembelianProdukPrint');
				Route::get('/print/supplier', 'LaporanController@pembelianProdukPrintSupplier');
				Route::post('/upload', 'LaporanController@upload');

				Route::get('/api/search', 'LaporanController@apiSearch');
				Route::get('/api/searchSingle', 'LaporanController@apiSearchSingle');

				Route::get('/supplier', 'LaporanController@isiSupplier');
				Route::get('/no-transaksi', 'LaporanController@noTransaksi');
				Route::get('/supplier-transaksi', 'LaporanController@supplierTransaksi');

				Route::post('/update/temp/{no}', 'LaporanController@updateProdukTemp');
			});

			// Laporan Stok Kartu
			Route::group(['middleware' => ['iskasir:admin']], function() {
				Route::get('/stok-kartu', 'LaporanController@stokKartu');
				Route::get('/stok-kartu/print', 'LaporanController@stokKartuPrint');
			});

			// Laporan Kas
			Route::group(['prefix' => '/kas', 'middleware' => ['iskasir:admin']], function () {
				Route::get('/', 'LaporanKasController@index');
				/*Route::get('/search', 'LaporanController@searchKas');*/
				Route::get('/tambah', 'LaporanKasController@kasTambah');
				Route::post('/store', 'LaporanKasController@kasStore');
				Route::post('/delete/{id}', 'LaporanKasController@deleteKas');
				Route::get('/edit/{id}', 'LaporanKasController@editKas');
				Route::post('/update/{id}', 'LaporanKasController@updateKas');
				Route::get('/print', 'LaporanKasController@kasPrint');
				Route::get('/search', 'LaporanKasController@kasSearch');
				Route::get('/konfirmasi/{id}', 'LaporanKasController@kasKonfirmasi');
			});

			// Laporan Penjualan
			Route::group(['middleware' => ['iskasir:adminKasir']], function() {
				Route::get('/penjualan', 'LaporanController@jualanLaporan');
				Route::get('/penjualan/api', 'LaporanController@jualanLaporanApi');
				Route::get('/penjualan?p=harian', 'LaporanController@jualanHari');
				Route::get('/penjualan?p=terbaik', 'LaporanController@jualanBest');
				Route::get('/penjualan/search', 'LaporanController@searchJual');
				Route::get('/penjualan/print', 'LaporanController@jualPrint');
				Route::get('/penjualan/print-periode', 'LaporanController@jualPrintPeriode');
				Route::get('/penjualan/print-daftar', 'LaporanController@printAction'); 
				Route::get('/penjualan/cancel-daftar', 'LaporanController@daftarPrint'); 
			});
			
			Route::group(['middleware' => ['iskasir:admin']], function () {
				// Route::get('/best-barang', 'LaporanController@bestBarang');
				// Route::get('/best-barang/print', 'LaporanController@bestPrint');
				Route::get('/total-barang-terjual', 'LaporanController@bestBarang');
			});

			Route::get('/laporan-kasir', 'LaporanController@laporanKasir');

		});

		Route::group(['prefix' => '/supplier', 'middleware' => ['iskasir:adminTukang']], function() {
			Route::get('/', 'SupplierController@index');
			Route::get('/create', 'SupplierController@create');
			Route::post('/create', 'SupplierController@store');
			Route::get('/edit/{id}', 'SupplierController@edit');
			Route::post('/update/{id}', 'SupplierController@update');
			Route::get('/delete/{id}', 'SupplierController@delete');
			Route::get('/search', 'SupplierController@search');
		});

		Route::get('/api/tambah-barang', 'BarangController@apiBarang');

		Route::get('/deposit-saldo', 'CashierController@deposit');
		Route::post('/deposit-saldo/konfirmasi', 'CashierController@depositPost');

		Route::get('/get', 'CashierController@get');

		Route::get('/dashboard', 'CashierController@dashboard');

		Route::group(['middleware' => ['iskasir:adminKasir']], function () {
			Route::get('/penjualan', 'CashierController@jualan');
			Route::get('/penjualan/{id}/cancel', 'CashierController@jualanCancel');
			Route::get('/penjualan/{id}/repeat', 'CashierController@jualanRepeat');
		});

		Route::group(['middleware' => ['iskasir:adminTukang']], function () {
			Route::get('/tambah-barang', 'BarangController@tambah');
			Route::post('/tambah-barang', 'BarangController@tambahBarang');
			Route::get('/tambah-category', 'CategoryController@tambah');
			Route::post('/tambah-category', 'CategoryController@tambahCategory');
			Route::get('/delete-category/{id}', 'CategoryController@deleteCategory');
			Route::get('/edit-category/{id}', 'CategoryController@editCategory');
			Route::post('/update-category/{id}', 'CategoryController@updateCategory');
			Route::get('/gudang-barang', 'BarangController@gudang');
			Route::get('/gudang-barang/print', 'BarangController@gudangPrint');
			Route::get('/gudang-barang/api', 'BarangController@gudangApi');
			Route::get('/edit-barang', 'BarangController@edit');
			Route::post('/edit-barang', 'BarangController@update');
			Route::get('/hapus-barang', 'BarangController@hapus');
			Route::get('/search-barang', 'BarangController@search');
			Route::get('/gudang-barang/duplikat', 'BarangController@gudangDuplikat');
		});
		

		Route::get('/', 'CashierController@cashier')->middleware('iskasir:adminKasir');
		Route::get('/text', 'CashierController@text');
		Route::get('/app', 'CashierController@app');

		// Stok Kartu
		Route::group(['middleware' => ['iskasir:admin']], function () {
			Route::get('/stok-kartu', 'CashierController@stok');
			Route::post('/stok-kartu', 'CashierController@poststok');
			//Route::get('/hapus-stok', 'CashierController@hapusstok');
			Route::get('/search-kartu', 'CashierController@searchstok');
		});
		

		Route::get('/riwayat-transaksi', 'CashierController@transaksi')->middleware('iskasir:admin');

		Route::get('/request-withdraw', 'CashierController@requestWithdraw');
		Route::post('/request-withdraw', 'CashierController@withdraw');

		Route::group(['prefix' => '/member', 'middleware' => ['iskasir:admin']], function() {
			Route::get('/register', 'Affiliasi\AffiliasiMemberController@register');
			Route::post('/register', 'Affiliasi\AffiliasiMemberController@postRegister');
		});

		Route::group(['prefix' => '/api'], function() {
			Route::get('/minggu', 'CashierController@minggu');
			Route::get('/bulan', 'CashierController@bulan');
			Route::get('/cek_aff', 'CashierController@cekAff');
			Route::get('/pembayaran', 'BuyController@index');
		});

		Route::get('/laporan-harian', 'CashierController@harian');
		Route::get('/laporan-bulanan', 'CashierController@bulanan');

		Route::get('/print-label', 'CashierController@print');

		// Promosi Barang
		Route::group(['middleware' => ['iskasir:admin']], function () {
			Route::get('/promosi-barang', 'CashierController@promosi');
			Route::get('/promosi-barang/tambah', 'CashierController@createPromosi');
			Route::post('/promosi-barang/tambah', 'CashierController@postPromosi');
			Route::get('/promosi-barang/hapus/{id}', 'CashierController@hapusPromosi');
			Route::get('/promosi-barang/edit/{id}', 'CashierController@editPromosi');
			Route::post('/promosi-barang/edit/{id}', 'CashierController@updatePromosi');
		});

		// Potongan Barang
		Route::group(['prefix' => '/potongan-barang', 'middleware' => ['iskasir:admin']], function() {
			Route::get('/', 'PotonganBarangController@index');
			Route::get('/created', 'PotonganBarangController@created');
			Route::post('/created', 'PotonganBarangController@store');
			Route::get('/delete/{id}', 'PotonganBarangController@delete');
			Route::get('/edit/{id}', 'PotonganBarangController@edit');
			Route::post('/update', 'PotonganBarangController@update');
		});
		Route::get('/potongan-barang/api', 'PotonganBarangController@api');
		Route::get('/potongan-barang/cek', 'PotonganBarangController@cek');

		// Promosi Toko
		Route::group(['middleware' => ['iskasir:admin']], function () {
			Route::get('/promosi-toko', 'CashierController@promosiToko');
			Route::get('/promosi-toko/tambah', 'CashierController@promosiTokoCreate');
			Route::post('/promosi-toko/tambah', 'CashierController@promosiTokoPost');
			Route::get('/promosi-toko/hapus/{id}', 'CashierController@hapusPromosiToko');
			Route::get('/promosi-toko/edit/{id}', 'CashierController@editPromosiToko');
			Route::post('/promosi-toko/edit/{id}', 'CashierController@updatePromosiToko');
		});
		
		// Notifikasi
		Route::group(['middleware' => ['iskasir:adminKasir']], function () {	
			Route::get('/notifikasi', 'CashierController@showNotifikasi');
			Route::get('/notifikasi/confirm/{id}', 'CashierController@confirmNotifikasi');
			Route::get('/api/notifikasi', 'CashierController@showBarang');
			Route::get('/ajax/notify-count', 'CashierController@countNotifikasi');
		});

		// Stok Opname
		Route::group(['middleware' => ['iskasir:admin']], function () {
			Route::get('/stok-opname', 'StokOpnameController@index');
			Route::post('/stok-opname/update', 'StokOpnameController@updateOpname');
			Route::get('/stok-opname/opname', 'StokOpnameController@printSelisih');
			Route::post('/stok-opname/stok-update', 'StokOpnameController@updateStok');
			Route::post('/stok-opname/stok-adjust', 'StokOpnameController@stokAdjust');
		});
		
	});
});

Route::group(['prefix' => '/api-destination'], function() {
	Route::get('/kota', 'API\DestinationController@kota');
	Route::get('/kecamatan', 'API\DestinationController@kecamatan');
});

Route::get('/', 'PagesController@index');
Route::get('/api/provinsi', 'PagesController@apiProvinsi');
Route::get('/api/kota', 'PagesController@apiKota');
Route::get('/api/searchToko', 'PagesController@apiSearch');

Route::get('/ajax-kota', 'PagesController@prov');
Route::get('/ajax-kota-barang', 'PagesController@provBarang');

Route::get('/ajax-kecamatan', 'PagesController@kecamatan');
Route::get('/ajax-kecamatan-barang', 'PagesController@kotaBarang');

Route::get('/ajax-final-barang', 'PagesController@kecamatanBarang');

Route::group(['prefix' => '/toko-usahakumart'], function () {
	Route::get('/{id}', 'PagesController@showToko');
	Route::get('/login/{id}', 'PagesController@login');
	Route::post('/login/{id}', 'PagesController@postLogin');
	Route::get('/logout/{id}', 'PagesController@logout');

	Route::get('/search/{id}', 'PagesController@search');

	Route::post('/cart/{id}', 'CartOfllineController@store');
	Route::post('/jumlah/{id}', 'CartOfllineController@update');
	Route::post('/cart/delete/{id}', 'CartOfllineController@delete');

	Route::get('/pembelian/{id}', 'PagesController@pembelian');
	Route::get('/search/{id}', 'PagesController@search');

	Route::post('/transaksi/{id}', 'PagesController@transaksi');
	Route::get('/transaksi/sukses/{idtransaksi}', 'PagesController@transaksiSukses');
	Route::post('/transaksi/confirm/{idtransaksi}', 'PagesController@transaksiConfirm');

	Route::get('/daftar-order/{id}', 'PagesController@order');
});

Route::get('/login', 'PagesController@loginUser');
Route::post('/login', 'PagesController@postLoginUser');

Route::group(['middleware' => ['member']], function() {
	Route::get('/member', 'AffiliasiController@index');
	Route::get('/history', 'AffiliasiController@history');
	Route::get('/history/{id}', 'AffiliasiController@history');
	Route::post('/saldo-deposit/tambah', 'SaldoDepositController@store');
	Route::get('/logout', 'PagesController@logout');

	Route::get('/profile/pengaturan', 'AffiliasiController@pengaturan');
	Route::post('/profile/pengaturan/update', 'AffiliasiController@pengaturanUpdate');
	Route::get('/profile/password/change', 'AffiliasiController@passChange');
	Route::post('/profile/password/update', 'AffiliasiController@passUpdate');

	Route::get('/belanja', function() {
		return view('usahakumart.front');
	});

	Route::group(['prefix' => '/affiliasi'], function() {
		Route::get('/{id}', 'AffiliasiController@index');
		/*Route::post('/{id}/payout', 'AffiliasiController@payout');*/
	});
});
