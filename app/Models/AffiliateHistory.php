<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AffiliateHistory extends Model
{
    protected $table 			=	'affiliasi_history';

    protected $guarded 			=	['id'];
}
