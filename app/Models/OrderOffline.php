<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderOffline extends Model
{
    protected $table 	=	'order_offline';

    protected $guarded	=	['id'];
}
