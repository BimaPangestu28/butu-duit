<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AffiliasiTotal extends Model
{
    protected $table 		=	'affiliasi_total';

    protected $guarded 		=	['id'];
}
