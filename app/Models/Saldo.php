<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Saldo extends Model
{
    protected $table 	=	'saldo_toko';

    protected $guarded	=	['id'];
}
