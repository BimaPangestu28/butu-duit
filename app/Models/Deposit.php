<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Deposit extends Model
{
    protected $table 		=	'konfirmasi_deposit';

    protected $guarded 		=	['id'];
}
