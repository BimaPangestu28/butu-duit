<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PromoToko extends Model
{
    protected $table 	=	'promo_toko';

    protected $guarded 	=	['id'];
}
