<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NumberCard extends Model
{
    protected $table 	=	'number_card';

    protected $guarded	=	['id'];
}
