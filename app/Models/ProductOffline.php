<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductOffline extends Model
{
    protected $table 	=	'product_offline';

    protected $guarded	=	['id'];
}
