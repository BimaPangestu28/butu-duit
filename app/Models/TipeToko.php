<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TipeToko extends Model
{
    protected $table 		=	'tipe_toko';

    protected $guarded 		=	['id'];
}
