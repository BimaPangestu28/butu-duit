<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AffiliasiMember extends Model
{
    protected $table 	=	'affiliasi_member';

    protected $guarded	=	['id'];
}
