<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TokoOffline extends Model
{
    protected $table 		=	'toko_offline';

    protected $guarded 		=	['id'];
}
