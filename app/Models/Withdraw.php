<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Withdraw extends Model
{
    protected $table 		=	'withdraw_toko';

    protected $guarded 		=	['id'];
}
