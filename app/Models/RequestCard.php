<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RequestCard extends Model
{
    protected $table 			=	'request_card';

    protected $guarded 			=	['id'];
}
