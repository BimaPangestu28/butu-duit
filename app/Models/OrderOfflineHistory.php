<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderOfflineHistory extends Model
{
    protected $table 	=	'order_offline_history';

    protected $guarded	=	['id'];
}
