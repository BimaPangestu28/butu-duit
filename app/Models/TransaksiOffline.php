<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TransaksiOffline extends Model
{
    protected $table 		=	'transaksi_offline';

    protected $guarded 		=	['id'];
}
