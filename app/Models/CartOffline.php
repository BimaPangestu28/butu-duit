<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CartOffline extends Model
{
    protected $table 			=	'cart_offline';

    protected $guarded 			=	['id'];
}
