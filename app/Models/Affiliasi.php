<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Affiliasi extends Model
{
    protected $table 		=	'affiliasi';

    protected $guarded 		=	['id'];
}
