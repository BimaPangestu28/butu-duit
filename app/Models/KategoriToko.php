<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class KategoriToko extends Model
{
    protected $table 		=	'kategori_toko';

    protected $guarded 		=	['id'];
}
