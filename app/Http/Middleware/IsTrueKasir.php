<?php

namespace App\Http\Middleware;

use Closure;

class IsTrueKasir
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $level)
    {
        if ($level == 'admin') {
            if(session('user-toko')->level == 1) {
                return $next($request);
            } else {
                return redirect('/cashier/dashboard')->with('alert', 'Permission denied!');
            }
        }
        else if ($level == 'kasir') {
            if(session('user-toko')->level == 3) {
                return $next($request);
            } else {
                return redirect('/cashier/dashboard')->with('alert', 'Permission denied!');
            }    
        }
        else if ($level == 'tukang') {
            if(session('user-toko')->level == 2) {
                return $next($request);
            } else {
                return redirect('/cashier/dashboard')->with('alert', 'Permission denied!');
            }    
        }
        else if ($level == 'adminKasir') {
            if(session('user-toko')->level == 1 || session('user-toko')->level == 3) {
                return $next($request);
            } else {
                return redirect('/cashier/dashboard')->with('alert', 'Permission denied!');
            }    
        }
        else if ($level == 'adminTukang') {
            if(session('user-toko')->level == 1 || session('user-toko')->level == 2) {
                return $next($request);
            } else {
                return redirect('/cashier/dashboard')->with('alert', 'Permission denied!');
            }
        }
    }
}
