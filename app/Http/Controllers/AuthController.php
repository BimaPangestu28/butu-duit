<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\UserToko;
use App\LogUser as Log;
use App\Models\TokoOffline as Toko;

class AuthController extends Controller
{
    function login(Request $request) {
    	return view('login');
    }

    function postLogin(Request $request) {
    	$this->validate($request, [
    		'username'	=>	'required',
    		'password'	=>	'required'
    	]);

    	if (Auth::attempt(['username' => $request->username, 'password' => $request->password, 'aktif' => 1])) {
            if (Auth::attempt(['username' => $request->username, 'password' => $request->password, 'aktif' => 1, 'level' => 1])) {
                return redirect('/usahakumart/dashboard')->with('alert', 'Anda Berhasil Login');
            }
            elseif (Auth::attempt(['username' => $request->username, 'password' => $request->password, 'aktif' => 1, 'level' => 2])) {
    		    return redirect('/cashier/login')->with('alert', 'Anda Berhasil Login, Silahkan Login Dengan Username dan Password Karyawan Anda');

            }

            session()->put('id_user', Auth::user()->id);
    	} else {
    		return redirect()->back()->with('alert', 'Username atau Password Salah');
    	}
    }

    function logout() {
        if (session('user-tokos')) {
            Log::create(['id_user' => session('user-toko')->id, 'type' => 'LOG OUT']);
        }

        Auth::logout();

    	return redirect('/usahakumart-administrator/login');
    }

    function loginCashier() {
        return view('usahakumart.users.login');
    }

    function loginCashierPost(Request $request) {
        $username       =   $request->username;
        $password       =   $request->password;

        $fcek           =   UserToko::where('username', $username)->first();

        if ($fcek != null) {
               if (\Hash::check($password, $fcek->password)) {
                    session()->put('user-toko', $fcek);

                    if (session('user-toko')->status == 1) {
                      Log::create(['id_user' => session('user-toko')->id, 'type' => 'LOG IN']);

                      return redirect('/cashier/dashboard');
                    } else {
                      return redirect()->back()->with('alert', 'Akun telah dinonaktifkan');    
                    }
               } else {
                   return redirect()->back()->with('alert', 'Password salah');
               }
        } else {
            return redirect()->back()->with('alert', 'Username salah');
        }
    }
}
