<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\TokoOffline;
use App\Models\Provinsi;
use App\Models\Kota;
use App\Models\Kecamatan;
use App\Models\KategoriToko;

use Auth;
use Hash;

class PengaturanController extends Controller
{
    function index() {
    	$data 	=	TokoOffline::where('id', Auth::user()->id)->first();

    	return view('administrator.pages.pengaturan.index', compact('data'));
    }

    function indexCashier() {

    	$data 		=	TokoOffline::where('toko_offline.id', Auth::user()->id)
                        ->leftJoin('provinsi', 'provinsi.id', 'toko_offline.id_provinsi')
                        ->leftJoin('kecamatan', 'kecamatan.id', 'toko_offline.id_kecamatan')
                        ->leftJoin('kota', 'kota.id', 'toko_offline.id_kota')
                        ->leftJoin('kategori_toko', 'kategori_toko.id', 'toko_offline.id_kategori')
                        ->select('toko_offline.*', 'provinsi.*', 'kecamatan.*', 'kota.*', 'provinsi.id as provinsi_id', 'kecamatan.id as kecamatan_id', 'kota.id as kota_id', 'toko_offline.id as id', 'kota.id_provinsi as kota_id_provinsi', 'kecamatan.id_kota as kecamatan_id_kota', 'toko_offline.id_provinsi as id_provinsi', 'kategori_toko.id as kategori_id', 'kategori_toko.*')
                        ->first();

        $provinsi   =   Provinsi::orderBy('provinsi', 'ASC')->get();

        $prov       =   $provinsi->first();

        $kota       =   Kota::where('id_provinsi', $prov->id)->orderBy('kota', 'ASC')->get();

        if ($kota->count() > 0) {
            $kot        =   $kota->first();

            $kecamatan  =   Kecamatan::where('id_kota', $kot->id)->orderBy('kecamatan', 'ASC')->get();
        } else {
            $kecamatan  =   [];
        }

        $kategori       =   KategoriToko::all();


    	return view('usahakumart.pengaturan.index', compact('data', 'provinsi', 'kota', 'kecamatan', 'kategori'));
    }

    function update($id, $type, Request $request) {
    	$data 	=	TokoOffline::where('id', Auth::user()->id)->first();

      if ($type == 'username') {
        $this->validate($request, [
               'username' => 'required|unique:toko_offline|min:8|max:16',
               'password' => 'required'
        ]);

        if (Hash::check($request->password, $data->password)) {
          TokoOffline::where('id', $id)->update($request->except('_token', 'password'));

          return redirect()->back()->with('alert', 'Pengaturan berhasil diupdate');
        } else {
          return redirect()->back()->with('alert', 'Password Salah');
        }
      }

      if ($type == 'password') {
        $this->validate($request, [
               'password'        => 'required|min:8|max:16',
               'repeat_password' => 'same:password',
               'old_password'    => 'required'
        ]);

        if (Hash::check($request->old_password, $data->password)) {
          $request->request->add(['password' => bcrypt($request->password)]);

        	TokoOffline::where('id', $id)->update($request->except('_token', 'repeat_password', 'old_password'));

          return redirect()->back()->with('alert', 'Pengaturan berhasil diupdate');
        } else {
          return redirect()->back()->with('alert', 'Password Salah');
        }
      }
    }

    function updateToko(Request $request) {
      $data 	=	TokoOffline::where('id', Auth::user()->id)->first();
      $this->validate($request, [
          'username' => 'required|unique:toko_offline|min:8|max:16',
      ]);

      if ($request->password != '') {
          $request->request->add(['password' => bcrypt($request->password)]);
          TokoOffline::where('id', $data->id)->update($request->except(['_token']));
      } else {
          TokoOffline::where('id', $data->id)->update($request->except(['_token', 'password']));
      }

      return redirect('/cashier/pengaturan')->with('alert', 'Toko Berhasil Diupdate');
      
    }
}
