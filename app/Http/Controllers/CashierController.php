<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Response;
use Illuminate\Support\Facades\Auth;
use DB;
use PDF;
use Illuminate\Support\Facades\Storage;

use App\Jobs\ProcessPodcast;
use App\Models\ProductOffline as Product;
use App\Models\OrderOffline as Order;
use App\Models\OrderOfflineHistory as History;
use App\Models\HistoryPemasukanBarang as HistoryBarang;
use App\Models\NumberCard as Card;
use App\Models\TokoOffline as Toko;
use App\Models\AffiliasiMember as Member;
use App\Models\AffiliasiTotal as Total;
use App\Models\Promosi;
use App\Models\PromoToko;
use App\Models\TransaksiOffline as Transaksi;
use App\Models\CartOffline as Cart;
use App\Models\Saldo;
use App\Models\Withdraw;
use App\Models\Deposit;
use App\Models\RequestCard;
use App\SaldoDepositRequest;
use App\Models\SaldoUser;
use App\PemasukanBarang;
use App\LogUser as Log;

class CashierController extends Controller
{
    function get(Request $request) {
    	$result 	=	Product::where('id_toko', Auth::user()->id)
    					->where('barcode', $request->barcode)->first();

		if($request->produk != null) {
			$produk 	=	array_count_values(explode(",", $request->produk));
		} else {
			$produk 	= [];
		}

    	if (count($produk) > 0) {
			// dd($result->id);
    		if(array_key_exists($result->id, $produk)) {
				$cek 		=	$result->stok - $produk[$result->id];
				// dd($cek);
				if ($cek <= 0) {
					$return = 	'alert';
				} else {
					$return = 	$result;
				}
			} else {
				$return = $result;
			}
    	} else {
			$cek	=	$result->stok - 1;

    		if($cek < 0) {
				$return = 	'alert';
			} else {
				$return = 	$result;
			}
    	}

    	return Response::json($return);
    }

 	function dashboard() {;
 		$posts 	=   Product::where('id_toko', Auth::user()->id)
					->orderBy('terjual', 'DESC')->paginate(10);

		$toko 	=	Toko::where('id', Auth::user()->id)->first();

		return view('usahakumart.pages.dashboard', compact('posts', 'toko'));
	}

	function minggu() {
		$currentDate = \Carbon\Carbon::now();
		$agoDate 	 = \Carbon\Carbon::today()->subWeek();

		$minggu = Order::selectRaw('date(created_at) as date, SUM(total_pembayaran) as count')
					    ->whereBetween( DB::raw('date(created_at)'), [$agoDate, $currentDate] )
					    ->where('id_toko', Auth::user()->id)
					    ->orderBy('date', 'DESC')
					    ->groupBy('date')->get();

		return Response::json($minggu);
	}

	function bulan() {
		$currentDate = \Carbon\Carbon::now();
		$agoDate 	 = \Carbon\Carbon::today()->subMonth();

		$minggu = Order::selectRaw('date(created_at) as date, SUM(total_pembayaran) as count')
					    ->whereBetween( DB::raw('date(created_at)'), [$agoDate, $currentDate] )
					    ->where('id_toko', Auth::user()->id)
					    ->orderBy('date', 'DESC')
					    ->groupBy('date')->get();

		return Response::json($minggu);
	}


	function jualan() {

		$data 		= Saldo::where('id_toko', Auth::user()->id)->first();
		$last       = Order::orderBy('id', 'DESC')->first();

		$no_tran 	= str_pad($last->no_transaksi + 1, 10, '0', STR_PAD_LEFT);

		return view('usahakumart.pages.penjualan', compact('data', 'no_tran'));
	}

	function jualanCancel($id) {
		$items 		= History::where('order_id', $id)->get();

		foreach ($items as $item) {
			Product::where('id', $item->id_product)->increment('stok');
			Product::where('id', $item->id_product)->decrement('terjual');
		}

		$order 		=	Order::where('id', $id)->first();

		if ($order->id_member != null) {
			$total 	=	Total::where('id_member', $order->id_member)->first();

			$member =	Member::where('id', $order->id_member)->first();

			if ($member->premium_cash < 500000) {
				$reset 	=	$order->affiliasi / 2;
				$cash 	=	$order->affiliasi / 2;
			} else {
				$reset 	=	$order->affiliasi;
				$cash 	=	0;
			}

			Total::where('id_member', $order->id_member)->update(['total_affiliasi' => $total->total_affiliasi - $reset]);
			Member::where('id', $order->id_member)->update(['premium_cash' => $member->premium_cash - $cash]);
		}

		Log::create(['id_user' => session('user-toko')->id, 'type' => 'CANCEL TRANSAKSI']);

		History::where('order_id', $id)->delete();
		Order::where('id', $id)->delete();

		//return redirect('/cashier/penjualan/' . $id . '/repeat');
		return redirect()->back()->with('alert', 'Transaksi Berhasil Dihapus');
	}

	function jualanRepeat($id) {
		$data 		= Saldo::where('id_toko', Auth::user()->id)->first();
		$last       = Order::orderBy('id', 'DESC')->first();
		$item 		= History::join('product_offline', 'order_offline_history.id_product', 'product_offline.id')
							   ->where('order_id', $id)
							   ->select('product_offline.id as id', 'name', 'harga_jual', 'stok', 'affiliasi', 'diskon')
							   ->distinct()
							   ->get();

		$id_pro 	= '';

		foreach ($item as $i) {
			if ($id_pro == '') {
				$id_pro = $i->id;
			} else {
				$id_pro .= ',' . $i->id;
			}
		}

		$total 		=	0;

		foreach ($item as $i) {

		}

		$no_tran 	= str_pad($last->no_transaksi + 1, 10, '0', STR_PAD_LEFT);

		return view('usahakumart.pages.penjualanCancel', compact('data', 'no_tran', 'item', 'id_pro'));
	}

	function cashier() {
		return view('usahakumart.front');
	}

	function transaksi() {
		
		$order 		=	Order::where('id_toko', Auth::user()->id)->orderBy('id', 'DESC')->paginate(10);

		$pendapatan	  = 	Order::where('id_toko', Auth::user()->id)->sum('total_pembayaran');
		$pendapatHari =		Order::where('id_toko', Auth::user()->id)
									->whereDate('created_at', DB::raw('CURDATE()'))
									->sum('total_pembayaran');

		$trans 		= 	Transaksi::where('transaksi_offline.status', 1)
		                ->where('transaksi_offline.id_toko', Auth::user()->id)
		                ->paginate(8);

		return view('usahakumart/pages/riwayat', compact('order', 'trans', 'pendapatan', 'pendapatHari'));
	}

	static function today($id) {
		$result 	=	History::where('id_toko', Auth::user()->id)
						->where('id_product', $id)
						->whereDate('created_at', DB::raw('CURDATE()'))
						->count();

		return $result;
	}

	static function todayIn($id) {
		$result 	=	HistoryBarang::where('id_toko', Auth::user()->id)
						->where('id_product', $id)
						->whereDate('created_at', DB::raw('CURDATE()'))
						->get();

		$total 		=	0;

		foreach ($result as $key => $value) {
			$total += $value->pemasukan;
		}

		return $total;
	}

	static function month($id) {
		$result 	=	History::where('id_toko', Auth::user()->id)
						->where('id_product', $id)
						->whereMonth('created_at', '=', date('m'))
						->whereYear('created_at', '=', date('Y'))
						->count();

		return $result;
	}

	static function monthIn($id) {
		$result 	=	HistoryBarang::where('id_toko', Auth::user()->id)
						->where('id_product', $id)
						->whereMonth('created_at', '=', date('m'))
						->whereYear('created_at', '=', date('Y'))
						->get();

		$total 		=	0;

		foreach ($result as $key => $value) {
			$total += $value->pemasukan;
		}

		return $total;
	}

	static function monthAwal($id) {
		$result 	=	HistoryBarang::where('id_toko', Auth::user()->id)
						->where('id_product', $id)
						->whereMonth('created_at', '=', date('m'))
						->whereYear('created_at', '=', date('Y'))
						->orderBy('id', 'ASC')
						->first();

		return $result->stok_awal;
	}

	static function year($id) {
		$result 	=	History::where('id_toko', Auth::user()->id)
						->where('id_product', $id)
						->whereYear('created_at', '=', date('Y'))
						->count();

		return $result;
	}

	function text(Request $request) {
		$name =	'cookie-' . Auth::user()->id . '.txt';

		if(!empty($request->html)){
			Storage::put($name, $request->html);
		}

		return Response::json('/' . $name);
	}

	function app(Request $request) {
		$name =	'member-' . Auth::user()->id . '.txt';

		if(!empty($request->html)){
			Storage::put($name, $request->html);
		}

		return Response::json('/' . $name);
	}

	function stok() {
		
		$stok 	=	Card::orderBy('id', 'DESC')->where('id_toko', Auth::user()->id)->paginate(10);

		$sisa 	=	Card::where('id_toko', null)
						->where('status', 1)->count();

		return view('usahakumart.pages.stok', compact('stok', 'sisa'));
	}

	function poststok(Request $request) {
		$this->validate($request, [
			'jumlah_kartu'	=>	'required'
		]);

		$request->request->add([
			'id_toko'	=>	Auth::user()->id,
            'status'	=>  0
		]);

		if ($request->gambare != null) {
			$nama   =   str_slug(Auth::user()->id) . '-' . str_slug(Auth::user()->name) . '.' .
	                    $request->gambare->getClientOriginalExtension();

	    	$request->request->add([
	            'gambar'    =>  $nama,
	    	]);

	    	$request->gambare->move(public_path('gambar'), $nama);
		}

		RequestCard::create($request->except(['_token', 'gambare']));

		return redirect()->back()->with('alert', 'Request Berhasil Dikirim');
	}

	function hapusstok(Request $request) {
		Card::where('id', $request->id)->delete();

		return redirect()->back()->with('alert', 'Stok Berhasil Dihapus');
	}

	function riwayattransaksi() {
		$order      =   Order::where('id_toko', Auth::user()->id)
                        ->join('affiliasi_member', 'affiliasi_member.id', 'order_offline.id_member')
                        ->select('affiliasi_member.nama', 'order_offline.*')
                        ->orderBy('id', 'DESC')
                        ->paginate(10);

        return view('usahakumart/pages/history', compact('order'));
	}

	function searchstok(Request $request) {
		$stok 	=	Card::where('number_card', 'LIKE', '%' . $request->q . '%')
					->where('id_toko', Auth::user()->id)
					->orderBy('id', 'DESC')->paginate(10);

		return view('usahakumart.pages.stok', compact('stok'));
	}

	function harian() {
		$output = 	History::where('order_offline_history.id_toko', Auth::user()->id)
					->whereDate('order_offline_history.created_at', DB::raw('CURDATE()'))
					->join('product_offline', 'product_offline.id', 'order_offline_history.id_product')
					->get();
		$arr 	=	[];
		$omset  =	0;
		$laba 	=	0;

		foreach ($output as $key => $value) {
			if (in_array($value, $arr)) {

			} else {
				array_push($arr, $value);
			}

			$omset += $value->harga_jual;
			$laba  += ($value->keuntungan - $value->affiliasi);
		}

		$input	 = 	HistoryBarang::where('history_pemasukan_barang.id_toko', Auth::user()->id)
					->whereDate('history_pemasukan_barang.created_at', DB::raw('CURDATE()'))
					->join('product_offline', 'product_offline.id', 'history_pemasukan_barang.id_product')
					->get();
		$arrA 	 =	[];

		foreach ($input as $key => $value) {
			if (in_array($value, $arr)) {

			} else {
				array_push($arrA, $value);
			}
		}

		/*return view('laporan.harian', compact('arr', 'arrA', 'omset'));*/
		$pdf    =   PDF::loadView('laporan.harian', compact('arr', 'arrA', 'omset', 'laba'))
					->setPaper('a4', 'landscape');

        return $pdf->download('laporan_harian_' . date('d_m_y') . '.pdf');
	}

	function bulanan() {
		$output = 	History::where('order_offline_history.id_toko', Auth::user()->id)
					->join('product_offline', 'product_offline.id', 'order_offline_history.id_product')
					->whereMonth('order_offline_history.created_at', '=', date('m'))
					->whereYear('order_offline_history.created_at', '=', date('Y'))
					->get();
		$arr 	=	[];
		$omset  =	0;
		$laba 	=	0;

		foreach ($output as $key => $value) {
			if (in_array($value, $arr)) {

			} else {
				array_push($arr, $value);
			}

			$omset += $value->harga_jual;
			$laba  += ($value->keuntungan - $value->affiliasi);
		}

		$input	 = 	HistoryBarang::where('history_pemasukan_barang.id_toko', Auth::user()->id)
					->join('product_offline', 'product_offline.id', 'history_pemasukan_barang.id_product')
					->whereMonth('history_pemasukan_barang.created_at', '=', date('m'))
					->whereYear('history_pemasukan_barang.created_at', '=', date('Y'))
					->get();
		$arrA 	 =	[];

		foreach ($input as $key => $value) {
			if (in_array($value, $arr)) {

			} else {
				array_push($arrA, $value);
			}
		}

		/*return view('laporan.bulanan', compact('arr', 'arrA', 'omset'));*/
		$pdf    =   PDF::loadView('laporan.bulanan', compact('arr', 'arrA', 'omset', 'laba'))
					->setPaper('a4', 'landscape');

       return $pdf->download('laporan_bulanan_' . date('d_m_y') . '.pdf');
	}

	function gudangApi(Request $request) {
		$arr 		=	[];

		$supp 		=	PemasukanBarang::where('supplier', $request->supplier)->get();

		foreach ($supp as $key) {
			array_push($arr, $key->produk);
		}

		$product 	=	Product::where('id_toko', Auth::user()->id)
						->whereIn('name', $arr)->get();

		return $product;
	}

	function print(Request $request) {
		if ($request->id_product == null) {
			$arr 		=	[];

			$supp 		=	PemasukanBarang::where('supplier', $request->supplier)->get();

			foreach ($supp as $key) {
				array_push($arr, $key->produk);
			}

			$product 	=	Product::where('id_toko', Auth::user()->id)
							->whereIn('name', $arr)->get();
		} else {
			$arr 	=	explode(',', $request->id_product);

			$product 	=	Product::where('id_toko', Auth::user()->id)
							->whereIn('id', $arr)->get();
		}

		$count 		=	1;

		$type 		=	$request->type;

		if ($type == 0) {
			$size =	[4, 6];
		} elseif ($type == 2) {
			$size = [2, 3];
		} else {
			$size =	[2.5, 5];
		}

		$pdf    	=   PDF::loadView('laporan.label', compact('product', 'count', 'size', 'type'))
						->setPaper('a4', 'landscape');

		return $pdf->download('label_product.pdf');
		//return view('laporan.label', compact('product', 'count', 'size', 'type'));
	}

	function cekAff(Request $request) {
		$data 	=	Member::where('number_card', $request->card)
            				  ->join('affiliasi_total', 'affiliasi_total.id_member', 'affiliasi_member.id')
            					->join('member', 'member.id_member', 'affiliasi_member.id_member')
            					->join('saldo_users', 'saldo_users.id_member', 'member.id_member')
                      ->select('affiliasi_total.*', 'member.*', 'saldo_users.*', 'affiliasi_member.*', 'affiliasi_member.status as status_member')
            					->first();

		if ($data != null && $data->status_member == 1) {
			return Response::json($data);
		} else if ($data != null && $data->status_member == 0) {
      return Response::json(['cekData' => false, 'warning' => 'Akun sedang dinonaktifkan, silahkan hubungi admin']);
    } else {
			return Response::json(['cekData' => false, 'warning' => 'Member tidak terdaftar']);
		}
	}

	function promosi() {
		
		$t = Toko::where('id', Auth::user()->id)->first();

        if ($t->permission == 0) {
            return redirect('/cashier/gudang-barang')->with('alert', 'Anda Harus Mendapatkan Izin Dari Administrator Gudang, Silahkan Hubungi Administrator Gudang Untuk Mendapatkan Izin Terlebih Dahulu');
        }

        $promo = Promosi::all();

		return view('usahakumart.pages.promosi', compact('promo'));
	}

	function createPromosi() {
		
		$t = Toko::where('id', Auth::user()->id)->first();

        if ($t->permission == 0) {
            return redirect('/cashier/gudang-barang')->with('alert', 'Anda Harus Mendapatkan Izin Dari Administrator Gudang, Silahkan Hubungi Administrator Gudang Untuk Mendapatkan Izin Terlebih Dahulu');
        }

		$product 	=	Product::where('id_toko', Auth::user()->id)->orderBy('name', 'ASC')
						->where('diskon', 0)->paginate(10000);

		return view('usahakumart.pages.create_promosi', compact('product'));
	}

	function postPromosi(Request $request) {
		$t = Toko::where('id', Auth::user()->id)->first();

        if ($t->permission == 0) {
            return redirect('/cashier/gudang-barang')->with('alert', 'Anda Harus Mendapatkan Izin Dari Administrator Gudang, Silahkan Hubungi Administrator Gudang Untuk Mendapatkan Izin Terlebih Dahulu');
        }

        $this->validate($request, [
        	'judul_promo'	=>	'required',
        	'diskon'		=>	'required',
        	'id_product'	=>	'required',
        	'gambare'		=>	'required|image',
        ]);

        $product =	implode(",", $request->id_product);
        $arr 	 =  explode(",", $product);

		foreach ($arr as $key => $value) {
			Product::where('id', $value)->update(['diskon' => $request->diskon]);
		}

		$request->request->add([
			'id_toko'		=>	Auth::user()->id,
			'id_product'	=>	$product
		]);

		$nama   =   str_slug($request->judul_promo) . '.' .
                    $request->gambare->getClientOriginalExtension();

    	$request->request->add([
            'gambar'    =>  $nama,
    	]);

        $request->gambare->move(public_path('gambar'), $nama);

		Promosi::create($request->except(['_token', 'diskon', 'gambare']));

		return redirect()->back()->with('alert', 'Promo Telah Ditambahkan');
	}

	function hapusPromosi(Request $request) {
		$promo 		=	Promosi::where('id', $request->id)->first();

		$arr 	 	=  explode(",", $promo->id_product);

		foreach ($arr as $key => $value) {
			Product::where('id', $value)->update(['diskon' => 0]);
		}

		$promo->delete();

		return redirect()->back()->with('alert', 'Promo Telah Dihapus');
	}

	function editPromosi(Request $request) {
		$t = Toko::where('id', Auth::user()->id)->first();

        if ($t->permission == 0) {
            return redirect('/cashier/gudang-barang')->with('alert', 'Anda Harus Mendapatkan Izin Dari Administrator Gudang, Silahkan Hubungi Administrator Gudang Untuk Mendapatkan Izin Terlebih Dahulu');
        }

        $promo 		=	Promosi::where('id', $request->id)->first();

        $arr 	 	=  explode(",", $promo->id_product);

        $product 	=	Product::where('id_toko', Auth::user()->id)
        				->where('diskon', 0)->orWhereIn('id', $arr)
        				->orderBy('name', 'ASC')->paginate(10);

        $diskon 	=	Product::where('id', $arr)->first();

		return view('usahakumart.pages.edit_promosi', compact('product', 'promo', 'arr', 'diskon'));
	}

	function updatePromosi(Request $request) {
		$t = Toko::where('id', Auth::user()->id)->first();

        if ($t->permission == 0) {
            return redirect('/cashier/gudang-barang')->with('alert', 'Anda Harus Mendapatkan Izin Dari Administrator Gudang, Silahkan Hubungi Administrator Gudang Untuk Mendapatkan Izin Terlebih Dahulu');
        }

        $this->validate($request, [
        	'judul_promo'	=>	'required',
        	'diskon'		=>	'required',
        	'id_product'	=>	'required',
        	'gambare'		=>	'image'
        ]);

        $product =	implode(",", $request->id_product);
        $arr 	 =  explode(",", $product);

		foreach ($arr as $key => $value) {
			Product::where('id', $value)->update(['diskon' => $request->diskon]);
		}

		$request->request->add([
			'id_toko'		=>	Auth::user()->id,
			'id_product'	=>	$product
		]);

		$promo 		=	Promosi::where('id', $request->id)->first();

		if ($request->gambare != null) {
			unlink('/gambar/' . $promo->gambar);

			$nama   =   str_slug($request->judul_promo) . '.' .
	                    $request->gambare->getClientOriginalExtension();

	    	$request->request->add([
	            'gambar'    =>  $nama,
	    	]);

	    	$request->gambare->move(public_path('gambar'), $nama);
		}

		Promosi::where('id', $request->id)->update($request->except(['_token', 'diskon', 'gambare']));

		return redirect()->back()->with('alert', 'Promo Telah Diupdate');
	}

  function promosiToko() {

  	$promo 		=	PromoToko::where('id_toko', Auth::user()->id)->get();

  	return view('usahakumart.pages.promosi_toko', compact('promo'));
  }

  function promosiTokoCreate() {
  	return view('usahakumart.pages.create_promosi_toko');
  }

  function promosiTokoPost(Request $request) {
		$t = Toko::where('id', Auth::user()->id)->first();

        $this->validate($request, [
        	'judul_promo'	=>	'required',
        	'gambare'		=>	'required|image',
        ]);

        $cek 	=	PromoToko::where('id_toko', Auth::user()->id)->count();

        if ($cek > 3) {
        	return redirect()->with('alert', 'Maksimal Promo Toko Hanya 3');
        }

		$request->request->add([
			'id_toko'		=>	Auth::user()->id,
		]);

		$nama   =   str_slug($request->judul_promo) . '.' . '-' . Auth::user()->id .
	                    $request->gambare->getClientOriginalExtension();

    	$request->request->add([
            'gambar'    =>  $nama,
    	]);

        $request->gambare->move(public_path('gambar'), $nama);

		PromoToko::create($request->except(['_token', 'gambare']));

		return redirect()->back()->with('alert', 'Promo Telah Ditambahkan');
	}

	function editPromosiToko(Request $request) {
		$promo 		=	PromoToko::where('id', $request->id)->first();

		return view('usahakumart.pages.edit_promosi_toko', compact('promo'));
	}

	function updatePromosiToko(Request $request) {
        $this->validate($request, [
        	'judul_promo'	=>	'required',
        	'gambare'		=>	'image'
        ]);

		$request->request->add([
			'id_toko'		=>	Auth::user()->id,
		]);

		$promo 		=	PromoToko::where('id', $request->id)->first();

		if ($request->gambare != null) {
			unlink('/gambar/' . $promo->gambar);

			$nama   =   str_slug($request->judul_promo) . '.' . '-' . Auth::user()->id .
	                    $request->gambare->getClientOriginalExtension();

	    	$request->request->add([
	            'gambar'    =>  $nama,
	    	]);

	    	$request->gambare->move(public_path('gambar'), $nama);
		}

		PromoToko::where('id', $request->id)->update($request->except(['_token', 'gambare']));

		return redirect()->back()->with('alert', 'Promo Telah Diupdate');
	}

	function hapusPromosiToko(Request $request) {
		$promo 		=	PromoToko::where('id', $request->id)->first();

		$promo->delete();

		unlink('/gambar/' . $promo->gambar);

		return redirect()->back()->with('alert', 'Promo Telah Dihapus');
	}

  function showNotifikasi()
  {

    $trans 		= Transaksi::where('transaksi_offline.status', 0)
    			  ->where('transaksi_offline.id_toko', Auth::user()->id)
            	  ->orderBy('id', 'DESC')->paginate(8);

    return view('usahakumart.pages.notifikasi', compact('trans'));
  }

  function showBarang(Request $request) {
  	$result 	=	Cart::join('product_offline', 'product_offline.id', 'cart_offline.id_barang')
  					->where('cart_offline.id_toko', Auth::user()->id)
  					->where('kode_transaksi', $request->id)->get();

  	$trans 		=	Transaksi::where('id', $request->id)->first();

  	$member 	=	Member::where('id_member', $trans->id_member)->first();

  	$order 		=	Order::orderBy('id', 'DESC')->first();

  	$aff 		=	0;

  	foreach ($result as $key) {
  		$aff += $key->affiliasi * (25 / 100);
  	}


  	return Response::json([$result, $trans, $member, $aff, str_pad($order->no_transaksi + 1, 10, '0', STR_PAD_LEFT)]);
  }

  function countNotifikasi()
  {
    $data = Transaksi::where('transaksi_offline.status', 0)
    		->where('transaksi_offline.id_toko', Auth::user()->id)->count();
    return Response::json($data);
  }

  function confirmNotifikasi($id) {
  	Transaksi::where('id', $id)->update(['status' => 2]);

  	return redirect()->back()->with('alert', 'Order telah diconfirm');
  }

  function requestWithdraw() {
  	if (Auth::user()->id_tipe != 2) {
  		return redirect()->back();
  	}

  	$saldo 		=	Saldo::where('id_toko', Auth::user()->id)->first();

  	$withdraw 	=	Withdraw::where('id_toko', Auth::user()->id)->paginate(8);

  	return view('usahakumart.pages.withdraw', compact('saldo', 'withdraw'));
  }

  function countWithdraw() {
  	$count 	=	Withdraw::where('status', 0)->count();

  	return $count;
  }

  function countDeposit() {
  	$count 	=	Deposit::where('status', 0)->count();

  	return $count;
  }

  function countDepo() {
  	$count 	=	SaldoDepositRequest::where('status', 0)->count();

  	return $count;
  }

  function countKartu() {
  	$count 	=	RequestCard::where('status', 0)->count();

  	return $count;
  }

  function withdraw() {
  	$saldo 	=	Saldo::where('id_toko', Auth::user()->id)->first();

  	if ($saldo->saldo_withdraw < 20000) {
  		return redirect()->back()->with('alert', 'Saldo Minimal Rp. 20.000');
  	}

  	Withdraw::create([
  		'id_toko' 				=>	Auth::user()->id,
  		'jumlah_withdraw'		=>	$saldo->saldo_withdraw,
  		'status'				=> 	0
  	]);

  	Saldo::where('id_toko', Auth::user()->id)->update(['saldo_withdraw' => 0]);

  	return redirect()->back()->with('alert', 'Request Telah Dikirim');
  }

  function depositUser() {
  	$deposit 	=	SaldoDepositRequest::where('status', 0)
  					->orderBy('id', 'DESC')->get();

  	return view('administrator.pages.depositUser', compact('deposit'));
  }

  function depositUserKonf($type, Request $request) {
  	$id 	=	$request->id;

  	if ($type == 'tolak') {
  		SaldoDepositRequest::where('id', $id)->update(['status' => 1]);

  		$alert = 'Request Deposit Telah Ditolak';
  	} else {
  		$update = SaldoDepositRequest::where('id', $id)->update(['status' => 2]);

  		$up 	= SaldoDepositRequest::where('id', $id)->first();

  		$saldo  = SaldoUser::where('id_member', $up->id_member)->first();

  		SaldoUser::where('id_member', $up->id_member)->update(['saldo' => $saldo->saldo += $up->jumlah]);

  		$alert  = 'Request Deposit Telah Dikonfirmasi';
  	}

  	return redirect()->back()->with('alert', $alert);
  }

  function deposit() {
  	$deposit 	=	Deposit::where('id_toko', Auth::user()->id)
  					->orderBy('id', 'DESC')->get();

  	return view('usahakumart.pages.deposit', compact('deposit'));
  }

  function depositPost(Request $request) {
  	$this->validate($request, [
    	'jumlah_transfer'	=>	'required',
    	'gambare'			=>	'required|image',
    ]);

	$request->request->add([
		'id_toko'		=>	Auth::user()->id,
		'status'		=>	0
	]);

	$nama   =   str_slug(now() . '-' . Auth::user()->username) . '.' .
                $request->gambare->getClientOriginalExtension();

	$request->request->add([
        'gambar'    =>  $nama,
	]);

    $request->gambare->move(public_path('gambar'), $nama);

	Deposit::create($request->except(['_token', 'gambare']));

	return redirect()->back()->with('alert', 'Konfirmasi Deposit Telah Dikirim, Silahkan Tunggu');
  }
}
