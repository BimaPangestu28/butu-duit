<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Provinsi;
use App\Models\Kota;
use App\Models\Kecamatan;
use App\Models\TokoOffline as Toko;

class ShopController extends Controller
{
    function index() {
    	$provinsi 	=	Provinsi::orderBy('provinsi', 'ASC')->get();

    	$prov 		=	$provinsi->first();

    	$kota 		=	Kota::where('id_provinsi', $prov->id)->orderBy('kota', 'ASC')->get();

    	$kot 		=	$kota->first();

    	if ($kota->count() != null) {
    		$kecamatan 	=	Kecamatan::where('id_kota', $kot->id)->orderBy('kecamatan', 'ASC')->get();
    	} else {
    		$kecamatan = [];
    	}

    	return view('shop.index', compact('provinsi', 'kota', 'kecamatan'));
    }

    function post(Request $request) {
    	$cek 	=	Toko::where('id_provinsi', $request->id_provinsi)
    				->where('id_kota', $request->id_kota)
    				->where('id_kecamatan', $request->id_kecamatan)
    				->first();

    	if ($cek->count() > 0) {
    		session()->put('toko', $cek);

    		return redirect('/toko-usahakumart');
    	} else {
    		return redirect()->back()->with('alert', 'Tidak Ada UsahakuMart Didaerah Tersebut');
    	}
    }
}
