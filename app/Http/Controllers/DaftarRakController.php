<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ProductOffline;

use Auth;

class DaftarRakController extends Controller
{
    function index(Request $request) {
		
    	$data 	=	ProductOffline::where('rak', $request->q)
    				->where('id_toko', Auth::user()->id)->get();

    	return view('usahakumart.rak.index', compact('data'));
    }

    function search(Request $request) {
    	$data 	=	ProductOffline::where('rak', $request->q)
    				->where('id_toko', Auth::user()->id)->get();

    	return view('usahakumart.rak.index', compact('data'));
    }

    function update($id) {
    	return view('usahakumart.rak.edit', compact('id'));	
    }

    function updatePost($id, Request $request) {
    	$data 	=	ProductOffline::where('id', $id)->where('id_toko', Auth::user()->id)->first();

    	$result =	$data->jumlah_pada_rak += $request->jumlah;

    	$data->update(['jumlah_pada_rak' => $result]);

    	return redirect('/cashier/daftar-rak/search-rak?q=' . $data->rak)->with('alert', 'Jumlah Barang Telah Ditambah');
    }
}
