<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Response;

use App\Models\ProductOffline as Product;
use App\Models\Affiliasi as Aff;
use App\Models\AffiliasiTotal as Total;
use App\Models\AffiliateHistory as History;
use App\Models\AffiliasiMember as Member;
use App\Models\AffiliasiOutIn as Affiliasi;
use App\Models\AffiliasiDetail as Detail;
use App\Models\Member as User;
use App\Models\OrderOffline as Order;
use App\Models\OrderOfflineHistory as OrderHistory;
use App\Models\TransaksiOffline as Transaksi;
use App\Models\Saldo;
use App\Models\TokoOffline as Toko;
use App\Models\SaldoUser;

use App\PotonganBarang;
use App\OrderOfflinePotongan;

class BuyController extends Controller
{
    function index(Request $request) {
        $detail     =     Detail::where('id', 1)->first();

        if ($request->toko_id != '') {
            $id_toko_user   =   $request->toko_id;

            Transaksi::where('id', $request->id_transaksi)->update(['status' => 1]);
        } else {
            $id_toko_user   =   Auth::user()->id;
        }

        $toko_data  =   Toko::where('id', $id_toko_user)->first();

    	if ($request->member_card != null) {
            $id_pro     =   explode(",", $request->product_id);

            $product    =   Product::whereIn('id', $id_pro)->get();

            $number     =   $request->member_card;

            $member     =   Member::where('number_card', $number)->first();

            $cek_status =   User::where('id_member', $member->id_member)->first();

            if ($member->premium_cash >= 500000 && $cek_status->premium == 0) {
                User::where('id_member', $member->id_member)->update(['premium' => 1]);
            }

            $id         =   $member->id;

            $count      =   array_count_values($id_pro);

            $percent    =   100;

            $cek        =   Aff::where('id_member', $id)->first();

            $pembeli    =   25;
            $percent -= $pembeli;

            $total_aff_pembelian    =   0;

            $affiliasi_seluruh      =   0;

            $affiliasi_seluruh_s    =   0;

            foreach ($product as $key) {
                /*dd($key->affiliasi);*/
                if ($member->premium_cash <= 500000 && $cek_status->premium == 0) {
                    $pembeli_aff          =   ($key->affiliasi * ($pembeli / 100)) / 2;
                    $pembeli_aff_premium  =   ($key->affiliasi * ($pembeli / 100)) / 2;
                    $total_pembeli_aff    =   $key->affiliasi * ($pembeli / 100);
                } else {
                    $pembeli_aff          =   $key->affiliasi * ($pembeli / 100);
                    $pembeli_aff_premium  =   0;
                    $total_pembeli_aff    =   $key->affiliasi * ($pembeli / 100);
                }
                $total          =   Total::where('id_member', $id)->first();

                foreach ($count as $c => $val) {
                    if ($key->id == $c) {
                        for ($i=0; $i < $val; $i++) {
                            $total_aff_pembelian += $key->affiliasi;

                            Member::where('number_card', $number)
                                   ->update(['premium_cash' => $member->premium_cash += floor($pembeli_aff_premium)]);

                            History::create(['id_member' => $id, 'total_affiliasi' => floor($total_pembeli_aff)]);

                            Total::where('id_member', $id)
                                   ->update(['total_affiliasi' => $total->total_affiliasi += floor($pembeli_aff)]);

                            $affiliasi_seluruh   += floor($total_pembeli_aff);
                            $affiliasi_seluruh_s += floor($key->affiliasi);

                            Affiliasi::create([
                                          'jumlah'    =>  floor($pembeli_aff),
                                          'type'      =>  'Masuk',
                                          'milik'     =>  'Member',
                                          'member_id' =>  $id,
                                          'id_toko'   =>  $id_toko_user
                                       ]);

                           if (floor($pembeli_aff) > 0) {
                             Detail::where('id', 1)->update([
                                     'affiliasi_member'    =>    $detail->affiliasi_member += floor($pembeli_aff)
                             ]);
                           }
                        }
                    }
                }
            }

            if ($toko_data->id_tipe == 2) {
                $saldo = Saldo::where('id_toko', $toko_data->id)->first();

                if (($saldo->saldo_deposit - $total_aff_pembelian) < 0) {
                    if ($request->toko_id != '') {
                        return redirect()->back()->with('alert', 'Saldo Deposit Kurang, Silahkan Deposit Dulu');
                    } else {
                        return redirect()->back()->with('alert', 'Saldo Deposit Kurang, Silahkan Deposit Dulu');
                    }
                } else {
                    Saldo::where('id_toko', $toko_data->id)
                       ->update(['saldo_deposit' => $saldo->saldo_deposit -= $total_aff_pembelian]);
                }
            }

            //Generasi #1
            $gen1       =   Aff::where('id', $cek->id_parent)->first();

            if ($gen1 != null) {
                $gen1_per   =   10;
                $percent -= $gen1_per;

                foreach ($product as $key) {
                    $member1        =   Member::where('id', $gen1->id_member)->first();
                    if ($member1->premium_cash <= 500000 && $member1->premium == 0) {
                        $gen1_aff           =   (($key->affiliasi * (75 / 100)) * ($gen1_per / 100)) / 2;
                        $gen1_aff_premium   =   (($key->affiliasi * (75 / 100)) * ($gen1_per / 100)) / 2;
                        $gen1_aff_total     =   ($key->affiliasi * (75 / 100)) * ($gen1_per / 100);
                    } else {
                        $gen1_aff           =   ($key->affiliasi * (75 / 100)) * ($gen1_per / 100);
                        $gen1_aff_premium   =   0;
                        $gen1_aff_total     =   ($key->affiliasi * (75 / 100)) * ($gen1_per / 100);
                    }

                    $total      =   Total::where('id_member', $gen1->id_member)->first();

                    foreach ($count as $c => $val) {
                        if ($key->id == $c) {
                            for ($i=0; $i < $val; $i++) {
                                Member::where('id', $gen1->id_member)
                                   ->update(['premium_cash' => $member1->premium_cash += floor($gen1_aff_premium)]);

                                History::create(['id_member' => $gen1->id_member, 'total_affiliasi' => floor($gen1_aff_total)]);

                                Total::where('id_member', $gen1->id_member)
                                       ->update(['total_affiliasi' => $total->total_affiliasi += floor($gen1_aff)]);

                                $affiliasi_seluruh += floor($gen1_aff_total);

                                 Affiliasi::create([
                                               'jumlah'    =>  floor($gen1_aff),
                                               'type'      =>  'Masuk',
                                               'milik'     =>  'Member',
                                               'member_id' =>  $gen1->id_member,
                                               'id_toko'   =>  $id_toko_user
                                            ]);

                                if (floor($gen1_aff) > 0) {
                                  Detail::where('id', 1)->update([
                                          'affiliasi_member'    =>    $detail->affiliasi_member += floor($gen1_aff)
                                  ]);
                                }
                            }
                        }
                    }
                }

                //Generasi #2
                $gen2       =   Aff::where('id', $gen1->id_parent)->first();

            if ($gen2 != null) {
                $gen2_per   =   7;
                $percent -= $gen2_per;

                foreach ($product as $key) {
                    $member2        =   Member::where('id', $gen2->id_member)->first();
                    if ($member2->premium_cash <= 500000 && $member2->premium == 0) {
                        $gen2_aff         =   (($key->affiliasi * (75 / 100)) * ($gen2_per / 100)) / 2;
                        $gen2_aff_premium =   (($key->affiliasi * (75 / 100)) * ($gen2_per / 100)) / 2;
                        $gen2_aff_total   =   ($key->affiliasi * (75 / 100)) * ($gen2_per / 100);
                    } else {
                        $gen2_aff         =   ($key->affiliasi * (75 / 100)) * ($gen2_per / 100);
                        $gen2_aff_premium =   0;
                        $gen2_aff_total   =   ($key->affiliasi * (75 / 100)) * ($gen2_per / 100);
                    }
                    $total  =   Total::where('id_member', $gen2->id_member)->first();

                    foreach ($count as $c => $val) {
                        if ($key->id == $c) {
                            for ($i=0; $i < $val; $i++) {
                                Member::where('id', $gen2->id_member)
                                   ->update(['premium_cash' => $member2->premium_cash += floor($gen2_aff_premium)]);

                                History::create(['id_member' => $gen2->id_member, 'total_affiliasi' => floor($gen2_aff_total)]);

                                Total::where('id_member', $gen2->id_member)
                                       ->update(['total_affiliasi' => $total->total_affiliasi += floor($gen2_aff)]);

                                $affiliasi_seluruh += floor($gen2_aff_total);

                                Affiliasi::create([
                                             'jumlah'    =>  floor($gen2_aff),
                                             'type'      =>  'Masuk',
                                             'milik'     =>  'Member',
                                             'member_id' =>  $gen2->id_member,
                                             'id_toko'   =>  $id_toko_user
                                          ]);

                                if (floor($gen2_aff) > 0) {
                                  Detail::where('id', 1)->update([
                                          'affiliasi_member'    =>    $detail->affiliasi_member += floor($gen2_aff)
                                  ]);
                                }
                            }
                        }
                    }
                }

                //Generasi #3
                $gen3   =   Aff::where('id', $gen2->id_parent)->first();

            if ($gen3 != null) {
                $gen3_per   =   4;
                $percent -= $gen3_per;

                foreach ($product as $key) {
                    $member3        =   Member::where('id', $gen3->id_member)->first();
                    if ($member3->premium_cash <= 500000 && $member3->premium == 0) {
                        $gen3_aff         =   (($key->affiliasi * (75 / 100)) * ($gen3_per / 100)) / 2;
                        $gen3_aff_premium =   (($key->affiliasi * (75 / 100)) * ($gen3_per / 100)) / 2;
                        $gen3_aff_total   =   ($key->affiliasi * (75 / 100)) * ($gen3_per / 100);
                    } else {
                        $gen3_aff         =   ($key->affiliasi * (75 / 100)) * ($gen3_per / 100);
                        $gen3_aff_premium =   0;
                        $gen3_aff_total   =   ($key->affiliasi * (75 / 100)) * ($gen3_per / 100);
                    }
                    $total      =   Total::where('id_member', $gen3->id_member)->first();

                    foreach ($count as $c => $val) {
                        if ($key->id == $c) {
                            for ($i=0; $i < $val; $i++) {
                                Member::where('id', $gen3->id_member)
                                   ->update(['premium_cash' => $member3->premium_cash += floor($gen3_aff_premium)]);

                                History::create(['id_member' => $gen3->id_member, 'total_affiliasi' => floor($gen3_aff_total)]);

                                Total::where('id_member', $gen3->id_member)
                                       ->update(['total_affiliasi' => $total->total_affiliasi += floor($gen3_aff)]);

                                $affiliasi_seluruh += floor($gen3_aff_total);

                                Affiliasi::create([
                                            'jumlah'    =>  floor($gen3_aff),
                                            'type'      =>  'Masuk',
                                            'milik'     =>  'Member',
                                            'member_id' =>  $gen3->id_member,
                                            'id_toko'   =>  $id_toko_user
                                         ]);

                               if (floor($gen3_aff) > 0) {
                                 Detail::where('id', 1)->update([
                                         'affiliasi_member'    =>    $detail->affiliasi_member += floor($gen3_aff)
                                 ]);
                               }
                            }
                        }
                    }
                }

                //Generasi #4
                $gen4   =   Aff::where('id', $gen3->id_parent)->first();

            if ($gen4 != null) {
                $gen4_per   =   3;
                $percent -= $gen4_per;

                foreach ($product as $key) {
                    $member4        =   Member::where('id', $gen4->id_member)->first();
                    if ($member4->premium_cash <= 500000 && $member4->premium == 0) {
                        $gen4_aff         =   (($key->affiliasi * (75 / 100)) * ($gen4_per / 100)) / 2;
                        $gen4_aff_premium =   (($key->affiliasi * (75 / 100)) * ($gen4_per / 100)) / 2;
                        $gen4_aff_total   =   ($key->affiliasi * (75 / 100)) * ($gen4_per / 100);
                    } else {
                        $gen4_aff         =   ($key->affiliasi * (75 / 100)) * ($gen4_per / 100);
                        $gen4_aff_premium =   0;
                        $gen4_aff_total   =   ($key->affiliasi * (75 / 100)) * ($gen4_per / 100);
                    }
                    $total          =   Total::where('id_member', $gen4->id_member)->first();

                    foreach ($count as $c => $val) {
                        if ($key->id == $c) {
                            for ($i=0; $i < $val; $i++) {
                                Member::where('id', $gen4->id_member)
                                        ->update(['premium_cash' => $member4->premium_cash += floor($gen4_aff_premium)]);

                                History::create(['id_member' => $gen4->id_member, 'total_affiliasi' => floor($gen4_aff_total)]);

                                Total::where('id_member', $gen4->id_member)
                                       ->update(['total_affiliasi' => $total->total_affiliasi += floor($gen4_aff)]);

                                $affiliasi_seluruh += floor($gen4_aff_total);

                                 Affiliasi::create([
                                             'jumlah'    =>  floor($gen4_aff),
                                             'type'      =>  'Masuk',
                                             'milik'     =>  'Member',
                                             'member_id' =>  $gen4->id_member,
                                             'id_toko'   =>  $id_toko_user
                                          ]);

                                if (floor($gen4_aff) > 0) {
                                  Detail::where('id', 1)->update([
                                          'affiliasi_member'    =>    $detail->affiliasi_member += floor($gen4_aff)
                                  ]);
                                }
                            }
                        }
                    }
                }

                //Generasi #5
                $gen5   =   Aff::where('id', $gen4->id_parent)->first();

            if ($gen5 != null) {
                $gen5_per   =   3;
                $percent -= $gen5_per;

                foreach ($product as $key) {
                    $member5        =   Member::where('id', $gen5->id_member)->first();
                    if ($member5->premium_cash <= 500000 && $member5->premium == 0) {
                        $gen5_aff         =   (($key->affiliasi * (75 / 100)) * ($gen5_per / 100)) / 2;
                        $gen5_aff_premium =   (($key->affiliasi * (75 / 100)) * ($gen5_per / 100)) / 2;
                        $gen5_aff_total   =   ($key->affiliasi * (75 / 100)) * ($gen5_per / 100);
                    } else {
                        $gen5_aff         =   ($key->affiliasi * (75 / 100)) * ($gen5_per / 100);
                        $gen5_aff_premium =   0;
                        $gen5_aff_total   =   ($key->affiliasi * (75 / 100)) * ($gen5_per / 100);
                    }
                    $total  =   Total::where('id_member', $gen5->id_member)->first();

                    foreach ($count as $c => $val) {
                        if ($key->id == $c) {
                            for ($i=0; $i < $val; $i++) {
                                Member::where('id', $gen5->id_member)
                                   ->update(['premium_cash' => $member5->premium_cash += floor($gen5_aff_premium)]);

                                History::create(['id_member' => $gen5->id_member, 'total_affiliasi' => floor($gen5_aff_total)]);

                                Total::where('id_member', $gen5->id_member)
                                       ->update(['total_affiliasi' => $total->total_affiliasi += floor($gen5_aff)]);

                                $affiliasi_seluruh += floor($gen5_aff_total);

                                 Affiliasi::create([
                                             'jumlah'    =>  floor($gen5_aff),
                                             'type'      =>  'Masuk',
                                             'milik'     =>  'Member',
                                             'member_id' =>  $gen5->id_member,
                                             'id_toko'   =>  $id_toko_user
                                          ]);

                                if (floor($gen5_aff) > 0) {
                                  Detail::where('id', 1)->update([
                                          'affiliasi_member'    =>    $detail->affiliasi_member += floor($gen5_aff)
                                  ]);
                                }
                            }
                        }
                    }
                }

                //Generasi #6
                $gen6   =   Aff::where('id', $gen5->id_parent)->first();

            if ($gen6 != null) {
                $gen6_per   =   3;
                $percent -= $gen6_per;

                foreach ($product as $key) {
                    $member6        =   Member::where('id', $gen6->id_member)->first();
                    if ($member6->premium_cash <= 500000 && $member6->premium == 0) {
                        $gen6_aff         =   (($key->affiliasi * (75 / 100)) * ($gen6_per / 100)) / 2;
                        $gen6_aff_premium =   (($key->affiliasi * (75 / 100)) * ($gen6_per / 100)) / 2;
                        $gen6_aff_total   =   ($key->affiliasi * (75 / 100)) * ($gen6_per / 100);
                    } else {
                        $gen6_aff         =   ($key->affiliasi * (75 / 100)) * ($gen6_per / 100);
                        $gen6_aff_premium =   0;
                        $gen6_aff_total   =   ($key->affiliasi * (75 / 100)) * ($gen6_per / 100);
                    }
                    $total  =   Total::where('id_member', $gen6->id_member)->first();


                    foreach ($count as $c => $val) {
                        if ($key->id == $c) {
                            for ($i=0; $i < $val; $i++) {
                                Member::where('id', $gen6->id_member)
                                   ->update(['premium_cash' => $member6->premium_cash += floor($gen6_aff_premium)]);

                                History::create(['id_member' => $gen6->id_member, 'total_affiliasi' => floor($gen6_aff_total)]);

                                Total::where('id_member', $gen6->id_member)
                                       ->update(['total_affiliasi' => $total->total_affiliasi += floor($gen6_aff)]);

                                $affiliasi_seluruh += floor($gen6_aff_total);

                                 Affiliasi::create([
                                             'jumlah'    =>  floor($gen6_aff),
                                             'type'      =>  'Masuk',
                                             'milik'     =>  'Member',
                                             'member_id' =>  $gen6->id_member,
                                             'id_toko'   =>  $id_toko_user
                                          ]);

                                if (floor($gen6_aff) > 0) {
                                  Detail::where('id', 1)->update([
                                          'affiliasi_member'    =>    $detail->affiliasi_member += floor($gen6_aff)
                                  ]);
                                }
                            }
                        }
                    }
                }

                //Generasi #7
                $gen7   =   Aff::where('id', $gen6->id_parent)->first();

            if ($gen7 != null) {
                $gen7_per   =   3;
                $percent -= $gen7_per;

                foreach ($product as $key) {
                    $member7        =   Member::where('id', $gen7->id_member)->first();
                    if ($member7->premium_cash <= 500000 && $member7->premium == 0) {
                        $gen7_aff         =   (($key->affiliasi * (75 / 100)) * ($gen7_per / 100)) / 2;
                        $gen7_aff_premium =   (($key->affiliasi * (75 / 100)) * ($gen7_per / 100)) / 2;
                        $gen7_aff_total   =   ($key->affiliasi * (75 / 100)) * ($gen7_per / 100);
                    } else {
                        $gen7_aff         =   ($key->affiliasi * (75 / 100)) * ($gen7_per / 100);
                        $gen7_aff_premium =   0;
                        $gen7_aff_total   =   ($key->affiliasi * (75 / 100)) * ($gen7_per / 100);
                    }
                    $total      =   Total::where('id_member', $gen7->id_member)->first();

                    foreach ($count as $c => $val) {
                        if ($key->id == $c) {
                            for ($i=0; $i < $val; $i++) {
                                Member::where('id', $gen7->id_member)
                                   ->update(['premium_cash' => $member7->premium_cash += floor($gen7_aff_premium)]);

                                History::create(['id_member' => $gen7->id_member, 'total_affiliasi' => floor($gen7_aff_total)]);

                                Total::where('id_member', $gen7->id_member)
                                       ->update(['total_affiliasi' => $total->total_affiliasi += floor($gen7_aff)]);

                                $affiliasi_seluruh += floor($gen7_aff_total);

                                 Affiliasi::create([
                                             'jumlah'    =>  floor($gen7_aff),
                                             'type'      =>  'Masuk',
                                             'milik'     =>  'Member',
                                             'member_id' =>  $gen7->id_member,
                                             'id_toko'   =>  $id_toko_user
                                          ]);

                                if (floor($gen7_aff) > 0) {
                                  Detail::where('id', 1)->update([
                                          'affiliasi_member'    =>    $detail->affiliasi_member += floor($gen7_aff)
                                  ]);
                                }
                            }
                        }
                    }
                }

                //Generasi #8
                $gen8   =   Aff::where('id', $gen7->id_parent)->first();

            if ($gen8 != null) {
                $gen8_per   =   3;
                $percent -= $gen8_per;

                foreach ($product as $key) {
                    $member8        =   Member::where('id', $gen8->id_member)->first();
                    if ($member8->premium_cash <= 500000 && $member8->premium == 0) {
                        $gen8_aff         =   (($key->affiliasi * (75 / 100)) * ($gen8_per / 100)) / 2;
                        $gen8_aff_premium =   (($key->affiliasi * (75 / 100)) * ($gen8_per / 100)) / 2;
                        $gen8_aff_total   =   ($key->affiliasi * (75 / 100)) * ($gen8_per / 100);
                    } else {
                        $gen8_aff         =   ($key->affiliasi * (75 / 100)) * ($gen8_per / 100);
                        $gen8_aff_premium =   0;
                        $gen8_aff_total   =   ($key->affiliasi * (75 / 100)) * ($gen8_per / 100);
                    }
                    $total          =   Total::where('id_member', $gen8->id_member)->first();

                    foreach ($count as $c => $val) {
                        if ($key->id == $c) {
                            for ($i=0; $i < $val; $i++) {
                                Member::where('id', $gen8->id_member)
                                   ->update(['premium_cash' => $member8->premium_cash += floor($gen8_aff_premium)]);

                                History::create(['id_member' => $gen8->id_member, 'total_affiliasi' => floor($gen8_aff_total)]);

                                Total::where('id_member', $gen8->id_member)
                                       ->update(['total_affiliasi' => $total->total_affiliasi += floor($gen8_aff)]);

                                $affiliasi_seluruh += floor($gen8_aff_total);

                               Affiliasi::create([
                                           'jumlah'    =>  floor($gen8_aff),
                                           'type'      =>  'Masuk',
                                           'milik'     =>  'Member',
                                           'member_id' =>  $gen8->id_member,
                                           'id_toko'   =>  $id_toko_user
                                        ]);

                                if (floor($gen8_aff) > 0) {
                                  Detail::where('id', 1)->update([
                                          'affiliasi_member'    =>    $detail->affiliasi_member += floor($gen8_aff)
                                  ]);
                                }
                            }
                        }
                    }
                }

                //Generasi #9
                $gen9   =   Aff::where('id', $gen8->id_parent)->first();

            if ($gen9 != null) {
                $gen9_per   =   3;
                $percent -= $gen9_per;

                foreach ($product as $key) {
                    $member9        =   Member::where('id', $gen9->id_member)->first();
                    if ($member9->premium_cash <= 500000 && $member9->premium == 0) {
                        $gen9_aff         =   (($key->affiliasi * (75 / 100)) * ($gen9_per / 100)) / 2;
                        $gen9_aff_premium =   (($key->affiliasi * (75 / 100)) * ($gen9_per / 100)) / 2;
                        $gen9_aff_total   =   ($key->affiliasi * (75 / 100)) * ($gen9_per / 100);
                    } else {
                        $gen9_aff         =   ($key->affiliasi * (75 / 100)) * ($gen9_per / 100);
                        $gen9_aff_premium =   0;
                        $gen9_aff_total   =   ($key->affiliasi * (75 / 100)) * ($gen9_per / 100);
                    }
                    $total  =   Total::where('id_member', $gen9->id_member)->first();

                    foreach ($count as $c => $val) {
                        if ($key->id == $c) {
                            for ($i=0; $i < $val; $i++) {
                                Member::where('id', $gen9->id_member)
                                   ->update(['premium_cash' => $member9->premium_cash += floor($gen9_aff_premium)]);

                                History::create(['id_member' => $gen9->id_member, 'total_affiliasi' => floor($gen9_aff_total)]);

                                Total::where('id_member', $gen9->id_member)
                                       ->update(['total_affiliasi' => $total->total_affiliasi += floor($gen9_aff)]);

                                $affiliasi_seluruh += floor($gen9_aff_total);

                                 Affiliasi::create([
                                             'jumlah'    =>  floor($gen9_aff),
                                             'type'      =>  'Masuk',
                                             'milik'     =>  'Member',
                                             'member_id' =>  $gen9->id_member,
                                             'id_toko'   =>  $id_toko_user
                                          ]);

                                if (floor($gen9_aff) > 0) {
                                  Detail::where('id', 1)->update([
                                          'affiliasi_member'    =>    $detail->affiliasi_member += floor($gen9_aff)
                                  ]);
                                }
                            }
                        }
                    }
                }

                //Generasi 10
                $gen10  =   Aff::where('id', $gen9->id_parent)->first();

            if ($gen10 != null) {

                $gen10_per  =   3;
                $percent -= $gen10_per;

                foreach ($product as $key) {
                    $member10        =   Member::where('id', $gen10->id_member)->first();
                    if ($member10->premium_cash <= 500000 && $member10->premium == 0) {
                        $gen10_aff         =   (($key->affiliasi * (75 / 100)) * ($gen10_per / 100)) / 2;
                        $gen10_aff_premium =   (($key->affiliasi * (75 / 100)) * ($gen10_per / 100)) / 2;
                        $gen10_aff_total   =   ($key->affiliasi * (75 / 100)) * ($gen10_per / 100);
                    } else {
                        $gen10_aff         =   ($key->affiliasi * (75 / 100)) * ($gen10_per / 100);
                        $gen10_aff_premium =   0;
                        $gen10_aff_total   =   ($key->affiliasi * (75 / 100)) * ($gen10_per / 100);
                    }
                    $total      =   Total::where('id_member', $gen10->id_member)->first();

                    foreach ($count as $c => $val) {
                        if ($key->id == $c) {
                            for ($i=0; $i < $val; $i++) {
                                Member::where('id', $gen10->id_member)
                                   ->update(['premium_cash' => $member10->premium_cash += floor($gen10_aff_premium)]);

                                History::create(['id_member' => $gen10->id_member, 'total_affiliasi' => floor($gen10_aff_total)]);

                                Total::where('id_member', $gen10->id_member)
                                       ->update(['total_affiliasi' => $total->total_affiliasi += floor($gen10_aff)]);

                                $affiliasi_seluruh += floor($gen10_aff_total);

                                 Affiliasi::create([
                                             'jumlah'    =>  floor($gen10_aff),
                                             'type'      =>  'Masuk',
                                             'milik'     =>  'Member',
                                             'member_id' =>  $gen10->id_member,
                                             'id_toko'   =>  $id_toko_user
                                          ]);

                                if (floor($gen10_aff) > 0) {
                                  Detail::where('id', 1)->update([
                                          'affiliasi_member'    =>    $detail->affiliasi_member += floor($gen10_aff)
                                  ]);
                                }
                            }
                        }
                    }
                }

            }
            }
            }
            }
            }
            }
            }
            }
            }
            }

            $affiliasi_perusahaan   =   $affiliasi_seluruh_s - $affiliasi_seluruh;

            Affiliasi::create([
                        'jumlah'    =>  floor($affiliasi_perusahaan),
                        'type'      =>  'Masuk',
                        'milik'     =>  'Perusahaan',
                        'id_toko'   =>  $id_toko_user
                     ]);

             if (floor($affiliasi_perusahaan) > 0) {
               Detail::where('id', 1)->update([
                       'affiliasi_perusahaan'    =>    $detail->affiliasi_perusahaan += floor($affiliasi_perusahaan)
               ]);
             }

            if ($request->created_at != null) {
                $date   =   $request->created_at;
            } else {
                $date   =   now();
            }

            $hpp        =   0;

            foreach ($product as $key) {
                foreach ($count as $c => $val) {
                    if ($key->id == $c) {
                        for ($i=0; $i < $val; $i++) {
                            $hpp += $key->price;
                        }
                    }
                }
            }

            $kembalian  =   $request->pembayaran - $request->total;

            $last       =   Order::orderBy('id', 'DESC')->first();

            $order      =   Order::create([
                                'id_member'           =>    $id,
                                'total_pembayaran'    =>    $request->total,
                                'id_toko'             =>    $id_toko_user,
                                'pembayaran'          =>    $request->pembayaran,
                                'kembalian'           =>    $kembalian,
                                'id_user'             =>    session('user-toko')->id,
                                'no_transaksi'        =>    str_pad($last->no_transaksi + 1, 10, '0', STR_PAD_LEFT),
                                'type'                =>    $request->type,
                                'created_at'          =>    $date,
                                'affiliasi'           =>    $request->total_affiliasi,
                                'hpp'                 =>    $hpp
                            ]);

            foreach ($count as $key => $value) {
                $cek =  PotonganBarang::where('id_product', $key)->get();

                if ($cek != null) {
                    foreach ($cek as $keys) {
                        $jumlah     =   floor($value / $keys->jumlah_dibeli) * $keys->potongan;

                        if ($jumlah != 0) {
                            OrderOfflinePotongan::create([
                                'order_id'      =>  $order->id,
                                'id_product'    =>  $keys->id_product,
                                'potongan'      =>  $jumlah,
                                'created_at'    =>  $date
                            ]);
                        }
                    }
                }
            }

            //History Pembelanjaan
            foreach ($product as $key) {
                foreach ($count as $c => $val) {
                    if ($key->id == $c) {
                        for ($i=0; $i < $val; $i++) {
                            OrderHistory::create(['id_toko'     =>  $id_toko_user,
                                                  'id_product'  =>  $key->id,
                                                  'order_id'    =>  $order->id,
                                                  'created_at'  =>  $date
                                                ]);
                            $pros   =   Product::where('id', $key->id)->first();
                            if ($pros->stok > 0) {
                                $pros->decrement('stok');
                                /*$pros->decrement('jumlah_pada_rak');*/
                            };
                            Product::where('id', $key->id)->increment('terjual');
                        }
                    }
                }
            }

            if ($request->total_potongan != null) {
                $order->update(['potongan' => $request->total_potongan]);
            }

            if ($request->barang_bonus != null) {
                $order->update(['bonus_barang' => $request->barang_bonus]);
            }

            $ar_bb  =   explode(",", $request->barang_bonus);
            foreach ($ar_bb as $key => $value) {
                   Product::where('id', $value)->decrement('stok');
            }

            if ($request->type == 'aff' || $request->type == 'affiliasi') {
                $total_aff = Total::where('id_member', $member->id)->first();

                Total::where('id_member', $member->id)
                ->update(['total_affiliasi' => $total_aff->total_affiliasi - $request->pembayaran]);

                Affiliasi::create([
                            'jumlah'    =>  $request->pembayaran,
                            'type'      =>  'Keluar',
                            'milik'     =>  'Member',
                            'member_id' =>  $member->id,
                            'id_toko'   =>  $id_toko_user
                         ]);

                 if ($request->pembayaran > 0) {
                   Detail::where('id', 1)->update([
                           'affiliasi_member'    =>    $detail->affiliasi_member - $request->pembayaran
                   ]);
                 }

                if ($toko_data->id_tipe == 2) {
                    $saldo = Saldo::where('id_toko', $toko_data->id)->first();

                    Saldo::where('id_toko', $toko_data->id)
                           ->update(['saldo_withdraw' => $saldo->saldo_withdraw += $request->total]);
                }
            }

            if ($request->type == 'saldo') {
              $total_saldo = SaldoUser::where('id_member', $member->id_member)->first();

              SaldoUser::where('id_member', $member->id_member)
              ->update(['saldo' => $total_saldo->saldo - $request->pembayaran]);
            }

            if ($request->toko_id != '') {
                return redirect()->back()->with('alert', 'Pembelian berhasil, Jangan Lupa Order Lagi');
            } else {
                return Response::json($kembalian);
            }
        } else {
            if ($request->created_at != null) {
                $date   =   $request->created_at;
            } else {
                $date   =   now();
            }
            /*dd($request->request);*/
            $id_pro     =   explode(",", $request->product_id);

            $product    =   Product::whereIn('id', $id_pro)->get();

            $count      =   array_count_values($id_pro);

            $kembalian  =   $request->pembayaran - $request->total;

            $last       =   Order::orderBy('id', 'DESC')->first();

            $hpp        =   0;

            foreach ($product as $key) {
                foreach ($count as $c => $val) {
                    if ($key->id == $c) {
                        for ($i=0; $i < $val; $i++) {
                            $hpp += $key->price;
                        }
                    }
                }
            }

            $orderPost  =   Order::create([
                                'total_pembayaran'    =>    $request->total,
                                'id_toko'             =>    $id_toko_user,
                                'pembayaran'          =>    $request->pembayaran,
                                'kembalian'           =>    $kembalian,
                                'id_user'             =>    session('user-toko')->id,
                                'no_transaksi'        =>    str_pad($last->no_transaksi + 1, 10, '0', STR_PAD_LEFT),
                                'type'                =>    $request->type,
                                'created_at'          =>    $date,
                                'hpp'                 =>    $hpp
                            ]);

            foreach ($count as $key => $value) {
                $cek =  PotonganBarang::where('id_product', $key)->get();

                if ($cek != null) {
                    foreach ($cek as $keys) {
                        $jumlah     =   floor($value / $keys->jumlah_dibeli) * $keys->potongan;

                        if ($jumlah != 0) {
                            OrderOfflinePotongan::create([
                                'order_id'      =>  $orderPost->id,
                                'id_product'    =>  $keys->id_product,
                                'potongan'      =>  $jumlah,
                                'created_at'    =>  $date
                            ]);
                        }
                    }
                }
            }

            //History Pembelanjaan
            foreach ($product as $key) {
                foreach ($count as $c => $val) {
                    if ($key->id == $c) {
                        for ($i=0; $i < $val; $i++) {
                            OrderHistory::create([
                                                  'id_toko'     =>  $id_toko_user,
                                                  'id_product'  =>  $key->id,
                                                  'order_id'    =>  $orderPost->id,
                                                  'created_at'  =>  $date
                                                ]);
                            $pros   =   Product::where('id', $key->id)->first();
                            if ($pros->stok > 0) {
                                $pros->decrement('stok');
                                /*$pros->decrement('jumlah_pada_rak');*/
                            };
                            Product::where('id', $key->id)->increment('terjual');
                        }
                    }
                }
            }

            if ($request->total_potongan != null) {
                $orderPost->update(['potongan' => $request->total_potongan]);
            }

            if ($request->barang_bonus != null) {
                $orderPost->update(['bonus_barang' => $request->barang_bonus]);
            }

            $ar_bb  =   explode(",", $request->barang_bonus);
            foreach ($ar_bb as $key => $value) {
                   Product::where('id', $value)->decrement('stok');
            }


            return redirect()->back()->with('alert', 'Pembayaran berhasil. Total Kembalian Rp. ' . $kembalian);
        }
    }
}
