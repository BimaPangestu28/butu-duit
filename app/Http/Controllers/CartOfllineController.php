<?php

namespace App\Http\Controllers;

use Session;
use Response;
use Illuminate\Http\Request;
use App\Models\Member;
use App\Models\TokoOffline;
use App\Models\ProductOffline as Product;
use App\Models\CartOffline as Cart;

class cartOfllineController extends Controller
{
    function store(Request $request, $id)
    {
      $cart_toko = $_POST['toko'];
      $cart_member = Session::get('member')->id_member;
      $cart_barang = $_POST['barang'];

      $cekBarang = Cart::where('id_barang', $cart_barang)->where('kode_transaksi', null)->where('id_member', $cart_member)->count();

      if ($cekBarang >= 1) {
        $isiBarang = Cart::where('id_barang', $cart_barang)->where('kode_transaksi', null)->where('id_member', $cart_member)->first();
        $isiBarang->jumlah_barang = $isiBarang->jumlah_barang + 1;
        $isiBarang->update();
        return Response::json($isiBarang);
      }
      else{
        $cart = new Cart;
        $cart->id_toko = $cart_toko;
        $cart->id_member = $cart_member;
        $cart->id_barang = $cart_barang;
        $cart->jumlah_barang = 1;
        $cart->level = 0;
        $cart->kode_transaksi = null;

        $cart->save();
        return Response::json($cart);
      }

    }

    function update($id)
    {
      $jumlah = $_POST['jumlah_barang'];
      $cart_member = Session::get('member')->id_member;

      $cart = Cart::where('id_barang', $id)
              ->where('id_member', $cart_member)
              ->where('kode_transaksi', null)
              ->first();
      $cart->jumlah_barang = $jumlah;
      $cart->update();
      return Response::json($cart);
    }

    function delete()
    {
      $id = $_POST['id_barang'];
      $cart = Cart::where('id_barang', $id)->where('kode_transaksi', null)->where('id_member', Session::get('member')->id_member)->first();
      $cart->delete();
    }
}
