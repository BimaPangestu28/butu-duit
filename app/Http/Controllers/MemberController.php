<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\AffiliasiMember as Member;
use App\Models\AffiliasiTotal as Total;
use App\Models\AffiliasiOutIn as OutIn;
use App\Models\Affiliasi;
use App\Models\Payout;
use App\Models\Member as User;

class MemberController extends Controller
{
    function hapus($id) {

      /* Hapus Jaringan Affiliasi */
      $aff_user               =     Affiliasi::where('id_member', $id)->first();

      if ($aff_user) {
        $aff_user_parent_1      =     Affiliasi::where('id_member', $aff_user->id_parent)->first();

        if($aff_user_parent_1) {
          $aff_user_parent_2      =     Affiliasi::where('id_member', $aff_user_parent_1->id_parent)->first();

          Affiliasi::where('id', $aff_user_parent_1->id)->decrement('child');

          if ($aff_user_parent_2) {
            $aff_user_parent_3      =     Affiliasi::where('id_member', $aff_user_parent_2->id_parent)->first();

            Affiliasi::where('id', $aff_user_parent_2->id)->decrement('child_2');

            if ($aff_user_parent_3) {
              $aff_user_parent_4      =     Affiliasi::where('id_member', $aff_user_parent_3->id_parent)->first();

              Affiliasi::where('id', $aff_user_parent_3->id)->decrement('child_3');

              if ($aff_user_parent_4) {
                $aff_user_parent_5      =     Affiliasi::where('id_member', $aff_user_parent_4->id_parent)->first();

                Affiliasi::where('id', $aff_user_parent_4->id)->decrement('child_4');

                if ($aff_user_parent_5) {
                  $aff_user_parent_6      =     Affiliasi::where('id_member', $aff_user_parent_5->id_parent)->first();

                  Affiliasi::where('id', $aff_user_parent_5->id)->decrement('child_5');

                  if ($aff_user_parent_6) {
                    $aff_user_parent_7      =     Affiliasi::where('id_member', $aff_user_parent_6->id_parent)->first();

                    Affiliasi::where('id', $aff_user_parent_6->id)->decrement('child_6');

                    if ($aff_user_parent_7) {
                      $aff_user_parent_8      =     Affiliasi::where('id_member', $aff_user_parent_7->id_parent)->first();

                      Affiliasi::where('id', $aff_user_parent_7->id)->decrement('child_7');

                      if ($aff_user_parent_8) {
                        $aff_user_parent_9      =     Affiliasi::where('id_member', $aff_user_parent_8->id_parent)->first();

                        Affiliasi::where('id', $aff_user_parent_8->id)->decrement('child_8');

                        if ($aff_user_parent_9) {
                          $aff_user_parent_10     =     Affiliasi::where('id_member', $aff_user_parent_9->id_parent)->first();

                          Affiliasi::where('id', $aff_user_parent_9->id)->decrement('child_9');

                          if ($aff_user_parent_10) {
                            Affiliasi::where('id', $aff_user_parent_10->id)->decrement('child_10');
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }

      /* Ganti parent child */
      $jumlah_child     =     Affiliasi::where('id_parent', $aff_user->id)->count();

      $child_count      =     10 - $jumlah_child;

      $parent           =     Affiliasi::where('id', $aff_user->id_parent)->first();

      if ($parent != null) {

        /* Parent sebelah */
        $count_parent   =   Affiliasi::where('id', $aff_user->id_parent)->where('child', '<', $child_count)->first();

        if ($count_parent != null) {
          Affiliasi::where('id_parent', $aff_user->id)->update(['id_parent' => $parent->id]);

          Affiliasi::where('id', $parent->id)->update(['child' => $parent->child + $child_count]);
        } else {

          /* Parent 1 */
          $parent_1    =   Affiliasi::where('code_invitation', 696969696969)->first();

          if ($parent_1 != null) {
            $cek_parent_1   =   Affiliasi::where('id_parent', $parent_1->id)
                                           ->where('child', '<', $child_count)
                                           ->first();

            if ($cek_parent_1 != null) {
              Affiliasi::where('id_parent', $aff_user->id)
                         ->update(['id_parent' => $cek_parent_1->id]);

              Affiliasi::where('id', $cek_parent_1->id_parent)
                         ->update(['child_2' => $perusahaan->child_2 + $child_count]);

              Affiliasi::where('id', $cek_parent_1->id)
                        ->update(['child' => $cek_parent_1->child + $child_count]);
            } else {

              /* Parent 2 */
              $parent_2   =   Affiliasi::where('id_parent', $parent_1->id)->first();

              if ($parent_2 != null) {
                $cek_parent_2   =     Affiliasi::where('id_parent', $parent_1->id)
                                                ->where('child', '<', $child_count)
                                                ->first();

                if ($cek_parent_2 != null) {
                  Affiliasi::where('id_parent', $aff_user->id)
                             ->update(['id_parent' => $cek_parent_2->id]);

                  Affiliasi::where('id', $cek_parent_1->id_parent)
                             ->update(['child_3' => $perusahaan->child_3 + $child_count]);

                  Affiliasi::where('id', $cek_parent_1->id)
                            ->update(['child_2' => $cek_parent_1->child_2 + $child_count]);

                  Affiliasi::where('id', $cek_parent_2->id)
                           ->update(['child' => $cek_parent_2->child + $child_count]);
                } else {

                  /* Parent 3 */
                  $parent_3   =     Affiliasi::where('id_parent', $parent_2->id)->first();

                  if ($parent_3 != null) {
                    $cek_parent_3   =     Affiliasi::where('id_parent', $parent_2->id)
                                                    ->where('child', '<', $child_count)
                                                    ->first();

                    if ($cek_parent_3 != null) {
                      Affiliasi::where('id_parent', $aff_user->id)
                                 ->update(['id_parent' => $cek_parent_3->id]);

                      Affiliasi::where('id', $cek_parent_1->id_parent)
                                 ->update(['child_4' => $perusahaan->child_4 + $child_count]);

                      Affiliasi::where('id', $cek_parent_1->id)
                                 ->update(['child_3' => $cek_parent_1->child_3 + $child_count]);

                      Affiliasi::where('id', $cek_parent_2->id)
                                ->update(['child_2' => $cek_parent_2->child_2 + $child_count]);

                      Affiliasi::where('id', $cek_parent_3->id)
                                ->update(['child' => $cek_parent_3->child + $child_count]);
                    } else {

                      /* Parent 4 */
                      $parent_4   =     Affiliasi::where('id_parent', $parent_3->id)->first();

                      if ($parent_4 != null) {
                        $cek_parent_4   =     Affiliasi::where('id_parent', $parent_3->id)
                                                        ->where('child', '<', $child_count)
                                                        ->first();

                        if ($cek_parent_4 != null) {
                          Affiliasi::where('id_parent', $aff_user->id)
                                     ->update(['id_parent' => $cek_parent_4->id]);

                          Affiliasi::where('id', $cek_parent_1->id_parent)
                                     ->update(['child_5' => $perusahaan->child_5 + $child_count]);

                          Affiliasi::where('id', $cek_parent_1->id)
                                     ->update(['child_4' => $cek_parent_1->child_4 + $child_count]);

                          Affiliasi::where('id', $cek_parent_2->id)
                                    ->update(['child_3' => $cek_parent_2->child_3 + $child_count]);

                          Affiliasi::where('id', $cek_parent_3->id)
                                    ->update(['child_2' => $cek_parent_3->child_2 + $child_count]);

                          Affiliasi::where('id', $cek_parent_4->id)
                                    ->update(['child' => $cek_parent_4->child + $child_count]);
                        } else {

                          /* Parent 5 */
                          $parent_5   =     Affiliasi::where('id_parent', $parent_4->id)->first();

                          if ($parent_5 != null) {
                            $cek_parent_5   =     Affiliasi::where('id_parent', $parent_4->id)
                                                            ->where('child', '<', $child_count)
                                                            ->first();

                            if ($cek_parent_5 != null) {
                              Affiliasi::where('id_parent', $aff_user->id)
                                         ->update(['id_parent' => $cek_parent_5->id]);

                              Affiliasi::where('id', $cek_parent_1->id_parent)
                                         ->update(['child_6' => $perusahaan->child_6 + $child_count]);

                              Affiliasi::where('id', $cek_parent_1->id)
                                         ->update(['child_5' => $cek_parent_1->child_5 + $child_count]);

                              Affiliasi::where('id', $cek_parent_2->id)
                                        ->update(['child_4' => $cek_parent_2->child_4 + $child_count]);

                              Affiliasi::where('id', $cek_parent_3->id)
                                        ->update(['child_3' => $cek_parent_3->child_3 + $child_count]);

                              Affiliasi::where('id', $cek_parent_4->id)
                                        ->update(['child_2' => $cek_parent_4->child_2 + $child_count]);

                              Affiliasi::where('id', $cek_parent_5->id)
                                        ->update(['child' => $cek_parent_5->child + $child_count]);
                            } else {

                              /* Parent 6 */
                              $parent_6   =     Affiliasi::where('id_parent', $parent_5->id)->first();

                              if ($parent_6 != null) {
                                $cek_parent_6   =     Affiliasi::where('id_parent', $parent_5->id)
                                                                ->where('child', '<', $child_count)
                                                                ->first();

                                if ($cek_parent_6 != null) {
                                  Affiliasi::where('id_parent', $aff_user->id)
                                             ->update(['id_parent' => $cek_parent_6->id]);

                                  Affiliasi::where('id', $cek_parent_1->id_parent)
                                             ->update(['child_7' => $perusahaan->child_7 + $child_count]);

                                  Affiliasi::where('id', $cek_parent_1->id)
                                             ->update(['child_6' => $cek_parent_1->child_6 + $child_count]);

                                  Affiliasi::where('id', $cek_parent_2->id)
                                            ->update(['child_5' => $cek_parent_2->child_5 + $child_count]);

                                  Affiliasi::where('id', $cek_parent_3->id)
                                            ->update(['child_4' => $cek_parent_3->child_4 + $child_count]);

                                  Affiliasi::where('id', $cek_parent_4->id)
                                            ->update(['child_3' => $cek_parent_4->child_3 + $child_count]);

                                  Affiliasi::where('id', $cek_parent_5->id)
                                            ->update(['child_2' => $cek_parent_5->child_2 + $child_count]);

                                  Affiliasi::where('id', $cek_parent_6->id)
                                            ->update(['child' => $cek_parent_6->child + $child_count]);
                                } else {

                                  /* Parent 7 */
                                  $parent_7   =     Affiliasi::where('id_parent', $parent_6->id)->first();

                                  if ($parent_7 != null) {
                                    $cek_parent_7   =     Affiliasi::where('id_parent', $parent_6->id)
                                                                    ->where('child', '<', $child_count)
                                                                    ->first();

                                    if ($cek_parent_7 != null) {
                                      Affiliasi::where('id_parent', $aff_user->id)
                                                 ->update(['id_parent' => $cek_parent_6->id]);

                                      Affiliasi::where('id', $cek_parent_1->id_parent)
                                                 ->update(['child_8' => $perusahaan->child_8 + $child_count]);

                                      Affiliasi::where('id', $cek_parent_1->id)
                                                 ->update(['child_7' => $cek_parent_1->child_7 + $child_count]);

                                      Affiliasi::where('id', $cek_parent_2->id)
                                                ->update(['child_6' => $cek_parent_2->child_6 + $child_count]);

                                      Affiliasi::where('id', $cek_parent_3->id)
                                                ->update(['child_5' => $cek_parent_3->child_5 + $child_count]);

                                      Affiliasi::where('id', $cek_parent_4->id)
                                                ->update(['child_4' => $cek_parent_4->child_4 + $child_count]);

                                      Affiliasi::where('id', $cek_parent_5->id)
                                                ->update(['child_3' => $cek_parent_5->child_3 + $child_count]);

                                      Affiliasi::where('id', $cek_parent_6->id)
                                                ->update(['child_2' => $cek_parent_6->child_2 + $child_count]);

                                      Affiliasi::where('id', $cek_parent_7->id)
                                                ->update(['child' => $cek_parent_7->child + $child_count]);
                                    } else {

                                      /* Parent 8 */
                                      $parent_8   =     Affiliasi::where('id_parent', $parent_7->id)->first();

                                      if ($parent_8 != null) {
                                        $cek_parent_8   =     Affiliasi::where('id_parent', $parent_7->id)
                                                                        ->where('child', '<', $child_count)
                                                                        ->first();

                                        if ($cek_parent_8) {
                                          Affiliasi::where('id_parent', $aff_user->id)
                                                     ->update(['id_parent' => $cek_parent_6->id]);

                                          Affiliasi::where('id', $cek_parent_1->id_parent)
                                                     ->update(['child_9' => $perusahaan->child_9 + $child_count]);

                                          Affiliasi::where('id', $cek_parent_1->id)
                                                     ->update(['child_8' => $cek_parent_1->child_8 + $child_count]);

                                          Affiliasi::where('id', $cek_parent_2->id)
                                                    ->update(['child_7' => $cek_parent_2->child_7 + $child_count]);

                                          Affiliasi::where('id', $cek_parent_3->id)
                                                    ->update(['child_6' => $cek_parent_3->child_6 + $child_count]);

                                          Affiliasi::where('id', $cek_parent_4->id)
                                                    ->update(['child_5' => $cek_parent_4->child_5 + $child_count]);

                                          Affiliasi::where('id', $cek_parent_5->id)
                                                    ->update(['child_4' => $cek_parent_5->child_4 + $child_count]);

                                          Affiliasi::where('id', $cek_parent_6->id)
                                                    ->update(['child_3' => $cek_parent_6->child_3 + $child_count]);

                                          Affiliasi::where('id', $cek_parent_7->id)
                                                    ->update(['child_2' => $cek_parent_7->child_2 + $child_count]);

                                          Affiliasi::where('id', $cek_parent_8->id)
                                                    ->update(['child' => $cek_parent_8->child + $child_count]);
                                        } else {
                                          $parent_9   =     Affiliasi::where('id_parent', $parent_8->id)->first();

                                          if ($parent_9 != null) {
                                            $cek_parent_9   =     Affiliasi::where('id_parent', $parent_8->id)
                                                                            ->where('child', '<', $child_count)
                                                                            ->first();

                                            if ($cek_parent_9 != null) {
                                              Affiliasi::where('id_parent', $aff_user->id)
                                                         ->update(['id_parent' => $cek_parent_6->id]);

                                              Affiliasi::where('id', $cek_parent_1->id_parent)
                                                         ->update(['child_10' => $perusahaan->child_10 + $child_count]);

                                              Affiliasi::where('id', $cek_parent_1->id)
                                                         ->update(['child_9' => $cek_parent_1->child_9 + $child_count]);

                                              Affiliasi::where('id', $cek_parent_2->id)
                                                        ->update(['child_8' => $cek_parent_2->child_8 + $child_count]);

                                              Affiliasi::where('id', $cek_parent_3->id)
                                                        ->update(['child_7' => $cek_parent_3->child_7 + $child_count]);

                                              Affiliasi::where('id', $cek_parent_4->id)
                                                        ->update(['child_6' => $cek_parent_4->child_6 + $child_count]);

                                              Affiliasi::where('id', $cek_parent_5->id)
                                                        ->update(['child_5' => $cek_parent_5->child_5 + $child_count]);

                                              Affiliasi::where('id', $cek_parent_6->id)
                                                        ->update(['child_4' => $cek_parent_6->child_4 + $child_count]);

                                              Affiliasi::where('id', $cek_parent_7->id)
                                                        ->update(['child_3' => $cek_parent_7->child_3 + $child_count]);

                                              Affiliasi::where('id', $cek_parent_8->id)
                                                        ->update(['child_2' => $cek_parent_8->child_2 + $child_count]);

                                              Affiliasi::where('id', $cek_parent_9->id)
                                                        ->update(['child' => $cek_parent_9->child + $child_count]);
                                            } else {
                                              /* Cek terakhir, random parent */
                                              $random_parent    =     Affiliasi::where('child', '<', $child_count)
                                                                                ->first();

                                              Affiliasi::where('id_parent', $aff_user->id)
                                                         ->update(['id_parent' => $random_parent->id]);
                                            }
                                          } else {
                                            /* Cek terakhir, random parent */
                                            $random_parent    =     Affiliasi::where('child', '<', $child_count)
                                                                              ->first();

                                            Affiliasi::where('id_parent', $aff_user->id)
                                                       ->update(['id_parent' => $random_parent->id]);
                                          }
                                        }
                                      } else {
                                        /* Cek terakhir, random parent */
                                        $random_parent    =     Affiliasi::where('child', '<', $child_count)
                                                                          ->first();

                                        Affiliasi::where('id_parent', $aff_user->id)
                                                   ->update(['id_parent' => $random_parent->id]);
                                      }
                                    }
                                  } else {
                                    /* Cek terakhir, random parent */
                                    $random_parent    =     Affiliasi::where('child', '<', $child_count)
                                                                      ->first();

                                    Affiliasi::where('id_parent', $aff_user->id)
                                               ->update(['id_parent' => $random_parent->id]);
                                  }
                                }
                              } else {
                                /* Cek terakhir, random parent */
                                $random_parent    =     Affiliasi::where('child', '<', $child_count)
                                                                  ->first();

                                Affiliasi::where('id_parent', $aff_user->id)
                                           ->update(['id_parent' => $random_parent->id]);
                              }
                            }
                          } else {
                            /* Cek terakhir, random parent */
                            $random_parent    =     Affiliasi::where('child', '<', $child_count)
                                                              ->first();

                            Affiliasi::where('id_parent', $aff_user->id)
                                       ->update(['id_parent' => $random_parent->id]);
                          }
                        }
                      } else {
                        /* Cek terakhir, random parent */
                        $random_parent    =     Affiliasi::where('child', '>', $child_count)
                                                          ->first();

                        Affiliasi::where('id_parent', $aff_user->id)
                                   ->update(['id_parent' => $random_parent->id]);
                      }
                    }
                  } else {
                    /* Cek terakhir, random parent */
                    $random_parent    =     Affiliasi::where('child', '<', $child_count)
                                                      ->first();

                    Affiliasi::where('id_parent', $aff_user->id)
                               ->update(['id_parent' => $random_parent->id]);
                  }
                }
              } else {
                /* Cek terakhir, random parent */
                $random_parent    =     Affiliasi::where('child', '<', $child_count)
                                                  ->first();

                Affiliasi::where('id_parent', $aff_user->id)
                           ->update(['id_parent' => $random_parent->id]);
              }
            }
          } else {
            /* Cek terakhir, random parent */
            $random_parent    =     Affiliasi::where('child', '<', $child_count)
                                              ->first();

            Affiliasi::where('id_parent', $aff_user->id)
                       ->update(['id_parent' => $random_parent->id]);
          }
        }
      } else {
        /* Cek terakhir, random parent */
        $random_parent    =     Affiliasi::where('child', '<', $child_count)
                                          ->first();

        Affiliasi::where('id_parent', $aff_user->id)
                   ->update(['id_parent' => $random_parent->id]);
      }

      /* Hapus Member */
      User::where('id_member', $aff_user->id_member)->delete();

      /* Hapus data affiliasi payout */
      Payout::where('id_member', $aff_user->id)->delete();

      /* Hapus data total affiliasi dan buat laporan affiliasi */
      OutIn::where('member_id', $aff_user->id)->delete();

      $total    =   Total::where('id_member', $aff_user->id)->first();

      OutIn::create([
             'jumlah'     =>      $total->total_affiliasi,
             'type'       =>      'Masuk',
             'milik'      =>      'Perusahaan',
      ]);

      Total::where('id_member', $aff_user->id)->delete();

      Affiliasi::where('id_member', $id)->delete();

      Member::where('id', $id)->delete();

      return redirect()->back()->with('alert', 'Member berhasil dihapus');
    }
}
