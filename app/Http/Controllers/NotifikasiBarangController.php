<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\NotifikasiBarang as Notifikasi;
use App\SupplierOffline as Supplier;
use App\Models\ProductOffline as Product;

use Auth;

class NotifikasiBarangController extends Controller
{
    function index(Request $request) {
		
        $supplier   =   $request->supplier;

    	$data 	    =	Product::where('product_offline.id_toko', Auth::user()->id)
                            ->join('pemasukan_barangs', 'product_offline.barcode', 'pemasukan_barangs.barcode')
                            ->leftJoin('notifikasi_barangs', 'product_offline.id', 'notifikasi_barangs.id_product')
                            ->where('supplier', $supplier)
                            ->select([
                                'product_offline.*', 'notifikasi_barangs.*',
                                'pemasukan_barangs.*', 'notifikasi_barangs.id as id',
                                'product_offline.id as id_p'
                            ]);

        $sup        =   Supplier::where('id_toko', Auth::user()->id)->get();

    	if ($request->q != null) {
    		$data->where('name', 'LIKE', '%' . $request->q . '%');
    	}

    	$datas 	=	$data->get();

    	return view('usahakumart.notifikasi.index', compact('datas', 'sup'));
    }

    function created() {
    	return view('usahakumart.notifikasi.created');
    }

    function store(Request $request) {
    	$this->validate($request, [
    		'id_product' 		=>  'required',
    		'jumlah_minimum'	=>	'required'
    	]);

    	$request->request->add(['id_toko' => Auth::user()->id]);

        $id     =   $request->id;

        if ($id == null) {
            Notifikasi::create($request->except(['_token']));
        } else {
            Notifikasi::where('id', $id)->update($request->except(['_token']));
        }

    	return redirect()->back()->with('alert', 'Notifikasi Barang ' . $request->nama_barang . ' Telah Ditambahkan Dengan Jumlah Minimal ' . $request->jumlah_minimum);
    }

    function edit($id, Request $request) {
    	$data  	= 	Notifikasi::where('notifikasi_barangs.id', $id)
    							->join('product_offline', 'notifikasi_barangs.id_product', 'product_offline.id')
    							->select(['product_offline.*', 'notifikasi_barangs.*', 'notifikasi_barangs.id as id', 'product_offline.id as id_p'])
    							->first();

    	return view('usahakumart.notifikasi.edit', compact('data'));
    }

    function update(Request $request) {
    	$this->validate($request, [
    		'id_product' 		=>  'required',
    		'jumlah_minimum'	=>	'required'
    	]);

    	$request->request->add(['id_toko' => Auth::user()->id]);

    	Notifikasi::where('id', $request->id)
    				->update($request->except(['_token', 'barcode', 'nama_barang']));

    	return redirect()->back()->with('alert', 'Notifikasi Barang ' . $request->nama_barang . ' Telah Ditambahkan Dengan Jumlah Minimal ' . $request->jumlah_minimum);
    }

    function delete($id) {
		$data  	= 	Notifikasi::where('notifikasi_barangs.id', $id)
    							->join('product_offline', 'notifikasi_barangs.id_product', 'product_offline.id')
    							->select(['product_offline.*', 'notifikasi_barangs.*', 'notifikasi_barangs.id as id', 'product_offline.id as id_p'])
    							->first();

    	$data->delete();


    	return redirect()->back()->with('alert', 'Notifikasi Barang ' . $data->name . ' Berhasil Dihapus');
    }

    function count() {
    	return Notifikasi::leftJoin('product_offline', 'notifikasi_barangs.id_product', 'product_offline.id')
    					->where('stok', '<=', \DB::raw('jumlah_minimum'))
    					->count();
    }

    function alert() {
    	$datas 		=	Notifikasi::leftJoin('product_offline', 'notifikasi_barangs.id_product', 'product_offline.id')
    					->where('stok', '<=', \DB::raw('jumlah_minimum'))
    					->get();

    	return view('usahakumart.notifikasi.alert', compact('datas'));
    }
}
