<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Response;

use App\Models\AffiliasiOutIn as Affiliasi;
use App\Models\AffiliasiDetail as Detail;

class LaporanAffiliasiController extends Controller
{
    function index() {


      $masuk_perusahaan   =   Affiliasi::select(\DB::raw('SUM(jumlah) as jumlah,  DATE(created_at) as date'))
                              ->groupBy('date')
                              ->whereMonth('created_at', date('m'))
                              ->whereYear('created_at', date('Y'))
                              ->where('type', 'Masuk')
                              ->where('milik', 'Perusahaan')
                              ->orderBy('date')
                              ->get();

      $masuk_member       =   Affiliasi::select(\DB::raw('SUM(jumlah) as jumlah,  DATE(created_at) as date'))
                              ->groupBy('date')
                              ->whereMonth('created_at', date('m'))
                              ->whereYear('created_at', date('Y'))
                              ->where('type', 'Masuk')
                              ->where('milik', 'Member')
                              ->orderBy('date')
                              ->get();

      $keluar_member      =   Affiliasi::select(\DB::raw('SUM(jumlah) as jumlah,  DATE(created_at) as date'))
                              ->groupBy('date')
                              ->whereMonth('created_at', date('m'))
                              ->whereYear('created_at', date('Y'))
                              ->where('type', 'Keluar')
                              ->where('milik', 'Member')
                              ->orderBy('date')
                              ->get();

      $total_mp           =   Affiliasi::select(\DB::raw('SUM(jumlah) as jumlah,  MONTH(created_at) as date'))
                              ->groupBy('date')
                              ->whereMonth('created_at', date('m'))
                              ->whereYear('created_at', date('Y'))
                              ->where('type', 'Masuk')
                              ->where('milik', 'Perusahaan')
                              ->first();

      $total_mm           =   Affiliasi::select(\DB::raw('SUM(jumlah) as jumlah,  MONTH(created_at) as date'))
                              ->groupBy('date')
                              ->whereMonth('created_at', date('m'))
                              ->whereYear('created_at', date('Y'))
                              ->where('type', 'Masuk')
                              ->where('milik', 'Member')
                              ->first();

      $total_km           =   Affiliasi::select(\DB::raw('SUM(jumlah) as jumlah,  MONTH(created_at) as date'))
                              ->groupBy('date')
                              ->whereMonth('created_at', date('m'))
                              ->whereYear('created_at', date('Y'))
                              ->where('type', 'Keluar')
                              ->where('milik', 'Member')
                              ->first();

      $total              =   Detail::where('id', 1)->first();

      return view('usahakumart.affiliasi.index', compact('masuk_perusahaan', 'masuk_member', 'keluar_member', 'total_mp', 'total_mm', 'total_km', 'total'));
    }

    function api(Request $request) {
      $id   =   $request->id;

      $data =   Affiliasi::whereDate('created_at', $id)->get();

      return Response::json($data);
    }

    function searchApi(Request $request) {
      $data   =   Affiliasi::where('type', $request->type)
                  ->where('milik', $request->pemilik)
                  ->select(\DB::raw('SUM(jumlah) as jumlah,  DATE(created_at) as date'))
                  ->groupBy('date')
                  ->whereMonth('created_at', $request->bulan)
                  ->whereYear('created_at', $request->tahun)
                  ->get();

      $count  =   Affiliasi::select(\DB::raw('SUM(jumlah) as jumlah,  MONTH(created_at) as date'))
                  ->groupBy('date')
                  ->whereMonth('created_at', $request->bulan)
                  ->whereYear('created_at', $request->tahun)
                  ->where('type', $request->type)
                  ->where('milik', $request->pemilik)
                  ->first();

      return Response::json([$data, $count]);
    }

    function affPrint(Request $request) {
      $dates = explode('-', $request->to);
      $month = $dates[1];
      $year = $dates[0];
      $type = $request->type;
      $pemilik = $request->pemilik;

      $data   =   Affiliasi::where('type', $type)
                  ->where('milik', $pemilik)
                  ->select(\DB::raw('SUM(jumlah) as jumlah,  DATE(created_at) as date'))
                  ->groupBy('date')
                  ->whereMonth('created_at', $month)
                  ->whereYear('created_at', $year)
                  ->get();

      $count  =   Affiliasi::select(\DB::raw('SUM(jumlah) as jumlah,  MONTH(created_at) as date'))
                  ->groupBy('date')
                  ->whereMonth('created_at', $month)
                  ->whereYear('created_at', $year)
                  ->where('type', $type)
                  ->where('milik', $pemilik)
                  ->first();
      
      return view('usahakumart.affiliasi.print', compact('data', 'count', 'type', 'pemilik', 'month', 'year'));
    }
}
