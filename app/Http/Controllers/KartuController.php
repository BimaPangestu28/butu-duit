<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use PDF;
use DNS1D;
use DNS2D;

class KartuController extends Controller
{
    function cetak() {
    	return view('administrator.pages.toko.cetak_kartu');
    }

    function cetakKartu(Request $request) {
    	$arr 		=	[];
        $range      =   range($request->mulai, $request->berakhir);
    	$submit 	=	$request->submit;

    	for ($i=$request->mulai; $i <= $request->berakhir; $i++) { 
    		array_push($arr, '<img src="data:image/png;base64,' . DNS1D::getBarcodePNG($i, "C128") . '">');
    	}

        $no =   1;

    	$pdf    	=   PDF::loadView('administrator.pages.toko.id_card', compact('arr', 'range', 'no', 'submit'))->setPaper('a4', 'potrait');

        if ($submit == 'Cetak Kartu Depan') {
            return $pdf->download('cetak_kartu_depan_' . $request->mulai . '_sampai_' . $request->berakhir . '.pdf');
        } else {
            return $pdf->download('cetak_kartu_belakang_' . $request->mulai . '_sampai_' . $request->berakhir . '.pdf');
        }
		return view('administrator.pages.toko.id_card', compact('arr', 'range', 'no', 'submit'));
    }

    static function barcode($number) {
        return '<img src="data:image/png;base64,' . DNS1D::getBarcodePNG($number, "C128") . '" height="30" width="160" alt="barcode" style="margin-top: 5px; ">';
    }
}
