<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use PDF;
use App\PemasukanBarang;
use App\KasToko;
use App\UserToko;
use App\Models\OrderOffline as History;
use \Carbon\Carbon;

use Auth;
use DateTime;
use DateInterval;
use DatePeriod;

class LaporanKasController extends Controller
{
    function index(Request $request) {

    	$kasMasuk   = kasToko::where('id_toko', Auth::user()->id)
                                    ->where('tipe', 1)->get();

        $kasKeluar  = kasToko::where('id_toko', Auth::user()->id)
                                    ->where('tipe', 2)->get();

        $kasMasukM  = PemasukanBarang::where('tipe', 'kredit')
        						     ->where('status_pembayaran', 0)
        						     ->distinct()
        							 ->orderBy('id', 'DESC')
        							 ->select(['no_transaksi', 'supplier'])
        							 ->get();

        $kasKeluarM = kasToko::where('id_toko', Auth::user()->id)
									->where('tipe', 4)->get();
									
		$totalJualan = History::where('id_toko', Auth::user()->id)
									->sum('total_pembayaran');

        $totalSupplierHarga = PemasukanBarang::where('id_toko', Auth::user()->id)
                                    ->where('tipe', 'Tunai')
                                    ->orWhere('tipe', 'Titipan')
									->sum('total_harga');
									
		$totalMasuk = $totalJualan + $kasMasuk->sum('total');
		$totalKeluar = $totalSupplierHarga + $kasKeluar->sum('total');
		$totalAkhir = $totalMasuk - $totalKeluar;

        return view('usahakumart.laporan.halamanKas', compact('kasMasuk', 'kasMasukM', 'kasKeluar', 'kasKeluarM', 'totalMasuk', 'totalKeluar', 'totalAkhir', 'totalJualan', 'totalSupplierHarga'));
    }

    static function total($no) {
    	$return 	=	0;

    	$t 			=	PemasukanBarang::where('no_transaksi', $no)->where('status_pembayaran', 0)
    									->where('tipe', 'Kredit')->get();

    	foreach ($t as $q) {
    		$return += $q->total_harga;
    	}

    	return $return;
    }

    static function data($no) {
    	$t 			=	PemasukanBarang::where('no_transaksi', $no)->where('status_pembayaran', 0)
    									->where('tipe', 'Kredit')
    									->where('created_at', '!=', null)
    									->first();    	

  		return $t;
    }

    function kasKonfirmasi($id, Request $request) {
    	PemasukanBarang::where('no_transaksi', $id)->where('supplier', $request->supplier)
    					 ->where('status_pembayaran', 0)->where('tipe', 'Kredit')
    					 ->update(['status_pembayaran' => 1]);

    	return redirect()->back()->with('alert', 'Berhasil Konfirmasi Pembayaran');
	}
	
	function kasTambah()
    {
        $kasLists = kasToko::where('id_toko', Auth::user()->id);
        return view('usahakumart.laporan.halamanKasTambah', compact('kasLists'));
    }

    function kasStore(Request $request)
    {
        $this->validate($request, [
        	'jenis'		    	=>	'required',
        	'ke_kas'			=>	'required',
        	'keterangan'		=>	'required',
        	'total'     		=>	'required',
        	'tipe'				=>	'required',
        ]);

        $kas 				= new kasToko;
        $kas->id_toko		= Auth::user()->id;
        $kas->no_transaksi 	= $request->input('no_transaksi');
        $kas->jenis 		= $request->input('jenis');
        $kas->ke_kas 		= $request->input('ke_kas');
        $kas->keterangan    = $request->input('keterangan');
        $kas->total 	    = $request->input('total');
        $kas->tipe 			= $request->input('tipe');

        $kas->save();

        return redirect('/cashier/laporan/kas')->with('alert', 'Data berhasil ditambahkan');
    }

    function deleteKas($id)
    {
        $kas = kasToko::where('id', $id)
                        ->where('id_toko', Auth::user()->id)->first();
        $kas->delete();

        return redirect()->back()->with('alert', 'Data Berhasil Dihapus');
    }

    function editKas($id)
    {
        $kas = kasToko::where('id', $id)
                        ->where('id_toko', Auth::user()->id)->first();
        return view('usahakumart.laporan.halamanKasEdit', compact('kas'));
    }

    function updateKas(Request $request, $id) {
        $kas 				= kasToko::where('id', $id)
                                    ->where('id_toko', Auth::user()->id)
                                    ->first();

        $kas->no_transaksi 	= $request->input('no_transaksi');
        $kas->jenis 		= $request->input('jenis');
        $kas->ke_kas 		= $request->input('ke_kas');
        $kas->keterangan    = $request->input('keterangan');
        $kas->total 	    = $request->input('total');
        $kas->tipe 			= $request->input('tipe');

        $kas->update();
        return redirect('/cashier/laporan/kas')->with('alert', 'Data berhasil update');
    }

    function kasPrint(Request $request) {
        if ($request->data == 'masuk') {
            $from = Carbon::parse($request->from)->startOfDay();
            $to = Carbon::parse($request->to)->endOfDay();

            $kases   = kasToko::where('id_toko', Auth::user()->id)
                                    ->whereBetween('created_at', [$from, $to])
                                        ->where('tipe', 1)->get();

                                        
            $totalJualan = History::where('id_toko', Auth::user()->id)
                                        ->whereBetween('created_at', [$from, $to])
                                        ->sum('total_pembayaran');
            
            $pdf    =   PDF::loadView('laporan.laporan_kas', compact('kases', 'totalJualan', 'totalSupplierHarga'))
                    ->setPaper('a4', 'landscape');

            return $pdf->download('laporan_pemasukan_' . date('d-m-Y', strtotime($from)) .'_'. date('d-m-Y', strtotime($to)) . '.pdf');

        }else if ($request->data == 'keluar') {
            $from = Carbon::parse($request->from)->startOfDay();
            $to = Carbon::parse($request->to)->endOfDay();

            $kases   = kasToko::where('id_toko', Auth::user()->id)
                                    ->whereBetween('created_at', [$from, $to])
                                        ->where('tipe', 2)->get();


            $totalSupplierHarga = PemasukanBarang::where('id_toko', Auth::user()->id)
                                        ->whereBetween('created_at', [$from, $to])
                                        ->sum('total_harga');
            $pdf    =   PDF::loadView('laporan.laporan_kas', compact('kases', 'totalJualan', 'totalSupplierHarga'))
                        ->setPaper('a4', 'landscape');

            return $pdf->download('laporan_pengeluaran_' . date('d-m-Y', strtotime($from)) .'_'. date('d-m-Y', strtotime($to)) . '.pdf');                                        
        }

        // return view('laporan.laporan_kas', compact('kases', 'totalJualan', 'totalSupplierHarga'));
    }

    function kasSearch(Request $request) {

		
		$from = Carbon::parse($request->from)->startOfDay();
		$to = Carbon::parse($request->to)->endOfDay();

		$kasMasuk   = kasToko::where('id_toko', Auth::user()->id)
								->whereBetween('created_at', [$from, $to])
                                    ->where('tipe', 1)->get();

		$kasKeluar  = kasToko::where('id_toko', Auth::user()->id)
								->whereBetween('created_at', [$from, $to])
                                    ->where('tipe', 2)->get();

        $kasMasukNoDate   = kasToko::where('id_toko', Auth::user()->id)
                            ->where('tipe', 1)->get();

        $kasKeluarNoDate  = kasToko::where('id_toko', Auth::user()->id)
                            ->where('tipe', 2)->get();

        $kasMasukM  = PemasukanBarang::where('tipe', 'kredit')
        						     ->where('status_pembayaran', 0)
        						     ->distinct()
        							 ->orderBy('id', 'DESC')
        							 ->select(['no_transaksi', 'supplier'])
        							 ->get();

        $kasKeluarM = kasToko::where('id_toko', Auth::user()->id)
									->where('tipe', 4)->get();
									
		$totalJualan = History::where('id_toko', Auth::user()->id)
									->sum('total_pembayaran');

		$totalSupplierHarga = PemasukanBarang::where('id_toko', Auth::user()->id)
                                    ->where('tipe', 'Tunai')
                                    ->orWhere('tipe', 'Titipan')
									->sum('total_harga');
									
		$totalMasuk = $totalJualan + $kasMasukNoDate->sum('total');
		$totalKeluar = $totalSupplierHarga + $kasKeluarNoDate->sum('total');
		$totalAkhir = $totalMasuk - $totalKeluar;

        return view('usahakumart.laporan.halamanKas', compact('kasMasuk', 'kasMasukM', 'kasKeluar', 'kasKeluarM', 'totalMasuk', 'totalKeluar', 'totalAkhir', 'totalJualan', 'totalSupplierHarga'));
    }

    function backOffice(Request $request) {

        $start_date = $request->start;
        $end_date   = date('Y-m-d', strtotime($request->end . '+1 day'));

        $start      = new DateTime($start_date);
        $end        = new DateTime($end_date);
        $interval   = new DateInterval('P1D'); // 1 day interval
        $period     = new DatePeriod($start, $interval, $end);

        $laba             = 0;
        $pendapatan       = 0;
        $hpp              = 0;
        $potongan         = 0;
        $affiliasi        = 0;

        $no         = 0;

        $user       = UserToko::where('level', 3)->get();

        return view('usahakumart.laporan.backOffice', compact('period', 'laba', 'no', 'user', 'pendapatan', 'hpp', 'affiliasi', 'potongan'));
    }

    static function kasDate($date) {
        return KasToko::whereDate('created_at', $date)->where('id_toko', Auth::user()->id)->get();
    }

    function printPeriode(Request $request) {
        $start_date = $request->start;
        $end_date   = date('Y-m-d', strtotime($request->end . '+1 day'));

        $start      = new DateTime($start_date);
        $end        = new DateTime($end_date);
        $interval   = new DateInterval('P1D'); // 1 day interval
        $period     = new DatePeriod($start, $interval, $end);

        $laba             = 0;
        $pendapatan       = 0;
        $hpp              = 0;
        $potongan         = 0;
        $affiliasi        = 0;

        $no         = 0;

        $user       = UserToko::where('level', 3)->get();

        $pdf    =   PDF::loadView('laporan.laporan_back_office', compact('period', 'laba', 'no', 'user', 'pendapatan', 'hpp', 'affiliasi', 'potongan', 'start_date', 'end_date'))
                        ->setPaper('a4', 'landscape');

                        //dd($pdf);

        return $pdf->download('laporan_back_office_'. date('d-m-Y', strtotime($start_date)) . '_sampai_' . date('d-m-Y', strtotime($end_date)) . '.pdf');

        //return view('laporan.laporan_back_office', compact('period', 'laba', 'no', 'user', 'pendapatan', 'hpp', 'affiliasi', 'potongan', 'start_date', 'end_date'));
    }
}
