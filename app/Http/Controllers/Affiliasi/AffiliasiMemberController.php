<?php

namespace App\Http\Controllers\Affiliasi;

use Illuminate\Http\Request;
use \Carbon\Carbon;
use Auth;

use App\Models\NumberCard as Card;
use App\Models\AffiliasiMember as Member;
use App\Models\Affiliasi as Aff;
use App\Models\Member as User;
use App\Models\AffiliasiTotal as Total;
use App\Models\SaldoUser;

class AffiliasiMemberController extends Controller
{
    function register() {


    	$card 		=	Card::where('status', 1)->where('id_toko', Auth::user()->id)->get();

    	$user 		=	Member::get();

    	$arr   		=	[];

    	foreach ($user as $key) {
    		array_push($arr, $key->id_member);
    	}

    	if (count($arr) > 0) {
    		$member		=	User::whereNotIn('id_member', $arr)->get();
    	}

    	return view('register', compact('card', 'member'));
    }

    function postRegister(Request $request) {
    	$this->validate($request, [
    		'nama'				=>	'required',
    		'tempat_lahir'		=>	'required',
    		'telepon'			=>	'required',
    	]);

    	//Cek Jika Tidak Memiliki Code Invitation Maka Otomatis Akan Terisi Code Affiliasi Milik Usahaku
    	if ($request->code_invitation == null) {
    		$pemilik 				  = User::where('id_member', Auth::user()->id_pemilik)->first();
    		$usahaku 				  =	Member::where('id_member', $pemilik->id_member)->first();

			  $request->code_invitation = $usahaku->code_affiliasi;
    	}

    	//Ganti Status Kartu Menjadi Sudah Terpakai
    	Card::where('number_card', $request->number_card)->update(['status' => 0]);

    	//Input Data Member Baru Kecuali Code Affiliasi
    	$request->request->add(['status' => 1]);

		if ($request->id_member == "") {
			$user 		=	User::create([
      							'nama'				      => 	$request->nama,
      							'jenis_kelamin'		  => 	$request->jenis_kelamin,
      							'telepon'			      =>	$request->telepon,
      							'username'			    =>	$request->number_card,
      							'password'			    =>  bcrypt(str_replace("-", "", date('dmY', strtotime($request->tanggal_lahir)))),
      							'id_com_identitas'	=> 28,
      							'tgl_lahir'			    => $request->tanggal_lahir,
      							'last_login'		    => now()
      						]);

			$request->request->add([
				'id_member'	=> $user->id
			]);
		} else {
			$request->request->add([
				'id_member'	=> $request->id_member
			]);
		}

		$member 	=	Member::create($request->except([
							'_token', 'code_invitation', 'submit', 'telepon', 'jenis_kelamin'
						]));

		SaldoUser::create(['id_member' => $member->id_member]);

    	//Input Data Affiliasi Total
    	Total::create(['id_member' => $member->id, 'total_affiliasi' => 0]);

    	//Generate Code Affiliasi Berdasrkan Waktu Mendaftar Dan ID Member
    	//$code 		=	$member->id . date('mhsd');
    	$code 			=	$request->number_card;
    	Member::where('id', $member->id)->update(['code_affiliasi' => $code]);

    	//Mendapatkan ID Member Yang Memiliki Code Invitation
    	$invitation =	Member::where('code_affiliasi', $request->code_invitation)->first();

    	//Mendapatkan Affiliasi ID Parent Dari ID Member Yang Telah Didaptkan
    	$parent 	=	Aff::where('id_member', $invitation->id)->first();

    	/*Masuk Ke Dalam Dunia Affiliasi*/
    	//Menghitung Jumlah Column
    	$cek_column	=	Aff::where('id_parent', $parent->id)->count();

    	//Cek Apakah Column Sudah Mencapai 10 Atau Belum
    	if ($cek_column >= 10) {
			//Mendapatkan Column Child Dari Parent
			$child 		=	Aff::where('id_parent', $parent->id)->where('child', '<', 10)->first();

			//Cek Apakah Column Sudah Mencapai 10 Atau Belum
			if ($child == null) {
				$children	=	Aff::where('id_parent', $parent->id)->where('child', '>=', 10)
								->where('child_2', '<', 100)->first();

				if ($children == null) {
					$children2	=	Aff::where('id_parent', $parent->id)->where('child', '>=', 10)
									->where('child_2', '>=', 100)
									->where('child_3', '<', 1000)->first();
					if ($children2 == null) {
					$children3	=	Aff::where('id_parent', $parent->id)->where('child', '>=', 10)
									->where('child_2', '>=', 100)
									->where('child_3', '>=', 1000)
									->where('child_4', '<', 10000)->first();
					if ($children3 == null) {
					$children4	=	Aff::where('id_parent', $parent->id)->where('child', '>=', 10)
									->where('child_2', '>=', 100)
									->where('child_3', '>=', 1000)
									->where('child_4', '>=', 10000)
									->where('child_5', '<', 100000)->first();
					if ($children4 == null) {
					$children5	=	Aff::where('id_parent', $parent->id)->where('child', '>=', 10)
									->where('child_2', '>=', 100)
									->where('child_3', '>=', 1000)
									->where('child_4', '>=', 10000)
									->where('child_5', '>=', 100000)
									->where('child_6', '<', 1000000)->first();
					if ($children5 == null) {
					$children6	=	Aff::where('id_parent', $parent->id)->where('child', '>=', 10)
									->where('child_2', '>=', 100)
									->where('child_3', '>=', 1000)
									->where('child_4', '>=', 10000)
									->where('child_5', '>=', 100000)
									->where('child_6', '>=', 1000000)
									->where('child_7', '<', 10000000)->first();
					if ($children6 == null) {
					$children7	=	Aff::where('id_parent', $parent->id)->where('child', '>=', 10)
									->where('child_2', '>=', 100)
									->where('child_3', '>=', 1000)
									->where('child_4', '>=', 10000)
									->where('child_5', '>=', 100000)
									->where('child_6', '>=', 1000000)
									->where('child_7', '>=', 10000000)
									->where('child_8', '<', 100000000)->first();
					if ($children7 == null) {
					$children8	=	Aff::where('id_parent', $parent->id)->where('child', '>=', 10)
									->where('child_2', '>=', 100)
									->where('child_3', '>=', 1000)
									->where('child_4', '>=', 10000)
									->where('child_5', '>=', 100000)
									->where('child_6', '>=', 1000000)
									->where('child_7', '>=', 10000000)
									->where('child_8', '>=', 100000000)
									->where('child_9', '<', 1000000000)->first();

					if ($children8 == null) {
					$children9	=	Aff::where('id_parent', $parent->id)->where('child', '>=', 10)
									->where('child_2', '>=', 100)
									->where('child_3', '>=', 1000)
									->where('child_4', '>=', 10000)
									->where('child_5', '>=', 100000)
									->where('child_6', '>=', 1000000)
									->where('child_7', '>=', 10000000)
									->where('child_8', '>=', 100000000)
									->where('child_9', '>=', 1000000000)
									->where('child_10', '<', 10000000000)->first();
					if ($children9 == null) {
						dd('Maksimal');
					} else {
						$children_10	=	Aff::where('id_parent', $children9->id)
											->where('child_9', '<', 1000000000)
											->first();

						$kids_10	 	=	Aff::where('id_parent', $children_10->id)
											->where('child_8', '<', 100000000)
											->first();

						$kid_10 	  	=	Aff::where('id_parent', $kids_10->id)
											->where('child_7', '<', 10000000)
											->first();

						$anak_10		=	Aff::where('id_parent', $kid_10->id)
											->where('child_6', '<', 1000000)
											->first();

						$anaks_10		=	Aff::where('id_parent', $anak_10->id)
											->where('child_5', '<', 100000)
											->first();

						$boys_10		=	Aff::where('id_parent', $anaks_10->id)
											->where('child_4', '<', 10000)
											->first();

						$boy_10 		=	Aff::where('id_parent', $boys_10->id)
											->where('child_3', '<', 1000)
											->first();

						$last 			=	Aff::where('id_parent', $boy_10->id)
											->where('child_2', '<', 100)
											->first();

						$child_10 		=	Aff::where('id_parent', $last->id)
											->where('child', '<', 10)->first();

						//Memasukkan Data Affiliasi
						/* Generation #10 */
						Aff::create([
							'id_parent' 	  => $child_10->id,
			    			'id_member' 	  => $member->id,
			    			'code_invitation' => $request->code_invitation,
			    			'child'			  => 0
						]);

						Aff::where('id', $children9->id)->increment('child_10');
						Aff::where('id', $children_10->id)->increment('child_9');
						Aff::where('id', $kids_10->id)->increment('child_8');
						Aff::where('id', $kid_10->id)->increment('child_7');
						Aff::where('id', $anak_10->id)->increment('child_6');
						Aff::where('id', $anaks_10->id)->increment('child_5');
						Aff::where('id', $boys_10->id)->increment('child_4');
						Aff::where('id', $boy_10->id)->increment('child_3');
						Aff::where('id', $last->id)->increment('child_2');
						Aff::where('id', $child_10->id)->increment('child');
					}
					} else {
						$children_9	=	Aff::where('id_parent', $children8->id)
										->where('child_8', '<', 100000000)
										->first();

						$kids_9 	=	Aff::where('id_parent', $children_9->id)
										->where('child_7', '<', 10000000)
										->first();

						$kid_9  	=	Aff::where('id_parent', $kids_9->id)
										->where('child_6', '<', 1000000)
										->first();

						$anak_9		=	Aff::where('id_parent', $kid_9->id)
										->where('child_5', '<', 100000)
										->first();

						$anaks_9	=	Aff::where('id_parent', $anak_9->id)
										->where('child_4', '<', 10000)
										->first();

						$boys_9		=	Aff::where('id_parent', $anaks_9->id)
										->where('child_3', '<', 1000)
										->first();

						$boy_9		=	Aff::where('id_parent', $boys_9->id)
										->where('child_2', '<', 100)
										->first();

						$child_9 	=	Aff::where('id_parent', $boy_9->id)->where('child', '<', 10)
										->first();

						//Memasukkan Data Affiliasi
						/* Generation #9 */
						Aff::create([
							'id_parent' 	  => $child_9->id,
			    			'id_member' 	  => $member->id,
			    			'code_invitation' => $request->code_invitation,
			    			'child'			  => 0
						]);

						Aff::where('id', $parent->id)->increment('child_10');
						Aff::where('id', $children8->id)->increment('child_9');
						Aff::where('id', $children_9->id)->increment('child_8');
						Aff::where('id', $kids_9->id)->increment('child_7');
						Aff::where('id', $kid_9->id)->increment('child_6');
						Aff::where('id', $anak_9->id)->increment('child_5');
						Aff::where('id', $anaks_9->id)->increment('child_4');
						Aff::where('id', $boys_9->id)->increment('child_3');
						Aff::where('id', $boy_9->id)->increment('child_2');
						Aff::where('id', $child_9->id)->increment('child');
					}
					} else {
						$children_8	=	Aff::where('id_parent', $children7->id)
										->where('child_7', '<', 10000000)
										->first();

						$kids_8 	=	Aff::where('id_parent', $children_8->id)
										->where('child_6', '<', 1000000)
										->first();

						$kid_8  	=	Aff::where('id_parent', $kids_8->id)
										->where('child_5', '<', 100000)
										->first();

						$anak_8		=	Aff::where('id_parent', $kid_8->id)->where('child_4', '<', 10000)
										->first();

						$anaks_8	=	Aff::where('id_parent', $anak_8->id)->where('child_3', '<', 1000)
										->first();

						$boys_8		=	Aff::where('id_parent', $anaks_8->id)->where('child_2', '<', 100)
										->first();

						$child_8 	=	Aff::where('id_parent', $boys_8->id)->where('child', '<', 10)
										->first();

						//Memasukkan Data Affiliasi
						/* Generation #9 */
						Aff::create([
							'id_parent' 	  => $child_8->id,
			    			'id_member' 	  => $member->id,
			    			'code_invitation' => $request->code_invitation,
			    			'child'			  => 0
						]);

						Aff::where('id', $parent->id)->increment('child_9');
						Aff::where('id', $children7->id)->increment('child_8');
						Aff::where('id', $children_8->id)->increment('child_7');
						Aff::where('id', $kids_8->id)->increment('child_6');
						Aff::where('id', $kid_8->id)->increment('child_5');
						Aff::where('id', $anak_8->id)->increment('child_4');
						Aff::where('id', $anaks_8->id)->increment('child_3');
						Aff::where('id', $boys_8->id)->increment('child_2');
						Aff::where('id', $child_8->id)->increment('child');
					}
					} else {
						$children_7	=	Aff::where('id_parent', $children6->id)
										->where('child_6', '<', 1000000)
										->first();

						$kids_7 	=	Aff::where('id_parent', $children_7->id)
										->where('child_5', '<', 100000)
										->first();

						$kid_7  	=	Aff::where('id_parent', $kids_7->id)
										->where('child_4', '<', 10000)
										->first();

						$anak_7		=	Aff::where('id_parent', $kid_7->id)->where('child_3', '<', 1000)
										->first();

						$anaks_7	=	Aff::where('id_parent', $anak_7->id)->where('child_2', '<', 100)
										->first();

						$child_7 	=	Aff::where('id_parent', $anaks_7->id)->where('child', '<', 10)
										->first();

						//Memasukkan Data Affiliasi
						/* Generation #8 */
						Aff::create([
							'id_parent' 	  => $child_7->id,
			    			'id_member' 	  => $member->id,
			    			'code_invitation' => $request->code_invitation,
			    			'child'			  => 0
						]);

						Aff::where('id', $parent->id)->increment('child_8');
						Aff::where('id', $children6->id)->increment('child_7');
						Aff::where('id', $children_7->id)->increment('child_6');
						Aff::where('id', $kids_7->id)->increment('child_5');
						Aff::where('id', $kid_7->id)->increment('child_4');
						Aff::where('id', $anak_7->id)->increment('child_3');
						Aff::where('id', $anaks_7->id)->increment('child_2');
						Aff::where('id', $child_7->id)->increment('child');
					}
					} else {
						$children_6	=	Aff::where('id_parent', $children5->id)->where('child_5', '<', 100000)
										->first();

						$kids_6 	=	Aff::where('id_parent', $children_6->id)->where('child_4', '<', 10000)
										->first();

						$kid_6  	=	Aff::where('id_parent', $kids_6->id)->where('child_3', '<', 1000)
										->first();

						$anak_6		=	Aff::where('id_parent', $kid_6->id)->where('child_2', '<', 100)
										->first();

						$child_6 	=	Aff::where('id_parent', $anak_6->id)->where('child', '<', 10)
										->first();

						//Memasukkan Data Affiliasi
						/* Generation #7 */
						Aff::create([
							'id_parent' 	  => $child_6->id,
			    			'id_member' 	  => $member->id,
			    			'code_invitation' => $request->code_invitation,
			    			'child'			  => 0
						]);

						Aff::where('id', $parent->id)->increment('child_7');
						Aff::where('id', $children5->id)->increment('child_6');
						Aff::where('id', $children_6->id)->increment('child_5');
						Aff::where('id', $kids_6->id)->increment('child_4');
						Aff::where('id', $kid_6->id)->increment('child_3');
						Aff::where('id', $anak_6->id)->increment('child_2');
						Aff::where('id', $child_6->id)->increment('child');
					}
					} else {
						$children_5	=	Aff::where('id_parent', $children4->id)->where('child_4', '<', 10000)
										->first();

						$kids_5 	=	Aff::where('id_parent', $children_5->id)->where('child_3', '<', 1000)
										->first();

						$kid_5  	=	Aff::where('id_parent', $kids_5->id)->where('child_2', '<', 100)
										->first();

						$child_5 	=	Aff::where('id_parent', $kid_5->id)->where('child', '<', 10)
										->first();

						//Memasukkan Data Affiliasi
						/* Generation #6 */
						Aff::create([
							'id_parent' 	  => $child_5->id,
			    			'id_member' 	  => $member->id,
			    			'code_invitation' => $request->code_invitation,
			    			'child'			  => 0
						]);

						Aff::where('id', $parent->id)->increment('child_6');
						Aff::where('id', $children4->id)->increment('child_5');
						Aff::where('id', $children_5->id)->increment('child_4');
						Aff::where('id', $kids_5->id)->increment('child_3');
						Aff::where('id', $kid_5->id)->increment('child_2');
						Aff::where('id', $child_5->id)->increment('child');
					}
					} else {
						$children_4	=	Aff::where('id_parent', $children3->id)->where('child_3', '<', 1000)
										->first();

						$kids_4 	=	Aff::where('id_parent', $children_4->id)->where('child_2', '<', 100)
										->first();

						$child_4 	=	Aff::where('id_parent', $kids_4->id)->where('child', '<', 10)
										->first();

						//Memasukkan Data Affiliasi
						/* Generation #5 */
						Aff::create([
							'id_parent' 	  => $child_4->id,
			    			'id_member' 	  => $member->id,
			    			'code_invitation' => $request->code_invitation,
			    			'child'			  => 0
						]);

						Aff::where('id', $parent->id)->increment('child_5');
						Aff::where('id', $children3->id)->increment('child_4');
						Aff::where('id', $children_4->id)->increment('child_3');
						Aff::where('id', $kids_4->id)->increment('child_2');
						Aff::where('id', $child_4->id)->increment('child');
					}
					} else {
						$children_3	=	Aff::where('id_parent', $children2->id)->where('child_2', '<', 100)
										->first();

						$child_3 	=	Aff::where('id_parent', $children_3->id)->where('child', '<', 10)
										->first();

						//Memasukkan Data Affiliasi
						/* Generation #4 */
						Aff::create([
							'id_parent' 	  => $child_3->id,
			    			'id_member' 	  => $member->id,
			    			'code_invitation' => $request->code_invitation,
			    			'child'			  => 0
						]);

						Aff::where('id', $parent->id)->increment('child_4');
						Aff::where('id', $children2->id)->increment('child_3');
						Aff::where('id', $children_3->id)->increment('child_2');
						Aff::where('id', $child_3->id)->increment('child');
					}
				} else {
					$child_2	=	Aff::where('id_parent', $children->id)->where('child', '<', 10)->first();

					//Memasukkan Data Affiliasi
					/* Generation #3 */
					Aff::create([
						'id_parent' 	  => $child_2->id,
		    			'id_member' 	  => $member->id,
		    			'code_invitation' => $request->code_invitation,
		    			'child'			  => 0
					]);

					Aff::where('id', $parent->id)->increment('child_3');
					Aff::where('id', $children->id)->increment('child_2');
					Aff::where('id', $child_2->id)->increment('child');
				}
			} else {
				//Memasukkan Data Affiliasi
				/* Generation #2 */
				Aff::create([
					'id_parent' 	  => $child->id,
	    			'id_member' 	  => $member->id,
	    			'code_invitation' => $request->code_invitation,
	    			'child'			  => 0
				]);

				Aff::where('id', $child->id)->increment('child');
				Aff::where('id', $parent->id)->increment('child_2');
			}
    	} else {
    		//Memasukkan Data Affiliasi
    		/* Generation #1 */
    		Aff::create([
    			'id_parent' 	  => $parent->id,
    			'id_member' 	  => $member->id,
    			'code_invitation' => $request->code_invitation,
    			'child'			  => 0
    		]);

    		Aff::where('id', $parent->id)->increment('child');
    	}


    	return redirect()->back()->with('alert', 'Member Berhasil Ditambahkan');
    }
}
