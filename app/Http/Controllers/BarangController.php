<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Models\ProductOffline as Product;
use App\Models\TokoOffline as Toko;
use App\Models\HistoryPemasukanBarang as History;
use DB;
use App\PemasukanBarang;
use App\Category;

class BarangController extends Controller
{
    function tambah(Request $request) {
        

        $t          = Toko::where('id', Auth::user()->id)->first();

        /*$barang     = PemasukanBarang::where('id_toko', Auth::user()->id)->get();

        $arr        = [];

        foreach ($barang as $b) {
            if (!in_array($b->produk, $arr)) {
                array_push($arr, $b->produk);
            }
        }*/

        $cat    =   Category::orderBy('category', 'ASC')->get();

        if ($t->permission == 0) {
            return redirect('/cashier/gudang-barang')->with('alert', 'Anda Harus Mendapatkan Izin Dari Administrator Gudang, Silahkan Hubungi Administrator Gudang Untuk Mendapatkan Izin Terlebih Dahulu');
        }

        if ($request->produk == null) {
            return view('usahakumart/pages/tambah_barang', compact('cat'));
        } else {
            $product    =   PemasukanBarang::where('id', $request->produk)->first();

            return view('usahakumart/pages/tambah_barang_update', compact('cat', 'product'));
        }
    }

    function apiBarang(Request $request) {
        return PemasukanBarang::where('produk', $request->name)->orderBy('id', 'DESC')->first();
    }

    function gudangApi(Request $request) {
        $arr        =   [];

        //dd($request->supplier);

        $supp       =   PemasukanBarang::where('supplier', $request->supplier)->get();
        //$supp       =   PemasukanBarang::get();

        foreach ($supp as $key) {
            array_push($arr, $key->produk);
        }

        $product    =   Product::where('id_toko', Auth::user()->id)
                        ->where(function($query) use($arr) {
                            for ($i=0; $i < count($arr); $i++) {
                                $query->orWhere('name', 'LIKE', '%' . $arr[$i] . '%')
                                    ->orWhere('name', $arr[$i]);
                            }
                        })->get();

        //$product    =   Product::select('name')->get();

        return $product;
    }

    function gudang() {
        

        //$arr        =   [];

        //$supp       =   PemasukanBarang::where('supplier', $request->supplier)->get();
        /*$supp       =   PemasukanBarang::get();

        foreach ($supp as $key) {
            array_push($arr, $key->produk);
        }

        $pro    =   Product::whereIn('name', $arr)->get();

        $ur     =   [];

        foreach ($pro as $key) {
            array_push($ur, $key->id);
        }

        Product::whereNotIn('id', $ur)->delete();

        dd('tyes');*/

        $product    =   Product::where('id_toko', Auth::user()->id)->orderBy('name', 'ASC')->paginate(100);

        $dataSup = PemasukanBarang::where('id_toko', Auth::user()->id)->distinct()->get(['supplier']);

        $total_nilai_barang    =   Product::where('id_toko', Auth::user()->id)
                                ->select(\DB::raw('sum(stok * price) AS tot'))->first();

        return view('usahakumart/pages/gudang', compact('product', 'dataSup', 'total_nilai_barang'));
    }

    function tambahBarang(Request $request) {
        $t = Toko::where('id', Auth::user()->id)->first();

        if ($t->permission == 0) {
            return redirect('/cashier/gudang-barang')->with('alert', 'Anda Harus Mendapatkan Izin Dari Administrator Gudang, Silahkan Hubungi Administrator Gudang Untuk Mendapatkan Izin Terlebih Dahulu');
        }

    	$this->validate($request, [
    		'name'		      =>	'required',
            'harga_jual'      =>    'required',
    		'price'		      =>	'required',
    		/*'affiliasi'	      =>	'required',*/
    		'stok'		      =>	'required',
    		'barcode'	      =>	'required',
            'gambare'         =>    'required|image',
            'id_category'     =>    'required',
            'rak'             =>    'required',
    	]);

        $nama   =   str_slug(str_replace(' ', '', $request->barcode) . $request->name) . '.' .
                    $request->gambare->getClientOriginalExtension();

    	$cek 	=	Product::where('name', $request->name)->where('id_toko', Auth::user()->id)->count();

    	$request->request->add([
    		'id_toko'	=>	Auth::user()->id,
            'gambar'    =>  $nama,
            'terjual'   => 0,
            'diskon'    => 0,
            'affiliasi' => $request->keuntungan * (10/100)
    	]);

        $request->gambare->move(public_path('gambar'), $nama);

    	if ($cek == null) {
    		$pro = Product::create($request->except('_token', 'gambare'));

            History::create([
                'pemasukan'     =>  $pro->stok,
                'id_toko'       =>  Auth::user()->id,
                'id_product'    =>  $pro->id,
                'stok_awal'     =>  0
            ]);
    	} else {
			return redirect()->back()->with('alert', 'Barang sudah ditambahkan, silahkan cek gudang');
    	}

    	return redirect()->back()->with('alert', 'Barang berhasil ditambahkan');
    }

    function edit(Request $request) {
        $t = Toko::where('id', Auth::user()->id)->first();

        if ($t->permission == 0) {
            return redirect('/cashier/gudang-barang')->with('alert', 'Anda Harus Mendapatkan Izin Dari Administrator Gudang, Silahkan Hubungi Administrator Gudang Untuk Mendapatkan Izin Terlebih Dahulu');
        }

    	$product 	=	Product::where('id', $request->id)->first();

    	return view('usahakumart/pages/edit_barang', compact('product'));
    }

    function update(Request $request) {
        $t = Toko::where('id', Auth::user()->id)->first();

        if ($t->permission == 0) {
            return redirect('/cashier/gudang-barang')->with('alert', 'Anda Harus Mendapatkan Izin Dari Administrator Gudang, Silahkan Hubungi Administrator Gudang Untuk Mendapatkan Izin Terlebih Dahulu');
        }

    	$this->validate($request, [
            'harga_jual'      =>    'required',
            'price'           =>    'required',
            'affiliasi'       =>    'required',
            'stok'            =>    'required',
            'barcode'         =>    'required',
            'gambare'         =>    'image'
    	]);

    	$cek 	=	Product::where('name', $request->name)->where('id_toko', Auth::user()->id)->count();

    	$request->request->add([
    		'id_toko'	=>	Auth::user()->id
    	]);

        $pro    =   Product::where('id', $request->id)->first();

        if ($request->stok) {
            $cek    =   History::where(DB::raw("DATE(created_at) = '".date('Y-m-d')."'"))->first();

            if ($cek != null) {
                History::where('id', $cek->id)->update([
                    'pemasukan' => $cek->pemasukan + ($request->stok - $pro->stok)
                ]);
            } else {
                History::create([
                    'pemasukan'     =>  $request->stok - $pro->stok,
                    'id_toko'       =>  Auth::user()->id,
                    'id_product'    =>  $request->id,
                    'stok_awal'     =>  $request->stok
                ]);
            }
        }

        if ($request->gambare != null) {
            $p = Product::where('id', $request->id)->first();

            unlink(public_path('gambar/' . $p->gambar));

            $nama   =   str_slug(str_replace(' ', '', $request->barcode) . $request->name) . '.' .
                    $request->gambare->getClientOriginalExtension();

            $request->request->add([
                'gambar'    =>  $nama,
            ]);

            $request->gambare->move(public_path('gambar'), $nama);
        }

    	if ($cek < 2) {
    		Product::where('id', $request->id)->update($request->except('_token', 'gambare'));
    	} else {
			return redirect()->back()->with('alert', 'Update Gagal! Barang sudah ditambahkan, silahkan cek gudang');
    	}

    	return redirect()->back()->with('alert', 'Barang berhasil diupdate');
    }

    function search(Request $request) {
    	$product 	=	Product::where('id_toko', Auth::user()->id)
    							 ->where('name', 'LIKE', '%' . $request->q . '%')
                                 ->orWhere('barcode', $request->q)
                                 ->get();

        $dataSup = PemasukanBarang::where('id_toko', Auth::user()->id)->distinct()->get(['supplier']);

        
        $total_nilai_barang    =   Product::where('id_toko', Auth::user()->id)
                                ->select(\DB::raw('sum(stok * price) AS tot'))->first();

    	return view('usahakumart/pages/gudang', compact('product', 'dataSup', 'total_nilai_barang'));
    }

    function hapus(Request $request) {
        $t = Toko::where('id', Auth::user()->id)->first();

        if ($t->permission == 0) {
            return redirect('/cashier/gudang-barang')->with('alert', 'Anda Harus Mendapatkan Izin Dari Administrator Gudang, Silahkan Hubungi Administrator Gudang Untuk Mendapatkan Izin Terlebih Dahulu');
        }

        $product = Product::where('id', $request->id)->first();

        if (file_exists(public_path('gambar/' . $product->gambar))) {
            unlink(public_path('gambar/' . $product->gambar));
        }

        Product::where('id', $request->id)->delete();

        return redirect()->back()->with('alert', 'Barang berhasil dihapus');
    }

    function gudangPrint(Request $request)
    {
        $key = $request->key;
        $product    =   Product::where('id_toko', Auth::user()->id)->where('name', 'LIKE', $key.'%')->orderBy('name', 'ASC')->get();
        $total    =   Product::where('id_toko', Auth::user()->id)->where('name', 'LIKE', $key.'%')->select(DB::raw('sum(stok * harga_jual) AS tot'))->first();
        return view('laporan.laporan_barang', compact('product', 'key', 'total'));
    }

    // Mencari Duplikat Barang
    function gudangDuplikat(Request $request)
    {
        $product    =   Product::select('barcode', DB::raw('COUNT(*) as `data_count`'))
                                ->groupBy('barcode')
                                ->havingRaw('COUNT(*) > 1')
                                ->paginate(100);
        
        if ($request->t) {
            $product    =   Product::select($request->t, DB::raw('COUNT(*) as `data_count`'))
                                ->groupBy($request->t)
                                ->havingRaw('COUNT(*) > 1')
                                ->paginate(100);
        }

        return view('usahakumart/pages/gudang_duplikat', compact('product'));
    }
}
