<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use Response;

use App\Models\Kota;
use App\Models\Kecamatan;

class DestinationController extends Controller
{
    function kota(Request $request) {
    	$id 	 			=	$request->id_prov;

    	$result 			=	Kota::where('id_provinsi', $id)->orderBy('kota', 'ASC')->get();

    	$kota 	 			=	Kota::where('id_provinsi', $id)->orderBy('kota', 'ASC')->first();

    	$kecamatan 			=	Kecamatan::where('id_kota', $kota->id)->orderBy('kecamatan', 'ASC')->get();

    	return Response::json([$result, $kecamatan]);
    }

    function kecamatan(Request $request) {
    	$id 	 			=	$request->id_kota;

    	$result 			=	Kecamatan::where('id_kota', $id)->orderBy('kecamatan', 'ASC')->get();

    	return Response::json($result);
    }
}
