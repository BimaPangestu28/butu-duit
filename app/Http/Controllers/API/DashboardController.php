<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use Response;
use Carbon\Carbon;

use App\Models\AffiliasiMember as Member;
use App\Models\NumberCard as Kartu;
use App\Models\TokoOffline as Toko;

class DashboardController extends Controller
{
    function anggota() {
    	$data 	=	Member::where('created_at', '>', Carbon::now()->subMonth())
    				->where('created_at', '<', Carbon::now())
    				->where('status', 1)->count();

    	return Response::json($data);
    }

    function totalAnggota() {
    	$data 	=	Member::count();

    	return Response::json($data);	
    }

    function kartu() {
    	$data 	=	Kartu::where('status', 1)->count();

    	return Response::json($data);		
    }

    function toko() {
    	$data 	=	Toko::where('aktif', 1)
                    ->where('level', 2)->count();

    	return Response::json($data);		
    }

    function tidakAktif() {
    	$data 	=	Member::where('status', 0)->count();

    	return Response::json($data);	
    }

    function aktif() {
    	$data 	=	Member::where('status', 1)->count();

    	return Response::json($data);	
    }
}
