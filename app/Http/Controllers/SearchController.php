<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use App\Models\ProductOffline as Product;
use App\PemasukanBarang;

class SearchController extends Controller
{
    // liveSearch
    function liveSearch(Request $request) {
        // $keywords = Input::get('keywords');
        // $counter = Product::where('id_toko', Auth::user()->id)
        //             ->where(function ($counter) use ($keywords) {
        //                 $counter->where('barcode', $keywords)
        //                 ->orWhere('name', 'LIKE', '%'.$keywords.'%');
        //             })->paginate(3);

        // $products = Product::where('id_toko', Auth::user()->id)
        //             ->where(function ($products) use ($keywords) {
        //                 $products->where('barcode', $keywords)
        //                 ->orWhere('name', 'LIKE', '%'.$keywords.'%');
        //             })->paginate(3);

        // return view('usahakumart/items/searchProduct', compact('counter', 'keywords'))->with('products', $products);

        $keywords = $request->nama;

        $products = Product::where('id_toko', Auth::user()->id)
                    ->where(function ($products) use ($keywords) {
                        $products->where('barcode', $keywords)
                        ->orWhere('name', 'LIKE', '%'.$keywords.'%');
                    })->take(5)->get();

        //$product    =   Product::select('name')->get();

        return $products;
    }
}
