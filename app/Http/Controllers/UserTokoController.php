<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UserToko as User;
use App\LogUser as Log;

use Auth;
use DB;

class UserTokoController extends Controller
{
    function index() {

    	$users 		=	User::all();

    	return view('usahakumart.users.index', compact('users'));
    }

    function create() {

    	return view('usahakumart.users.create');
    }

    function store(Request $request) {
    	$this->validate($request, [
    		'nama'		=>	'required',
    		'username'	=>	'required',
    		'level'		=>	'required',
    		'password'	=>	'required'
    	]);

    	$request->request->add([
    		'id_toko'	=>	Auth::user()->id,
            'password'  =>  bcrypt($request->password)
    	]);

    	User::create($request->except(['_token']));

    	return redirect('/cashier/user-toko')->with('alert', 'User Toko Berhasil Ditambahkan');
    }

    function edit($id) {
        $user   =   User::where('id', $id)->first();

        return view('usahakumart.users.edit', compact('user'));
    }

    function update($id, Request $request) {
        $this->validate($request, [
            'level'     =>  'required'
        ]);

        if ($request->password != '') {
            $request->request->add(['password' => bcrypt($request->password)]);
            User::where('id', $id)->update($request->except(['_token', 'username', 'nama']));
        } else {
            User::where('id', $id)->update($request->except(['_token', 'password', 'username', 'nama']));
        }

        return redirect('/cashier/user-toko')->with('alert', 'User Toko Berhasil Diupdate');
    }

    function delete($id) {
        User::where('id', $id)->delete();

        return redirect('/cashier/user-toko')->with('alert', 'User Toko Berhasil Dihapus');
    }

    function aktivitas($id, Request $request) {
        $log       =   Log::where('id_user', $id);

        $user      =   User::where('id', $id)->first();

        if ($request->date == null) {
            $logs  =   $log->whereDate('created_at', DB::raw('CURDATE()'))->get();
        } else {
            $logs  =   $log->whereDate('created_at', $request->date)->get();
        }

        return view('usahakumart.users.aktivitas', compact('logs', 'user'));
    }

    function status($type, $id) {
      if ($type == 'aktif') {
        $status   =   1;
      } else {
        $status   =   0;
      }

      User::where('id', $id)->update(['status' => $status]);

      return redirect()->back()->with('alert', 'Status Berhasil Dihapus');
    }
}
