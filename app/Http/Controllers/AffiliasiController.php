<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

use App\Models\AffiliasiTotal as Total;
use App\Models\AffiliasiMember as Member;
use App\Models\AffiliateHistory as History;
use App\Models\Affiliasi;
use App\Models\Member as User;
use App\Models\Payout;
use App\Models\Withdraw;
use App\Models\SaldoUser;
use App\SaldoDepositRequest;

class AffiliasiController extends Controller
{
    function index(Request $request) {
    	$member =	Member::where('id_member', session()->get('member')->id_member)->first();

		  $total 	=	Total::where('id_member', $member->id)->first();

    	if ($request->id_line == null) {
			$tree    	=	Affiliasi::where('id_parent', $member->id)
					       	->join('affiliasi_member', 'affiliasi_member.id', 'affiliasi.id_member')
						    ->select(DB::raw('affiliasi_member.*, affiliasi.*, affiliasi_member.id_member as id_mem'))
						    ->get();

			$i 		    =	0;

            $depositA   =   SaldoDepositRequest::where('id_member', session()->get('member')->id_member)->get();
    	} else {
    		$tree 	=	Affiliasi::where('id_parent', $request->id_line)
						->join('affiliasi_member', 'affiliasi_member.id', 'affiliasi.id_member')
						->select(DB::raw('affiliasi_member.*, affiliasi.*, affiliasi_member.id_member as id_mem'))
						->get();

			$i      = 1;
    	}

    	$no 	    = 1;

    	$id 	    = $request->id;

        $payout     = Payout::where('id_member', session()->get('member')->id_member)
                        ->orderBy('id', 'DESC')->paginate(10);

        $deposit    = SaldoUser::where('id_member', session('member')->id_member)->first();

    	return view('affiliasi.affiliasi', compact('member', 'total', 'tree', 'no', 'i', 'id', 'payout', 'deposit', 'depositA'));
    }

    function pengaturan() {
        if (\Session::get('member') == null) {
            return redirect('/login');
        }

        $member =   Member::where('id_member', session()->get('member')->id_member)->first();

        $total  =   Total::where('id_member', $member->id)->first();

        $no         = 1;

        $payout     = Payout::where('id_member', session()->get('member')->id_member)
                        ->orderBy('id', 'DESC')->paginate(10);

        $deposit    = SaldoUser::where('id_member', session('member')->id_member)->first();

        return view('affiliasi.pengaturan', compact('member', 'total', 'no', 'i', 'payout', 'deposit', 'depositA'));
    }

    function pengaturanUpdate(Request $request)
    {
        $this->validate($request, [
            'nama'           =>    'required',
        ]);

        if (\Session::get('member') == null) {
            return redirect('/login');
        }

        $member =   User::where('id_member', session()->get('member')->id_member)
                        ->update($request->except('_token'));

        return redirect()->back()->with('alert', 'Data Berhasil Dirubah');
    }

    static function cek($id) {
    	$cek 	=	User::where('id_member', $id)->first();

    	if ($cek->premium == 0) {
    		return 'User Biasa';
    	} else if ($cek->premium == 1) {
    		return 'Premium User';
    	} else if ($cek->premium == 2) {
    		return 'Platinum User';
    	}
    }

    function payout(Request $request) {
        $user       =   User::where('id_member', session()->get('member')->id_member)->first();

        $aff        =   Member::where('id_member', $user->id_member)->first();

        $total      =   Total::where('id_member', $aff->id)->first();

        $payout    =   $request->payout - ($request->payout * (10 / 100));

        if ($payout > $total->total_affiliasi) {
            return redirect()->back()->with('notif', 'Total Payout Lebih Besar Dari Total Affiliasi');
        } else if ($request->payout < 20000) {
            return redirect()->back()->with('notif', 'Total Payout Minimal Rp. 20.000');
        }

        $total->update(['total_affiliasi' => $total->total_affiliasi - $payout]);

        Payout::create([
            'id_member'     =>  session()->get('member')->id_member,
            'payout'        =>  $request->payout,
            'ppn'           =>  ($request->payout * (10 / 100)),
            'status'        =>  '0',
            'no_rek'        =>  $request->no_rek,
            'atas_nama'     =>  $request->atas_nama,
            'bank'          =>  $request->bank
        ]);

        return redirect()->back()->with('notif', 'Permintaan Payout Telah Dikirim, Silahkan Tunggu');
    }

    function list() {
        $payout     =   Withdraw::join('toko_offline', 'toko_offline.id', 'withdraw_toko.id_toko')
                        ->select(DB::raw('toko_offline.*, withdraw_toko.*, withdraw_toko.id as idw'))
                        ->where('status', 0)->orderBy('withdraw_toko.id', 'DESC')->paginate(20);

        return view('administrator.pages.affiliasi', compact('payout'));
    }

    function konfirm(Request $request) {
        $tes = Withdraw::where('id', $request->id)->update(['status' => 1]);

        return redirect()->back()->with('alert', 'Konfirmasi Berhasil');
    }

    function history($id = null) {
      $member     =   Member::where('id_member', \Session::get('member')->id_member)->first();

      $history    =   History::where('id_member', $member->id)->paginate(20);

      $total      =   Total::where('id_member', $member->id)->first();

      $id         =   $id;

      $deposit    =   SaldoUser::where('id_member', session('member')->id_member)->first();

      return view('affiliasi.history', compact('member', 'history', 'id', 'total', 'deposit'));
    }


    function passChange()
    {
        if (\Session::get('member') == null) {
            return redirect('/login');
        }

        $member =   Member::where('id_member', session()->get('member')->id_member)->first();

        $total  =   Total::where('id_member', $member->id)->first();

        $no         = 1;

        $payout     = Payout::where('id_member', session()->get('member')->id_member)
                        ->orderBy('id', 'DESC')->paginate(10);

        $deposit    = SaldoUser::where('id_member', session('member')->id_member)->first();
        return view('affiliasi.passChange', compact('member', 'total', 'no', 'i', 'id', 'payout', 'deposit', 'depositA'));
    }
    function passUpdate(Request $request)
    {
        if (\Session::get('member') == null) {
            return redirect('/login');
        }
        
        $this->validate($request, [
            'password'      =>      'required|string|min:8',
            'old_pass'      =>      'required',
            'confirm_pass'  =>      'required'
        ]);

        $validate =   User::where('id_member', session()->get('member')->id_member)->first();
        
        if(\Hash::check($request->old_pass, $validate->password)) {
            $request->request->add(['password' => bcrypt($request->password)]);
            $member =   User::where('id_member', session()->get('member')->id_member)
                            ->update($request->except('_token', 'old_pass', 'confirm_pass'));
            
            session()->forget('member');
            return redirect('/logout')->with('alert', 'Password Berhasil dirubah, silahkan login kembali');
        } else {
            return redirect()->back()->with('alert', 'Password lama salah');
        }
    }
}
