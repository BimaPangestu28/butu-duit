<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ProductOffline;
use App\PolaPerilakuHarga;

use Auth;

class PolaPerilakuHargaController extends Controller
{
	function index() {

    	$data 	=	PolaPerilakuHarga::where('pola_perilaku_hargas.id_toko', Auth::user()->id)
    				->join('product_offline', 'product_offline.id', 'pola_perilaku_hargas.beli_barang')
                    ->get();

    	return view('usahakumart.pola.index', compact('data'));
    }

    static function bonus($id) {
        return ProductOffline::where('id', $id)->first();
    }

    function apiCount(Request $request) {
        $arr = [];
        foreach ($request->data as $key => $value) {
            $datas = PolaPerilakuHarga::where('beli_barang', $key)->where('total_beli', $value)
                    ->where('aktif', 1)
                    ->join('product_offline', 'product_offline.id', 'pola_perilaku_hargas.beli_barang')
                    ->first();    

            if ($datas != null) {
                array_push($arr, $datas);
            }
        }

        return $arr;
    }

    function create() {

    	return view('usahakumart.pola.create');
    }

    function store(Request $request) {
    	$this->validate($request, [
    		'beli_barang'	=>	'required',
    		'total_beli'	=> 	'required',
    		'option'		=>	'required'
    	]);

        $beli_barang    =   ProductOffline::where('name', $request->beli_barang)->first();

        if ($request->bonus_barang != null) {
            $bonus_barang   =   ProductOffline::where('name', $request->bonus_barang)->first();

            $request->request->add([
                'bonus_barang'       =>  $bonus_barang->id
            ]);
        }

    	$request->request->add([
    		'id_toko' 		=> 	Auth::user()->id,
    		'aktif'			=> 	1,
            'beli_barang'   =>  $beli_barang->id
    	]);

    	PolaPerilakuHarga::create($request->except(['_token']));

    	return redirect()->back()->with('alert', 'Pola Perilaku Harga Berhasil Ditambahkan');
    }

    function api(Request $request) {
    	return ProductOffline::where('name', 'LIKE', '%' . $request->data . '%')->get();
    }

    function nonaktif($id) {
    	PolaPerilakuHarga::where('id', $id)->update(['aktif' => 0]);

    	return redirect()->back()->with('alert', 'Pola Perilaku Harga Berhasil Di Nonaktifkan');
    }

    function aktif($id) {
    	PolaPerilakuHarga::where('id', $id)->update(['aktif' => 1]);

    	return redirect()->back()->with('alert', 'Pola Perilaku Harga Berhasil Di Aktifkan');
    }

    function delete($id) {
    	PolaPerilakuHarga::where('id', $id)->delete();

    	return redirect()->back()->with('alert', 'Pola Perilaku Harga Berhasil Di Hapus');
    }
}
