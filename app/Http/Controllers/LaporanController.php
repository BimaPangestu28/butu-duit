<?php

namespace App\Http\Controllers;

use Auth;
use DB;
use PDF;
use Illuminate\Http\Request;
use App\PemasukanBarang;
use App\KasToko;
use \Carbon\Carbon;
use App\Models\NumberCard;
use App\SupplierOffline;
use Excel;
use \Maatwebsite\Excel\Excel as Ex;
use App\Models\OrderOffline as History;
use App\Models\OrderOfflineHistory as HistoryOffline;
use App\Models\HistoryPemasukanBarang;
use App\Models\ProductOffline as Product;
use App\Category;
use App\Models\TokoOffline as Toko;
use App\UserToko;
use App\LogUser as Log;

use App\PotonganBarang;
use App\OrderOfflinePotongan;

class LaporanController extends Controller
{
    function pembelianProduk(Request $request) {

        if ($request->q != null) {
            $dataSup = PemasukanBarang::where('id_toko', Auth::user()->id)
                                    ->where('supplier', 'LIKE', '%' . $request->q . '%')
                                    ->groupBy('supplier')
                                    ->select('supplier', DB::raw('count(*) as total'))->get();

            $dataNo  = PemasukanBarang::where('id_toko', Auth::user()->id)
                                    ->where('supplier', 'LIKE', '%' . $request->q . '%')
                                    ->groupBy('no_transaksi')
                                    ->select('no_transaksi', DB::raw('count(*) as total'))->get();
        } else {
            $dataSup = PemasukanBarang::where('id_toko', Auth::user()->id)->groupBy('supplier')
                                    ->select('supplier', DB::raw('count(*) as total'))->get();

            $dataNo  = PemasukanBarang::where('id_toko', Auth::user()->id)->groupBy('no_transaksi')
                                    ->select('no_transaksi', DB::raw('count(*) as total'))->get();

            $dataTemp  = PemasukanBarang::where('id_toko', Auth::user()->id)
                                    ->where('tipe', 'Kredit')
                                    ->groupBy('no_transaksi')
                                    ->select('no_transaksi', DB::raw('count(*) as total'))->get();
        }

        $suppliers = SupplierOffline::orderBy('id', 'DESC')->get();


    	return view('usahakumart.laporan.pembelianProduk', compact('dataSup', 'dataNo', 'suppliers', 'dataTemp'));
    }

    static function noSupplier($no)
    {
        $return = PemasukanBarang::where('id_toko', Auth::user()->id)
                                ->where('tipe', 'Kredit')
                                ->where('no_transaksi', $no)
                                ->first();
        return $return['supplier'];
    }

    static function noSupplierTotal($no)
    {
        $return = PemasukanBarang::where('id_toko', Auth::user()->id)
                                ->where('tipe', 'Kredit')
                                ->where('no_transaksi', $no)
                                ->sum('total_harga');
        return $return;
    }

    // Rubah Tipe Pembayaran
    function updateProdukTemp($no)
    {
        $bars = PemasukanBarang::where('id_toko', Auth::user()->id)
                                ->where('tipe', 'Kredit')
                                ->where('no_transaksi', $no)
                                ->update(['tipe' => 'Tunai']);

        return redirect()->back()->with('alert', 'Data Berhasil Dirubah');
    }

    function searchProduk(Request $request) {
        $supplier = $request->q2;
        $data       = PemasukanBarang::where('pemasukan_barangs.id_toko', Auth::user()->id)
                                    ->where('supplier', $supplier)
                                    ->leftJoin('product_offline', 'pemasukan_barangs.barcode', 'product_offline.barcode')
                                    ->orderBy('pemasukan_barangs.id', 'DESC')
                                    /*->select('pemasukan_barangs.*', 'product_offline.*', 'pemasukan_barangs.id as id')*/
                                    ->distinct();

        if ($request->q != null) {
            $data->whereDate('created_at', '=', date('Y-m-d', strtotime($request->q)));
        }

        if ($request->q2 != null) {
            $data->where('pemasukan_barangs.barcode', 'LIKE', '%' . $request->q2 . '%')
                 ->orWhere('produk', 'LIKE', '%' . $request->q2 . '%')
                 ->orWhere('supplier', 'LIKE', '%' . $request->q2 . '%');

            $dataTotal = PemasukanBarang::where('pemasukan_barangs.id_toko', Auth::user()->id)
                            ->where('supplier', $supplier)
                            ->orWhere('pemasukan_barangs.barcode', $supplier)
                            ->orWhere('produk', $supplier)
                            ->sum('total_harga');
            $dataPajak = PemasukanBarang::where('pemasukan_barangs.id_toko', Auth::user()->id)
                            ->where('supplier', $supplier)
                            ->orWhere('pemasukan_barangs.barcode', $supplier)
                            ->orWhere('produk', $supplier)
                            ->sum('pajak');
            $dataDiskon = PemasukanBarang::where('pemasukan_barangs.id_toko', Auth::user()->id)
                            ->where('supplier', $supplier)
                            ->orWhere('pemasukan_barangs.barcode', $supplier)
                            ->orWhere('produk', $supplier)
                            ->sum('diskon');
        }
                 
        $datas = $data->get([
                                'pemasukan_barangs.barcode', 
                                'produk', 
                                'id_category', 
                                'rak',
                                'gambar',
                                'product_offline.id',
                                'pemasukan_barangs.id as id'
                            ]);;
        
        return view('usahakumart.laporan.pembelianProdukDetail', compact('datas', 'supplier', 'dataTotal', 'dataPajak', 'dataDiskon'));   
    }

    function pembelianProdukTambah() {
        $supplier   =   SupplierOffline::orderBy('name')->get();
        $cat        =   Category::orderBy('category', 'ASC')->get();

    	return view('usahakumart.laporan.pembelianProdukTambah', compact('supplier', 'cat'));
    }

    function pembelianProdukStore(Request $request)
    {
        $t = Toko::where('id', Auth::user()->id)->first();

        if ($t->permission == 0) {
            return redirect('/cashier/gudang-barang')->with('alert', 'Anda Harus Mendapatkan Izin Dari Administrator Gudang, Silahkan Hubungi Administrator Gudang Untuk Mendapatkan Izin Terlebih Dahulu');
        }

    	if ($request->restok != null) {
            $this->validate($request, [
                'name'           =>    'required',
                'harga_jual'      =>    'required',
                'price'           =>    'required',
                'affiliasi'       =>    'required',
                'barcode'         =>    'required',
                'id_category'     =>    'required',
                'rak'             =>    'required',
                'supplier'        =>    'required',
                'jumlah_barang'   =>    'required',
                'tipe_jumlah'     =>    'required',
                'tipe'            =>    'required',
                'total_harga'     =>    'required',
                'created_at'      =>    'required'
            ]);
        } else {
            $this->validate($request, [
                'name'           =>    'required',
                'harga_jual'      =>    'required',
                'price'           =>    'required',
                'affiliasi'       =>    'required',
                'stok'            =>    'required',
                'barcode'         =>    'required',
                'gambare'         =>    'required|image',
                'id_category'     =>    'required',
                'rak'             =>    'required',
                'supplier'        =>    'required',
                'jumlah_barang'   =>    'required',
                'tipe_jumlah'     =>    'required',
                'tipe'            =>    'required',
                'total_harga'     =>    'required',
                'created_at'      =>    'required'
            ]);
        }

        if ($request->tipe == 'Kredit') {
            $this->validate($request, [
                'tanggal_pembayaran'    =>  'required'
            ]);
        }

        $cekNomer   =   PemasukanBarang::where('supplier', '!=', $request->supplier)
                                ->where('no_transaksi', $request->no_transaksi)->count();

        if ($cekNomer > 1) {
            return redirect()->back()->with('alert', 'Nomer Transaksi sudah digunakan oleh supplier lain');
        }

        $cek        =   Product::where('barcode', $request->barcode)
                                ->where('id_toko', Auth::user()->id)->first();


        $pemasukan 					= new PemasukanBarang;
        $pemasukan->id_toko			= Auth::user()->id;
        $pemasukan->barcode 		= $request->input('barcode');
        $pemasukan->produk 			= $request->input('name');
        $pemasukan->supplier 		= $request->input('supplier');
        $pemasukan->jumlah_barang 	= $request->input('jumlah_barang');
        $pemasukan->tipe_jumlah 	= $request->input('tipe_jumlah');
        $pemasukan->tipe 			= $request->input('tipe');
        $pemasukan->total_harga 	= $request->input('total_harga');
        $pemasukan->no_transaksi    = $request->input('no_transaksi');
        $pemasukan->pajak           = $request->input('pajak');
        $pemasukan->diskon          = $request->input('diskon');
        $pemasukan->created_at      = $request->input('created_at');

        if ($request->tanggal_pembayaran != null) {
            $pemasukan->tanggal_pembayaran      = $request->input('tanggal_pembayaran');
            $pemasukan->status_pembayaran       = 0;

            // Status Pembayaran
            // 0 => Belum Dibayar
            // 1 => Sudah Dibayar
        }

        $jumlah_satuan              =   $request->jumlah_barang;

        $pemasukan->jumlah_satuan 	=   $jumlah_satuan;
        $pemasukan->save();
        

        if ($cek != null) {
            Product::where('barcode', $request->barcode)->update([
                'stok'          =>  $cek->stok += $jumlah_satuan,
                'price'         =>  $request->input('price'),
                'harga_jual'    =>  $request->input('harga_jual')
            ]);
        } else {
            $nama   =   str_slug(str_replace(' ', '', $request->barcode) . $request->name) . '.' .
                    $request->gambare->getClientOriginalExtension();
            
            $pro                = new Product;
            $pro->name          = $request->input('name');
            $pro->price         = $request->input('price');
            $pro->affiliasi     = $request->input('affiliasi');
            $pro->barcode       = $request->input('barcode');
            $pro->id_toko       = Auth::user()->id;
            $pro->stok          = $request->input('stok');
            $pro->terjual       = 0;
            $pro->harga_jual    = $request->input('harga_jual');
            $pro->keuntungan    = $request->input('keuntungan');
            $pro->gambar        = $nama;
            $pro->diskon        = 0;
            $pro->kondisi       = $request->input('kondisi');
            $pro->deskripsi     = $request->input('deskripsi');
            $pro->id_category   = $request->input('id_category');
            $pro->rak           = $request->input('rak');

            $pro->save();

            HistoryPemasukanBarang::create([
                'pemasukan'     =>  $pro->stok,
                'id_toko'       =>  Auth::user()->id,
                'id_product'    =>  $pro->id,
                'stok_awal'     =>  0
            ]);

            $request->gambare->move(public_path('gambar'), $nama);

        }        

        return redirect()->back()->with('alert', 'Laporan Berhasil Ditambahkan');
    }

    static function cekGudang($name) {
        $cek    =   Product::where('name', $name)->first();

        if ($cek == null) {
            return true;
        } else {
            return false;
        }
    }

    static function countItem($supplier, $type) {
        if ($type == 'product') {
            $datas       = PemasukanBarang::where('pemasukan_barangs.id_toko', Auth::user()->id)
                                            ->where('supplier', $supplier)
                                            ->leftJoin('product_offline', 'pemasukan_barangs.barcode', 'product_offline.barcode')
                                            ->distinct()
                                            ->get([
                                                'pemasukan_barangs.barcode', 
                                                'produk', 
                                                'id_category', 
                                                'rak',
                                                'gambar',
                                                'product_offline.id'
                                            ]);
        } else {
            $datas      =  PemasukanBarang::where('supplier', $supplier)->distinct()->get(['no_transaksi']);
        }

        return count($datas);
    }

    function isiSupplier(Request $request) {
        $supplier = $request->supplier;
        $banyak = $request->banyak;

        if ($supplier == null) {
            $datas = PemasukanBarang::where('id_toko', Auth::user()->id)
                                    ->where('supplier',  null)
                                    ->get();
        } else {
            $dataAll       = PemasukanBarang::where('pemasukan_barangs.id_toko', Auth::user()->id)
                                    ->where('supplier', $supplier)
                                    ->leftJoin('product_offline', 'pemasukan_barangs.barcode', 'product_offline.barcode')
                                    ->orderBy('pemasukan_barangs.id', 'DESC')
                                    ->distinct()
                                    ->get([
                                        'pemasukan_barangs.barcode', 
                                        'produk', 
                                        'id_category', 
                                        'rak',
                                        'gambar',
                                        'product_offline.id',
                                        'pemasukan_barangs.id as id'
                                    ]);
            if($banyak) {
                $datas       = PemasukanBarang::where('pemasukan_barangs.id_toko', Auth::user()->id)
                                    ->where('supplier', $supplier)
                                    ->leftJoin('product_offline', 'pemasukan_barangs.barcode', 'product_offline.barcode')
                                    ->orderBy('pemasukan_barangs.id', 'DESC')
                                    ->distinct()
                                    ->take($banyak) //biar gak berat load page nya
                                    ->get([
                                        'pemasukan_barangs.barcode', 
                                        'produk', 
                                        'id_category', 
                                        'rak',
                                        'gambar',
                                        'product_offline.id',
                                        'pemasukan_barangs.id as id'
                                    ]);

            } else {
                $datas       = PemasukanBarang::where('pemasukan_barangs.id_toko', Auth::user()->id)
                                    ->where('supplier', $supplier)
                                    ->leftJoin('product_offline', 'pemasukan_barangs.barcode', 'product_offline.barcode')
                                    ->orderBy('pemasukan_barangs.id', 'DESC')
                                    ->distinct()
                                    ->take(100) //biar gak berat load page nya
                                    ->get([
                                        'pemasukan_barangs.barcode', 
                                        'produk', 
                                        'id_category', 
                                        'rak',
                                        'gambar',
                                        'product_offline.id',
                                        'pemasukan_barangs.id as id'
                                    ]);
            }

            $dataTotal  = PemasukanBarang::where('id_toko', Auth::user()->id)
                                    ->where('supplier', $supplier)
                                    ->sum('total_harga');

            $dataPajak  = PemasukanBarang::where('id_toko', Auth::user()->id)
                                    ->where('supplier', $supplier)
                                    ->sum('pajak');

            $dataDiskon = PemasukanBarang::where('id_toko', Auth::user()->id)
                                    ->where('supplier', $supplier)
                                    ->sum('diskon');

            $showMany = $dataAll->count();
        }

        return view('usahakumart.laporan.pembelianProdukDetail', compact('datas', 'supplier', 'dataTotal', 'dataPajak', 'dataDiskon', 'showMany'));
    }

    static function category($id) {
        $category   =   Category::where('id', $id)->first();

        return $category['category'];
    }

    function supplierTransaksi(Request $request) {
        $supplier   = $request->supplier;

        $dataNo    = PemasukanBarang::where('id_toko', Auth::user()->id)->where('supplier', $supplier)
                                    ->groupBy('no_transaksi')
                                    ->select('no_transaksi', DB::raw('count(*) as total'))->get();

        $dataTotal = PemasukanBarang::where('id_toko', Auth::user()->id)
                                ->where('supplier', $supplier)
                                ->sum('total_harga');
        $dataPajak = PemasukanBarang::where('id_toko', Auth::user()->id)
                                ->where('supplier', $supplier)
                                ->sum('pajak');
        $dataDiskon = PemasukanBarang::where('id_toko', Auth::user()->id)
                                ->where('supplier', $supplier)
                                ->sum('diskon');

        return view('usahakumart.laporan.pembelianProdukTransaksiSupplier', compact('dataNo', 'supplier', 'dataDiskon', 'dataTotal', 'dataPajak'));
    }

    static function dateTrans($no) {
        $data      = PemasukanBarang::where('no_transaksi', $no)->orderBy('id', 'DESC')->first();

        return date('d/m/Y', strtotime($data->created_at));
    }

    function noTransaksi(Request $request) {
        $transaksi = $request->transaksi;

        if ($transaksi == 'null') {
            $datas = PemasukanBarang::where('id_toko', Auth::user()->id)
                                    ->where('no_transaksi',  null)
                                    ->where('supplier', $request->supplier)
                                    ->get();
            $dataTotal = PemasukanBarang::where('id_toko', Auth::user()->id)
                                    ->where('no_transaksi', null)
                                    ->where('supplier', $request->supplier)
                                    ->sum('total_harga');
            $dataPajak = PemasukanBarang::where('id_toko', Auth::user()->id)
                                    ->where('no_transaksi', null)
                                    ->where('supplier', $request->supplier)
                                    ->sum('pajak');
            $dataDiskon = PemasukanBarang::where('id_toko', Auth::user()->id)
                                    ->where('no_transaksi', null)
                                    ->where('supplier', $request->supplier)
                                    ->sum('diskon');
        } else {
            $datas = PemasukanBarang::where('id_toko', Auth::user()->id)
                                    ->where('no_transaksi', $transaksi)
                                    ->where('supplier', $request->supplier)
                                    ->get();
            $dataTotal = PemasukanBarang::where('id_toko', Auth::user()->id)
                                    ->where('no_transaksi', $transaksi)
                                    ->where('supplier', $request->supplier)
                                    ->sum('total_harga');
            $dataPajak = PemasukanBarang::where('id_toko', Auth::user()->id)
                                    ->where('no_transaksi', $transaksi)
                                    ->where('supplier', $request->supplier)
                                    ->sum('pajak');
            $dataDiskon = PemasukanBarang::where('id_toko', Auth::user()->id)
                                    ->where('no_transaksi', $transaksi)
                                    ->where('supplier', $request->supplier)
                                    ->sum('diskon');

            /*foreach ($datas as $key) {
                PemasukanBarang::where('id', $key->id)->update(['supplier' => 'CV MEKAR JAYA MAKMUR MT (UNILEVER)']);
            }*/
        }
        
        /*dd($supplier);*/

        return view('usahakumart.laporan.pembelianProdukTransaksi', compact('datas', 'transaksi', 'dataTotal', 'dataPajak', 'dataDiskon'));
    }

    function deleteProduct($id)
    {
    	$product = PemasukanBarang::where('id', $id)
    								->where('id_toko', Auth::user()->id)
                                    ->first();

        $barang = Product::where('id_toko', Auth::user()->id)
                                    ->where('barcode', $product->barcode)
                                    ->where('name', $product->produk)
                                    ->first();
    	if($barang != null) {
            $product->delete();
    	    $barang->delete();
        } else {
            $product->delete();
        }

    	return redirect()->back()->with('alert', 'Data Berhasil Dihapus');
    }

    function editProduk($id)
    {
		$product = PemasukanBarang::where('id', $id)
								->where('id_toko', Auth::user()->id)
                                ->first();
        
        $supplier   =   SupplierOffline::orderBy('name')->get();

		return view('usahakumart.laporan.editProduk', compact('product', 'supplier'));
    }

    function updateProduk(Request $request, $id) 
    {
		$product = PemasukanBarang::where('id', $id)
								->where('id_toko', Auth::user()->id)
                                ->first();

        $barang = Product::where('id_toko', Auth::user()->id)
                        ->where('barcode', $product->barcode)
                        ->where('name', $product->produk)
                        ->first();

        $product->barcode 		= $request->input('barcode');
        $product->produk 		= $request->input('produk');
        $product->supplier 		= $request->input('supplier');
        $product->jumlah_barang = $request->input('jumlah_barang');
        $product->tipe_jumlah 	= $request->input('tipe_jumlah');
        $product->tipe 			= $request->input('tipe');
        $product->total_harga 	= $request->input('total_harga');
        $product->no_transaksi 	= $request->input('no_transaksi');
        $product->pajak        	= $request->input('pajak');
        $product->diskon       	= $request->input('diskon');

        
        $jumlah_satuan  =   $request->jumlah_barang;

        $product->jumlah_satuan 	= $jumlah_satuan;

        $barang->barcode        = $request->input('barcode');
        $barang->name           = $request->input('produk');

        $product->update();
        $barang->update();

        return redirect('/cashier/laporan/pembelian-produk/supplier?supplier=' . $request->supplier)->with('alert', 'Data berhasil di update');
    }

    function pembelianProdukPrint(Request $request) {
        if ($request->supplier) {
            $supplier = $request->supplier;
            $from = Carbon::parse($request->from)->startOfDay();
            $to = Carbon::parse($request->to)->endOfDay();

            $datas = PemasukanBarang::where('id_toko', Auth::user()->id)
                                    ->where('supplier', $supplier)
                                    ->whereBetween('created_at', [$from, $to])
                                    ->get();
            $dataTotal = PemasukanBarang::where('id_toko', Auth::user()->id)
                                    ->where('supplier', $supplier)
                                    ->whereBetween('created_at', [$from, $to])
                                    ->sum('total_harga');
            $dataPajak = PemasukanBarang::where('id_toko', Auth::user()->id)
                                    ->where('supplier', $supplier)
                                    ->whereBetween('created_at', [$from, $to])
                                    ->sum('pajak');
            $dataDiskon = PemasukanBarang::where('id_toko', Auth::user()->id)
                                    ->where('supplier', $supplier)
                                    ->whereBetween('created_at', [$from, $to])
                                    ->sum('diskon');
            $utang = PemasukanBarang::where('id_toko', Auth::user()->id)
                                    ->where('supplier', $supplier)
                                    ->where('tipe', 'utang')
                                    ->whereBetween('created_at', [$from, $to])
                                    ->sum('total_harga');

            $pdf    =   PDF::loadView('laporan.laporan_pembelian_produk', compact('datas', 'utang', 'supplier', 'dataTotal', 'dataPajak', 'dataDiskon', 'from', 'to'))->setPaper('a4', 'landscape');

            // return $pdf->download('laporan_pembelian_produk_'.$supplier.'_'. date('d-m-Y', strtotime($from)) .'_'.date('d-m-Y', strtotime($to)). '.pdf');
            return view('laporan.laporan_pembelian_produk', compact('datas', 'utang', 'supplier', 'dataTotal', 'dataPajak', 'dataDiskon', 'from', 'to'));
        } 
        elseif ($request->no_transaksi) {
            $no_transaksi = $request->no_transaksi;

            $datas = PemasukanBarang::where('id_toko', Auth::user()->id)
                                    ->where('no_transaksi', $no_transaksi)
                                    ->get();
            $dataTotal = PemasukanBarang::where('id_toko', Auth::user()->id)
                                    ->where('no_transaksi', $no_transaksi)
                                    ->sum('total_harga');
            $dataPajak = PemasukanBarang::where('id_toko', Auth::user()->id)
                                    ->where('no_transaksi', $no_transaksi)
                                    ->sum('pajak');
            $dataDiskon = PemasukanBarang::where('id_toko', Auth::user()->id)
                                    ->where('no_transaksi', $no_transaksi)
                                    ->sum('diskon');
            $utang = PemasukanBarang::where('id_toko', Auth::user()->id)
                                    ->where('no_transaksi', $no_transaksi)
                                    ->where('tipe', 'utang')
                                    ->sum('total_harga');
                                    
            $pdf    =   PDF::loadView('laporan.laporan_pembelian_transaksi', compact('datas', 'utang', 'no_transaksi', 'dataTotal', 'dataPajak', 'dataDiskon'))->setPaper('a4', 'landscape');

            // return $pdf->download('laporan_pembelian_produk_'.$no_transaksi.'_'. date('d_m_y') . '.pdf');
            return view('laporan.laporan_pembelian_transaksi', compact('datas', 'utang', 'no_transaksi', 'dataTotal', 'dataPajak', 'dataDiskon'));
        } else if($request->halaman == "supplier") {
            $from = Carbon::parse($request->from)->startOfDay();
            $to = Carbon::parse($request->to)->endOfDay();

            $datas = PemasukanBarang::where('id_toko', Auth::user()->id)
                                        ->whereBetween('created_at', [$from, $to])
                                        ->groupBy('supplier')
                                        ->select('supplier', DB::raw('sum(total_harga) as total'))->get();
            $total = PemasukanBarang::where('id_toko', Auth::user()->id)
                                    ->whereBetween('created_at', [$from, $to])
                                    ->sum('total_harga');

            $pdf    =   PDF::loadView('laporan.laporan_pembelian_produk_data', compact('datas', 'from', 'to', 'total'))->setPaper('a4', 'landscape');

            // return $pdf->download('laporan_keungan_pembelian_produk_'. date('d-m-Y', strtotime($from)) .'_'.date('d-m-Y', strtotime($to)). '.pdf');
            return view('laporan.laporan_pembelian_produk_data', compact('datas', 'from', 'to', 'total'));
        } else if($request->supplier_daftar == "daftar_supplier") {
            $from = Carbon::parse($request->from)->startOfDay();
            $to = Carbon::parse($request->to)->endOfDay();

            $datas = PemasukanBarang::where('id_toko', Auth::user()->id)
                                        ->whereBetween('created_at', [$from, $to])
                                        ->groupBy('supplier')
                                        ->select('supplier')->get();
            $total_tunai = PemasukanBarang::where('id_toko', Auth::user()->id)
                                    ->where('tipe', 'Tunai')
                                    ->whereBetween('created_at', [$from, $to])
                                    ->sum('total_harga');
                                    
            $total_kredit = PemasukanBarang::where('id_toko', Auth::user()->id)
                                    ->where('tipe', 'Kredit')
                                    ->whereBetween('created_at', [$from, $to])
                                    ->sum('total_harga');
            
            $total_titipan = PemasukanBarang::where('id_toko', Auth::user()->id)
                                    ->where('tipe', 'Titipan')
                                    ->whereBetween('created_at', [$from, $to])
                                    ->sum('total_harga');

            $total_semua = PemasukanBarang::where('id_toko', Auth::user()->id)
                                    ->whereBetween('created_at', [$from, $to])
                                    ->sum('total_harga');

            $pdf    =   PDF::loadView('laporan.laporan_pembelian_produk_data', compact('datas', 'from', 'to', 'total_tunai', 'total_kredit', 'total_titipan', 'total_semua'))->setPaper('a4', 'landscape');

            // return $pdf->download('laporan_keungan_pembelian_produk_'. date('d-m-Y', strtotime($from)) .'_'.date('d-m-Y', strtotime($to)). '.pdf');
            return view('laporan.laporan_pembelian_produk_data', compact('datas', 'from', 'to', 'total_tunai', 'total_kredit', 'total_titipan', 'total_semua'));
        }

    }

    static function hargaLaporan($supplier, $tipe)
    {
        if($tipe == "tunai") {
            $return = PemasukanBarang::where('id_toko', Auth::user()->id)
                            ->where('supplier', $supplier)
                            ->where('tipe', 'Tunai')
                            ->sum('total_harga');
        }

        else if($tipe == "kredit") {
            $return = PemasukanBarang::where('id_toko', Auth::user()->id)
                            ->where('supplier', $supplier)
                            ->where('tipe', 'Kredit')
                            ->sum('total_harga');
        }

        else if($tipe == "titipan") {
            $return = PemasukanBarang::where('id_toko', Auth::user()->id)
                            ->where('supplier', $supplier)
                            ->where('tipe', 'Titipan')
                            ->sum('total_harga');
        }

        return $return;
    }

    function pembelianProdukPrintSupplier(Request $request)
    {
        $from = Carbon::parse($request->from)->startOfDay();
        $to = Carbon::parse($request->to)->endOfDay();

        $datas = PemasukanBarang::where('id_toko', Auth::user()->id)
                                ->whereBetween('created_at', [$from, $to])
                                ->get();
        $dataTotal = PemasukanBarang::where('id_toko', Auth::user()->id)
                                ->whereBetween('created_at', [$from, $to])
                                ->sum('total_harga');
        $dataPajak = PemasukanBarang::where('id_toko', Auth::user()->id)
                                ->whereBetween('created_at', [$from, $to])
                                ->sum('pajak');
        $dataDiskon = PemasukanBarang::where('id_toko', Auth::user()->id)
                                ->whereBetween('created_at', [$from, $to])
                                ->sum('diskon');
        $utang = PemasukanBarang::where('id_toko', Auth::user()->id)
                                ->where('tipe', 'utang')
                                ->whereBetween('created_at', [$from, $to])
                                ->sum('total_harga');

        $pdf    =   PDF::loadView('laporan.laporan_pembelian_produk_supplier', compact('datas', 'utang', 'dataTotal', 'dataPajak', 'dataDiskon', 'from', 'to'))->setPaper('a4', 'landscape');

        // return $pdf->download('laporan_pembelian_produk_'. date('d-m-Y', strtotime($from)) .'_'.date('d-m-Y', strtotime($to)). '.pdf');
        return view('laporan.laporan_pembelian_produk_supplier', compact('datas', 'utang', 'dataTotal', 'dataPajak', 'dataDiskon', 'from', 'to'));
    }

    function stokKartu() {

        $jumlah_keseluruhan     =   NumberCard::where('id_toko', Auth::user()->id)->count();
        $jumlah_terpakai        =   NumberCard::where('id_toko', Auth::user()->id)
                                    ->where('status', 0)->count();
        $jumlah_belum           =   NumberCard::where('id_toko', Auth::user()->id)
                                    ->where('status', 1)->count();

        return view('usahakumart.laporan.stok_kartu', compact('jumlah_keseluruhan', 'jumlah_terpakai', 'jumlah_belum'));
    }

    function stokKartuPrint(Request $request) {
        $day    = Carbon::now()->format('d');
        $month  = Carbon::now()->format('m');
        $year   = Carbon::now()->format('Y');

        if ($request->type == 'today') {
            $jumlah_keseluruhan     =   NumberCard::where('id_toko', Auth::user()->id)
                                        ->where('created_at', '<=', now())->count();

            $jumlah_terpakai        =   NumberCard::where('id_toko', Auth::user()->id)
                                        ->where('status', 0)
                                        ->where('created_at', '<=', now())->count();

            $jumlah_belum           =   NumberCard::where('id_toko', Auth::user()->id)
                                        ->where('status', 1)
                                        ->where('created_at', '<=', now())->count();
        } else {

        }

        $pdf    =   PDF::loadView('usahakumart.laporan.stok_kartu_print', compact('jumlah_belum', 'jumlah_terpakai', 'jumlah_keseluruhan'))
                    ->setPaper('a4', 'landscape');

        // return $pdf->download('laporan_stok_kartu_' . date('d_m_y') . '.pdf');

        return view('usahakumart.laporan.stok_kartu_print', compact('jumlah_belum', 'jumlah_terpakai', 'jumlah_keseluruhan'));
    }

    function apiSearch(Request $request) {
        $return     =   PemasukanBarang::join('product_offline', 'pemasukan_barangs.barcode', 'product_offline.barcode')
                         ->join('category', 'product_offline.id_category', 'category.id')
                         ->where('pemasukan_barangs.barcode', $request->barcode)
                         ->where('pemasukan_barangs.id_toko', Auth::user()->id)
                         ->orderBy('pemasukan_barangs.id', 'DESC')->first();

        if($return != null) {
            return $return;
        } else {
            return "null";
        }
    }

    function apiSearchSingle(Request $request) {
        $return     =   Product::where('name', $request->barang)->first();

        return $return;
    }


    // Laporan penjualan

    function jualanLaporan(Request $request)
    {   

        $day    = Carbon::now()->format('d');
        $month  = Carbon::now()->format('m');
        $year   = Carbon::now()->format('Y');

        $kasir  =   UserToko::where('id_toko', Auth::user()->id)->where('level', 3)->get();

        $datas  = History::leftJoin('user_tokos', 'order_offline.id_user', 'user_tokos.id')
                        ->leftJoin('affiliasi_member', 'order_offline.id_member', 'affiliasi_member.id')
                        ->where('order_offline.id_toko', Auth::user()->id)
                        /*->whereDate('order_offline.created_at', DB::raw('CURDATE()'))*/
                        ->where(DB::raw('DAY(order_offline.created_at)'), $day)
                        ->where(DB::raw('MONTH(order_offline.created_at)'), $month)
                        ->where(DB::raw('YEAR(order_offline.created_at)'), $year)
                        ->orderBy('order_offline.id', 'DESC')
                        ->select('order_offline.*', 'user_tokos.*', 'affiliasi_member.nama as nama_member', 'order_offline.created_at as created_at', 'order_offline.id as id_order', 'user_tokos.nama as kasir')
                        ->get();

                                //dd($datas);
                                
        return view('usahakumart.laporan.jualanLaporan', compact('datas', 'page', 'kasir'));
    }

    static function potongan($id_product, $order_id) {
        $return = OrderOfflinePotongan::where('order_id', $order_id)
                                     ->where('id_product', $id_product)
                                     ->first();

        if ($return != null) {
            return $return->potongan;
        } else {
            return 0;
        }
    }

    static function totalPotongan($order_id) {
        $product    =   OrderOfflinePotongan::where('order_id', $order_id)->get();

                            /*dd($id);*/

        $return     =   0;

        foreach($product as $p) {
            $return += $p->potongan;
        } 

        return $return;
    }

    function daftarPrint(Request $request) {
        $master     =   UserToko::where('id_toko', Auth::user()->id)->where('level', 1)->first();

        $cek        =   \Hash::check($request->master, $master->password);

        if ($cek) {
            if ($request->type == 'print') {
                $data   =   HistoryOffline::where('order_id', $request->id)
                                        ->join('order_offline', 'order_offline_history.order_id', 'order_offline.id')
                                        ->join('product_offline', 'order_offline_history.id_product', 'product_offline.id')
                                        ->distinct()
                                        ->get([
                                            'name', 'harga_jual',
                                            'barcode', 'product_offline.affiliasi',
                                            'diskon', 'id_product',
                                            'order_id', 'price',
                                            'no_transaksi', 'pembayaran',
                                            'kembalian'
                                        ]);

                $single =   $data->first();

                Log::create(['id_user' => session('user-toko')->id, 'type' => 'REPRINT LAPORAN']);

                /*dd($data);*/

                $pdf    =   PDF::loadView('laporan.laporan_reprint', compact('data', 'single'))
                                ->setPaper('a4', 'landscape');

                // return $pdf->download('laporan_reprint_' . $single->no_transaksi . '.pdf');
                return view('laporan.laporan_reprint', compact('data', 'single'));
            } else {
                $items      = HistoryOffline::where('order_id', $request->id)->get();

                foreach ($items as $item) {
                    Product::where('id', $item->id_product)->increment('stok');
                    Product::where('id', $item->id_product)->decrement('terjual');
                }

                HistoryOffline::where('order_id', $request->id)->delete();
                History::where('id', $request->id)->delete();

                //return redirect('/cashier/penjualan/' . $id . '/repeat');
                return redirect()->back()->with('alert', 'Transaksi Berhasil Dihapus');
            }
        } else {
            return redirect()->back()->with('alert', 'Password salah');
        }
    }

    function printAction(Request $request)
    {
        if ($request->type == 'print') {
            $data   =   HistoryOffline::where('order_id', $request->id)
                                    ->join('order_offline', 'order_offline_history.order_id', 'order_offline.id')
                                    ->join('product_offline', 'order_offline_history.id_product', 'product_offline.id')
                                    ->distinct()
                                    ->get([
                                        'name', 'harga_jual',
                                        'barcode', 'product_offline.affiliasi',
                                        'diskon', 'id_product',
                                        'order_id', 'price',
                                        'no_transaksi', 'pembayaran',
                                        'kembalian'
                                    ]);

            $single =   $data->first();

            Log::create(['id_user' => session('user-toko')->id, 'type' => 'REPRINT LAPORAN']);

            /*dd($data);*/

            $pdf    =   PDF::loadView('laporan.laporan_reprint', compact('data', 'single'))
                            ->setPaper('a4', 'landscape');

            // return $pdf->download('laporan_reprint_' . $single->no_transaksi . '.pdf');
            return view('laporan.laporan_reprint', compact('data', 'single'));
        }
    }

    static function dataList($id) {
        $return     =   HistoryOffline::where('order_id', $id)
                         ->join('product_offline', 'order_offline_history.id_product', 'product_offline.id')
                         ->distinct()
                         ->get([
                            'name', 'harga_jual',
                            'barcode', 'affiliasi',
                            'diskon', 'id_product',
                            'order_id', 'price'
                           ]);


        // $return      =  [];

        // foreach($product as $p) {
        //     dd(in_array($p->name, $return));
        //     if(!in_array($p->name, $return)) {
        //         array_push($p);
        //     }
        // }

        return $return;
    }

    static function jumlah($pro, $order) {
        $return     =   HistoryOffline::where('order_id', $order)->where('id_product', $pro)->count();

        return $return;
    }

    function jualanLaporanApi(Request $request) {
        $return     =   HistoryOffline::where('order_id', $request->id)
                        ->join('product_offline', 'order_offline_history.id_product', 'product_offline.id')
                        ->get();

        return $return;
    }

    static function total($id, $type, $kasir = null) {
        $product    =   HistoryOffline::where('order_offline_history.order_id', $id)
                            ->join('product_offline', 'order_offline_history.id_product', 'product_offline.id')
                            ->get();

        $history    =   History::where('id', $id)->first();

        $potongan   =   $history->potongan;

        if($type == 'pembayaran') {
            $return     =   0;

            foreach($product as $p) {
                $return += $p->harga_jual;
            } 
        } else if($type == 'affiliasi') {
            $return     =   $history->affiliasi;
        } else if($type == 'hpp') {
            $return     =   $history->hpp;
        } else if($type == 'diskon') {
            $return     =   0;

            foreach($product as $p) {
                $return += ($p->diskon/100)*$p->harga_jual;
            } 
        } else if($type == 'pembayaran akhir') {
            $return     =   0;

            foreach($product as $p) {
                $return += ($p->harga_jual - (($p->diskon/100)*$p->harga_jual));
            } 

            $return     =   $return - $potongan;
        }

        return $return;
    }

    function searchJual(Request $request)
    {
        $date       =   $request->date;
        $kasir_id   =   $request->kasir;

        $kasir  =   UserToko::where('id_toko', Auth::user()->id)->where('level', 3)->get();

        $data   =   History::leftJoin('user_tokos', 'order_offline.id_user', 'user_tokos.id')
                            ->leftJoin('affiliasi_member', 'order_offline.id_member', 'affiliasi_member.id')
                            ->where('order_offline.id_toko', Auth::user()->id)
                            ->orderBy('order_offline.id', 'DESC')
                            ->select('order_offline.*', 'user_tokos.*', 'affiliasi_member.nama as nama_member', 'order_offline.created_at as created_at', 'order_offline.id as id_order', 'user_tokos.nama as kasir');
                    /*->whereBetween('order_offline.created_at', [$from, $to])*/

        if ($date != null) {
            $data->whereDate('order_offline.created_at', $date);
        }

        if ($kasir_id != null) {
            $data->where('id_user', $kasir_id);
        }

        $datas  =   $data->get();

        return view('usahakumart.laporan.jualanLaporan', compact('datas', 'page', 'kasir'));
    }

    function jualPrint(Request $request)
    {
        
        /*$from = Carbon::parse($request->from)->startOfDay();
        $to = Carbon::parse($request->to)->endOfDay();*/
        if ($request->date == '') {
            $date   =   date('Y-m-d');
        } else {
            $date   =   $request->date;
        }

        $kasir      =   $request->kasir;

        $data       =   History::leftJoin('user_tokos', 'order_offline.id_user', 'user_tokos.id')
                        ->leftJoin('affiliasi_member', 'order_offline.id_member', 'affiliasi_member.id')
                        ->where('order_offline.id_toko', Auth::user()->id)
                        ->whereDate('order_offline.created_at', $date)
                        ->orderBy('order_offline.id', 'DESC')
                        ->select('order_offline.*', 'user_tokos.*', 'affiliasi_member.nama as nama_member', 'order_offline.created_at as created_at', 'order_offline.id as id_order', 'user_tokos.nama as kasir');
                        /*->whereBetween('order_offline.created_at', [$from, $to])*/

        if ($kasir != null && $kasir != '' && $kasir != ' ') {
            $data->where('id_user', $kasir);
        }
                        
        $datas  =   $data->get();

        $pdf    =   PDF::loadView('laporan.laporan_penjualan', compact('datas', 'date', 'kasir'))
                        ->setPaper('a4', 'landscape');

                        //dd($pdf);

        // return $pdf->download('laporan_penjualan_'. date('d-m-Y', strtotime($date)) . '.pdf');
        return view('laporan.laporan_penjualan', compact('datas', 'date', 'kasir'));
    }

    function jualPrintPeriode(Request $request) {
        $kasir      =   $request->kasir;
        $dari       =   $request->dari;
        $sampai     =   $request->sampai;

        $data       =   History::where('order_offline.id_toko', Auth::user()->id)
                        ->whereBetween('order_offline.created_at', [$dari, $sampai])
                        ->orderBy('order_offline.id', 'ASC')
                        ->select(\DB::raw('Date(created_at) as date, SUM(total_pembayaran) as total_pembayaran, SUM(pembayaran) as pembayaran, SUM(kembalian) as kembalian,
                            SUM(potongan) as potongan'))
                        ->groupBy('date');

        if ($kasir != null && $kasir != '' && $kasir != ' ') {
            $data->where('id_user', $kasir);
        }
                        
        $datas  =   $data->get();

        $total_pembayaran   =   0;
        $potongan           =   0;
        $kembalian          =   0;

        foreach ($datas as $k) {
            $total_pembayaran += $k->total_pembayaran;
            $potongan         += $k->potongan;
            $kembalian        += $k->kembalian;
        }

        $pdf    =   PDF::loadView('laporan.laporan_penjualan_periode', compact('datas', 'dari', 'sampai', 'kasir', 'total_pembayaran', 'potongan', 'kembalian'))
                        ->setPaper('a4', 'landscape');

                        //dd($pdf);

        // return $pdf->download('laporan_penjualan_periode_'. date('d-m-Y', strtotime($dari)) . '_sampai_' . date('d-m-Y', strtotime($sampai)) . '.pdf');
        return view('laporan.laporan_penjualan_periode', compact('datas', 'dari', 'sampai', 'kasir', 'total_pembayaran', 'potongan', 'kembalian'));
    }

    static function totalPeriode($date, $kasir, $tipe) {
        if ($tipe == 'hpp') {
            $products    =   HistoryOffline::whereDate('order_offline_history.created_at', $date)
                                        ->join('product_offline', 'order_offline_history.id_product', 'product_offline.id')
                                        ->join('order_offline', 'order_offline_history.order_id', 'order_offline.id')
                                        ->select(\DB::raw('SUM(price) as price, SUM(order_offline.affiliasi) as affiliasi'));

            if ($kasir != null && $kasir != '' && $kasir != ' ') {
                $products->where('id_user', $kasir);
            }
                            
            $datas      =   $products->first();

            return $datas->price;
        } else if ($tipe == 'aff') {
            $products    =      History::whereDate('created_at',
                                $date)->select(\DB::raw('SUM(order_offline.affiliasi) as affiliasi'));

            if ($kasir != null && $kasir != '' && $kasir != ' ') {
                $products->where('id_user', $kasir);
            }

            $datas      =   $products->first();

            return $datas->affiliasi;
        } else if ($tipe == 'omset') {
                        $products    =      History::whereDate('created_at',
                                $date)->select(\DB::raw('SUM(order_offline.total_pembayaran) as total_pembayaran'));

            if ($kasir != null && $kasir != '' && $kasir != ' ') {
                $products->where('id_user', $kasir);
            }

            $datas      =   $products->first();

            return $datas->total_pembayaran;
        } else if ($tipe == 'potongan') {
                        $products    =      History::whereDate('created_at',
                                $date)->select(\DB::raw('SUM(order_offline.potongan) as potongan'));

            if ($kasir != null && $kasir != '' && $kasir != ' ') {
                $products->where('id_user', $kasir);
            }

            $datas      =   $products->first();

            return $datas->potongan;
        }
    }

    static function totalPeriodeSemua($dari, $sampai, $kasir, $tipe) {
        if ($tipe == 'hpp') {
            $products    =   HistoryOffline::whereBetween('order_offline_history.created_at', [$dari, $sampai])
                                        ->join('product_offline', 'order_offline_history.id_product', 'product_offline.id')
                                        ->join('order_offline', 'order_offline_history.order_id', 'order_offline.id')
                                        ->select(\DB::raw('SUM(price) as price'));

            if ($kasir != null && $kasir != '' && $kasir != ' ') {
                $products->where('id_user', $kasir);
            }
                            
            $datas      =   $products->first();

            return $datas->price;
        } else if ($tipe == 'aff') {
            $products    =   History::whereBetween('created_at', [$dari, $sampai])
                                        ->select(\DB::raw('SUM(affiliasi) as affiliasi'));

            if ($kasir != null && $kasir != '' && $kasir != ' ') {
                $products->where('id_user', $kasir);
            }
                            
            $datas      =   $products->first();

            return $datas->affiliasi;
        }
    }
    
    function bestBarang(Request $request)
    {

        $datas = Product::where('id_toko', Auth::user()->id)->orderBy('terjual', 'DESC')->paginate(20);
        $many = count(Product::where('id_toko', Auth::user()->id)->get());

        if($request->search) {
            $datas = Product::where('id_toko', Auth::user()->id)->where('barcode', 'LIKE', $request->search.'%')->orWhere('name', 'LIKE', $request->search.'%')->paginate(20);
        }

        if($request->id) {
            $chart = Product::where('id_toko', Auth::user()->id)
                            ->where('id', $request->id )
                            ->first();
                            
            $products = HistoryOffline::select('id', 'created_at')
                            ->where('id_toko', Auth::user()->id)
                            ->where('id_product', $request->id )
                            ->get()
                            ->groupBy(function($date) {
                                //return Carbon::parse($date->created_at)->format('Y'); //tahun
                                return Carbon::parse($date->created_at)->format('m'); //bulan
                            });
    
            $produkmcount = [];
            $produkArr = [];
    
            foreach ($products as $key => $value) {
                $produkmcount[(int)$key] = count($value);
            }

            for($i = 1; $i <= 12; $i++){
                if(!empty($produkmcount[$i])){
                    $produkArr[$i] = $produkmcount[$i];    
                }else{
                    $produkArr[$i] = 0;    
                }
            }

        }

        return view('usahakumart.laporan.bestBarang', compact('datas', 'many', 'chart', 'produkArr'));
    }
    

    function bestPrint(Request $request)
    {
        $lot = $request->lot;

        $datas = $datas = Product::where('id_toko', Auth::user()->id)->paginate($lot);

        $pdf    =   PDF::loadView('laporan.laporan_best', compact('datas', 'lot'))
                        ->setPaper('a4', 'landscape');

        // return $pdf->download('laporan_barang_best_'.$lot.'_'.'terbaik'.'_'. date('d_m_y') . '.pdf');
        return view('laporan.laporan_best', compact('datas', 'lot'));
    }

    // Static Funtion buat best barang
    static function monthBest($id) {
        $products = Product::select('id', 'created_at')
                    ->get()
                    ->groupBy(function($date) {
                        //return Carbon::parse($date->created_at)->format('Y'); //tahun
                        return Carbon::parse($date->created_at)->format('m'); //bulan
                    });

        $produkmcount = [];
        $produkArr = [];

        foreach ($products as $key => $value) {
            $produkmcount[(int)$key] = count($value);
        }

        dd($value);
        
	}

    function laporanKasir()
    {
        return view('usahakumart.laporan.laporanKasir');
    }

    static function totalHari($type, $date = null, $kasir = null) {
        $day    = Carbon::now()->format('d');
        $month  = Carbon::now()->format('m');
        $year   = Carbon::now()->format('Y');

        if ($date == null) {
            $products    =   HistoryOffline::where(DB::raw('DAY(order_offline_history.created_at)'), $day)
                                        ->where(DB::raw('MONTH(order_offline_history.created_at)'), $month)
                                        ->where(DB::raw('YEAR(order_offline_history.created_at)'), $year)
                                        ->join('product_offline', 'order_offline_history.id_product', 'product_offline.id')
                                        ->join('order_offline', 'order_offline_history.order_id', 'order_offline.id');

            $orders      =   History::where(DB::raw('DAY(created_at)'), $day)
                                        ->where(DB::raw('MONTH(created_at)'), $month)
                                        ->where(DB::raw('YEAR(created_at)'), $year);
        } else {
            $products    =   HistoryOffline::whereDate('order_offline_history.created_at', $date)
                                        ->join('product_offline', 'order_offline_history.id_product', 'product_offline.id')
                                        ->join('order_offline', 'order_offline_history.order_id', 'order_offline.id');

            $orders      =   History::whereDate('created_at', $date);
        }

        if ($kasir != null) {
            $products->where('id_user', $kasir);
            $orders->where('id_user', $kasir);
        }

        $product   =    $products->get();
        $order     =    $orders->get();

        if($type == 'pembayaran') {
            $return     =   0;

            foreach($order as $p) {
                $return += ($p->total_pembayaran + $p->potongan);
            } 
        } else if($type == 'hpp') {
            $return     =   0;

            foreach($product as $p) {
                $return += $p->price;
            } 
        } else if($type == 'affiliasi') {
            $return     =   0;

            foreach($order as $p) {
                $return += $p->affiliasi;
            }
        } else if($type == 'diskon') {
            $return     =   0;

            /*foreach($product as $p) {
                $return += ($p->diskon/100) * $p->harga_jual;
            } */

            foreach ($order as $o) {
                $return += $o->potongan;
            }
        } else if($type == 'pendapatan bersih') {
            $return     = 0;
            $jual       = 0;
            $aff        = 0;
            $diskon     = 0;
            $hpp        = 0;
            $potongan   = 0;

            foreach($order as $p) {
                $jual      +=  ($p->total_pembayaran + $p->potongan);
                $aff       += $p->affiliasi;
            } 

            foreach($product as $p) {
                $diskon     += ($p->diskon/100) * $p->harga_jual;
                $hpp        += $p->price;

                $return = $jual - $aff - $diskon - $hpp;
            }

            foreach ($order as $o) {
                $potongan += $o->potongan;
            }

            $return = $return - $potongan;
        }
        /*dd($return);*/
        return $return;
    }

}
