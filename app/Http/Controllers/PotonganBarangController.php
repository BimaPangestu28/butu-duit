<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\PotonganBarang;
use App\Models\ProductOffline;

use Response;

class PotonganBarangController extends Controller
{
    function index() {

    	$data 	=	PotonganBarang::join('product_offline', 'potongan_barangs.id_product', 'product_offline.id')
    								->select('product_offline.*', 'potongan_barangs.*', 'potongan_barangs.id as id')
    								->orderBy('potongan_barangs.id', 'DESC')->get();

    	return view('usahakumart.potongan.index', compact('data'));
    }


    function created() {
    	return view('usahakumart.potongan.created');
    }

    function api(Request $request) {
    	return ProductOffline::where('barcode', $request->barcode)->first();
    }

    function store(Request $request) {
    	$this->validate($request, [
    		'id_product'	=> 	'required',
    		'jumlah_dibeli'	=>	'required',
    		'potongan'		=>	'required'
    	]);

    	PotonganBarang::create($request->except(['barcode', '_token', 'nama_barang']));

    	return redirect()->back()->with('Potongan barang berhasil ditambahkan');
    }

    function delete($id) {
    	PotonganBarang::where('id', $id)->delete();

    	return redirect()->back()->with('alert', 'Potongan Barang Berhasil Dihapus');
    }

    function edit($id) {
    	$data 	=	PotonganBarang::join('product_offline', 'potongan_barangs.id_product', 'product_offline.id')
    								->select('product_offline.*', 'potongan_barangs.*', 'potongan_barangs.id as id', 'product_offline.id as id_p')
    								->where('potongan_barangs.id', $id)->first();

    	return view('usahakumart.potongan.edit', compact('data'));
    }

    function update(Request $request) {
    	$this->validate($request, [
    		'id_product'	=> 	'required',
    		'jumlah_dibeli'	=>	'required',
    		'potongan'		=>	'required'
    	]);

    	PotonganBarang::where('id', $request->id)
    					->update($request->except(['barcode', '_token', 'nama_barang']));

    	return redirect()->back()->with('alert', 'Data berhasil diupdate');
    }

    function cek(Request $request) {
        $id     =   $request->id;

        if ($request->jumlah != null) {
            $data   =   PotonganBarang::where('id_product', $id)
                                        ->where('jumlah_dibeli', 1)->first();
        } else {
            $data   =   PotonganBarang::where('id_product', $id)->first();
        }

        if ($data != null) {
            return $data;
        } else {
            return [];
        }
    }

    static function cekBarang($id, $jumlah) {
        $cek    =   PotonganBarang::where('id_product', $id)->first();

        if ($cek != null) {
            return floor($jumlah / $cek->jumlah_dibeli) * $cek->potongan;
        } else {
            return 0;
        }
    }
}
