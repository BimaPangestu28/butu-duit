<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SaldoDepositRequest as Req;

class SaldoDepositController extends Controller
{
    function store(Request $request) {
    	$this->validate($request, [
    		'bukti' 	=> 'required',
    		'jumlah'	=> 'required'
    	]);

    	$nama   =   str_slug(session('member')->nama . '-' . date('d-m-Y')) . '.' .
                    $request->bukti->getClientOriginalExtension();

        $request->request->add([
            'gambar'    =>  $nama,
            'id_member'	=>  session('member')->id_member
    	]);

        $request->bukti->move(public_path('gambar'), $nama);

        Req::create($request->except(['_token', 'bukti']));

        return redirect()->back()->with('notif', 'Request Telah Dikirim');
    }
}
