<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Response;
use Hash;
use Session;
use App\Models\Provinsi;
use App\Models\Kota;
use App\Models\Kecamatan;

use App\Models\TokoOffline as Toko;
use App\Models\ProductOffline as Product;
use App\Models\Member;
use App\Models\AffiliasiMember as AffMember;
use App\Models\CartOffline as Cart;
use App\Models\TransaksiOffline as Transaksi;
use App\Models\KategoriToko as Kategori;
use App\Models\Promosi;
use App\Models\PromoToko;
use App\Models\AffiliasiTotal;
use App\Models\SaldoUser;
use App\Models\AffiliasiMember;
use App\PotonganBarang;

class PagesController extends Controller
{
    function index()
    {
      	$provinsi = Provinsi::orderBy('provinsi', 'ASC')->get();
      	$kategori = Kategori::orderBy('kategori', 'ASC')->get();

    	return view('front', compact('provinsi', 'kategori'));
    }

    function prov(Request $request)
    {
    	$prov_id = $request->id_prov;
    	$kota = Kota::where('id_provinsi', '=', $prov_id)->get();
    	return Response::json($kota);
    }
    function provBarang(Request $request)
    {
    	$prov_id = $request->id_prov;
    	$toko = Toko::where('toko_offline.id_provinsi', '=', $prov_id)
                ->join('provinsi', 'provinsi.id', 'toko_offline.id_provinsi')
                ->join('kota', 'kota.id', 'toko_offline.id_kota')
                ->join('kecamatan', 'kecamatan.id', 'toko_offline.id_kecamatan')
                ->where('toko_offline.level', 2)
                ->get(['toko_offline.id','name', 'alamat', 'provinsi', 'kota', 'kecamatan']);
    	return Response::json($toko);
    }

    function kecamatan(Request $request)
    {
    	$kota_id = $request->id_kota;
    	$kecamatan = Kecamatan::where('id_kota', '=', $kota_id)->get();
    	return Response::json($kecamatan);
    }

    function kotaBarang(Request $request)
    {
    	$kota_id = $request->id_kota;
    	$toko = Toko::where('id_kota', '=', $kota_id)->where('toko_offline.level', 2)->get();
    	return Response::json($toko);
    }

    function kecamatanBarang(Request $request)
    {
    	$kecamatan_id = $request->id_kecamatan;
    	$toko = Toko::where('id_kecamatan', '=', $kecamatan_id)->where('toko_offline.level', 2)->get();
    	return Response::json($toko);
    }

    function showToko($id, Request $request)
    {
      $toko     = Toko::where('id', '=', $id)->first(['id', 'name', 'alamat']);
      $products = Product::where('id_toko', $id)->orderBy('terjual', 'DESC');
      $promo    = Promosi::where('id_toko', $id)->get();
      $ptoko    = PromoToko::where('id_toko', $id)->get();
      $no       = 0;
      $first    = $promo->first();
      $firstt   = $ptoko->first();
      $promoB   = null;

      if ($request->q != null) {
        $products->where('name', 'LIKE', '%' . $request->q . '%');
      }

      $product  = $products->paginate(20);

      if ($request->promo != null) {
          $promoB   = Promosi::where('id', $request->promo)->where('id_toko', $id)->first();
          $arr      =  explode(",", $promoB->id_product);
          $barang   = Product::whereIn('id', $arr)->get();
      }

      if (session()->has('member')) {
        $cart	=	Cart::join('member', 'member.id_member', 'cart_offline.id_member')
        ->join('toko_offline', 'toko_offline.id', 'cart_offline.id_toko')
        ->join('product_offline', 'product_offline.id', 'cart_offline.id_barang')
        ->where('cart_offline.id_toko', $id)
        ->where('cart_offline.id_member', Session::get('member')->id_member)
        ->where('cart_offline.level', 0)
        ->get();
      }
      return view('front.toko', compact('toko', 'product', 'cart', 'promo', 'promoB', 'first', 'firstt', 'barang', 'ptoko','id'));
    }

    function login(Request $request) {
        if (session()->has('member')) {
            return redirect()->back();
        } else {
            $id = $request->id;
            return view('/front/login', compact('id'));
        }
    }

    function loginUser(Request $request) {
        if (session()->has('member')) {
            return redirect('/member');
        } else {
            return view('/front/login2');
        }
    }

    function postLogin(Request $request) {
        $this->validate($request, [
            'username'  =>  'required',
            'password'  =>  'required'
        ]);

        $cek    =   Member::where('username', $request->username)->first();
        
        if ($cek != null) {
            if (Hash::check($request->password, $cek->password)) {
                $valid  =   AffMember::where('id_member', $cek->id_member)->count();
                if ($valid != null) {
                    session()->put('member', $cek);

                    return redirect('/toko-usahakumart/' . $request->id);
                } else {
                    return redirect()->back()->with('alert', 'Anda Belum Terdaftar Sebagai Member UsahakuMart');
                }
            } else {
                return redirect()->back()->with('alert', 'Password Salah');
            }
        } else {
            return redirect()->back()->with('alert', 'Username Salah');
        }
    }

    function postLoginUser(Request $request) {
        $this->validate($request, [
            'username'  =>  'required',
            'password'  =>  'required'
        ]);

        $cek    =   Member::where('username', $request->username)->first();

        if ($cek != null) {
            if (Hash::check($request->password, $cek->password)) {
                $valid  =   AffMember::where('id_member', $cek->id_member)->count();
                if ($valid != null) {
                    session()->put('member', $cek);

                    return redirect('/member');
                } else {
                    return redirect()->back()->with('alert', 'Anda Belum Terdaftar Sebagai Member UsahakuMart');
                }
            } else {
                return redirect()->back()->with('alert', 'Password Salah');
            }
        } else {
            return redirect()->back()->with('alert', 'Username Salah');
        }
    }

    function logout(Request $request) {
        session()->forget('member');
        if ($request->id != null) {
          return redirect('/toko-usahakumart/' . $request->id);
        }else {
          return redirect('/login');
        }
    }

    function pembelian($id)
    {
      if (session()->has('member')) {
        $toko = Toko::where('id', '=', $id)->first(['id', 'name', 'alamat']);
        $cart	=	Cart::join('member', 'member.id_member', 'cart_offline.id_member')
                    ->join('toko_offline', 'toko_offline.id', 'cart_offline.id_toko')
                    ->join('product_offline', 'product_offline.id', 'cart_offline.id_barang')
                    ->where('cart_offline.id_toko', $id)
                    ->where('cart_offline.id_member', Session::get('member')->id_member)
                    ->where('cart_offline.level', 0)
                    ->get();

        $member   = AffiliasiMember::where('id_member', session('member')->id_member)->first();

        $aff  = AffiliasiTotal::where('id_member', $member->id)->first();
        $dep  = SaldoUser::where('id_member', session('member')->id_member)->first();

        return view('front.pembelian', compact('toko', 'cart', 'aff', 'dep'));
      }else {
        return redirect('/toko-usahakumart/login/'.$id);
      }
    }

    function search($id)
    {
      $isi = $_GET['search'];
      $products = Product::where('id_toko', $id)->where('name', 'LIKE', '%' . $isi . '%')->get();
      return view('front.search', compact('products', 'isi'));
    }

    function transaksi(Request $request, $id)
    {
      $this->validate($request, [
        'nama'    =>  'required',
        'phone'   =>  'required',
        'alamat'  =>  'required'
      ]);

      $cart	=	Cart::join('member', 'member.id_member', 'cart_offline.id_member')
                  ->join('toko_offline', 'toko_offline.id', 'cart_offline.id_toko')
                  ->join('product_offline', 'product_offline.id', 'cart_offline.id_barang')
                  ->where('cart_offline.id_toko', $id)
                  ->where('cart_offline.id_member', Session::get('member')->id_member)
                  ->where('cart_offline.level', 0)
                  ->get();

      $harga      = 0;

      foreach ($cart as $nan) {
        $harga += $nan->jumlah_barang * ($nan->harga_jual - ($nan->harga_jual * ($nan->diskon / 100)));
      }

      $potongan   = 0;

      foreach ($cart as $key) {
        $cek    =   PotonganBarang::where('id_product', $key->id_barang)->first();

        if ($cek != null) {
            $p  = floor($key->jumlah_barang / $cek->jumlah_dibeli) * $cek->potongan;
        } else {
            $p  = 0;
        }

        $potongan += $p;
      }

      if ($request->metode_pembayaran == 'affiliasi') {
        $member   = AffiliasiMember::where('id_member', session('member')->id_member)->first();
        
        $cek      = AffiliasiTotal::where('id_member', $member->id)->first();

        if ($cek->total_affiliasi < 25000) {
          return redirect()->back()->with('alert', 'Saldo Affiliasi Anda Tidak Cukup. Minimal Rp. 25.000');
        } else if ($cek->total_affiliasi <= $harga) {
          return redirect()->back()->with('alert', 'Saldo Affiliasi Anda Tidak Cukup');
        } else if ($cek->total_affiliasi >= $harga) {
          $total = $cek->total_affiliasi - $harga;

          AffiliasiTotal::where('id_member', $member->id)->update(['total_affiliasi' => $total]);
        }
      } else if($request->metode_pembayaran == 'deposit') {
        $cek  = SaldoUser::where('id_member', session('member')->id_member)->first();

        if ($cek->saldo < 25000) {
          return redirect()->back()->with('alert', 'Saldo Deposit Anda Tidak Cukup. Minimal Rp. 25.000');
        } else if ($cek->saldo <= harga) {
          return redirect()->back()->with('alert', 'Saldo Deposit Anda Tidak Cukup');
        } else if ($cek->saldo >= harga) {
          AffiliasiTotal::where('id_member', session('member')->id)->update(['total_affiliasi' => $cek->saldo -= $harga]);
        }
      }

      $transaksi                    = New Transaksi;
      $transaksi->id_member         = Session::get('member')->id_member;
      $transaksi->penerima          = $request->input('nama');
      $transaksi->alamat            = $request->input('alamat');
      $transaksi->total_belanja     = $harga;
      $transaksi->status            = 0;
      $transaksi->id_toko           = $id;
      $transaksi->phone             = $request->input('phone');
      $transaksi->metode_pembayaran = $request->metode_pembayaran;
      $transaksi->potongan          = $potongan;

      $transaksi->save();
      $kode_transaksi               = $transaksi->id;
      $level                        = 1;

      Cart::where('id_toko', $id)
            ->where('id_member', Session::get('member')->id_member)
            ->where('level', 0)
            ->update(['kode_transaksi' => $kode_transaksi, 'level' => $level]);

      return redirect('/toko-usahakumart/transaksi/sukses/'.$kode_transaksi);

    }

    function transaksiSukses($idtransaksi)
    {
        if (session()->has('member')) {
          $cart	    =	Cart::join('member', 'member.id_member', 'cart_offline.id_member')
                      ->join('toko_offline', 'toko_offline.id', 'cart_offline.id_toko')
                      ->join('product_offline', 'product_offline.id', 'cart_offline.id_barang')
                      ->where('cart_offline.kode_transaksi', $idtransaksi)
                      ->where('cart_offline.id_member', Session::get('member')->id_member)
                      /*->where('cart_offline.level', 1)*/
                      ->get();
          $transaksi = Transaksi::where('id', $idtransaksi)->first();
          $aff       = AffMember::where('id_member', session()->get('member')->id_member)
                                  ->first();

          $affiliasi  = 0;
          $hpp        = 0;

          foreach ($cart as $nan) {
            $affiliasi += $nan->jumlah_barang * ($nan->affiliasi * (25 / 100));

            $hpp += $nan->jumlah_barang * $nan->price;
          }

          if (Session::get('member')->id_member == $transaksi->id_member) {
              return view('front.sukses', compact('transaksi', 'cart', 'aff', 'affiliasi', 'hpp'));
          } else {
              return "Maaf ini bukan transaksi anda";
          }
        }else {
          return ('Maaf Anda Harus Login Dulu');
        }

    }

    function transaksiConfirm($idtransaksi)
    {
      if (session()->has('member')) {
        $transaksi = Transaksi::where('id', $idtransaksi)->first();
        if (Session::get('member')->id_member == $transaksi->id_member) {
          $transaksi->status = 1;
          $transaksi->update();
          return redirect()->back();
        } else {
          return "Maaf Bukan Transaksi Anda";
        }
      } else {
        return "Login dulu";
      }
    }

    function apiProvinsi(Request $request) {
      $kota       =   Kota::where('id_provinsi', $request->id)
                      ->orderBy('kota', 'ASC')->get();

      $kota_id    =   $kota->first();

      $kecamatan  =   Kecamatan::where('id_kota', $kota_id->id)
                      ->orderBy('kecamatan', 'ASC')->get();

      return Response::json([$kota, $kecamatan]);
    }

    function apiKota(Request $request) {
      $kecamatan  =   Kecamatan::where('id_kota', $request->id)
                      ->orderBy('kecamatan', 'ASC')->get();

      return Response::json($kecamatan);
    }

    function apiSearch(Request $request) {
      $toko       =   Toko::orderBy('name', 'ASC')->where('level', 2);

      if ($request->provinsi !== 'default') {
        $toko->where('id_provinsi', $request->provinsi);
      }

      if ($request->kota !== 'default') {
        $toko->where('id_kota', $request->kota);
      }

      if ($request->kecamatan !== 'default') {
        $toko->where('id_kecamatan', $request->kecamatan);
      }

      if ($request->kategori !== 'default') {
        $toko->where('id_kategori', $request->kategori);
      }

      if ($request->queryS != null) {
        $toko->where('name', 'LIKE', '%' . $request->queryS . '%');
      }

      $result     =   $toko->get();

      return Response::json($result);
    }

    function order(Request $request) {
      if (session()->has('member')){
        $trans  =   Transaksi::where('id_member', session()->get('member')->id_member)
                    ->orderBy('id', 'DESC')->paginate(8);
        $aff    =   AffMember::where('id_member', session()->get('member')->id_member)->first();
        $id     =   $request->id;

        return view('front.order', compact('trans', 'aff', 'id'));
      } else {
        return redirect('/toko-usahakumart/login/'.$id);
      }
    }

    static function getProduct($id) {
      $data     =   Cart::where('kode_transaksi', $id)->get();
      $result   =   null;

      foreach ($data as $key => $value) {
        for ($i=0; $i < $value->jumlah_barang; $i++) {
          if ($result == null) {
            $result .= $value->id_barang;
          } else {
            $result .= ',' . $value->id_barang;
          }
        }
      }

      return $result;
    }
}
