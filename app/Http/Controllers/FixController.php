<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\OrderOffline as Order;
use App\Models\OrderOfflineHistory as History;
use App\PemasukanBarang;
use App\Models\Member;
use App\Models\AffiliasiDetail as Detail;
use App\Models\AffiliasiOutIn as Affiliasi;

class FixController extends Controller
{
    function index($option, Request $request) {
    	if ($option == 'order') {
    		$return 	=	Order::whereDate('created_at', '<', '2018-04-04')->delete();
    	} else if ($option == 'history') {
    		$return 	=	History::whereDate('created_at', '<', '2018-04-04')->delete();
    	} else if ($option == 'transaksi') {
    		$order 		=	Order::whereDate('created_at', '2018-04-10')->get();
            $last       =   Order::whereDate('created_at', '2018-04-09')->orderBy('created_at', 'DESC')->first();
            /*dd($last->no_transaksi);*/

    		$no 		=   $last->no_transaksi;

    		foreach ($order as $value) {
    			Order::where('id', $value->id)->update(['no_transaksi' => str_pad($no += 1, 10, '0', STR_PAD_LEFT)]);
    		}

    		$return 	=	$no;
    	} else if ($option == 'order_id') {
    		$order 		=	Order::whereDate('created_at', '2018-04-07')
                                   ->where('fix', 0)->orderBy('id', 'ASC')->get();

            $history    =   History::where('fix', 0)
                                    ->join('product_offline', 'order_offline_history.id_product', 'product_offline.id')
                                    ->orderBy('order_offline_history.id', 'ASC')
                                    ->whereDate('order_offline_history.created_at', '2018-04-07')->get();

            foreach ($order as $o) {
                $total  =  $o->total_pembayaran;
                foreach ($history as $h) {
                    $count  =  0;

                    print_r($count);
                    if ($count <= $total) {
                        $count += $h->harga_jual;
                         echo $count . "<br>";
                    }
                 }
            }

            dd($order);

    		/*for ($i=0; $i < count($order); $i++) {
    			if ($i + 1 < count($order) ) {
    				$from 	=	$order[$i]->created_at;
    				$to 	=	$order[$i + 1]->created_at;


    				$tes = History::where('created_at', '>=', $from)
                             ->where('created_at', '<', $to)->update(['order_id' => $order[$i]->id]);
    				echo $order[$i]->created_at . ' - ' . $order[$i + 1]->created_at . '-' . $order[$i]->id . '<br>';
                    echo $tes;
    			}
    		}*/

    		$return 	=	'';
    	} else if ($option == 'PemasukanBarang') {
            PemasukanBarang::where('tipe', 'utang')->update(['tipe' => 'Kredit']);
            PemasukanBarang::where('tipe', 'Bayar Langsung')->update(['tipe' => 'Tunai']);

            $return = 1;
        } else if ($option == 'utang') {
            return PemasukanBarang::where('tipe', 'Kredit')->update(['status_pembayaran' => '0']);
        } else if ($option == 'transkasi_tanggal') {
            $dari     = $request->dari;
            $ke       = $request->ke;
            $kasir    = $request->kasir;

            $return   = Order::whereDate('created_at', $dari)->where('id_user', $kasir)
                              ->update(['created_at' => $ke]);
        } else if ($option == 'transkasi_tanggal_order') {
            $id       = [];

            $data     = Order::whereDate('created_at', $request->dari)
                               ->where('id_user', 4)->get();

            foreach ($data as $key) {
                array_push($id, $key->id);
            }

            $return   = History::whereIn('order_id', $id)
                              ->update(['created_at' => $request->ke]);
        } else if ($option == 'supplier') {
            $return = PemasukanBarang::where('supplier', 'PT MEKAR JAYA MAKMUR MT ( UNILEVER )')->update(['supplier' => 'CV MEKAR JAYA MAKMUR MT (UNILEVER)']);
        } else if ($option == 'hpp') {
            $cek    =   Order::where('hpp', 0)->whereDate('created_at', '2018-04-07')->get(['id']);

            foreach ($cek as $key) {
                $hpp    =   0;

                $data   =   History::where('order_id', $key->id)
                                    ->join('product_offline', 'order_offline_history.id_product', 'product_offline.id')->get(['price']);

                foreach ($data as $d) {
                    $hpp += $d->price;
                }

                Order::where('id', $key->id)->update(['hpp' => $hpp]);
            }

            $return = $cek;
        } else if ($option == 'member_fix') {
            $member     =   Member::get();

            foreach ($member as $m) {
                Member::where('id_member', $m->id_member)->update(['password' => bcrypt('usahaku-' . $m->username)]);
            }

            $return = 'sukses mengubah data';
        } else if ($option == 'saldo_affiliasi') {
          Detail::create(['affiliasi_perusahaan' => 0, 'affiliasi_member' => 0]);

          $tm_perusahaan      =   Affiliasi::where('type', 'Masuk')
                                  ->where('milik', 'Perusahaan')
                                  ->select(\DB::raw('SUM(jumlah) as jumlah'))
                                  ->first();

          $tm_member          =   Affiliasi::where('type', 'Masuk')
                                  ->where('milik', 'Member')
                                  ->select(\DB::raw('SUM(jumlah) as jumlah'))
                                  ->first();

          $tk_member          =   Affiliasi::where('type', 'Keluar')
                                  ->where('milik', 'Member')
                                  ->select(\DB::raw('SUM(jumlah) as jumlah'))
                                  ->first();

          $return = Detail::where('id', 1)->update([
                      'affiliasi_perusahaan' => $tm_perusahaan->jumlah,
                      'affiliasi_member'     => $tm_member->jumlah - $tk_member->jumlah
                    ]);
        } else if ($option == 'fix_data') {
          $return   =   Affiliasi::where('jumlah', 0)->delete();
        }

    	return $return;
    }
}
