<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use DB;
use PDF;
use Illuminate\Support\Facades\Auth;
use Response;

use App\Models\TokoOffline as Toko;
use App\Models\Provinsi;
use App\Models\Kota;
use App\Models\Kecamatan;
use App\Models\KategoriToko;
use App\Models\TipeToko;
use App\Models\Saldo;
use App\Models\AffiliasiMember as Member;
use App\Models\Member as MemberToko;
use App\Models\OrderOffline as Order;
use App\Models\ProductOffline as Produk;
use App\Models\Deposit;
use App\Models\RequestCard;
use App\Models\NumberCard as Card;
use App\UserToko;

class AdminController extends Controller
{
    function index() {
    	$toko	=	Toko::join('provinsi', 'provinsi.id', 'toko_offline.id_provinsi')
    				->join('kota', 'kota.id', 'toko_offline.id_kota')
    				->join('kecamatan', 'kecamatan.id', 'toko_offline.id_kecamatan')
    				->where('aktif', 1)->where('level', 2)->paginate(10);

    	if (Auth::user()->level == 1) {
            return view('administrator/pages/dashboard', compact('toko'));
        } else {
            return view('usahakumart/pages/dashboard', compact('toko'));
        }
    }

    function durasiToko(Request $request) {
        $toko       =   Toko::where('id', $request->id)->first();

        $duration   =   \Carbon\Carbon::parse($toko->duration_toko)->addMonth($request->durasi);

        $update     =   Toko::where('id', $request->id)->update(['duration_toko' => $duration]);

        if ($update) {
          return Response::json(['success' => 'Durasi toko telah ditambahkan', 'durasi' => $duration->format('Y-m-d')]);
        } else {
          return Response::json(['error' => 'Toko tidak ditemukan']);
        }
    }

    function tambahToko() {
    	$provinsi 	=	Provinsi::orderBy('provinsi', 'ASC')->get();

    	$prov 		=	$provinsi->first();

    	$kota 		=	Kota::where('id_provinsi', $prov->id)->orderBy('kota', 'ASC')->get();

    	if ($kota->count() > 0) {
            $kot        =   $kota->first();

            $kecamatan  =   Kecamatan::where('id_kota', $kot->id)->orderBy('kecamatan', 'ASC')->get();
        } else {
            $kecamatan  =   [];
        }

        $tipe           =   TipeToko::all();
        $kategori       =   KategoriToko::all();
        $member         =   MemberToko::where('status', 1)->where('premium', 2)->get();

    	return view('administrator/pages/toko/tambah', compact('provinsi', 'kota', 'kecamatan', 'tipe', 'kategori', 'member'));
    }

    function postTambahToko(Request $request) {
    	$this->validate($request, [
    		'username'		    =>	'required',
            'password'          =>  'required',
            'id_provinsi'       =>  'required',
            'id_kota'           =>  'required',
    		    'id_kecamatan'      =>	'required',
            'alamat'            =>  'required',
            'name'              =>  'required'
    	]);

    	$request->request->add([
    		    'password'	    =>	bcrypt($request->password),
            'level'         =>  2,
            'permission'    =>  1
    	]);

      if ($request->toko_level) {
        $request->request->add([
          'duration_toko'   =>  \Carbon\Carbon::now()->addMonth()
        ]);
      }

    	$toko = Toko::create($request->except('_token'));

        if ($request->id_tipe == 2) {
            Saldo::create(['saldo_deposit' => 0, 'saldo_withdraw' => 0, 'id_toko' => $toko->id]);
        }

        UserToko::create([
                    'username'      =>  'admin',
                    'password'      =>  bcrypt('admin'),
                    'level'         =>  1,
                    'id_toko'       =>  $toko->id,
                    'nama'          =>  'Administrator'
                  ]);

    	return redirect()->back()->with('alert', 'Toko Telah Ditambahkan');
    }

    function daftarToko(Request $request) {
    	if ($request->tipe != null) {
         $toko   =   Toko::join('provinsi', 'provinsi.id', 'toko_offline.id_provinsi')
                      ->join('kota', 'kota.id', 'toko_offline.id_kota')
                      ->join('kecamatan', 'kecamatan.id', 'toko_offline.id_kecamatan')
                      ->select(DB::RAW('toko_offline.*, kota.kota, provinsi.provinsi, kecamatan.kecamatan'))
                      ->where('level', 2)->where('id_tipe', $request->tipe)
                      ->paginate(10);
       } else {
         $toko   =   Toko::leftJoin('provinsi', 'provinsi.id', 'toko_offline.id_provinsi')
                      ->leftJoin('kota', 'kota.id', 'toko_offline.id_kota')
                      ->leftJoin('kecamatan', 'kecamatan.id', 'toko_offline.id_kecamatan')
                      ->select(DB::RAW('toko_offline.*, kota.kota, provinsi.provinsi, kecamatan.kecamatan'))
                      ->where('level', 2)
                      ->paginate(10);
      }

      $tipe   =   TipeToko::get();

    	return view('administrator/pages/toko/daftar', compact('toko', 'tipe'));
    }

    function editToko(Request $request) {
        $data       =   Toko::join('provinsi', 'provinsi.id', 'toko_offline.id_provinsi')
                        ->join('kota', 'kota.id', 'toko_offline.id_kota')
                        ->join('kecamatan', 'kecamatan.id', 'toko_offline.id_kecamatan')
                        ->join('member', 'member.id_member', 'toko_offline.id_pemilik')
                        ->join('tipe_toko', 'tipe_toko.id', 'toko_offline.id_tipe')
                        ->join('kategori_toko', 'kategori_toko.id', 'toko_offline.id_kategori')
                        ->select(DB::RAW('toko_offline.*, kota.kota, provinsi.provinsi, kecamatan.kecamatan, member.*, tipe_toko.*, tipe_toko.id as tipe_id, kategori_toko.*, kategori_toko.id as kategori_id, toko_offline.id as id_toko, toko_offline.username as user_toko, toko_offline.alamat as alamat_toko'))
                        ->where('level', 2)
                        ->where('toko_offline.username', $request->nama)
                        ->first();

        $provinsi   =   Provinsi::where('id', '!=', $data->id_provinsi)->orderBy('provinsi', 'ASC')->get();

        $prov       =   $provinsi->first();

        $kota       =   Kota::where('id', $data->id_kota)
                        ->where('id_provinsi', '!=', $prov->id)->orderBy('kota', 'ASC')->get();

        if ($kota->count() > 0) {
            $kot        =   $kota->first();

            $kecamatan  =   Kecamatan::where('id', $data->id_kecamatan)
                            ->where('id_kota', '!=', $kot->id)->orderBy('kecamatan', 'ASC')->get();
        } else {
            $kecamatan  =   [];
        }

        $tipe           =   TipeToko::where('id', '!=', $data->id_tipe)->get();
        $kategori       =   KategoriToko::where('id', '!=', $data->id_kategori)->get();
        $member         =   MemberToko::where('status', 1)->where('premium', 2)
                            ->where('id_member', '!=', $data->id_pemilik)->get();

        return view('administrator/pages/toko/edit', compact('provinsi', 'kota', 'kecamatan', 'data', 'tipe', 'kategori', 'member'));
    }

    function updateToko(Request $request) {
        if ($request->password == null) {
            $request->request->add([
                'password'  =>  bcrypt($request->oldpass)
            ]);
        }else {
            $request->request->add([
                'password'  =>  bcrypt($request->password)
            ]);
        }

        Toko::where('id', $request->id)->update($request->except(['_token', 'oldpass']));

        if ($request->id_tipe == 2) {
            $cek = Saldo::where('id_toko', $request->id)->count();

            if ($cek < 1) {
                Saldo::create(['saldo_deposit' => 0, 'saldo_withdraw' => 0, 'id_toko' => $request->id]);
            }
        }

        return redirect('/usahakumart/daftar-toko')->with('alert', 'Toko Berhasil Diupdate');
    }

    function hapusToko(Request $request) {
        Toko::where('username', $request->nama)->delete();

        return redirect()->back()->with('alert', 'Toko Berhasil Dihapus');
    }

    function nonaktif(Request $request) {
    	Toko::where('id', $request->id)->update(['aktif' => 0]);

    	return redirect()->back()->with('alert', 'Toko Berhasil Di Non Aktfikan');
    }

    function aktif(Request $request) {
    	Toko::where('id', $request->id)->update(['aktif' => 1]);

    	return redirect()->back()->with('alert', 'Toko Berhasil Di Aktfikan');
    }

    function daftarMember(Request $request) {
    	$members 	 =	Member::where('nama', '!=', null);

      if ($request->q) {
        $members->where('nama', $request->q);
      }

      $member    =  $members->paginate(20);

    	return view('administrator/pages/member/daftar', compact('member'));
    }

    function Membernonaktif(Request $request) {
    	Member::where('id', $request->id)->whereNotNull('number_card')->update(['status' => 0]);

    	return redirect()->back()->with('alert', 'Member Berhasil Di Non Aktfikan');
    }

    function Memberaktif(Request $request) {
    	Member::where('id', $request->id)->whereNotNull('number_card')->update(['status' => 1]);

    	return redirect()->back()->with('alert', 'Member Berhasil Di Aktfikan');
    }

    function search(Request $request) {
        $toko   =   Toko::join('provinsi', 'provinsi.id', 'toko_offline.id_provinsi')
                    ->join('kota', 'kota.id', 'toko_offline.id_kota')
                    ->join('kecamatan', 'kecamatan.id', 'toko_offline.id_kecamatan')
                    ->select(DB::RAW('toko_offline.*, kota.kota, provinsi.provinsi, kecamatan.kecamatan'))
                    ->where('username', 'LIKE', '%' . $request->q . '%')
                    ->orWhere('kecamatan.kecamatan', 'LIKE', '%' . $request->q . '%')
                    ->orWhere('provinsi.provinsi', 'LIKE', '%' . $request->q . '%')
                    ->orWhere('kota.kota', 'LIKE', '%' . $request->q . '%')
                    ->orWhere('name', 'LIKE', '%' . $request->q . '%')
                    ->paginate(10);

        return view('administrator/pages/toko/daftar', compact('toko'));
    }

    function historyToko(Request $request) {
        $order      =   Order::where('id_toko', $request->id)
                        ->join('affiliasi_member', 'affiliasi_member.id', 'order_offline.id_member')
                        ->select('affiliasi_member.nama', 'order_offline.*')
                        ->whereDay('order_offline.created_at', '=', date('d'))
                        ->whereMonth('order_offline.created_at', '=', date('m'))
                        ->whereYear('order_offline.created_at', '=', date('Y'))
                        ->paginate(10);

        $month      =   Order::where('id_toko', $request->id)
                        ->join('affiliasi_member', 'affiliasi_member.id', 'order_offline.id_member')
                        ->select('affiliasi_member.nama', 'order_offline.*')
                        ->whereMonth('order_offline.created_at', '=', date('m'))
                        ->whereYear('order_offline.created_at', '=', date('Y'))
                        ->paginate(10);

        $produk     =   Produk::where('id_toko', $request->id)->paginate(10);

        $populer    =   Produk::where('id_toko', $request->id)->orderBy('terjual', 'DESC')->paginate(10);

        $id         =   $request->id;

        return view('administrator/pages/toko/history', compact('order', 'produk', 'populer', 'id', 'month'));
    }

    function populerAll(Request $request) {
        $populer    =   Produk::where('id_toko', $request->id)->orderBy('terjual', 'DESC')->get();

        return view('administrator.pages.toko.populer', compact('populer'));
    }

    function gudang(Request $request) {
        $produk     =   Produk::where('id_toko', $request->id)->get();

        return view('administrator.pages.toko.gudang', compact('produk'));
    }

    function riwayatPembelian(Request $request) {
        if ($request->jenis == 'hari') {
            $order      =   Order::where('id_toko', $request->id)
                        ->join('affiliasi_member', 'affiliasi_member.id', 'order_offline.id_member')
                        ->select('affiliasi_member.nama', 'order_offline.*')
                        ->whereDay('order_offline.created_at', '=', date('d'))
                        ->whereMonth('order_offline.created_at', '=', date('m'))
                        ->whereYear('order_offline.created_at', '=', date('Y'))
                        ->get();
        } else {
            $order      =   Order::where('id_toko', $request->id)
                        ->join('affiliasi_member', 'affiliasi_member.id', 'order_offline.id_member')
                        ->select('affiliasi_member.nama', 'order_offline.*')
                        ->whereMonth('order_offline.created_at', '=', date('m'))
                        ->whereYear('order_offline.created_at', '=', date('Y'))
                        ->get();
        }

        return view('administrator.pages.toko.riwayat', compact('order'));
    }

    function printLaporan(Request $request) {
        $toko   =   Toko::where('id', $request->id)->first();

        if ($request->jenis == 'hari') {
            $order      =   Order::where('id_toko', $request->id)
                        ->join('affiliasi_member', 'affiliasi_member.id', 'order_offline.id_member')
                        ->select('affiliasi_member.nama', 'order_offline.*')
                        ->whereDay('order_offline.created_at', '=', date('d'))
                        ->whereMonth('order_offline.created_at', '=', date('m'))
                        ->whereYear('order_offline.created_at', '=', date('Y'))
                        ->get();
            $name       =   'laporan_harian_' . date('d/m/Y') . '.pdf';
            $tipe       =   'Harian';
            $harga      =   0;
            $bayar      =   0;
            $kembali    =   0;

            foreach ($order as $key) {
                $harga += $key->total_harga;
                $bayar += $key->pembayaran;
                $kembali += $key->kembalian;
            }
        } else {
            $order      =   Order::where('id_toko', $request->id)
                        ->join('affiliasi_member', 'affiliasi_member.id', 'order_offline.id_member')
                        ->select('affiliasi_member.nama', 'order_offline.*')
                        ->whereMonth('order_offline.created_at', '=', date('m'))
                        ->whereYear('order_offline.created_at', '=', date('Y'))
                        ->get();
            $name       =   'laporan_bulanan_' . date('d/m/Y') . '.pdf';
            $tipe       =   'Bulanan';
            $harga      =   0;
            $bayar      =   0;
            $kembali    =   0;

            foreach ($order as $key) {
                $harga += $key->total_pembayaran;
                $bayar += $key->pembayaran;
                $kembali += $key->kembalian;
            }
        }

        $pdf    =   PDF::loadView('laporan.admin', compact('toko', 'order', 'tipe', 'harga', 'bayar', 'kembali'))->setPaper('a4', 'landscape');

        return $pdf->download($name);
    }

    function alamatToko() {
        $provinsi   =   Provinsi::orderBy('provinsi', 'ASC')->get();

        $kota       =   Kota::leftJoin('provinsi', 'kota.id_provinsi', 'provinsi.id')
                              ->select('kota.*', 'provinsi.provinsi')
                              ->orderBy('kota', 'ASC')->get();

        $kecamatan  =   Kecamatan::leftJoin('kota', 'kecamatan.id_kota', 'kota.id')
                                   ->leftJoin('provinsi', 'kota.id_provinsi', 'provinsi.id')
                                   ->select('kecamatan.*', 'provinsi.provinsi', 'kota.kota')
                                   ->orderBy('kecamatan', 'ASC')->get();

        return view('administrator/pages/toko/alamat', compact('provinsi', 'kota', 'kecamatan'));
    }

    function tambahAlamat(Request $request) {
        if ($request->type == 'provinsi') {
            $count  =   Provinsi::where('provinsi', $request->provinsi)->count();

            if ($count <= 0) {
                $provinsi =   Provinsi::create($request->except(['_token', 'type']));

                $return   =   ['success' => 'Data Provinsi Berhasil Ditambahkan', 'id' => $provinsi->id];
            } else {
                $return   =   ['error' => 'Data Provinsi Sudah Ada'];
            }
        } else if ($request->type == 'kota') {
          $count  =   Kota::where('kota', $request->kota)->where('id_provinsi', $request->id_provinsi)->count();

          if ($count <= 0) {
              $kota     =   Kota::create($request->except(['_token', 'type']));

              $provinsi =   Provinsi::where('id', $request->id_provinsi)->first();

              $return   =   [
                             'success'      =>  'Data Kota Berhasil Ditambahkan',
                             'id'           =>  $kota->id,
                             'provinsi'     =>  $provinsi->provinsi,
                             'id_provinsi'  =>  $request->id_provinsi
                            ];
          } else {
              $return   =   ['error' => 'Data Kota Sudah Ada'];
          }
        } else {
          $count  =   Kecamatan::where('kecamatan', $request->kecamatan)->where('id_kota', $request->id_kota)->count();

          if ($count <= 0) {
              $kecamatan     =   Kecamatan::create($request->except(['_token', 'type']));

              $kota          =   Kota::where('id', $request->id_kota)->first();

              $return        =   [
                                   'success'      =>  'Data Kecamatan Berhasil Ditambahkan',
                                   'id'           =>  $kecamatan->id,
                                   'kota'         =>  $kota->kota,
                                   'id_kota'      =>  $request->id_kota
                                  ];
          } else {
              $return   =   ['error' => 'Data Kecamatan Sudah Ada'];
          }
        }

        return Response::json($return);
    }

    function hapusAlamat(Request $request) {
      if ($request->type == 'provinsi') {
        $provinsi   =   Provinsi::where('id', $request->id)->delete();

        if ($provinsi) {
          $return   =   ['success' => 'Data Provinsi Berhasil Dihapus'];
        } else {
          $return   =   ['warning' => 'Data Provinsi Tidak Ada'];
        }
      } else if ($request->type == 'kota') {
        $kota       =   Kota::where('id', $request->id)->delete();

        if ($kota) {
          $return   =   ['success' => 'Data Kota Berhasil Dihapus'];
        } else {
          $return   =   ['warning' => 'Data Kota Tidak Ada'];
        }
      } else {
        $kecamatan  =   Kecamatan::where('id', $request->id)->delete();

        if ($kecamatan) {
          $return   =   ['success' => 'Data Kecamatan Berhasil Dihapus'];
        } else {
          $return   =   ['warning' => 'Data Kecamatan Tidak Ada'];
        }
      }

      return Response::json($return);
    }

    function updateAlamat(Request $request) {
      if ($request->type == 'provinsi') {
        $update   =   Provinsi::where('id', $request->id)->update($request->except(['id', 'type']));

        if ($update) {
          $return   =   ['success' => 'Data Provinsi Berhasil Diupdate'];
        } else {
          $return   =   ['warning' => 'Data Provinsi Tidak Ada'];
        }
      } else if ($request->type == 'kota') {
        $update   =   Kota::where('id', $request->id)->update($request->except(['id', 'type']));

        if ($update) {
          $return   =   ['success' => 'Data Kota Berhasil Diupdate'];
        } else {
          $return   =   ['warning' => 'Data Kota Tidak Ada'];
        }
      } else {
        $update   =   Kecamatan::where('id', $request->id)->update($request->except(['id', 'type']));

        if ($update) {
          $return   =   ['success' => 'Data Kecamatan Berhasil Diupdate'];
        } else {
          $return   =   ['warning' => 'Data Kecamatan Tidak Ada'];
        }
      }

      return Response::json($return);
    }

    function allow(Request $request) {
        Toko::where('id', $request->id)->update(['permission' => 1]);

        return redirect()->back()->with('alert', 'Toko Berhasil Diizinkan');
    }

    function deny(Request $request) {
        Toko::where('id', $request->id)->update(['permission' => 0]);

        return redirect()->back()->with('alert', 'Toko Dibatalkan izinnya');
    }

    function kategoriToko() {
        $kategori   =   KategoriToko::all();

        return view('administrator.pages.toko.kategori', compact('kategori'));
    }

    function tambahKategoriToko(Request $request) {
        $this->validate($request, [ 'kategori' => 'required' ]);

        $cek    =   KategoriToko::where('kategori', $request->kategori)->count();

        if ($cek > 0) {
            return redirect()->back()->with('alert', 'Kategori Tersebut Sudah Tersedia');
        } else {
            KategoriToko::create($request->except('_token'));

            return redirect()->back()->with('alert', 'Kategori Berhasil Ditambahkan');
        }
    }

    function hapusKategoriToko(Request $request) {
        KategoriToko::where('id', $request->id)->delete();

        return redirect()->back()->with('alert', 'Kategori Berhasil Dihapus');
    }

    function editKategoriToko(Request $request) {
        $kategori   =   KategoriToko::where('id', $request->id)->first();

        return view('administrator.pages.toko.edit_kategori', compact('kategori'));
    }

    function updateKategoriToko(Request $request) {
        KategoriToko::where('id', $request->id)->update(['kategori' => $request->kategori]);

        return redirect('/usahakumart/kategori-toko')->with('alert', 'Kategori Berhasil Diupdate');
    }

    function tipeToko() {
        $tipe   =   TipeToko::all();

        return view('administrator.pages.toko.tipe', compact('tipe'));
    }

    function tambahTipeToko(Request $request) {
        $this->validate($request, [ 'tipe' => 'required' ]);

        $cek    =   TipeToko::where('tipe', $request->tipe)->count();

        if ($cek > 0) {
            return redirect()->back()->with('alert', 'Tipe Tersebut Sudah Tersedia');
        } else {
            TipeToko::create($request->except('_token'));

            return redirect()->back()->with('alert', 'Tipe Berhasil Ditambahkan');
        }
    }

    function hapusTipeToko(Request $request) {
        TipeToko::where('id', $request->id)->delete();

        return redirect()->back()->with('alert', 'Tipe Berhasil Dihapus');
    }

    function editTipeToko(Request $request) {
        $tipe   =   TipeToko::where('id', $request->id)->first();

        return view('administrator.pages.toko.edit_tipe', compact('tipe'));
    }

    function updateTipeToko(Request $request) {
        TipeToko::where('id', $request->id)->update(['tipe' => $request->tipe]);

        return redirect('/usahakumart/tipe-toko')->with('alert', 'Tipe Berhasil Diupdate');
    }

    function tambahSaldoToko() {
        $toko   =   Toko::where('id_tipe', 2)
                    ->join('saldo_toko', 'saldo_toko.id_toko', 'toko_offline.id')
                    ->select(DB::raw('saldo_toko.*, toko_offline.*, saldo_toko.id as id_saldo'))->get();

        return view('administrator.pages.toko.tambah_saldo', compact('toko'));
    }

    function tambahSaldo(Request $request) {
        $saldo = Saldo::where('id_toko', $request->id_toko)->first();

        Saldo::where('id_toko', $request->id_toko)
               ->update(['saldo_deposit' => $saldo->saldo_deposit += $request->saldo]);

        return redirect()->back()->with('alert', 'Saldo telah ditambahkan');
    }

    function deposit() {
        $toko   =   Toko::where('id_tipe', 2)
                    ->join('saldo_toko', 'saldo_toko.id_toko', 'toko_offline.id')
                    ->select(DB::raw('saldo_toko.*, toko_offline.*, saldo_toko.id as id_saldo'))->get();

        $deposit    =   Deposit::where('status', 0)
                        ->join('toko_offline', 'toko_offline.id', 'konfirmasi_deposit.id_toko')
                        ->select(DB::raw('konfirmasi_deposit.*, toko_offline.*, konfirmasi_deposit.id as id_deposit'))
                        ->orderBy('id_deposit', 'DESC')
                        ->get();

        return view('administrator.pages.toko.tambah_saldo', compact('toko', 'deposit'));
    }

    function konfirmDeposit(Request $request) {
        $id         =   $request->id;

        $deposit    =   Deposit::where('id', $id)->first();

        $saldo      =   Saldo::where('id_toko', $deposit->id_toko)->first();

        Saldo::where('id_toko', $deposit->id_toko)
               ->update(['saldo_deposit' => $saldo->saldo_deposit += $deposit->jumlah_transfer]);

        Deposit::where('id', $id)->update(['status' => 1]);

        return redirect()->back()->with('alert', 'Saldo telah ditambahkan');
    }

    function Kartu() {
        $toko   =   Toko::where('level', 2)->get();

        $kartu    =   RequestCard::where('status', 0)
                        ->join('toko_offline', 'toko_offline.id', 'request_card.id_toko')
                        ->select(DB::raw('request_card.*, toko_offline.*, request_card.id as id_request'))
                        ->orderBy('id_request', 'DESC')
                        ->paginate(10);

        return view('administrator.pages.toko.request_card', compact('toko', 'kartu'));
    }

    function konfirmKartu(Request $request) {
        $id         =   $request->id;

        $kartu      =   Card::take($request->jumlah)
                        ->where('id_toko', null)
                        ->where('status', 1)->get();

        $hitung     =   Card::where('id_toko', null)
                        ->where('status', 1)->count();

        if ($hitung < $request->jumlah) {
            return redirect()->back()->with('alert', 'Stok Kartu Kurang Dari Jumlah Request');
        } else {
            $id_card    =   [];

            foreach ($kartu as $key) {
                array_push($id_card, $key->id);
            }

            $card       =   RequestCard::where('id', $id)->first();

            Card::whereIn('id', $id_card)->update(['id_toko' => $card->id_toko]);

            RequestCard::where('id', $id)->update(['status' => 1]);

            return redirect()->back()->with('alert', 'Saldo telah ditambahkan');
        }
    }

    static function jumlahKartu($id) {
        $result     =   Card::where('id_toko', $id)->count();

        return $result;
    }

    static function jumlahKartuDigunakan($id) {
        $result     =   Card::where('id_toko', $id)
                        ->where('status', 0)->count();

        return $result;
    }

    static function jumlahKartuBelum($id) {
        $result     =   Card::where('id_toko', $id)
                        ->where('status', 1)->count();

        return $result;
    }

    function stokKartu() {
        $card       =   Card::where('id_toko', null)->orderBy('number_card', 'DESC')->paginate(20);

        $jumlah     =   Card::count();
        $belum      =   Card::where('id_toko', null)->count();
        $sudah      =   Card::where('id_toko', '!=', null)->count();

        return view('administrator.pages.toko.stok_kartu', compact('card', 'jumlah', 'belum', 'sudah'));
    }

    function upgrade(Request $request) {
        $members    =   MemberToko::where('premium', '!=', 2)
                        ->orderBy('nama', 'ASC');

        if ($request->q) {
          $members->where('nama', 'LIKE', '%' . $request->q . '%');
        }

        $member     =   $members->paginate(20);

        return view('administrator.pages.member.upgrade', compact('member'));
    }

    function upgradeAkun(Request $request) {
        MemberToko::where('id_member', $request->id)->update(['premium' => 2]);

        return redirect()->back()->with('alert', 'Akun telah diupgrade');
    }

    function upgradeAkunPremium(Request $request) {
        MemberToko::where('id_member', $request->id)->update(['premium' => 1]);

        return redirect()->back()->with('alert', 'Akun telah diupgrade');
    }

    function stokKartuTambah(Request $request) {
        for ($i = $request->min_card; $i <= $request->max_card; $i++) {
            $request->request->add(['status' => 1, 'number_card' => $i]);
            Card::create($request->except(['_token', 'min_card', 'max_card']));
        }

        return redirect()->back()->with('alert', 'Stok Kartu Telah Ditambah');
    }

    function stokKartuHapus(Request $request) {
        Card::where('id', $request->id)->delete();

        return redirect()->back()->with('alert', 'Stok Kartu Telah Dihapus');
    }
}
