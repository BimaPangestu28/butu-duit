<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\SupplierOffline;
use Auth;

class SupplierController extends Controller
{
    function index() {

    	$supplier 	=	SupplierOffline::where('id_toko', Auth::user()->id)
                                        ->orderBy('name')->paginate(100000);

    	return view('usahakumart.supplier.index', compact('supplier'));
    }

    function create() {

    	return view('usahakumart.supplier.create');
    }

    function store(Request $request) {
    	$this->validate($request, ['name' => 'required']);

        $cek    =   SupplierOffline::where('name', $request->name)->count();

        if ($cek > 0) {
            return redirect()->back()->with('alert', 'Supplier Sudah Ada');
        }

        $request->request->add(['id_toko' => Auth::user()->id]);

    	SupplierOffline::create($request->except('_token'));

    	return redirect('/cashier/supplier')->with('alert', 'Supplier Berhasil Ditambah');
    }

    function delete($id) {
    	SupplierOffline::where('id', $id)->delete();

    	return redirect('/cashier/supplier')->with('alert', 'Supplier Berhasil Dihapus');
    }

    function edit($id) {
    	$supplier = SupplierOffline::where('id', $id)->first();

    	return view('usahakumart.supplier.edit', compact('supplier'));
    }

    function update(Request $request, $id) {
    	$this->validate($request, ['name' => 'required']);

        $cek    =   SupplierOffline::where('name', $request->name)->count();

        if ($cek > 0) {
            return redirect()->back()->with('alert', 'Supplier Sudah Ada');
        }

    	SupplierOffline::where('id', $id)->update($request->except('_token'));

    	return redirect('/cashier/supplier')->with('alert', 'Supplier Berhasil Diupdate');
    }

    function search(Request $request) {
        $supplier   =   SupplierOffline::where('name', 'LIKE', '%' . $request->q . '%')
                        ->orderBy('name')->get();

        return view('usahakumart.supplier.index', compact('supplier'));   
    }
}
