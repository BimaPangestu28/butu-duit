<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;

class CategoryController extends Controller
{
    function tambah() {
		
    	$category 	=	Category::paginate(20);

    	$cat 		=	null;

    	return view('usahakumart.category.index', compact('category', 'cat'));
    }

    function tambahCategory(Request $request) {
    	$this->validate($request, [
    		'category'	=>	'required'
    	]);

    	$cek 	=	Category::where('category', $request->category)->count();

    	if ($cek > 0) {
    		return redirect()->back()->with('alert', 'Nama Category Sudah Ada');
    	}

    	Category::create($request->except(['_token']));

    	return redirect()->back()->with('alert', 'Category Berhasil Ditambah');
    }

    function deleteCategory($id) {
    	Category::where('id', $id)->delete();

    	return redirect()->back()->with('alert', 'Category Berhasil Dihapus');
    }

    function editCategory($id) {
    	$category 	=	Category::paginate(20);
    	
    	$cat 		=	Category::where('id', $id)->first();

    	return view('usahakumart.category.index', compact('category', 'cat'));	
    }

    function updateCategory($id, Request $request) {
    	$this->validate($request, [
    		'category'	=>	'required'
    	]);

    	Category::where('id', $id)->update($request->except(['_token']));

    	return redirect()->back()->with('alert', 'Category Berhasil Diupdate');
    }
}
