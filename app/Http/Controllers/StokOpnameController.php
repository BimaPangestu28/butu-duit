<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ProductOffline as Product;
use Auth;

class StokOpnameController extends Controller
{
    function index(Request $request)
    {

        if ($request->key) {
            $abj = $request->key;

            $product    =   Product::where('id_toko', Auth::user()->id)->where('name', 'LIKE', $abj.'%')->orderBy('name', 'ASC')->get();
            return view('usahakumart/pages/stok_opname', compact('product', 'abj'));
        } else {
            $product = [];
            return view('usahakumart/pages/stok_opname', compact('product'));
        }
    }

    function updateOpname(Request $request)
    {
        $id = $request->id;
        $stok_opname = $request->stok_opname;

        $product = Product::where('id', $id)->where('id_toko', Auth::user()->id)->first();
        $product->stok_opname = $stok_opname;
        $product->update();

        return response()->json(['status' => true]);
    }

    function printSelisih(Request $request)
    {
        $key = $request->key;
        $product    =   Product::where('id_toko', Auth::user()->id)
                                ->where('name', 'LIKE', $key.'%')
                                ->where('stok', '!=', \DB::raw('stok_opname'))
                                ->orderBy('name', 'ASC')
                                ->get();
        $total_awal    =   Product::where('id_toko', Auth::user()->id)
                                ->where('name', 'LIKE', $key.'%')
                                ->where('stok', '!=', \DB::raw('stok_opname'))
                                ->select(\DB::raw('sum(stok * price) AS tot'))->first();
        $total_akhir    =   Product::where('id_toko', Auth::user()->id)
                                ->where('name', 'LIKE', $key.'%')
                                ->where('stok', '!=', \DB::raw('stok_opname'))
                                ->select(\DB::raw('sum(stok_opname * price) AS tot'))->first();

        $total_awal_semua    =   Product::where('id_toko', Auth::user()->id)
                                ->where('stok', '!=', \DB::raw('stok_opname'))
                                ->select(\DB::raw('sum(stok * price) AS tot'))->first();
        $total_akhir_semua    =   Product::where('id_toko', Auth::user()->id)
                                ->where('stok', '!=', \DB::raw('stok_opname'))
                                ->select(\DB::raw('sum(stok_opname * price) AS tot'))->first();

        $total_semua = $total_awal_semua->tot - $total_akhir_semua->tot;

        return view('/laporan/stok_opname_selisih', compact('product', 'key', 'total_awal', 'total_akhir', 'total_semua'));
    }

    function updateStok(Request $request)
    {
        $key = $request->key;
        $product    =   Product::where('id_toko', Auth::user()->id)
                                ->where('name', 'LIKE', $key.'%')
                                ->where('stok', '!=', \DB::raw('stok_opname'))
                                ->orderBy('name', 'ASC')
                                ->get();
        foreach ($product as $t) {
            $update = Product::where('id_toko', Auth::user()->id)
                                ->where('id', $t->id)
                                ->where('stok', '!=', \DB::raw('stok_opname'))
                                ->update(['stok' => $t->stok_opname]);
        }

        return redirect('/cashier/stok-opname/opname?key='.$key);
    }

    function stokAdjust(Request $request)
    {
        $key = $request->key;
        $product    =   Product::where('id_toko', Auth::user()->id)
                                ->where('name', 'LIKE', $key.'%')
                                ->where('stok_opname', '!=', null)
                                ->orderBy('name', 'ASC')
                                ->get();
        foreach ($product as $t) {
            $update = Product::where('id_toko', Auth::user()->id)
                                ->where('id', $t->id)
                                ->where('stok_opname', '!=', null)
                                ->update(['stok_opname' => null]);
        }

        return redirect('/cashier/stok-opname?key='.$key);
    }
}
