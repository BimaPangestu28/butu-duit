<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SupplierOffline extends Model
{
    protected $guarded = ['id'];
}
